Base model (ska_ser_test_equipment.base)
========================================

.. automodule:: ska_ser_test_equipment.base
    :members:
    :special-members:
