API
===

.. toctree::
  :caption: Devices
  :maxdepth: 2

  Arbitrary waveform generator (AWG) subpackage (ska_ser_test_equipment.awg)<awg/index>
  Signal generator subpackage (ska_ser_test_equipment.signal_generator)<signal_generator/index>
  Spectrum analyser subpackage (ska_ser_test_equipment.spectrum_analyser)<spectrum_analyser/index>
  Spectrum analyser Anritsu subpackage (ska_ser_test_equipment.spectrum_analyser_anritsu)<spectrum_analyser_anritsu/index>


.. toctree::
  :caption: Other
  :maxdepth: 2

  Base module (sks_ser_test_equipment.base)<base>
  Interface definitions module (ska_ser_test_equipment.interface_definitions)<interface_definitions>
  Mixins subpackage (ska_ser_test_equipment.mixins)<mixins/index>
