ska-ser-test-equipment
======================

This project supports monitoring and control of test equipment within
the `SKA`_.

.. _SKA: https://skao.int/

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   api/index
   guide/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
