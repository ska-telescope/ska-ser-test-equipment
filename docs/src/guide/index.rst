Guide
=====
At present, tests are only run against the Tango device test context and not against the
simulator which are available through the chart deployment.

Environment
~~~~~~~~~~~
In order to use or develop `ska-ser-test-equipment`, you need to be in a
suitable SKAO development environment, with tango installed.

For developers who use vscode, a remote container configuration is
provided.

Minikube
^^^^^^^^
If you haven't set up `minikube` yet, please follow the instructions under
`ska-cicd-deploy-minikube <https://gitlab.com/ska-telescope/sdi/ska-cicd-deploy-minikube>`_

The `test-chart` deployment will automatically spin up a device simulator for each device
instance selected.
In your local minikube environment enter the `ska-ser-test-equipment` directory
then use the following instructions

.. code-block:: shell-session

   # if you run docker
   me@local:~/ska-ser-test-equipment$ eval $(minikube -p minikube docker-env)
   # or if you run podman
   me@local:~/ska-ser-test-equipment$ eval $(minikube -p minikube podman-env)
   me@local:~/ska-ser-test-equipment$ make oci-build # build the container image
   me@local:~/ska-ser-test-equipment$ make k8s-install-chart
   me@local:~/ska-ser-test-equipment$ make k8s-watch # Ctrl-C to quit
   me@local:~/ska-ser-test-equipment$ kubectl -n ska-ser-test-equipment exec --stdin --tty ska-tango-base-itango-console  -- itango3
   Defaulted container "itango" out of: itango, check-dependencies-0 (init)
   ITango 9.3.3 -- An interactive Tango client.

   Running on top of Python 3.7.3, IPython 7.21 and PyTango 9.3.3

   help      -> ITango's help system.
   object?   -> Details about 'object'. ?object also works, ?? prints more.

   IPython profile: tango

   hint: Try typing: mydev = Device("<tab>

   In [1]: d = Device("test-itf/siggen/1
                        sys/access_control/1 sys/tg_test/1
                        sys/database/2       test-itf/siggen/1
                        sys/rest/0           test-itf/siggen/2
                        <unknown>

Taranta GUI
^^^^^^^^^^^

If you have **Taranta** running as part of your chart deployment, you can upload an example dashboard
file provided in this repository's `dashboards` directory.
You will need to log into taranta (credentials are available in its documentation) to do so.
This will connect to the two Signal Generator devices running against the simulator.

.. _figure-1-dashboard-demo:

.. figure:: dashboard_example.png
   :scale: 30%
   :alt: Example Taranta dashboard for signal generators
   :align: center
   :figclass: figborder

   Example dashboard

Deployment in a Test Facility
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The SKA Test Equipment Helm Chart will be a part of the system (ITF) deployments.
As an example how to customise the chart deployment, two basic facility-specific
``values-<TELESCOPE>-itf.yaml`` files are included. Modify as needed, and use as follows
to override the `test-parent` chart values.

.. code-block:: shell-session

   me@local:~$ make k8s-install-chart VALUES=--values=charts/values-low-itf.yaml

The Low ITF values file specifies extra Kubernetes resources for the test equipment pods,
e.g. `skao.int/signal-generator-01`. These resources control exclusive access to the test
equipment and are are defined by the `SpookD device plugin <https://gitlab.com/ska-telescope/ska-ser-k8s-spookd/>`_,
which is configured and deployed by the Ansible playbooks in `ska-cicd-deploy-low-itf <https://gitlab.com/ska-telescope/sdi/ska-cicd-deploy-low-itf/>`_.

Running the tests
~~~~~~~~~~~~~~~~~
To run the test suite:

.. code-block:: shell-session

   me@local:~$ make python-test

For each test, the device(s) to be tested will be deployed in a
lightweight Tango test context, and then the test will be run in that
context.

It is also possible to run the functional (BDD) tests against a full
Tango system, in which the device(s) to be tested are already deployed.
This can be done either by providing the ``--true-context`` option to
pytest:

.. code-block:: shell-session

   me@local:~$ pytest --true-context tests/functional

or by setting the ``TRUE_TANGO_CONTEXT`` environment variable:

.. code-block:: shell-session

   me@local:~$ TRUE_TANGO_CONTEXT=yep pytest tests/functional


Testing against hardware
^^^^^^^^^^^^^^^^^^^^^^^^
If running tests in an environment where hardware is available to test
against, provide environment variables that specify the address for
the model:

.. code-block:: shell-session

   me@local:~$ TSG4101A_ADDRESS=telnet:localhost:10000 make python-test

and the tests will be run against the hardware as well as against a
simulator.


Supported devices
~~~~~~~~~~~~~~~~~
This repository currently supports a range of hardware models (arbitrary waveform
generators, signal generators, spectrum analysers).

Arbitrary Waveform Generator:
   * Tektronix AWG5208

Programmable Attenuator:
   * Mini Circuits RC4DAT-6G-95

Signal Generators:
   * Aimtti TGR2051
   * Rohde & Schwarz SMB100A
   * Tektronix TSG4104A

Anritsu Spectrum Analysers:
   * Anritsu MS2090A (trace data not yet available)

Tektronix Spectrum Analysers:
   * Tektronix RSA5103B
   * Tektronix SPECMON26B

Switch Matrices:
   * Mini Circuits RC-2SP6T-A12
   * Mini Circuits ZTRC-4SPDT-A18
   * Mini Circuits ZTRC-8SPDT-A18

SCPI interface definitions
~~~~~~~~~~~~~~~~~~~~~~~~~~
`ska-ser-test-equipment` endeavours to make it easy to support different
hardware models, by declaratively defining SCPI interfaces using a YAML
file. The format is currently quite rudimentary: it simply maps the
expected attributes to SCPI field names, and specifies the field type.
Note, the specific implementation of scpi can be found in `ska-ser-scpi`.
For example:

.. literalinclude:: ../../../src/ska_ser_test_equipment/signal_generator/tektronix_tsg4104a.yaml
   :language: yaml

To register a new interface definition, create a new YAML file, and then
add it to the initialiser of
:py:class:`~ska_ser_test_equipment.interface_definitions.InterfaceDefinitionFactory`.
