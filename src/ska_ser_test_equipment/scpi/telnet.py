"""
Do the telnet thing here.

Not used for anything at this stage.
"""

# pylint: disable=useless-suppression
# pylint: disable=deprecated-module

import logging
import telnetlib

_module_logger = logging.getLogger(__name__)


def telnet(
    hostname: str, port: int, cmd: str, sentinel_string: str, timeout: float = 5.0
) -> str | None:
    """
    Send command with telnet.

    :param hostname: host name or IP address
    :param port: port number
    :param cmd: command to send
    :param sentinel_string: added to end of command
    :param timeout: timeout in seconds

    :returns: message as received
    """
    _module_logger.info("Connect to %s:%d", hostname, port)
    tn_conn = telnetlib.Telnet(hostname, port, timeout)

    _module_logger.debug("Telnet send/receive %s : %s", type(cmd), cmd)
    cmd += sentinel_string

    tn_conn.write(cmd.encode("ascii"))
    try:
        r_str = tn_conn.read_all().decode("ascii").strip()
    except TimeoutError:
        _module_logger.warning("Timeout after %f seconds", timeout)
        r_str = ""
    # print(r_str)
    return r_str
