"""
Communicate with test instrument using Virtual Instrument Software Architecture (VISA).

Uses VISA library, see https://pyvisa.readthedocs.io/en/latest/
"""

# pylint: disable=bad-option-value,too-many-positional-arguments,useless-suppression

import decimal
import logging
import math
import socket
from typing import Any, Optional

import numpy as np
import pyvisa
import pyvisa_py  # type: ignore[import]

from ska_ser_test_equipment.scpi.netcat import netcat, netcat_tx
from ska_ser_test_equipment.scpi.telnet import telnet


def eng_number(val: int | float | None, unit: Optional[str] = "") -> str:
    """
    Convert number to engineering format.

    :param val: input value
    :param unit: unit added to end of string

    :returns: formatted string
    """
    if val is None:
        return ""
    f_str: str = decimal.Decimal(f"{val:.4E}").normalize().to_eng_string()
    f_str = f_str.replace("E-12", "p")
    f_str = f_str.replace("E-9", "n")
    f_str = f_str.replace("E-6", "u")
    f_str = f_str.replace("E-3", "m")
    f_str = f_str.replace("E+3", "k")
    f_str = f_str.replace("E+6", "M")
    f_str = f_str.replace("E+9", "G")
    f_str = f_str.replace("E+12", "T")
    f_str += unit  # type: ignore[operator]
    return f_str


# pylint: disable-next=too-many-instance-attributes,too-many-public-methods
class VirtualInstrument:
    """Do the VISA thing."""

    visa_hw: Any = None
    socket: Any = None

    # pylint: disable-next=too-many-arguments
    def __init__(  # noqa: C901
        self,
        logger: logging.Logger,
        host: str,
        port: int,
        sentinel_string: str,
        timeout: float,
        use_telnet: bool,
    ):
        """
        Connect to VISA and send SCPI commands.

        :param logger: logging handle
        :param host: instrument IP address or hostname
        :param port: port number for telnet (zero means VISA)
        :param sentinel_string: added to end of command in TCP mode
        :param timeout: timeout period in
        :param use_telnet: use telnet protocol
        """
        self.logger = logger
        self.host = host
        self.port = port
        self.sentinel_string = sentinel_string
        self.timeout = timeout
        self.use_telnet = use_telnet
        if self.port:
            self.visa_hw = f"{self.host}:{self.port}"  # type: ignore[assignment]
            self.logger.info("Connect to socket %s:%d", self.host, self.port)
        else:
            # VISA resource address
            self.visa_addr = f"TCPIP::{self.host}::INSTR"
            self.logger.debug("Connect to VISA %s (%d sec)", self.visa_addr, timeout)
            try:
                self.visa_rm = pyvisa.ResourceManager("@py")
            except PermissionError as p_err:
                self.logger.warning("Resource manager: %s", str(p_err))
            # pylint: disable-next=broad-exception-caught
            except Exception as e_err:
                self.logger.warning("Error: %s", str(e_err))
            # self.logger.debug("Resources: %s", self.visa_rm.list_resources())
            try:
                self.logger.info("Open VISA resource %s", self.visa_addr)
                self.visa_hw = self.visa_rm.open_resource(self.visa_addr)
            except ConnectionRefusedError as a_err:
                # pylint: disable-next=broad-exception-raised
                raise Exception(f"Connection refused to {self.visa_addr}") from a_err
            except pyvisa_py.protocols.rpc.RPCError as b_err:
                # pylint: disable-next=broad-exception-raised
                raise Exception(
                    f"RPC error: could not connect to VISA {self.visa_addr}"
                ) from b_err
            except pyvisa.errors.VisaIOError as c_err:
                # pylint: disable-next=broad-exception-raised
                raise Exception(
                    f"I/O error: could not connect to VISA {self.visa_addr}"
                ) from c_err
            except OSError as d_err:
                # pylint: disable-next=broad-exception-raised
                raise Exception(
                    f"OS error: could not connect to VISA {self.visa_addr}"
                ) from d_err
            # pylint: disable-next=broad-exception-caught
            except Exception as e_err:
                self.logger.warning("VISA resource error: %s", str(e_err))
            try:
                self.visa_hw.timeout = self.timeout * 1000
            # pylint: disable-next=broad-exception-caught
            except Exception as e_err:
                self.logger.warning("Timeout error: %s", str(e_err))

    def socket_open(self) -> None:
        """Open socket connection with settings for instrument control."""
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setblocking(False)
        self.socket.settimeout(self.timeout)
        self.socket.connect((self.host, self.port))

    def socket_close(self) -> None:
        """Gracefully close connection."""
        self.socket.shutdown(socket.SHUT_RDWR)
        self.socket.close()

    def query(self, cmd: str) -> str:
        """
        Send a query to the instrument.

        :param cmd: SCPI command to be sent
        :returns: reply as string.
        """
        # self.write(cmd)

        msg = f"{cmd}\n"
        self.socket.send(msg.encode("latin_1"))

        # Read continuously until termination character is found.
        response = b""
        while response[-1:] != b"\n":
            response += self.socket.recv(1024)

        # Strip out whitespace and return.
        return response.decode("latin_1").strip()

    def binblockread(self, dtype: Any = np.int8, debug: bool = False) -> np.ndarray:
        """
        Read data with IEEE 488.2 binary block format.

        :param dtype: data type
        :param debug: message level
        :raises ValueError: buffer not in correct format
        :returns: data block

        The waveform is formatted as:
        #<x><yyy><data><newline>, where:
        <x> is the number of y bytes. For example, if <yyy>=500, then <x>=3.
        NOTE: <x> is a hexadecimal number.
        <yyy> is the number of bytes to transfer. Care must be taken
        when selecting the data type used to interpret the data.
        The dtype argument used to read the data must match the data
        type used by the instrument that sends the data.
        <data> is the curve data in binary format.
        <newline> is a single byte new line character at the end of the data.
        """
        self.socket_open()

        # Read # character, raise exception if not present.
        if self.socket.recv(1) != b"#":
            raise ValueError("Data in buffer is not in binblock format.")

        # Extract header length and number of bytes in bin block.
        header_len = int(self.socket.recv(1).decode("latin_1"), 16)
        num_bytes = int(self.socket.recv(header_len).decode("latin_1"))

        if debug:
            print(f"Header: #{header_len}{num_bytes}")

        raw_data = bytearray(num_bytes)
        buf = memoryview(raw_data)

        # While there is data left to read...
        while num_bytes:
            # Read data from instrument into buffer.
            bytes_rx = self.socket.recv_into(buf, num_bytes)
            # Slice buffer to preserve data already written to it.
            buf = buf[bytes_rx:]
            # Subtract bytes received from total bytes.
            num_bytes -= bytes_rx
            if debug:
                print(f"numBytes: {num_bytes}, bytesRecv: {bytes_rx}")

        # Receive termination character.
        term = self.socket.recv(1)
        if debug:
            print("Term char: ", term)
        # If term char is incorrect or not present, raise exception.
        if term != b"\n":
            print(f"Term char: {term}, rawData Length: {len(raw_data)}")
            raise ValueError("Data not terminated correctly.")

        self.socket_close()

        # Convert binary data to NumPy array of specified data type and return.
        return np.frombuffer(raw_data, dtype=dtype)

    def binblock_header(self, data: Any, q_trail: int = 0) -> str:
        """
        Get a IEEE 488.2 binary block header.

        :param data: binary data
        :param q_trail: trailing data hack
        :returns: IEEE 488.2 header

        #<x><yyy>..., where:
        <x> is the number of y bytes. For example, if <yyy>=500, then <x>=3.
        NOTE: <x> is a hexadecimal number.
        <yyy> is the number of bytes to transfer.
        """
        num_bytes = memoryview(data).nbytes + q_trail
        return f"#{len(str(num_bytes))}{num_bytes}"

    def binblockwrite(self, msg: str, data: Any, q_trail: int = 0) -> None:
        """
        Send data with IEEE 488.2 binary block format.

        :param msg: SCPI command
        :param data: binary data
        :param q_trail: trailing data hack
        :raises ValueError: non-zero ESR

        The data is formatted as:
        #<x><yyy><data><newline>, where:
        <x> is the number of y bytes. For example, if <yyy>=500, then <x>=3.
        NOTE: <x> is a hexadecimal number.
        <yyy> is the number of bytes to transfer. Care must be taken
        when selecting the data type used to interpret the data.
        The dtype argument used to read the data must match the data
        type used by the instrument that sends the data.
        <data> is the curve data in binary format.
        <newline> is a single byte new line character at the end of the data.
        """
        self.socket_open()
        header = self.binblock_header(data, q_trail)

        self.logger.debug(
            "Binary block header: %s msg: %d+%d bytes", header, len(msg), q_trail
        )

        # Send message, header, data, and termination
        self.socket.send(msg.encode("latin_1"))
        self.socket.send(header.encode("latin_1"))
        self.socket.send(data)
        self.socket.send(b"\r\n")
        # self.socket.send(b'\n')

        # Check error status register and notify of problems
        r = self.query("*ESR?")
        if int(r) != 0:
            raise ValueError(f"Non-zero ESR: {r}")
        self.socket_close()

    # pylint: disable-next=unused-argument
    def wfm_writer(self, name: str, data: Any, debug: bool = False) -> None:
        """
        Write waveform data to AWG.

        :param name: wave name
        :param data: data block
        :param debug: message flag
        :raises ValueError: waveform data not a multiple of 4 bytes
        """
        block_data = memoryview(data)
        num_samples, err = divmod(block_data.nbytes, 4)
        if err != 0:
            raise ValueError("Total waveform data must be a multiple of 4 bytes.")
        # The maximum write size is 250 MSamples (1 GB)
        max_write = 249999999
        self.logger.debug("Write waveform data %s (%d bytes)", name, block_data.nbytes)
        if block_data.nbytes < max_write:  # If waveform is small enough, one write
            self.binblockwrite(f'wlist:waveform:data "{name}", 0,', block_data)
        else:
            # If it's too big, multiple writes are required
            for offset in range(0, num_samples, max_write):
                partial_data = block_data[offset : offset + max_write]
                self.binblockwrite(
                    f'wlist:waveform:data "{name}", {offset},', partial_data
                )

    def write_scpi(self, cmd: str) -> None:
        """
        Send SCPI command using telnet.

        :param cmd: command string
        """
        self.logger.info("Send: %s", cmd)
        if self.use_telnet and self.port is not None:
            telnet(self.host, self.port, cmd, self.sentinel_string)
        else:
            self.visa_hw.write(cmd)

    def check_connection(self) -> bool:
        """
        Check that instrument is still online.

        :returns: true if online
        """
        if self.port == 0:
            return True
        self.logger.info("Check connection to instrument %s:%d", self.host, self.port)
        # skt_ready = False
        skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        if self.timeout is not None:
            self.logger.debug("Set timeout to %f seconds", self.timeout)
            skt.settimeout(self.timeout)
        try:
            skt.connect((self.host, self.port))
            skt_ready = True
            skt.close()
        except TimeoutError:
            self.logger.error("Connection timed out")
            skt_ready = False
        except ConnectionRefusedError:
            self.logger.error(
                "Could not connect to instrument %s:%d", self.host, self.port
            )
            skt_ready = False
        return skt_ready

    def clear_events(self) -> None:
        """
        Clear stuff related to events.

        Clears the following:
        Event Queue
        Standard Event Status Register
        Status Byte Register (except the MAV bit)
        """
        self.write_command("clear events", "*CLS")

    def print_query(self, descrptn: str, scpi_cmd: str, unit: str = "") -> None:
        """
        Write SCPI command to instrument and print reply.

        :param descrptn: description of what is happening
        :param scpi_cmd: command sent to instrument
        :param unit: units of value
        """
        if self.visa_hw is None:
            print(f"/* {descrptn} */\n{scpi_cmd}\n")
        elif self.use_telnet and self.port is not None:
            self.logger.debug(
                "Telnet [%s] '%s' to %s:%d",
                descrptn,
                scpi_cmd,
                self.host,
                self.port,
            )
            rval = telnet(self.host, self.port, scpi_cmd, self.sentinel_string)
            print(f"{descrptn:40} : {rval} {unit}")
        elif self.port:
            self.logger.debug(
                "TCP [%s] '%s' to %s:%d",
                descrptn,
                scpi_cmd,
                self.host,
                self.port,
            )
            rval = netcat(self.host, self.port, scpi_cmd, self.sentinel_string)
            print(f"{descrptn:40} : {rval} {unit}")
        else:
            self.logger.debug("VISA [%s] '%s'", descrptn, scpi_cmd)
            try:
                print(f"{descrptn:40} : {self.visa_hw.query(scpi_cmd).strip()} {unit}")
            except pyvisa.errors.VisaIOError as v_err:
                print(f"{descrptn:40} : ---")
                self.logger.warning("Could not read: %s", v_err)

    def read_query_str(self, descrptn: str, scpi_cmd: str, unit: str = "") -> str:
        """
        Write SCPI command to instrument and read reply as string.

        :param descrptn: description of what is happening
        :param scpi_cmd: command sent to instrument
        :param unit: units of value
        :returns: string value returned by instrument
        """
        rval: str | None
        if self.visa_hw is None:
            rval = None
            print(f"/* {descrptn} */\n{scpi_cmd}\n")
        elif self.use_telnet and self.port is not None:
            self.logger.debug(
                "Telnet [%s] '%s' to %s:%d",
                descrptn,
                scpi_cmd,
                self.host,
                self.port,
            )
            rval = telnet(self.host, self.port, scpi_cmd, self.sentinel_string)
            self.logger.debug("%s : %s %s", descrptn, rval, unit)
        elif self.port:
            self.logger.debug(
                "TCP [%s] '%s' to %s:%d", descrptn, scpi_cmd, self.host, self.port
            )
            rval = netcat(self.host, self.port, scpi_cmd, self.sentinel_string)
            self.logger.debug("%s '%s' : %s %s", descrptn, scpi_cmd, rval, unit)
        else:
            self.logger.debug("VISA [%s] '%s'", descrptn, scpi_cmd)
            try:
                rval = self.visa_hw.query(scpi_cmd).strip()
                self.logger.debug("Got reply '%s'", rval)
            except pyvisa.errors.VisaIOError as v_err:
                self.logger.warning("Could not read: %s", v_err)
                rval = None
        if rval is None:
            return ""
        return rval

    def read_query_state(
        self, descrptn: str, scpi_cmd: str, unit: str = ""
    ) -> bool | None:
        """
        Write SCPI command to instrument and read reply as boolean.

        :param descrptn: description of what is happening
        :param scpi_cmd: command sent to instrument
        :param unit: units of value
        :returns: boolean value returned by instrument
        """
        reply = self.read_query_str(descrptn, scpi_cmd, unit)
        try:
            if reply in ("0", "OFF"):
                rval = False
            elif reply in ("1", "ON"):
                rval = True
            else:
                self.logger.warning(
                    "Could not process reply to %s: '%s", scpi_cmd, reply
                )
                rval = None
        except ValueError:
            if " " in reply:  # type: ignore[operator]
                reply2 = reply.split(" ")  # type: ignore[union-attr]
                reply3 = reply2[1]
                try:
                    rval = bool(reply3)
                except ValueError:
                    self.logger.warning(
                        "Could not read %s from '%s' %s", descrptn, reply, reply2
                    )
                    rval = None
            else:
                self.logger.warning("Could not read %s from '%s'", descrptn, reply)
                rval = None
        return rval

    def read_query_int(
        self, descrptn: str, scpi_cmd: str, unit: str = ""
    ) -> int | None:
        """
        Write SCPI command to instrument and read reply as integer.

        :param descrptn: description of what is happening
        :param scpi_cmd: command sent to instrument
        :param unit: units of value
        :returns: integer value returned by instrument
        """
        reply = self.read_query_str(descrptn, scpi_cmd, unit)
        if reply is None:
            return None
        try:
            if " " in reply:
                reply2 = reply.split(" ")[1]
                try:
                    clean_text = "".join(char for char in reply2 if char.isprintable())
                    rval = int(clean_text)
                except ValueError:
                    self.logger.warning(
                        "Could not read %s from '%s' %s", descrptn, reply, reply2
                    )
                    rval = None
            else:
                clean_text = "".join(char for char in reply if char.isprintable())
                rval = int(clean_text)
        except ValueError:
            self.logger.warning("Could not read %s from '%s'", descrptn, reply)
            rval = None
        return rval

    def read_query_float(self, descrptn: str, scpi_cmd: str, unit: str = "") -> float:
        """
        Write SCPI command to instrument and read reply as floating point number.

        :param descrptn: description of what is happening
        :param scpi_cmd: command sent to instrument
        :param unit: units of value
        :returns: floating point number as returned by instrument
        """
        reply: str = ""
        rval: float
        try:
            reply = self.read_query_str(descrptn, scpi_cmd, unit)
            if not reply:
                return float("nan")
            if " " in reply:
                reply = reply.split(" ")[1]
            clean_text = "".join(char for char in reply if char.isprintable())
            rval = float(clean_text)
            if rval >= 9.9100e37:
                rval = float("nan")
        except ValueError as err:
            self.logger.warning("Could not read %s from '%s': %s", descrptn, reply, err)
            rval = float("nan")
        except ConnectionRefusedError as cerr:
            self.logger.warning("Could not read %s from AWG: %s", descrptn, cerr)
            rval = float("nan")
        return rval

    def write_query(self, descrptn: str, scpi_cmd: str) -> None:
        """
        Change setting to specified value.

        :param descrptn: description of what is happening
        :param scpi_cmd: command sent to instrument
        """
        self.write_command(descrptn, f"{scpi_cmd}")

    def write_query_str(
        self,
        descrptn: str,
        scpi_cmd: str,
        value: str,
        # pylint: disable-next=unused-argument
        unit: str = "",
    ) -> str:
        """
        Change setting to specified value.

        :param descrptn: description of what is happening
        :param scpi_cmd: command sent to instrument
        :param value: new value
        :param unit: units of value
        :returns: floating point number as returned by instrument
        """
        self.write_command(descrptn, f"{scpi_cmd} {value}")
        self.wait()
        new_val: str | None = self.read_query_str(descrptn, f"{scpi_cmd}?")
        if new_val is None:
            self.logger.warning("Could not value for %s", descrptn)
            return ""
        if new_val != value:
            # Check for substring, e.g. TRI instead of TRIANGLE
            if new_val == value[0 : len(new_val)]:
                self.logger.info("Set value for %s to %s", descrptn, new_val)
            else:
                self.logger.warning(
                    "Could not set value for %s, got %s", descrptn, new_val
                )
                return ""
        return new_val

    def write_query_state(
        self,
        descrptn: str,
        scpi_cmd: str,
        value: str,
    ) -> str | None:
        """
        Change setting to specified value.

        :param descrptn: description of what is happening
        :param scpi_cmd: command sent to instrument
        :param value: new value
        :returns: floating point number as returned by instrument
        """
        self.write_command(descrptn, f"{scpi_cmd} {value}")
        self.wait()
        new_val: str | None = self.read_query_str(descrptn, f"{scpi_cmd}?")
        if new_val is None:
            self.logger.warning("Could not write value for %s", descrptn)
            return None
        if new_val != value:
            # Check for substring, e.g. TRI instead of TRIANGLE
            try:
                chk_val = value[0 : len(new_val)]
            except TypeError:
                chk_val = new_val
            if value == "ON" and new_val == "1":
                self.logger.info("Set value for %s to %s", descrptn, new_val)
            elif value == "OFF" and new_val == "0":
                self.logger.info("Set value for %s to %s", descrptn, new_val)
            elif new_val == chk_val:
                self.logger.info("Set value for %s to %s", descrptn, new_val)
            else:
                self.logger.warning(
                    "Could not set value for %s, got %s", descrptn, new_val
                )
                return None
        return new_val

    def write_query_int(
        self, descrptn: str, scpi_cmd: str, value: int | None = None, unit: str = ""
    ) -> int | None:
        """
        Change setting to specified value.

        :param descrptn: description of what is happening
        :param scpi_cmd: command sent to instrument
        :param value: new value
        :param unit: units of value
        :returns: floating point number as returned by instrument
        """
        if value is not None:
            self.write_command(descrptn, f"{scpi_cmd} {value}")
        else:
            self.write_command(descrptn, scpi_cmd)
        self.wait()
        new_val: int | None = self.read_query_int(descrptn, f"{scpi_cmd}?")
        if new_val is None:
            self.logger.warning("Could not value for %s", descrptn)
            return None
        if new_val != value:
            self.logger.warning("Could not set value for %s", descrptn)
            return None
        new_str: str = eng_number(new_val, unit)
        print(f"{descrptn:45} : {new_str}")
        return new_val

    def write_query_float(
        self, descrptn: str, scpi_cmd: str, value: float | None = None, unit: str = ""
    ) -> float:
        """
        Change setting to specified value.

        :param descrptn: description of what is happening
        :param scpi_cmd: command sent to instrument
        :param value: new value
        :param unit: units of value
        :returns: floating point number as returned by instrument
        """
        if value is not None:
            self.write_command(descrptn, f"{scpi_cmd} {value}")
        else:
            self.write_command(descrptn, scpi_cmd)
        self.wait()
        new_val: float = self.read_query_float(descrptn, f"{scpi_cmd}?")
        if math.isnan(new_val):
            self.logger.warning("Could not write value for %s", descrptn)
            return new_val
        if value is not None:
            if new_val != value:
                self.logger.warning(
                    "Could not set value for %s, got %s", descrptn, str(new_val)
                )
                return float("nan")
        new_str: str = eng_number(new_val, unit)
        print(f"{descrptn:45} : {new_str}")
        return new_val

    def write_command(self, descrptn: str, scpi_cmd: str) -> None:
        """
        Write SCPI command to instrument without waiting for reply.

        :param descrptn: description of what is happening
        :param scpi_cmd: command sent to instrument
        """
        if self.visa_hw is None:
            print(f"/* {descrptn} */\n{scpi_cmd}\n")
        elif self.use_telnet and self.port is not None:
            self.logger.debug(
                "Send [%s] '%s' to %s:%d",
                descrptn,
                scpi_cmd,
                self.host,
                self.port,
            )
            telnet(self.host, self.port, scpi_cmd, self.sentinel_string)
        elif self.port:
            self.logger.debug(
                "Send %s command '%s' to %s:%d",
                descrptn,
                scpi_cmd,
                self.host,
                self.port,
            )
            netcat_tx(self.host, self.port, scpi_cmd, self.sentinel_string)
        else:
            self.logger.debug("Send %s command '%s'", descrptn, scpi_cmd)
            self.visa_hw.write(scpi_cmd)
            self.visa_hw.write("*WAI")

    def read_file(self, file_name: str) -> None:
        """
        Read SCPI commands from file.

        :param file_name: file to read
        """
        self.logger.info("Read SCPI command file %s", file_name)
        # pylint: disable-next=unspecified-encoding,consider-using-with
        scpi_file = open(file_name, "r")
        scpi_line = scpi_file.readline()
        while scpi_line:
            scpi_line = scpi_file.readline()
            scpi_cmd = scpi_line.strip()
            if "/*" in scpi_cmd:
                print(scpi_cmd)
                continue
            if not scpi_cmd:
                continue
            self.logger.debug("Process command '%s'", scpi_cmd)
            descrptn = scpi_cmd.lower().replace(":", " ").replace("?", "")
            if "?" in scpi_cmd:
                self.print_query(f"Read {descrptn}", scpi_cmd)
            else:
                self.write_command(f"Write {descrptn}", scpi_cmd)
        scpi_file.close()

    def identity(self) -> str:
        """
        Get identity of instrument.

        :returns: comma seperated identity values
        """
        scpi_cmd = "*IDN?"
        if self.visa_hw is None:
            # print(f"*/ identity */\n{scpi_cmd}\n")
            return "Tektronix,,,"
        self.logger.info("Get identity")
        self.logger.debug("Send [identity] '%s'", scpi_cmd)
        if self.port:
            scpi_id = self.read_query_str("Identity", scpi_cmd)
        else:
            try:
                scpi_id = self.visa_hw.query(scpi_cmd).strip()
                self.logger.info("Identity : %s", scpi_id)
            except UnicodeDecodeError as v_err:
                self.logger.error("I/O error: could not decode identity (%s)", v_err)
                return ""
            except pyvisa.errors.VisaIOError as v_err:
                self.logger.error("I/O error: could not read identity (%s)", v_err)
                return ""
        return scpi_id

    def reset(self) -> None:
        """Connect to oscilloscope and send reset command."""
        self.write_command("reset", "*CLS")

    def wait(self) -> None:
        """Connect to oscilloscope and send reset command."""
        self.write_command("wait", "*WAI")

    def read_esr(self) -> int:
        """
        Connect to oscilloscope and read event status register.

        :returns: error found

        Bit         Function
        7 (MSB) PON Power On. Shows that the oscilloscope was powered on.
        6       URQ User Request. Indicates that an application event has occurred.
        5       CME Command Error. occurred while parsing a command or query.
        4       EXE Execution Error while executing command or query.
        3       DDE Device Error shows that a device error occurred.
        2       QYE Query Error when no data was present or pending, or data was lost.
        1       RQC Request Control is not used.
        0 (LSB) OPC Operation Complete shows that operation is complete.
        """
        esr = self.read_query_int("event status register", "*ESR?")
        if esr is None:
            self.logger.warning("Could not read event status register")
            return 1
        # pylint: disable-next=consider-using-f-string
        print("Event status register : %x" % esr)
        self.print_query("event message", "EVMsg?")
        if esr & 0b1000000:
            print("Power On")
        if esr & 0b100000:
            print("User Request")
        if esr & 0b10000:
            print("Command Error")
        if esr & 0b1000:
            print("Device Error")
        if esr & 0b100:
            print("Query Error")
        if esr & 0b10:
            print("Request Control")
        if esr & 0b1:
            print("Operation Complete")
            esr = 0
        return esr

    def write_data_binary(self, q_str: str, y_vals: Any, q_trail: int = 0) -> None:
        """
        Write binary data to instrument.

        :param q_str: query string
        :param y_vals: binary values
        :param q_trail: trailing data hack
        """
        self.logger.debug(
            "Write binary data %s (%d+%d bytes)", q_str, len(y_vals), q_trail
        )
        if self.use_telnet:
            self.binblockwrite(q_str, y_vals, q_trail)
            telnet(self.host, self.port, "", self.sentinel_string)
        else:
            self.visa_hw.write_binary_values(q_str, y_vals)

    def read_data_binary(self, q_str: str) -> np.ndarray:
        """
        Read binary data from instrument.

        :param q_str: query string
        :returns: array with binary data
        """
        if self.use_telnet:
            binary_data = self.binblockread(np.float64)
        else:
            binary_data = self.visa_hw.query_binary_values(
                q_str, datatype="f", is_big_endian=False
            )
        return binary_data

    def __del__(self) -> None:
        """Shut down this thing."""
        self.logger.debug("Disconnect")
        if self.port:
            pass
        elif self.use_telnet:
            pass
        elif self.visa_hw is not None:
            self.visa_hw.close()
            self.visa_rm.close()
        else:
            pass
