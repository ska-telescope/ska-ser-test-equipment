"""Default values for SCPI communications."""

import json
import logging
import os
from typing import Any, Dict, List, Tuple

import yaml

from ska_ser_test_equipment.interface_definitions import InterfaceDefinitionFactory

logging.basicConfig()
_module_logger = logging.getLogger(__name__)
_module_logger.setLevel(logging.WARNING)

TANGO_LOG_LEVEL = {
    0: "OFF",
    1: "FATAL",
    2: "ERROR",
    3: "WARNING",
    4: "INFO",
    5: "DEBUG",
}
POLL_COUNT = 20
VALUES_FILE = "/app/charts/values-mid-itf.yaml"


def find_files(search_path: str, file_name: str) -> List[str]:
    """
    Walk top-down from the root to find files.

    :param search_path: start here
    :param file_name: file to search for
    :returns: list of matches
    """
    result = []

    _module_logger.debug("Find %s in %s", file_name, search_path)
    for f_root, _f_dir, files in os.walk(search_path):
        if file_name in files:
            result.append(os.path.join(f_root, file_name))
    return result


def get_host_port(dev_name: str) -> Tuple[str, int]:
    """
    Get hostname and port number from YAML file.

    :param dev_name: device name as used in YAML file
    :returns: hostname and port number
    """
    host = os.getenv("SIMULATOR_HOST", "")
    if host:
        sport = os.getenv("SIMULATOR_PORT")
        if sport:
            port = int(sport)
        else:
            port = 0
    else:
        _module_logger.info("Read network configuration from %s", VALUES_FILE)
        try:
            # pylint: disable-next=unspecified-encoding
            with open(VALUES_FILE, "r") as file:
                config = yaml.safe_load(file)
        except FileNotFoundError:
            return "", 0
        _module_logger.debug("%s", json.dumps(config, indent=4))
        if dev_name not in config["test_equipment"]["deviceServers"]:
            _module_logger.error(
                "Device %s not in %s",
                dev_name,
                config["test_equipment"]["deviceServers"],
            )
            return "", 0
        host = config["test_equipment"]["deviceServers"][dev_name]["devices"][0]["host"]
        port = int(
            config["test_equipment"]["deviceServers"][dev_name]["devices"][0]["port"]
        )
    _module_logger.info("Connect to %s:%d", host, port)
    return host, port


# pylint: disable-next=too-many-branches,too-many-statements
def get_scpi_config(model: str) -> Tuple[dict, dict, dict, dict]:  # noqa: C901
    """
    Read SCPI fields and values from YAML file.

    :param model: hardware model e.g. MS2090A

    :returns: dictionaries with fields and values
    """
    scpi_fields: Dict[str, str] = {}
    scpi_values: Dict[str, bool | float | int | str] = {}
    scpi_min_values: Dict[str, bool | float | int | str] = {}
    scpi_max_values: Dict[str, bool | float | int | str] = {}
    config = InterfaceDefinitionFactory()(model)

    _module_logger.debug("%s", json.dumps(config, indent=4))
    attributes = config["attributes"]
    _module_logger.debug("Attributes %s", attributes)
    for attribute in sorted(attributes):
        try:
            value: bool | float | int | str | None = None
            min_value: float | int | None = None
            max_value: float | int | None = None
            if "read" in attributes[attribute]:
                field = attributes[attribute]["read"]["field"]
                if attributes[attribute]["read"]["field_type"] == "int":
                    if "value" in attributes[attribute]["read"]:
                        value = int(
                            attributes[attribute]["read"][
                                "value"
                            ]  # type: ignore[arg-type]
                        )
                    else:
                        _module_logger.info("No read value for %s", attribute)
                    if "min_value" in attributes[attribute]["read"]:
                        min_value = int(attributes[attribute]["read"]["min_value"])
                    if "max_value" in attributes[attribute]["read"]:
                        max_value = int(attributes[attribute]["read"]["max_value"])
                elif attributes[attribute]["read"]["field_type"] == "float":
                    value = float(
                        attributes[attribute]["read"]["value"]  # type: ignore[arg-type]
                    )
                    if "min_value" in attributes[attribute]["read"]:
                        min_value = float(attributes[attribute]["read"]["min_value"])
                    if "max_value" in attributes[attribute]["read"]:
                        max_value = float(attributes[attribute]["read"]["max_value"])
                elif attributes[attribute]["read"]["field_type"] == "bool":
                    value = bool(attributes[attribute]["read"]["value"])
                elif attributes[attribute]["read"]["field_type"] == "str":
                    value = str(attributes[attribute]["read"]["value"])
                else:
                    value = str(attributes[attribute]["read"]["value"])
                _module_logger.info("Read value for %s is %s", attribute, str(value))
            elif "read_write" in attributes[attribute]:
                field = attributes[attribute]["read_write"]["field"]
                if attributes[attribute]["read_write"]["field_type"] == "int":
                    value = int(
                        attributes[attribute]["read_write"][
                            "value"
                        ]  # type: ignore[arg-type]
                    )
                elif attributes[attribute]["read_write"]["field_type"] == "float":
                    value = float(
                        attributes[attribute]["read_write"][
                            "value"
                        ]  # type: ignore[arg-type]
                    )
                elif attributes[attribute]["read_write"]["field_type"] == "bool":
                    value = bool(attributes[attribute]["read_write"]["value"])
                elif attributes[attribute]["read_write"]["field_type"] == "str":
                    value = str(attributes[attribute]["read_write"]["value"])
                else:
                    value = str(attributes[attribute]["read_write"]["value"])
                _module_logger.info(
                    "Read write value for %s is %s", attribute, str(value)
                )
            elif "write" in attributes[attribute]:
                # TODO read this value
                field = attributes[attribute]["write"]["field"]
                _module_logger.info("Skip write value for %s", attribute)
                value = None
            else:
                _module_logger.info("No read or write entry for %s", attribute)
                field = None
                value = None
            if field is not None:
                _module_logger.info("Attribute %s field: %s", attribute, field)
                scpi_fields[attribute] = field
            else:
                _module_logger.info("No field for %s", attribute)
            if value is not None:
                _module_logger.info("Attribute %s value: %s", attribute, str(value))
                scpi_values[attribute] = value
            else:
                _module_logger.info(
                    "No value for %s in %s", attribute, attributes[attribute]
                )
            if min_value is not None:
                _module_logger.info(
                    "Attribute %s minimum value: %s", attribute, str(min_value)
                )
                scpi_min_values[attribute] = min_value
            if max_value is not None:
                _module_logger.info(
                    "Attribute %s maximum value: %s", attribute, str(max_value)
                )
                scpi_max_values[attribute] = max_value
        except KeyError as err_msg:
            _module_logger.error("Could not read attribute %s: %s", attribute, err_msg)
    return scpi_fields, scpi_values, scpi_min_values, scpi_max_values


def get_scpi_read_write(model: str) -> Tuple[dict, dict]:  # noqa: C901
    """
    Read SCPI fields and values from YAML file.

    :param model: hardware model e.g. MS2090A

    :returns: dictionaries with fields and values
    """
    # scpi_read: Dict[str, Tuple[str, str | None]] = {}
    # scpi_read: tuple[
    #     str, float | int | str | list[float | int | Any] | None, str
    # ] = {}  # type: ignore[assignment]
    scpi_read: dict[Any, Any] = {}
    scpi_write: Dict[str, Tuple[str, str | None]] = {}
    config = InterfaceDefinitionFactory()(model)
    attributes = config["attributes"]
    _module_logger.debug("Attributes %s", attributes)
    for attribute in sorted(attributes):
        if "read" in attributes[attribute]:
            try:
                field = attributes[attribute]["read"]["field"]
                if "value" in attributes[attribute]["read"]:
                    value = attributes[attribute]["read"][
                        "value"
                    ]  # type: ignore[arg-type]
                else:
                    value = None
                ftype = attributes[attribute]["read"]["field_type"]
                _module_logger.info(
                    "Read %s : %s = %s (%s)", attribute, field, value, ftype
                )
                scpi_read[attribute] = (field, value, ftype)  # type: ignore[index]
            except KeyError as err_msg:
                _module_logger.error(
                    "Could not get read attribute %s: %s", attribute, err_msg
                )
        if "write" in attributes[attribute]:
            try:
                field = attributes[attribute]["write"]["field"]
                value = None
                if "field_type" in attributes[attribute]["write"]:
                    ftype = attributes[attribute]["write"]["field_type"]
                else:
                    ftype = "NONE"
                scpi_write[attribute] = (
                    field,
                    value,
                    ftype,
                )  # type: ignore[assignment]
                _module_logger.info("Write %s : %s (%s)", attribute, field, ftype)
            except KeyError as err_msg:
                _module_logger.error(
                    "Could not get write attribute %s: %s", attribute, err_msg
                )
    return scpi_read, scpi_write


def get_scpi_polls(scpi_fields: dict) -> dict:
    """
    Get hard-coded poll count.

    :param scpi_fields: dictionary with SCPI fields, used for attribute names

    :returns: dictionary with poll counts
    """
    # TODO probably not needed
    scpi_polls = {}
    for field in scpi_fields:
        scpi_polls[field] = POLL_COUNT
    return scpi_polls
