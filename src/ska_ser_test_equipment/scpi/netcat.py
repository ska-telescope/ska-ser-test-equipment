"""Implement simple Telnet client."""

import logging
import socket

_module_logger = logging.getLogger(__name__)


def netcat(
    hostname: str,
    port: int,
    cmd: str,
    sentinel_string: str,
    timeout: float | None = None,
) -> str | None:
    """
    Set up Telnet session with instrument.

    :param hostname: instrument IP address or hostname
    :param port: instrument port number
    :param cmd: SCPI command to be sent to instrument
    :param sentinel_string: appended to command
    :param timeout: timeout in seconds

    :returns: message received
    """
    _module_logger.debug(
        "Connect to %s:%d send/receive %s : %s", hostname, port, type(cmd), cmd
    )
    cmd += sentinel_string
    content = bytes(cmd, encoding="utf-8")
    skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    if timeout is not None:
        _module_logger.debug("Set timeout to %f seconds", timeout)
        skt.settimeout(timeout)
    try:
        skt.connect((hostname, port))
    except ConnectionRefusedError:
        _module_logger.error("Could not connect to host %s:%d", hostname, port)
        return None
    # skt.sendall(content)
    skt.send(content)
    # skt.shutdown(socket.SHUT_WR)
    r_data = b""
    while 1:
        data = skt.recv(1024, socket.MSG_WAITALL)
        if len(data) == 0:
            break
        # _module_logger.debug("Received:%s", repr(data))
        r_data += data
    skt.close()
    _module_logger.debug("Received %05d bytes: %s", len(r_data), r_data)
    r_val = r_data.decode("utf-8").strip()
    return r_val


def netcat_tx(hostname: str, port: int, cmd: str, sentinel_string: str) -> None:
    """
    Set up Telnet session with instrument.

    :param hostname: instrument IP address or hostname
    :param port: instrument port number
    :param cmd: SCPI command to be sent to instrument
    :param sentinel_string: appended to command
    """
    _module_logger.debug("Send %s : %s", type(cmd), cmd)
    cmd += sentinel_string
    content = bytes(cmd, encoding="utf-8")
    skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        skt.connect((hostname, port))
    except ConnectionRefusedError:
        _module_logger.error("Could not connect to host %s:%d", hostname, port)
        return
    skt.sendall(content)
    # skt.send(content)
    skt.shutdown(socket.SHUT_WR)
    skt.close()
