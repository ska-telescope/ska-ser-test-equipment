"""
Communicate with test instrument using Virtual Instrument Software Architecture (VISA).

Uses VISA library, see https://pyvisa.readthedocs.io/en/latest/
"""

import logging

import pyvisa
import pyvisa_py  # type: ignore[import]

_module_logger = logging.getLogger(__name__)


def visa_tx_rx(host: str, scpi_tx: str, timeout: float = 0.0) -> str:
    """
    Use VISA to send a CSPI command and read reply.

    :param host: instrument IP address or hostname
    :param scpi_tx: SCPI command to be sent to instrument
    :param timeout: timeout period in seconds

    :returns: message received
    """
    visa_resource_addr = f"TCPIP::{host}::INSTR"
    _module_logger.debug("Send %s : %s (%d sec)", visa_resource_addr, scpi_tx, timeout)
    visa_rm = pyvisa.ResourceManager()
    try:
        visa_hw = visa_rm.open_resource(visa_resource_addr)
    except ConnectionRefusedError:
        _module_logger.error("Connection refused to %s", visa_resource_addr)
        return ""
    except pyvisa_py.protocols.rpc.RPCError:
        _module_logger.error("RPC error: could not connect to %s", visa_resource_addr)
        return ""
    except pyvisa.errors.VisaIOError:
        _module_logger.error("I/O error: could not connect to %s", visa_resource_addr)
        return ""
    if timeout != 0.0:
        visa_hw.timeout = timeout * 1000
    try:
        scpi_rx = visa_hw.query(scpi_tx).strip()  # type: ignore[attr-defined]
    except pyvisa.errors.VisaIOError as e_msg:
        _module_logger.error("I/O error on %s: %s", scpi_tx, e_msg)
        return ""
    _module_logger.debug("Sent VISA %s, received '%s'", scpi_tx, scpi_rx)
    visa_hw.close()
    visa_rm.close()
    return scpi_rx


def visa_tx(host: str, scpi_tx: str, timeout: float = 0.0) -> None:
    """
    Use VISA to send a CSPI command.

    :param host: instrument IP address or hostname
    :param scpi_tx: SCPI command to be sent to instrument
    :param timeout: timeout period in seconds
    """
    visa_resource_addr = f"TCPIP::{host}::INSTR"
    _module_logger.debug("Send %s : %s (%d sec)", visa_resource_addr, scpi_tx, timeout)
    visa_rm = pyvisa.ResourceManager()
    _module_logger.debug("Resources %s", visa_rm.list_resources())
    try:
        visa_hw = visa_rm.open_resource(visa_resource_addr)
    except ConnectionRefusedError:
        _module_logger.error("Connection refused to %s", visa_resource_addr)
        return
    except pyvisa_py.protocols.rpc.RPCError:
        _module_logger.error("RPC error: could not connect to %s", visa_resource_addr)
        return
    except pyvisa.errors.VisaIOError:
        _module_logger.error("I/O error: could not connect to %s", visa_resource_addr)
        return
    if timeout != 0.0:
        visa_hw.timeout = timeout * 1000
    try:
        visa_hw.write(scpi_tx.strip())  # type: ignore[attr-defined]
    except pyvisa.errors.VisaIOError as e_msg:
        _module_logger.error("I/O error: %s", e_msg)
        return
    visa_hw.close()
    visa_rm.close()
    return
