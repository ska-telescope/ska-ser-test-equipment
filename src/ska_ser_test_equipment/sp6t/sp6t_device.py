# -*- coding: utf-8 -*-
#
# (c) 2022 CSIRO.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""This module provides a Tango device for a switch matrix."""
from typing import Any

import tango
import tango.server

from ska_ser_test_equipment.base.base_device import TestEquipmentBaseDevice
from ska_ser_test_equipment.mixins import SCPIWriteQueryMixin

from ..interface_definitions import InterfaceDefinitionFactory
from .sp6t_component_manager import SP6TComponentManager

__all__ = ["SP6TDevice", "main"]


class SP6TDevice(TestEquipmentBaseDevice, SCPIWriteQueryMixin):
    """A Tango device for monitor and control of a switch matrix."""

    # --------------
    # Initialization
    # --------------

    def create_component_manager(self) -> SP6TComponentManager:
        """
        Create and return a component manager for this device.

        :returns: a component manager for this device.
        """
        # pylint: disable-next=attribute-defined-outside-init
        self._interface_definition = InterfaceDefinitionFactory()(self.Model)
        return SP6TComponentManager(
            self._interface_definition,
            self.Protocol,
            self.Host,
            self.Port,
            self.logger,
            self._communication_state_changed,
            self._component_state_changed,
            update_rate=self.UpdateRate,
        )


# ----------
# Run server
# ----------
def main(args: Any = None, **kwargs: Any) -> int:
    """
    Launch a `SwitchMatrixDevice` server instance.

    :param args: arguments to the switch matrix device.
    :param kwargs: keyword arguments to the server.

    :returns: the Tango server exit code.
    """
    return tango.server.run((SP6TDevice,), args=args, **kwargs)


if __name__ == "__main__":
    main()
