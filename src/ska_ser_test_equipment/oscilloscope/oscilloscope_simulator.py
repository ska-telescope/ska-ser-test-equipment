#!/usr/bin/env python
"""Implementation of a simple SCPI simulator for the oscilloscope."""
import logging
import math
import os
import socket
import threading
import time

# pylint: disable-next=unused-import,useless-suppression
from typing import Any, Dict, Final, Optional, Sized, cast  # noqa: E501,F401

import numpy as np
from scipy import signal  # type: ignore[import]
from ska_ser_devices.client_server import TcpServer
from ska_ser_scpi import ScpiSimulator, SupportedAttributeType

from ska_ser_test_equipment.interface_definitions import InterfaceDefinitionFactory
from ska_ser_test_equipment.oscilloscope.oscilloscope_data import (
    ACQUIRE,
    CHANNEL_1_CURVE,
    CHANNEL_1_PARAMETERS,
    CURRENT_SETTINGS,
    MEASUREMENT_DATA,
    SUPPORTED_MODELS,
    TRACE_PARAMETERS,
)

log_level_str = os.getenv("LOG_LEVEL", "WARNING").upper()
if log_level_str == "DEBUG":
    LOG_LEVEL = logging.DEBUG
elif log_level_str == "INFO":
    LOG_LEVEL = logging.INFO
elif log_level_str == "WARNING":
    LOG_LEVEL = logging.WARNING
elif log_level_str == "ERROR":
    LOG_LEVEL = logging.ERROR
else:
    LOG_LEVEL = logging.WARNING
logging.basicConfig(level=LOG_LEVEL)
_module_logger = logging.getLogger(__name__)


class OscilloscopeSimulator(ScpiSimulator):
    """An oscilloscope simulator TCP server."""

    DEFAULTS: Final[Dict[str, SupportedAttributeType]] = {
        # "acquire_numacq": 350,
        "acquisition": ACQUIRE,
        "acquisition_mode": "SAMPLE",
        "binary_field_width": 1,
        "busy": False,
        "ch1_attenuation": 1.0,
        "ch1_bandwidth": 500.0e06,
        "ch1_clipping": False,
        "ch1_coupling": "DC",
        "ch1_display_vertical_scale": 1.0,
        "ch1_ext_attenuation": 3.0,
        "ch1_offset": 1.0e-3,
        "ch1_parameters": CHANNEL_1_PARAMETERS,
        "ch1_position": 0.0,
        "ch1_probe_gain": 0.1,
        "ch1_probe_units": "V",
        "ch1_select": True,
        "ch1_trigger_a_level": 1.2,
        "ch1_trigger_b_level": 1.3,
        "ch1_vertical_position": 0.0,
        "ch1_vertical_scale": 0.5,
        "ch1_vertical_scale_ratio": 1.0,
        "ch2_attenuation": 1.0,
        "ch2_bandwidth": 500.0e06,
        "ch2_clipping": False,
        "ch2_coupling": "DC",
        "ch2_display_vertical_scale": 1.0,
        "ch2_ext_attenuation": 3.0,
        "ch2_offset": 1.0e-3,
        "ch2_parameters": CHANNEL_1_PARAMETERS,
        "ch2_position": 0.0,
        "ch2_probe_gain": 0.1,
        "ch2_probe_units": "V",
        "ch2_select": True,
        "ch2_trigger_a_level": 1.2,
        "ch2_trigger_b_level": 1.3,
        "ch2_vertical_position": 0.0,
        "ch2_vertical_scale": 0.5,
        "ch2_vertical_scale_ratio": 1.0,
        "ch3_attenuation": 1.0,
        "ch3_bandwidth": 500.0e06,
        "ch3_clipping": False,
        "ch3_coupling": "DC",
        "ch3_display_vertical_scale": 1.0,
        "ch3_ext_attenuation": 3.0,
        "ch3_offset": 1.0e-3,
        "ch3_parameters": CHANNEL_1_PARAMETERS,
        "ch3_position": 0.0,
        "ch3_probe_gain": 0.1,
        "ch3_probe_units": "V",
        "ch3_select": True,
        "ch3_trigger_a_level": 1.2,
        "ch3_trigger_b_level": 1.3,
        "ch3_vertical_position": 0.0,
        "ch3_vertical_scale": 0.5,
        "ch3_vertical_scale_ratio": 1.0,
        "ch4_attenuation": 1.0,
        "ch4_bandwidth": 500.0e06,
        "ch4_clipping": False,
        "ch4_coupling": "DC",
        "ch4_display_vertical_scale": 1.0,
        "ch4_ext_attenuation": 3.0,
        "ch4_offset": 1.0e-3,
        "ch4_parameters": CHANNEL_1_PARAMETERS,
        "ch4_position": 0.0,
        "ch4_probe_gain": 0.1,
        "ch4_probe_units": "V",
        "ch4_select": True,
        "ch4_trigger_a_level": 1.2,
        "ch4_trigger_b_level": 1.3,
        "ch4_vertical_position": 0.0,
        "ch4_vertical_scale": 0.5,
        "ch4_vertical_scale_ratio": 1.0,
        "command_error": False,
        "current_settings": CURRENT_SETTINGS,
        "data_encoding": "ASCII",
        "data_source": "CH1",
        "data_source_available": "CH1",
        "data_start": 1,
        "data_stop": 1000,
        "data_width": 1,
        "date": "2023-12-25",
        "device_error": False,
        "display_select_source": "CH1",
        "display_select_view": "WAVEVIEW1",
        "display_spectrum_source": "CH1",
        "execution_error": False,
        "flag_when_complete": True,
        "horizontal_delay_mode": False,
        "horizontal_delay_time": 0.0,
        "horizontal_divisions": 10.0,
        "horizontal_duration": 5.0e-9,
        "horizontal_increment": 5.0e-9,
        "horizontal_mode": "AUTO",
        "horizontal_position": 0.0,
        "horizontal_sample_rate": 5e6,
        "horizontal_scale": 20e-9,
        # "identity": "TEKTRONIX,MSO54,C100123,CF:91.1CT FV:1.2.0.2886",
        "identity": "TEKTRONIX,MSO64B,C047394,CF:91.1CT FV:1.44.3.433",
        "measurement": MEASUREMENT_DATA,
        "operation_complete": True,
        "power_cycled": True,
        "query_error": False,
        "sweep_points": 5000,
        "time": "23:59:49",
        "trace_data": CHANNEL_1_CURVE,
        "trace_format": "ASC",
        "trace_parameters": TRACE_PARAMETERS,
        "trace_source": "CH1",
        "trigger_zero": 0.0,
        "vertical_offset": 0.0,
        "vertical_position": 0.0,
        "vertical_scale": 100e-3,
    }

    def __init__(
        self,
        model: str,
        **kwargs: SupportedAttributeType,
    ) -> None:
        """
        Initialise a new oscilloscope simulator.

        :param model: the model identifier to use.
        :param kwargs: initial values for simulator attributes; where an
            initial value is not provided for an attribute, a default
            value will be used.
        """
        self._lock = threading.Lock()

        self._num_markers = 0

        self._tracing = False
        self._flag_when_complete = False
        self._command_complete = False

        interface_definition = InterfaceDefinitionFactory()(model)
        self.model = model
        _module_logger.info("Interface definition %s", interface_definition)

        # TODO for future use
        data_fn = os.getenv("SIMULATOR_DATA")
        if data_fn is not None:
            _module_logger.info("Read data from %s", data_fn)
            with open(data_fn, encoding="utf-8") as d_fh:
                trace_data = d_fh.read()
            self.DEFAULTS["trace_data"] = trace_data

        self._flag_when_complete = True

        initial_values = kwargs
        for key, value in self.DEFAULTS.items():
            initial_values.setdefault(key, value)

        super().__init__(interface_definition, initial_values)
        self._num_markers = 1

    # pylint: disable-next=no-self-use
    def clear_acquisitions(self) -> None:
        """Clear the acquisitions or whatever."""
        return

    def clear_markers(self) -> None:
        """Clear all markers."""
        self._num_markers = 0

    def add_marker(self, mk_flag: bool | None = None) -> None:
        """
        Add a marker.

        :param mk_flag: marker flag
        """
        _module_logger.info("Add marker (%s)", mk_flag)
        self._num_markers = 1

    # pylint: disable-next=no-self-use
    def sine_wave(self, ampl: float, freq: float) -> str:
        """
        Generate a sine wave.

        :param ampl: amplitude
        :param freq: frequency
        :returns: string with header and comma seperated values
        """
        duratn: float = 3 / freq
        data_len: int = 1000

        _module_logger.info("Generate sine wave, %fV %fHz", ampl, freq)
        xinc: float = float(duratn / data_len)
        t_zero: float = 0.0
        x_points: np.ndarray[Any, Any] = np.ndarray(data_len, float)
        y_points: np.ndarray[Any, Any] = np.ndarray(data_len, float)
        for i in range(0, data_len):
            # Create timestamp for the data point
            tval: float = t_zero + xinc * i
            x_points[i] = tval
            # Convert raw ADC value into a floating point value
            y_points[i] = ampl * math.sin(2 * np.pi * freq * tval)
            _module_logger.debug("%.3e : %.3e", x_points[i], y_points[i])

        data_str = f"{y_points[0]:.3e}"
        for y_point in y_points[1:]:
            data_str += f",{y_point:.3e}"
        data_len_str: str = f"{len(data_str)}"
        rval = f"#{len(data_len_str)}{data_len_str}{data_str}"
        _module_logger.info("Trace: %s", rval)
        return rval

    # pylint: disable-next=no-self-use
    def triangle_wave(self, ampl: float, freq: float) -> str:
        """
        Generate a triangle wave.

        :param ampl: amplitude
        :param freq: frequency
        :returns: string with header and comma seperated values
        """
        duratn: float = 3 / freq
        data_len: int = 1000

        _module_logger.info("Generate triangle wave, %fV %fHz", ampl, freq)
        xinc: float = float(duratn / data_len)
        t_zero: float = 0.0
        x_points: np.ndarray[Any, Any] = np.ndarray(data_len, float)
        y_points: np.ndarray[Any, Any] = np.ndarray(data_len, float)
        for i in range(0, data_len):
            # Create timestamp for the data point
            tval: float = t_zero + xinc * i
            x_points[i] = tval
            # Convert raw ADC value into a floating point value
            y_points[i] = (
                2 * ampl * np.arcsin(math.sin(2 * np.pi * freq * tval)) / np.pi
            )
            _module_logger.debug("%.3e : %.3e", x_points[i], y_points[i])

        data_str = f"{y_points[0]:.3e}"
        for y_point in y_points[1:]:
            data_str += f",{y_point:.3e}"
        data_len_str: str = f"{len(data_str)}"
        rval = f"#{len(data_len_str)}{data_len_str}{data_str}"
        _module_logger.info("Trace: %s", rval)
        return rval

    # pylint: disable-next=no-self-use
    def sawtooth_wave(self, ampl: float, freq: float) -> str:
        """
        Generate a sawtooth wave.

        :param ampl: amplitude
        :param freq: frequency
        :returns: string with header and comma seperated values
        """
        duratn: float = 3 / freq
        data_len: int = 1000

        _module_logger.info("Generate triangle wave, %fV %fHz", ampl, freq)
        xinc: float = float(duratn / data_len)
        t_zero: float = 0.0
        x_points: np.ndarray[Any, Any] = np.ndarray(data_len, float)
        y_points: np.ndarray[Any, Any] = np.ndarray(data_len, float)
        for i in range(0, data_len):
            # Create timestamp for the data point
            tval: float = t_zero + xinc * i
            x_points[i] = tval
            # Convert raw ADC value into a floating point value
            y_points[i] = ampl * signal.sawtooth(2 * np.pi * freq * tval)
            _module_logger.debug("%.3e : %.3e", x_points[i], y_points[i])

        data_str = f"{y_points[0]:.3e}"
        for y_point in y_points[1:]:
            data_str += f",{y_point:.3e}"
        data_len_str: str = f"{len(data_str)}"
        rval = f"#{len(data_len_str)}{data_len_str}{data_str}"
        _module_logger.info("Trace: %s", rval)
        return rval

    # pylint: disable-next=no-self-use
    def square_wave(self, ampl: float, freq: float) -> str:
        """
        Generate a square wave.

        :param ampl: amplitude
        :param freq: frequency
        :returns: string with header and comma seperated values
        """
        duratn: float = 3 / freq
        data_len: int = 1000

        _module_logger.info("Generate sine wave, %fV %fHz", ampl, freq)
        xinc: float = float(duratn / data_len)
        t_zero: float = 0.0
        x_points: np.ndarray[Any, Any] = np.ndarray(data_len, float)
        y_points: np.ndarray[Any, Any] = np.ndarray(data_len, float)
        for i in range(0, data_len):
            tval: float = t_zero + xinc * i
            x_points[i] = tval
            y_point = ampl * math.sin(2 * np.pi * freq * tval)
            if y_point > 0.0:
                y_points[i] = ampl
            else:
                y_points[i] = -1.0 * ampl
            _module_logger.debug("%.3e : %.3e", x_points[i], y_points[i])

        data_str = f"{y_points[0]:.3e}"
        for y_point in y_points[1:]:
            data_str += f",{y_point:.3e}"
        data_len_str: str = f"{len(data_str)}"
        rval = f"#{len(data_len_str)}{data_len_str}{data_str}"
        _module_logger.info("Trace: %s", rval)
        return rval

    def trace_data(self) -> str:
        """
        Get a trace.

        :returns: trace data string
        """
        data_source = self.get_attribute("data_source")
        _module_logger.info("Get trace for %s : %s", self.model, data_source)

        ampl: float = 4 * cast(float, self.get_attribute("vertical_scale"))
        freq: float = 2 / cast(float, self.get_attribute("horizontal_scale"))
        if data_source == "CH1":
            rval = self.sine_wave(ampl, freq)
        elif data_source == "CH2":
            rval = self.triangle_wave(ampl, freq)
        elif data_source == "CH3":
            rval = self.sawtooth_wave(ampl, freq)
        elif data_source == "CH4":
            rval = self.square_wave(ampl, freq)
        else:
            _module_logger.warning("Invalid data source %s", data_source)
            rval = "#10"
        return rval

    def reset(self) -> None:
        """Reset to factory default values."""
        self._num_markers = 0
        self.set_attribute("ch1_attenuation", 1.0)
        self.set_attribute("ch1_coupling", "DC")
        self.set_attribute("ch1_probe_gain", 0.1)
        self.set_attribute("ch1_offset", 1.0e-3)
        self.set_attribute("ch1_scale", 100.0000e-3)

    def flag_when_complete(self, c_flag: bool | None = None) -> bool:
        """
        Raise a flag when command is complete.

        :param c_flag: value to set
        :returns: current value
        """
        _module_logger.info("Flag completed (%s)", str(c_flag))
        if c_flag is not None:
            self._flag_when_complete = c_flag
        return self._flag_when_complete

    # pylint: disable-next=no-self-use
    def operation_complete(self) -> bool:
        """
        Return whether all previous operations are complete.

        This only returns true if :py:meth:`flag_when_complete` was
        called while the operation was running, and the operation is now
        complete.

        :returns: whether all previous operations are complete.
        """
        complete = True
        _module_logger.info("Operation complete %s", str(complete))
        return complete

    # pylint: disable-next=no-self-use
    def wait(self) -> None:
        """Wait for something to happen."""
        return


def main() -> None:
    """Run the socketserver main loop."""
    if _module_logger.isEnabledFor(logging.DEBUG):
        _module_logger.warning("Log level is DEBUG")
    elif _module_logger.isEnabledFor(logging.INFO):
        _module_logger.warning("Log level is INFO")
    elif _module_logger.isEnabledFor(logging.WARNING):
        _module_logger.warning("Log level is WARNING")
    else:
        pass
    model = os.getenv("SIMULATOR_MODEL", "MSO64B").upper()
    if model not in SUPPORTED_MODELS:
        _module_logger.error(
            "Model %s should be one of: %s", model, " ".join(SUPPORTED_MODELS)
        )
        return
    host = os.getenv("SIMULATOR_HOST", socket.gethostname())
    port = int(os.getenv("SIMULATOR_PORT", "4000"))
    _module_logger.info("Start simulator for %s on %s:%d", model, host, port)

    simulator = OscilloscopeSimulator(model)
    n_try = 0
    n_max = 5
    server = None
    while n_try <= n_max:
        n_try += 1
        try:
            server = TcpServer(host, port, simulator)
        except OSError as os_err:
            _module_logger.error(str(os_err))
            _module_logger.warning("Retry %d of %d", n_try, n_max)
            time.sleep(15)
            continue
        break
    if server is not None:
        _module_logger.info("Start server on %s:%d", host, port)
        with server:
            server.serve_forever()
    _module_logger.warning("Exit")
    return


if __name__ == "__main__":
    print("*** Simulator for oscilloscope ***")
    try:
        main()
    except KeyboardInterrupt:
        _module_logger.error("Interrupted")
