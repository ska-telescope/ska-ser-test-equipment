"""
Fetch Waveform Data for MSO/DPO 5K 7K & 70K Series Scopes.

This example demonstrates how to fetch waveform data from MSO/DSA/DPO
5K, 7K & 70K Series Scopes, convert the raw data into floating point values and
then save it to disk in a binary format.
"""
import base64
import io
import logging
import time
from datetime import datetime
from typing import Any, Tuple

import matplotlib  # type: ignore[import]
import matplotlib.pyplot  # type: ignore[import]
import numpy as np  # https://numpy.org/

from ska_ser_test_equipment.base.eng_number import eng_number
from ska_ser_test_equipment.scpi.virtual_instrument import VirtualInstrument

SCOPE_ZA = "za-itf-oscilloscope.ad.skatelescope.org"


# pylint: disable-next=too-many-public-methods
class TektronixMso(VirtualInstrument):
    """Connect to oscilloscope and do things."""

    visa_hw: Any = None

    # pylint: disable-next=too-many-arguments
    def __init__(
        self,
        logger: logging.Logger,
        ip_address: str,
        port: int | None,
        sentinel_string: str,
        timeout: float,
        use_telnet: bool,
    ):
        """
        Instantiate this class.

        :param logger: logger instance
        :param ip_address: IP address or host name
        :param port: port number for telnet (zero means VISA)
        :param sentinel_string: added to end of command in TCP mode
        :param timeout: timeout in seconds
        :param use_telnet: use telnet protocol
        :raises Exception: could not connect to instrument
        """
        self.logger = logger
        self.ip_address = ip_address
        self.port = port  # type: ignore[assignment]
        self.sentinel_string = sentinel_string
        self.timeout = timeout
        self.use_telnet = use_telnet
        if self.use_telnet:
            if not self.port:
                raise Exception("Port for telnet not specified")
            self.visa_hw = f"{self.ip_address}:{self.port}"  # type: ignore[assignment]
            self.logger.info("Telnet to %s:%d", self.ip_address, self.port)
        elif self.port:
            self.visa_hw = f"{self.ip_address}:{self.port}"  # type: ignore[assignment]
            self.logger.info("Connect to socket %s:%d", self.ip_address, self.port)
        else:
            try:
                super().__init__(
                    logger, ip_address, port, sentinel_string, timeout, use_telnet
                )
            except Exception as visa_err:
                raise visa_err

    def info(self) -> None:
        """Display information about hardware."""
        # maximum real-time sample rate
        self.print_query("options", "*OPT?")
        self.print_query("verbose", "VERBOSE?")
        self.print_query("sample rate", "ACQUIRE:MAXSAMPLERATE?")
        # Total number of hours the oscilloscope has been turned on [990]
        self.print_query("total uptime", "TOTALUPTIME?", "hours")
        # enable the socket server [985]
        self.print_query("socket server enable", "SOCKETSERVER:ENABLE?")
        self.print_query("socket server port", "SOCKETSERVER:PORT?")
        self.print_query("socket server protocol", "SOCKETSERVER:PROTOCOL?")
        # query date and time that instrument displays [988]
        self.print_query("date", "DATE?")
        self.print_query("time", "TIME?")

    def clear_all(self) -> None:
        """Clear acquisitions, measurements and waveforms."""
        self.write_command("clear acquisitions", "CLEAR")

    def learn(self) -> None:
        """Read the commands that list the instrument settings."""
        lrn_cmds = self.read_query_str("learn", "*LRN?")
        now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        print(f"/* Learned commands at {now} */")
        for lrn_cmd in lrn_cmds.split(";"):  # type: ignore[union-attr]
            print(lrn_cmd)

    def busy(self) -> bool:
        """
        Synchronize the operation of the instrument with this application.

        :returns: the status of the instrument
        """
        working = self.read_query_bool("busy", "BUSY?")
        if working:
            print(f"{'status':40} : not busy processing command")
            return True
        print(f"{'status':40} : busy processing command")
        return False

    def acquisition_mode(self, value: str | None = None) -> str | None:
        """
        Get acquisition mode details.

        :param value: new acquisition mode
        :returns: current acquisition mode
        """
        acq_mode = self.read_query_str("Mode", "ACQUIRE:MODE?")
        if self.visa_hw is None:
            return None
        acq_mode = acq_mode.upper()  # type: ignore[union-attr]
        # SAMPLE: first sampled value taken during acquisition interval
        if acq_mode[0:3] == "SAM":
            print(
                f"mode {acq_mode:35} : sampled value taken during acquisition interval"
            )
            rval = "SAMPLE"
        # PEAKDETECT: high-low range of samples taken from single waveform acquisition
        elif acq_mode[0:4] == "PEAK":
            print(
                f"Mode {acq_mode:35} :"
                " high-low range of samples taken from single waveform acquisition"
            )
            rval = "PEAK"
        # AVERAGE: average of SAMple data points from separate waveform acquisitions
        elif acq_mode[0:3] == "AVERAGE":
            print(
                f"Mode {acq_mode:35} :"
                " average of data points from separate waveform acquisitions"
            )
            rval = "AVE"
        # ENVELOPE: range of data points from several separate waveform acquisitions
        elif acq_mode[0:3] == "ENV":
            print(
                f"Mode {acq_mode:35} :"
                " peak data points from separate waveform acquisitions"
            )
            rval = "ENV"
        else:
            self.logger.error("Unknown acquisitions mode %s", acq_mode)
            print(f"mode {acq_mode:35}: unknown")
            rval = acq_mode.upper()
        if value is None:
            return rval
        if value not in ("SAMPLE", "PEAK", "AVERAGE", "ENVELOPE"):
            self.logger.error("Unknown acquisitions mode %s", value)
            return None
        self.write_command("acquisition mode", f"ACQUIRE:MODE {value}")
        acq_mode = self.read_query_str(  # type: ignore[union-attr]
            "Mode", "ACQUIRE:MODE?"
        ).upper()
        return acq_mode

    def check_fast_mode(self) -> None:
        """Check fast acquisition mode."""
        # fast acquisition mode [124]
        self.print_query("fast mode", "ACQUIRE:FASTACQ:STATE?")
        # number of FastFrame frames which have been acquired
        self.print_query("fast frames", "ACQUIRE:NUMFRAMESACQUIRED?")

    def single_mode(self) -> None:
        """Check status of single acquisition mode."""
        # In single acquisition, stop count is based on number of acquisitions [127]
        self.print_query("single mode", "ACQUIRE:SEQUENCE:MODE?")
        # In single acquisition mode, specify number of acquisitions in sequence [128]
        # default is 1.
        self.print_query("sequences", "ACQUIRE:SEQUENCE:NUMSEQUENCE?")

    def acquisition_state(
        self, new_state: str | None = None, stop_after: int | None = None
    ) -> tuple[str | None, int | None]:
        """
        Start or stop acquisitions [128].

        values are <NR1>|OFF|ON|RUN|STOP
        set <NR1> = 0 to stop acquisitions; any other value starts acquisitions

        :param new_state: new state
        :param stop_after: stop after
        :returns: altered state
        """
        acq_state = self.read_query_str("state", "ACQUIRE:STATE?")
        if acq_state == "0":
            print(f"{'state':40} : stop")
        elif acq_state == "1":
            print(f"{'state':40} : run")
        else:
            print(f"{'state':40} : {acq_state}")
        # Control whether the instrument acquires single sequence or continually [129]
        # RUNSTop|SEQUENCE
        # Pressing SINGLE on the front panel button is equivalent to sending:
        # ACQUIRE:STOPAFTER SEQUENCE
        # ACQUIRE:STATE 1.
        self.print_query("stop after", "ACQUIRE:STOPAFTER?")
        if new_state is None:
            return acq_state, None
        self.logger.info("Set acquisition state to %s", new_state)
        if new_state not in ("0", "1", "OFF", "ON", "RUN", "STOP"):
            self.logger.error("Unknown acquisition state %s", new_state)
            return None, None
        self.write_command("state", f"ACQUIRE:STATE {new_state}")
        acq_state = self.read_query_str("state", "ACQUIRE:STATE?")
        if stop_after:
            self.write_command("stop after", f"ACQUIRE:STOPAFTER {stop_after}")
            stop_val = self.read_query_int("stop after", "ACQUIRE:STOPAFTER?")
        else:
            stop_val = None
        return acq_state, stop_val

    def acquisition_check(self) -> None:
        """
        Query current acquisition state.

        When state is set to ON or RUN, a new acquisition will be started. If the last
        acquisition was a single acquisition sequence, a new single sequence acquisition
        will be started. If the last acquisition was continuous, a new continuous
        acquisition will be started.
        """
        self.acquisition_mode()
        self.check_fast_mode()
        self.single_mode()
        self.acquisition_state()
        # selected acquisition mode of instrument [124]
        # SAMPLE|PEAKDETECT|HIRES|AVERAGE|ENVELOP
        # number of acquisitions in the sequence completed [127]
        self.print_query("acquisitions_current", "ACQUIRE:SEQUENCE:CURRENT?")
        # number of waveform acquisitions since acquisitions were stopped [126]
        self.print_query("acquisitions_count", "ACQUIRE:NUMACQ?")
        # number of waveform acquisitions that make up averaged waveform [126]
        # Ranges from 2 to 10240
        self.print_query("averages_count", "ACQUIRE:NUMAVG?")

    def data_encoding(self) -> None:
        """
        Query the format of outgoing waveform data.

        ASCIi specifies the ASCII representation of signed INT, FLOAT. If ASCII is
        the value, then :BN_Fmt and :BYT_Or are ignored.

        RIBinary specifies signed integer data point representation with the most
        significant byte transferred first.

        When :BYT_Nr is 1, the range is from -128 through 127. When :BYT_Nr
        is 2, the range is from -32,768 through 32,767. When :BYT_Nr is 8, then
        the waveform being queried is set to Fast Acquisition mode. Center screen
        is 0 (zero). The upper limit is the top of the screen and the lower limit is the
        bottom of the screen. This is the default argument.

        RPBinary specifies the positive integer data-point representation, with the
        most significant byte transferred first.

        When :BYT_Nr is 1, the range from 0 through 255. When :BYT_Nr is 2, the
        range is from 0 to 65,535. When :BYT_Nr is 8, then the waveform being
        queried is set to Fast Acquisition mode. The center of the screen is 127.
        The upper limit is the top of the screen and the lower limit is the bottom of
        the screen.

        FPBinary specifies the floating point (width = 4) data.
        The range is from –3.4 × 1038 to 3.4 × 1038. The center of the screen is 0.
        The upper limit is the top of the screen and the lower limit is the bottom of
        the screen.

        The FPBinary argument is only applicable to math waveforms or ref
        waveforms saved from math waveforms.

        SRIbinary is the same as RIBinary except that the byte order is swapped,
        meaning that the least significant byte is transferred first. This format is
        useful when transferring data to IBM compatible PCs.

        SRPbinary is the same as RPBinary except that the byte order is swapped,
        meaning that the least significant byte is transferred first. This format is
        useful when transferring data to PCs.

        SFPbinary specifies floating point data in IBM PC format. The SFPbinary
        argument only works on math waveforms or ref waveforms saved from math
        waveforms.
        """
        self.print_query("Encoding", "DATA:ENCDG?")

    def autoscale(self, value: bool | None = None) -> Tuple[int | None, int | None]:
        """
        Automatically set vertical, horizontal, and trigger controls.

        :param value: new value
        :returns: horizontal and vertical flags
        """
        autoset_h = self.read_query_bool(
            "horizontal autoset", "AUTOSET:HORIZONTAL:ENABLE?"
        )
        autoset_v = self.read_query_bool("vertical autoset", "AUTOSET:VERTICAL:ENABLE?")
        if not value or value is None:
            return autoset_h, autoset_v
        if value and autoset_h and autoset_v:
            self.visa_hw.write("AUTOSET EXECUTE")
            print(f"Autoset     : {self.visa_hw.query('AUTOSET:ENABLE?').strip()}")
        elif value:
            self.logger.error("Autoset is not enabled")
            return autoset_h, autoset_v
        else:
            pass
        return autoset_h, autoset_v

    def scope_data(self) -> int | None:
        """
        Query how many points are actually available.

        :returns: points available
        """
        self.print_query("data_source_available", "DATA:SOURCE:AVAILABLE?")
        self.print_query("data_source", "DATA:SOURCE?")
        rec_len: int | None = self.read_query_int("points available", "WFMOUTPRE:NR_P?")
        return rec_len

    def horizontal_scale(self, value: float | None = None) -> float | None:
        """
        Query horizontal scale for specified analog channel.

        :param value: vertical scale in Volt per division
        :returns: Volt per division
        """
        div = self.read_query_float("horizontal_scale", "HORIZONTAL:ACQDURATION?")
        if div is None:
            self.logger.error("Could not read horizontal scale")
            return None
        div_str: str = eng_number(div, "s/div")
        print(f"Vertical scale   : {div_str}/div")
        if value is None:
            return div
        div = self.write_value_float(
            "horizontal_scale", "HORIZONTAL:ACQDURATION", value, "s/div"
        )
        return div

    def vertical_scale(self, ch_num: int, value: float | None = None) -> float | None:
        """
        Query vertical scale for specified analog channel.

        :param ch_num: channel number
        :param value: vertical scale in Volt per division
        :returns: Volt per division
        """
        div: float | None = self.read_query_float(
            "vertical_scale", f"CH{ch_num}:SCALE?"
        )
        if div is None:
            self.logger.error("Could not read vertical scale for channel %d", ch_num)
            return None
        div_str: str = eng_number(div, "V")
        print(f"Vertical scale   : {div_str}/div")
        if value is None:
            return div
        div = self.write_value_float(
            "vertical scale", f"CH{ch_num}:SCALE", value, "V/div"
        )
        return div

    def check_scope_quick(self, ch_num: int) -> None:
        """
        Connect to oscilloscope and read the most important settings.

        :param ch_num: channel number
        """
        # Query the horizontal scale in time per division.
        tdiv: float | None = self.read_query_float(
            "horizontal scale", "HORIZONTAL:SCALE?"
        )
        tdiv_str: str | None = eng_number(tdiv, "s")
        print(f"Horizontal scale : {tdiv_str} per division")
        # self.print_query(
        #     "horizontal scale", "HORIZONTAL:SCALE?", "seconds per division"
        # )
        # Query vertical scale for specified analog channel
        self.vertical_scale(ch_num)

    def check_horizontal(self) -> None:
        """Connect to oscilloscope and read horizontal settings."""
        # number of graticule divisions [404]
        self.print_query("horizontal_divisions", "HORIZONTAL:DIVISIONS?")
        # Get time base duration.
        self.print_query("horizontal_duration", "HORIZONTAL:ACQDURATION?", "seconds")
        # Query horizontal delay mode [403]
        self.print_query("horizontal_delay_mode", "HORIZONTAL:DELAY:MODE?")
        # Query horizontal delay time (position) that is used when delay is on.
        self.print_query("horizontal_delay_time", "HORIZONTAL:DELAY:TIME?", "seconds")
        # Returns number of graticule divisions over which waveform is displayed.
        self.print_query("horizontal_divisions", "HORIZONTAL:DIVISIONS?")
        # Query horizontal mode [412]
        self.print_query("horizontal_mode", "HORIZONTAL:MODE?")
        # Query waveform horizontal position, in percent, that is used when delay
        # is off, as a percent of screen width.
        self.print_query("horizontal_position", "HORIZONTAL:POSITION?", "%")
        # Query horizontal sample rate [418]
        self.print_query(
            "horizontal_sample_rate", "HORIZONTAL:SAMPLERATE?", "samples per second"
        )
        # Query horizontal zoom factor of specified zoom in specified waveview [369]
        self.print_query("horizontal_scale", "HORIZONTAL:SCALE?")

    def check_vertical(self, ch_num: int) -> None:
        """
        Connect to oscilloscope and read vertical settings.

        :param ch_num: channel number
        """
        # Get channel settings
        self.logger.info("Get channel %d vertical settings", ch_num)
        # self.print_query(f"ch{ch_num} settings", f"CH{ch_num}?")
        # self.print_query(f"ch{ch_num} probe", f"CH{ch_num}:PROBE?")
        # self.print_query(f"ch{ch_num} vertical parameters", f"CH{ch_num}?")
        # Selected status
        self.print_query(f"ch{ch_num} select", f"SELECT:CH{ch_num}?")
        # Vertical position
        self.print_query(
            f"ch{ch_num}_vertical_position",
            f"DISPLAY:WAVEVIEW1:CH{ch_num}:VERTICAL:POSITION?",
        )
        # Sets or queries the bandwidth of the specified channel.
        self.print_query(f"ch{ch_num}_bandwidth", f"CH{ch_num}:BANDWIDTH?", "Hz")
        # Queries whether specified channel’s input signal is clipping (exceeding)
        # channel vertical scale setting [238]
        self.print_query(f"ch{ch_num}_clipping", f"CH{ch_num}:CLIPPING?")
        # Sets or queries the coupling setting for the specified channel.
        self.print_query(f"ch{ch_num}_coupling", f"CH{ch_num}:COUPLING?")
        # sets or queries the vertical offset for the specified analog channel.
        self.print_query(f"ch{ch_num}_offset", f"CH{ch_num}:OFFSET?")
        # sets or queries the vertical position for the specified analog channel.
        self.print_query(f"ch{ch_num}_position", f"CH{ch_num}:POSITION?")
        # query-only command returns gain factor of probe attached to specified channel
        self.print_query(f"ch{ch_num}_probe_gain", f"CH{ch_num}:PROBE:GAIN?")
        # units of measure for probe attached to specified channel
        self.print_query(f"ch{ch_num}_probe_units", f"CH{ch_num}:PROBE:UNITS?")
        # sets or returns the vertical scale for the specified analog channel
        self.print_query(
            f"ch{ch_num}_vertical_scale", f"CH{ch_num}:SCALE?", "Volt per division"
        )
        self.print_query(
            f"ch{ch_num}_display_vertical_scale",
            f"DISPLAY:WAVEVIEW1:CH{ch_num}:VERTICAL:SCALE?",
            "Volt per division",
        )
        # sets or returns scale ratio for the specified analog channel.
        self.print_query(f"ch{ch_num}_vertical_scale_ratio", f"CH{ch_num}:SCALERATIO?")

    # pylint: disable-next=too-many-branches
    def check_trigger(self, ch_num: int = 0) -> None:  # noqa: C901
        """
        Read current trigger parameters for instrument.

        :param ch_num: channel number
        """
        if self.logger.isEnabledFor(logging.INFO):
            self.print_query("trigger", "TRIGGER?")
        # current state of the triggering system
        # ARMED indicates that the instrument is acquiring pretrigger information.
        # AUTO indicates that the instrument is in the automatic mode and acquires data
        # even in the absence of a trigger.
        # READY indicates that all pretrigger information is acquired and that the
        # instrument is ready to accept a trigger.
        # SAVE indicates that the instrument is in save mode and is not acquiring data.
        # TRIGGER indicates that the instrument triggered and is acquiring the post
        # trigger information.
        t_state = self.read_query_str("trigger state", "TRIGGER:STATE?")
        print(f"trigger state {t_state:26} : ", end="")
        if t_state == "ARMED":
            print("acquiring pretrigger information")
        elif t_state == "AUTO":
            print("automatic mode")
        elif t_state == "READY":
            print("pretrigger information acquired")
        elif t_state == "SAVE":
            print("not acquiring data")
        elif t_state == "TRIGGER":
            print("acquiring post trigger information")
        else:
            print(f"unknown {t_state}")
        # queries the source for an edge trigger [1106]
        for tid in ("A", "B"):
            self.print_query(
                f"trigger_{tid}_edge_source", f"TRIGGER:{tid}:EDGE:SOURCE?"
            )
        # queries the source for a window trigger [1129]
        for tid in ("A", "B"):
            self.print_query(
                f"trigger {tid} window source", f"TRIGGER:{tid}:WINDOW:SOURCE?"
            )
        # queries the type of holdoff for the A trigger [1131]
        # user-specified time (TIMe) or internally calculated random time value (RANDom)
        self.print_query("trigger_a_holdoff", "TRIGGER:A:HOLDOFF:BY?")
        # queries the A trigger mode [1133]
        self.print_query("trigger_a_mode", "TRIGGER:A:MODE?")
        # If B trigger state is on, it is part of triggering sequence.
        self.print_query("trigger_b_state", "TRIGGER:B:STATE?")
        # whether the B trigger occurs after a specified number of events or a specified
        # period of time after the A trigger [1134]
        self.print_query("trigger_b_by", "TRIGGER:B:BY?")
        # number of events that must occur before the B trigger [1134]
        self.print_query("trigger_b_events", "TRIGGER:B:EVENTS:COUNT?")
        # type of A or B trigger [1126]
        for tid in ("a", "b"):
            self.print_query(f"trigger_{tid}_type", f"TRIGGER:{tid}:TYPE?")
        # queries the type of coupling for the edge trigger [1104]
        for tid in ("a", "b"):
            self.print_query(
                f"trigger_{tid}_edge_coupling", f"TRIGGER:{tid}:EDGE:COUPLING?"
            )
        # queries the slope for the edge trigger [1105]
        for tid in ("a", "n"):
            self.print_query(f"trigger_{tid}_edge_slope", f"TRIGGER:{tid}:EDGE:SLOPE?")
        # queries the CH<x> trigger level for an Edge, Pulse Width, Runt or Rise/Fall
        # (Transition and Slew Rate) trigger [1106]
        if ch_num:
            for tid in ("a", "b"):
                self.print_query(
                    f"ch{ch_num}_trigger_{tid}_level",
                    f"TRIGGER:{tid}:LEVEL:CH{ch_num}?",
                )

    def check_measurement(self) -> None:
        """Display measurement stuff."""
        self.print_query("display_select_view", "DISPLAY:SELECT:VIEW?")
        self.print_query("display_select_source", "DISPLAY:SELECT:SOURCE?")
        self.print_query("display_spectrum_source", "DISPLAY:SELECT:SPECVIEW1:SOURCE?")
        self.print_query("display_wave_source", "DISPLAY:SELECT:WAVEVIEW1:SOURCE?")
        for m_num in range(1, 8):
            # display of statistics in measurement badges [513]
            self.print_query(
                f"measurement{m_num}_display_enable",
                f"MEASUREMENT:MEAS{m_num}:DISPLAYSTAT:ENABLE?",
            )
            # self.print_query(
            #     f"measurement {m_num} label", f"MEASUREMENT:MEAS{m_num}:LABEL?"
            # )
            self.print_query(
                f"measurement {m_num} source", f"MEASUREMENT:MEAS{m_num}:SOURCE?"
            )

    # pylint: disable-next=unused-argument
    def read_channel(self, ch_num: int, max_points: int = 1000) -> None:
        """
        Connect to oscilloscope and read data.

        :param ch_num: channel number
        :param max_points: count of ppoints to read
        """
        self.write_command("data_source", ":DATA:SOURCE CH{ch_num}")
        self.write_command("data_start", "DATA:START 1")
        self.write_command("data_end", f"DATA:STOP {max_points}")
        self.write_command("data_encoding", "WFMOUTPRE:ENCDG ASCII")
        self.write_command("byte", "WFMOUTPRE:BYT_Nr 4")
        self.write_command("header", "HEADER 1")
        self.print_query("current settings", "WFMOUTPRE?")
        self.print_query("get data", "CURVE?")

    def read_data(self) -> Any:
        """
        Read raw data.

        Data is sent back with IEEE defined header. i.e. #41000<binary data bytes><CR>
        PyVISA read_binary_values() automatically reads and parses IEEE block header

        :returns: data buffer
        """
        if self.port:
            rx_val = self.read_query_str("trace_data", "CURVE?")
            if rx_val is None:
                return ""
            if rx_val[0] != "#":
                self.logger.error("Invalid data '%s'", rx_val)
                return ""
            fld_len = int(rx_val[1])
            raw_data = []
            for val in rx_val[2 + fld_len :].split(","):
                raw_data.append(float(val))
            self.logger.info("Received data '%s'", raw_data)
        else:
            # Fetch waveform data
            self.logger.info("Fetch waveform data")
            self.write_command("fetch data", "CURVE?")
            try:
                raw_data = self.visa_hw.read_binary_values(
                    datatype="b",
                    is_big_endian=False,
                    container=np.ndarray,
                    header_fmt="ieee",
                    expect_termination=True,
                )
            except KeyboardInterrupt:
                self.logger.warning("Interrupted")
                # self.visa_hw.close()
                # rm.close()
                return None
        return raw_data

    def get_horizontal_scaling(self) -> Tuple[float | None, float | None, float | None]:
        """
        Fetch horizontal scaling factors.

        :returns: tuple with scaling factors
        """
        if self.visa_hw is None:
            return None, None, None
        self.logger.debug("Read horizontal scaling factors")
        # Horizontal point spacing [1163]
        xinc = self.read_query_float("x_increment", "WFMOUTPRE:XINCR?")
        # Sub-sample time between trigger sample (designated by PT_OFF) and occurrence
        # of actual trigger [1164]
        xzero = self.read_query_float("X zero", "WFMOUTPRE:XZERO?")
        # Get trigger point relative to data start for waveform specified [1161]
        pt_off = self.read_query_float("trigger_point", "WFMOUTPRE:PT_OFF?")
        if pt_off is None:
            self.logger.warning("Could not read trigger point")
            pt_off = 0
        self.logger.info(
            "Horizontal scaling factors : %.3E %.3E %.3E", xinc, xzero, pt_off
        )
        return xinc, xzero, pt_off

    def get_vertical_scaling(self) -> Tuple[float | None, float | None, float | None]:
        """
        Fetch vertical scaling factors.

        :returns: tuple with scaling factors
        """
        if self.visa_hw is None:
            return None, None, None
        self.logger.debug("Read vertical scaling factors")
        ymult = self.read_query_float("vertical_scale", "WFMOUTPRE:YMULT?")
        yzero = self.read_query_float("vertical_position", "WFMOUTPRE:YZERO?")
        yoff = self.read_query_float("vertical_offset", "WFMOUTPRE:YOFF?")
        self.logger.info(
            "Vertical scaling factors : %.3E %.3E %.3E", ymult, yzero, yoff
        )
        return ymult, yzero, yoff

    # pylint: disable-next=too-many-arguments
    def get_arrays(
        self,
        pt_off: float,
        xinc: float,
        xzero: float,
        yoff: float,
        ymult: float,
        yzero: float,
        raw_data: Any,
        data_len: int,
    ) -> Tuple[np.ndarray[Any, Any], np.ndarray[Any, Any]] | tuple[None, None]:
        """
        Create numpy arrays of floating point values for the X and Y axis.

        :param pt_off: horizontal scaling
        :param xinc: horizontal scaling
        :param xzero: horizontal scaling
        :param yoff: vertical scaling
        :param ymult: vertical scaling
        :param yzero: vertical scaling
        :param raw_data: data string
        :param data_len: data length
        :returns: tuple with arrays of X and Y values
        """
        self.logger.info("Create numpy arrays for %d values", data_len)
        t_zero = ((-1 * pt_off) * xinc) + xzero
        # xvalues = np.ndarray(data_len, np.float)
        # yvalues = np.ndarray(data_len, np.float)
        xvalues: np.ndarray[Any, Any] = np.ndarray(data_len, float)
        yvalues: np.ndarray[Any, Any] = np.ndarray(data_len, float)
        for i in range(0, data_len):
            # Create timestamp for the data point
            xvalues[i] = t_zero + xinc * i
            # Convert raw ADC value into a floating point value
            yvalues[i] = float(raw_data[i] - yoff) * ymult + yzero
            self.logger.debug(f"{xvalues[i]} : {yvalues[i]}")

        # TODO might be useful
        # Save data to disk in binary format.
        # Can be read back from file using np.load()
        # self.logger.info("Save data to disk")
        # xvalues.dump(r"./xvalues_numpy_array.dat")
        # yvalues.dump(r"./yvalues_numpy_array.dat")
        return xvalues, yvalues

    # pylint: disable-next=too-many-locals,too-many-statements,useless-suppression
    def read_scope_data(
        self,
        ch_num: int,
        start_point: int,
        end_point: int,
    ) -> tuple[np.ndarray[Any, Any] | None, np.ndarray[Any, Any] | None]:
        """
        Connect to oscilloscope and read trace data.

        :param ch_num: channel number
        :param start_point: start reading at this point
        :param end_point: stop reading at this point
        :returns: X and Y values
        """
        self.clear_all()
        # set instrument to either include or omit headers on query responses [401]
        self.write_command("header", "HEADER 0")

        # self.set_autoscale(True)

        self.logger.info("Set data source to CH%d", ch_num)
        d_source = self.read_query_str("data source", "DATA:SOURCE?")
        if d_source != f"CH{ch_num}":
            self.write_command("Data source", f"DATA:SOURCE CH{ch_num}")
            self.wait()
            time.sleep(5)
            d_source = self.read_query_str("data source", "DATA:SOURCE?")
            if d_source != f"CH{ch_num}":
                self.logger.error("Can not read source %s", d_source)
                return None, None
        # Signed Binary Format, LSB order
        self.write_command("Format", "DATA:ENCDG SRI")
        self.write_command("Width", "DATA:WIDTH 1")

        self.write_command("data start", f"DATA:START {start_point}")
        # TODO should this stay or go
        # Set data stop to max
        # self.write_command("Data stop to max", "DAT:STOP 1e10")
        # self.visa_hw.write("DAT:STOP 1000")
        # Query how many points are actually available
        # rec_len = self.read_query_int("points available", "WFMOUTPRE:NR_P?")
        # if rec_len is None:
        #     rec_len = start_point - end_point + 1
        #     self.logger.warning("Set record length to %d", rec_len)
        #     # return None, None
        #
        # # print(f"Data points : {rec_len}")
        # if rec_len:
        #     if rec_len > end_point:
        #         rec_len = end_point

        rec_len = end_point - start_point + 1
        self.logger.info(
            "Read records from %d to %d (length %d)", start_point, end_point, rec_len
        )

        # Set data stop to match points available
        self.write_command("data stop", f"DATA:STOP {end_point}")

        # TODO should this stay or go
        # self.logger.info("Set acquisition state to RUN")
        # self.acquisition_state("RUN", 1)
        # self.wait()

        # Fetch horizontal scaling factors
        xinc, xzero, pt_off = self.get_horizontal_scaling()
        if xinc is None or xzero is None or pt_off is None:
            self.logger.error("Could not read horizontal scaling factors")
            return None, None

        self.write_command("encoding", "WFMOUTPRE:ENCDG BINARY")
        self.write_command("byte 1", "WFMOUTPRE:BYT_NR 1")

        # Fetch vertical scaling factors
        ymult, yzero, yoff = self.get_vertical_scaling()
        if ymult is None or yzero is None or yoff is None:
            self.logger.error("Could not read vertical scaling factors")
            return None, None

        # Fetch waveform data
        raw_data: Any = self.read_data()
        data_len: int = len(raw_data)

        # TODO should this stay or go
        # self.acquisition_state("STOP")

        # Create numpy arrays of floating point values for the X and Y axis
        xvalues, yvalues = self.get_arrays(
            pt_off, xinc, xzero, yoff, ymult, yzero, raw_data, data_len
        )

        return xvalues, yvalues

    # pylint: disable-next=too-many-return-statements
    def read_scope_raw_data(  # noqa: C901
        self,
        ch_num: int,
        start_point: int,
        end_point: int,
    ) -> Tuple[np.ndarray[Any, Any], np.ndarray[Any, Any]] | tuple[None, None]:
        """
        Connect to oscilloscope and read trace data.

        :param ch_num: channel number
        :param start_point: data start point
        :param end_point: data end point
        :returns: X and Y values
        """
        self.write_command("data source", f"DATA:SOURCE CH{ch_num}")
        self.print_query("data source", "DATA:SOURCE?")

        self.write_command("encoding", "WFMOUTPRE:ENCDG BINARY")
        self.write_command("byte 1", "WFMOUTPRE:BYT_NR 1")

        self.write_command("data start", f"DAT:START {start_point}")
        # Set data stop to maximum
        self.write_command("data stop to max", "DAT:STOP 1e10")
        # Query how many points are actually available
        # rec_len = self.read_query_int("points available", "WFMOUTPRE:NR_P?")
        rec_len = self.read_query_int("points available", "WFMOUTPRE:NR_PT?")
        if rec_len is None:
            self.logger.error("Could not read available points")
            return None, None

        # Set data stop to match points available
        self.write_command("points available", f"DAT:STOP {rec_len}")
        # Fetch waveform data
        self.logger.info("Fetch waveform data (%d points)", rec_len)
        self.write_command("fetch data", "CURVE?")

        if self.visa_hw is None:
            return None, None
        if rec_len is None:
            return None, None

        if rec_len < 10:
            self.logger.error("Not enough data to plot")
            return None, None

        if start_point + rec_len > end_point:
            end_point = start_point + rec_len

        self.logger.info("Read data from %d to %d", start_point, end_point)
        if start_point > end_point:
            self.logger.error(
                "Start point %d is larger than start point %d", start_point, end_point
            )
            return None, None

        # Data is sent back with IEEE defined header, e.g. #41000<binary data bytes>\n
        # PyVISA read_binary_values() automatically reads and parses IEEE block header
        try:
            raw_data = self.visa_hw.read_binary_values(
                datatype="b",
                is_big_endian=False,
                container=np.ndarray,
                header_fmt="ieee",
                expect_termination=True,
            )
        except ValueError:
            self.logger.error("Could not read data")
            return None, None
        except KeyboardInterrupt:
            self.logger.warning("Interrupted")
            return None, None

        data_len = len(raw_data)
        xvalues: np.ndarray[Any, Any] = np.ndarray(data_len, float)
        yvalues: np.ndarray[Any, Any] = np.ndarray(data_len, float)
        # pylint: disable-next=consider-using-f-string
        self.logger.info("Read %d items" % data_len)
        for i in range(0, data_len):
            # Convert raw ADC value into a floating point value
            xvalues[i] = float(i)
            yvalues[i] = float(raw_data[i])
            if yvalues[i] != 0.0:
                # pylint: disable-next=consider-using-f-string
                self.logger.debug("\t%5d : %f" % (xvalues[i], yvalues[i]))

        return xvalues, yvalues

    # pylint: disable-next=too-many-arguments,too-many-locals
    def trace_svg(
        self,
        ch_num: int,
        start_point: int = 1,
        end_point: int = 4000,
        show: bool = False,
        hdr: bool = False,
    ) -> bytes | str:
        """
        Plot trace data.

        :param ch_num: channel number
        :param start_point: start here
        :param end_point: stop here
        :param show: display graph and return
        :param hdr: display everything when set or return as base64 for web use
        :returns: SVG string
        """
        x_points, y_points = self.read_scope_data(ch_num, start_point, end_point)
        if x_points is None or y_points is None:
            self.logger.error("No data to plot")
            return ""

        if not show:
            matplotlib.use("agg")
        fig, _ax = matplotlib.pyplot.subplots()

        matplotlib.pyplot.plot(x_points, y_points)

        xlabel = "Time (s)"
        ylabel = "Amplitude (V)"

        t_start = x_points[0]
        t_stop = x_points[-1]

        y_min = min(y_points)
        y_max = max(y_points)

        self.logger.info("Oscilloscope time from %.2E to %.2E", t_start, t_stop)
        self.logger.info("Oscilloscope values from %.2E to %.2E", y_min, y_max)
        matplotlib.pyplot.title(f"Oscilloscope channel {ch_num}")
        matplotlib.pyplot.axis(
            (t_start, t_stop, y_min, y_max)
        )  # type: ignore[arg-type]
        matplotlib.pyplot.ylabel(f"{ylabel} ")
        matplotlib.pyplot.xlabel(f"{xlabel} ")
        matplotlib.pyplot.grid(True)
        matplotlib.pyplot.autoscale(enable=True, axis="y")
        if show:
            matplotlib.pyplot.show()
            return "BOO!"

        imgdata = io.StringIO()
        fig.savefig(imgdata, format="svg")
        # rewind the data
        imgdata.seek(0)

        svg_dta = imgdata.read()
        if not hdr:
            d_start = svg_dta.find("<svg ")
            return base64.b64encode(bytes(svg_dta[d_start:], "utf-8"))
        return svg_dta
