# -*- coding: utf-8 -*-
#
# (c) 2022 CSIRO.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""This module provides for monitoring and control of a oscilloscope."""
from __future__ import annotations

import logging
import threading
from typing import Any, Callable, Dict, Final, Optional, Tuple

from ska_control_model import PowerState, TaskStatus
from ska_ser_scpi import (
    AttributeClient,
    AttributeRequest,
    AttributeResponse,
    InterfaceDefinitionType,
    ScpiBytesClientFactory,
    ScpiClient,
    SupportedProtocol,
)
from ska_tango_base.poller import PollingComponentManager

from ska_ser_test_equipment.mixins import SCPISendReceiveMixin

__NO_ARG__ = object()

_module_logger = logging.getLogger(__name__)


# pylint: disable-next=too-many-instance-attributes
class OscilloscopeComponentManager(
    PollingComponentManager[AttributeRequest, AttributeResponse], SCPISendReceiveMixin
):
    """A component manager for a oscilloscope."""

    def __init__(  # pylint: disable=too-many-arguments
        self,
        interface_definition: InterfaceDefinitionType,
        protocol: SupportedProtocol,
        host: str,
        port: int,
        logger: logging.Logger,
        communication_state_callback: Callable,
        component_state_callback: Callable,
        update_rate: float = 5.0,
    ) -> None:
        """
        Initialise a new instance.

        :param interface_definition: definition of the oscilloscope's SCPI interface.
        :param protocol: the network protocol to be used to communicate
            with the oscilloscope.
        :param host: the host name or IP address of the oscilloscope.
        :param port: the port of the oscilloscope.
        :param logger: a logger for this component manager to use for
            logging.
        :param communication_state_callback: callback to be called when
            the status of communications between the component manager
            and its component changes.
        :param component_state_callback: callback to be called when the
            state of the component changes.
        :param update_rate: how often updates to attribute values should
            be provided. This is not necessarily the same as the rate at
            which the instrument is polled. For example, the instrument
            may be polled every 0.1 seconds, thus ensuring that any
            invoked commands or writes will be executed promptly.
            However, if the `update_rate` is 5.0, then routine reads of
            instrument values will only occur every 50th poll (i.e.
            every 5 seconds).
        """
        self.logger = logger or _module_logger
        bytes_client = ScpiBytesClientFactory().create_client(
            protocol,
            host,
            port,
            interface_definition["timeout"],
            interface_definition["sentinel_string"],
            logger=logger or _module_logger,
        )
        scpi_client = ScpiClient(
            bytes_client, chain=interface_definition["supports_chains"]
        )
        self._attribute_client = AttributeClient(
            scpi_client, interface_definition["attributes"]
        )

        self._model = interface_definition["model"]
        self.logger.info("Oscilloscope model set to %s", self._model)
        self._max_tick: Final = int(update_rate / interface_definition["poll_rate"])

        # We'll count ticks upwards, but start at the maximum so that
        # our initial update request occurs as soon as possible.
        self._tick = self._max_tick
        self._identified = False
        self._marked = False

        # for tracking fault status
        self._device_error: bool = False
        self._autoattenuation: bool = False
        self._nonzero_attenuation: bool = False

        self._reset_status: Optional[TaskStatus] = None
        self._reset_callback: Optional[Callable] = None

        self._acquisition_status: Optional[TaskStatus] = None
        self._acquisition_callback: Optional[Callable] = None

        self._write_lock = threading.Lock()
        self._attributes_to_write: Dict[str, Any] = {}

        super().__init__(
            logger,
            communication_state_callback,
            component_state_callback,
            interface_definition["poll_rate"],
            # acquire_numacq=None,
            acquisition=None,
            acquisition_mode=None,
            binary_field_width=None,
            busy=None,
            ch1_attenuation=None,
            ch1_bandwidth=None,
            ch1_clipping=None,
            ch1_coupling=None,
            ch1_display_vertical_scale=None,
            ch1_ext_attenuation=None,
            ch1_offset=None,
            ch1_parameters=None,
            ch1_position=None,
            ch1_probe_gain=None,
            ch1_probe_units=None,
            ch1_select=None,
            ch1_trigger_a_level=None,
            ch1_trigger_b_level=None,
            ch1_vertical_position=None,
            ch1_vertical_scale=None,
            ch1_vertical_scale_ratio=None,
            ch2_attenuation=None,
            ch2_bandwidth=None,
            ch2_clipping=None,
            ch2_coupling=None,
            ch2_display_vertical_scale=None,
            ch2_ext_attenuation=None,
            ch2_offset=None,
            ch2_parameters=None,
            ch2_position=None,
            ch2_probe_gain=None,
            ch2_probe_units=None,
            ch2_select=None,
            ch2_trigger_a_level=None,
            ch2_trigger_b_level=None,
            ch2_vertical_position=None,
            ch2_vertical_scale=None,
            ch2_vertical_scale_ratio=None,
            ch3_attenuation=None,
            ch3_bandwidth=None,
            ch3_clipping=None,
            ch3_coupling=None,
            ch3_display_vertical_scale=None,
            ch3_ext_attenuation=None,
            ch3_offset=None,
            ch3_parameters=None,
            ch3_position=None,
            ch3_probe_gain=None,
            ch3_probe_units=None,
            ch3_select=None,
            ch3_trigger_a_level=None,
            ch3_trigger_b_level=None,
            ch3_vertical_position=None,
            ch3_vertical_scale=None,
            ch3_vertical_scale_ratio=None,
            ch4_attenuation=None,
            ch4_bandwidth=None,
            ch4_clipping=None,
            ch4_coupling=None,
            ch4_display_vertical_scale=None,
            ch4_ext_attenuation=None,
            ch4_offset=None,
            ch4_parameters=None,
            ch4_position=None,
            ch4_probe_gain=None,
            ch4_probe_units=None,
            ch4_select=None,
            ch4_trigger_a_level=None,
            ch4_trigger_b_level=None,
            ch4_vertical_position=None,
            ch4_vertical_scale=None,
            ch4_vertical_scale_ratio=None,
            current_settings=None,
            command_error=None,
            data_encoding=None,
            data_source=None,
            data_source_available=None,
            data_start=None,
            data_stop=None,
            data_width=None,
            date=None,
            device_error=None,
            display_select_source=None,
            display_select_view=None,
            display_spectrum_source=None,
            execution_error=None,
            flag_when_complete=None,
            horizontal_delay_mode=None,
            horizontal_delay_time=None,
            horizontal_divisions=None,
            horizontal_duration=None,
            horizontal_increment=None,
            horizontal_mode=None,
            horizontal_position=None,
            horizontal_sample_rate=None,
            horizontal_scale=None,
            identity=None,
            operation_complete=None,
            power_cycled=None,
            query_error=None,
            # sample_rate=None,
            sweep_points=None,
            time=None,
            trace_data=None,
            trace_format=None,
            trace_parameters=None,
            trace_source=None,
            trigger_zero=None,
            vertical_offset=None,
            vertical_position=None,
            vertical_scale=None,
        )

        self.logger.debug(
            f"Initialising oscilloscope component manager: "
            f"Update rate is {update_rate}. "
            f"Poll rate is {interface_definition['poll_rate']}. "
            f"Attributes will be updated roughly each {self._max_tick} polls."
        )

    def off(self, task_callback: Optional[Callable] = None) -> Tuple[TaskStatus, str]:
        """
        Turn the component off.

        :param task_callback: callback to be called when the status of
            the command changes.

        :raises NotImplementedError: because this command is not yet
            implemented.
        """
        raise NotImplementedError("The device cannot be turned off or on.")

    def standby(
        self, task_callback: Optional[Callable] = None
    ) -> Tuple[TaskStatus, str]:
        """
        Put the component into low-power standby mode.

        :param task_callback: callback to be called when the status of
            the command changes.

        :raises NotImplementedError: because this command is not yet
            implemented.
        """
        raise NotImplementedError("The device cannot be put into standby mode.")

    def on(self, task_callback: Optional[Callable] = None) -> Tuple[TaskStatus, str]:
        """
        Turn the component on.

        :param task_callback: callback to be called when the status of
            the command changes.

        :raises NotImplementedError: because this command is not yet
            implemented.
        """
        raise NotImplementedError("The device cannot be turned off or on.")

    def reset(self, task_callback: Optional[Callable] = None) -> Tuple[TaskStatus, str]:
        """
        Reset the component (from fault state).

        :param task_callback: callback to be called when the status of
            the command changes.

        :returns: the task status and a message.
        """
        self.logger.debug("reset method called; setting status to QUEUED")
        self._reset_status = TaskStatus.QUEUED
        self._reset_callback = task_callback

        if task_callback is not None:
            task_callback(status=TaskStatus.QUEUED)

        return (
            TaskStatus.QUEUED,
            "Reset command will be executed at next poll",
        )

    def acquire(
        self, task_callback: Optional[Callable] = None
    ) -> Tuple[TaskStatus, str]:
        """
        Collect a new trace.

        Depending on the configuration of the oscilloscope, this could be
        a long-running operation - for instance if the trace is configured to
        average over a number of samples.

        :param task_callback: callback to be called when the status of
            the command changes.

        :returns: the task status and a message.
        """
        self.logger.debug("acquire method called; setting status to QUEUED")
        self._acquisition_status = TaskStatus.QUEUED
        self._acquisition_callback = task_callback

        if self._acquisition_callback is not None:
            self._acquisition_callback(status=self._acquisition_status)

        return (
            self._acquisition_status,
            "Acquire command will be executed at next poll",
        )

    def write_command(self, *cmds: str) -> None:
        """
        Enqueue a no-argument SCPI command to be executed next poll.

        :param cmds: SCPI commands to enqueue
            as named in the device definition YAML file.
        """
        with self._write_lock:
            self._attributes_to_write.update({cmd: __NO_ARG__ for cmd in cmds})

    def write_attribute(self, **kwargs: Any) -> None:
        """
        Update oscilloscope attribute value(s).

        This doesn't actually immediately write to the oscilloscope.
        It only stores the details of the requested write where it will
        be picked up by the next iteration of the polling loop.

        :param kwargs: keyword arguments specifying attributes to be
            written along with their corresponding value.
        """
        self.logger.debug(f"Registering attribute writes for next poll: {kwargs}")
        with self._write_lock:
            self._attributes_to_write.update(kwargs)

    def poll(self, poll_request: AttributeRequest) -> AttributeResponse:
        """
        Poll the hardware.

        Connect to the hardware, write any values that are to be
        written, and then read all values.

        :param poll_request: specification of the reads and writes to be
            performed in this poll.

        :returns: responses to queries in this poll.
        """
        # TODO get rid of this
        # self.logger.debug(f"Poller is initiating next poll: {poll_request}")
        # poll_response = self._attribute_client.send_receive(poll_request)
        # self.logger.debug(f"Poller is returning result of next poll: {poll_response}")
        # self.logger.debug("POLLER is initiating next poll.")
        # poll_response = self._attribute_client.send_receive(poll_request)
        # self.logger.debug("POLLER is returning result of next poll.")
        poll_response = self.send_receive(poll_request)
        self.logger.debug(f"POLLER is returning result of next poll: {poll_response}")
        return poll_response

    def get_request(self) -> AttributeRequest:
        """
        Return the reads and writes to be executed in the next poll.

        :returns: reads and writes to be executed in the next poll.
        """
        self._tick += 1

        attribute_request = AttributeRequest()
        if not self._identified:
            self.logger.debug("Adding identity query.")
            attribute_request.add_setop("trace_format", "ASC")
            attribute_request.set_queries("identity")
        elif self._reset_status == TaskStatus.QUEUED:
            self.logger.debug("Adding queued setops.")
            # TODO: Ideally, this sequence of things that we need to do
            # to reset the instrument would be part of the interface
            # definition.
            attribute_request.add_setop("reset")
            self._reset_status = TaskStatus.IN_PROGRESS
            if self._reset_callback is not None:
                self._reset_callback(status=TaskStatus.IN_PROGRESS)
        else:
            self.logger.debug("Constructing request for next poll.")
            with self._write_lock:
                for name, value in self._attributes_to_write.items():
                    self.logger.debug(f"Adding write request setop: {name}={value}.")
                    if value is __NO_ARG__:
                        attribute_request.add_setop(name)
                    else:
                        attribute_request.add_setop(name, value)
                self._attributes_to_write.clear()

            if self._tick >= self._max_tick:
                self.logger.debug(f"Tick {self._tick} >= {self._max_tick}.")

                if self._acquisition_status == TaskStatus.QUEUED:
                    self.logger.debug("Adding queued acquisition setops.")
                    attribute_request.add_setop("trace_data")
                    attribute_request.add_setop("flag_when_complete")
                    self._acquisition_status = TaskStatus.IN_PROGRESS
                    if self._acquisition_callback is not None:
                        self._acquisition_callback(status=self._acquisition_status)

                self.logger.debug("Adding queries")
                attribute_request.set_queries(
                    # "acquire_numacq",
                    "acquisition",
                    "acquisition_mode",
                    "binary_field_width",
                    "busy",
                    "ch1_attenuation",
                    "ch1_bandwidth",
                    "ch1_clipping",
                    "ch1_coupling",
                    "ch1_display_vertical_scale",
                    "ch1_ext_attenuation",
                    "ch1_offset",
                    "ch1_parameters",
                    "ch1_position",
                    "ch1_probe_gain",
                    "ch1_probe_units",
                    "ch1_select",
                    "ch1_trigger_a_level",
                    "ch1_trigger_b_level",
                    "ch1_vertical_position",
                    "ch1_vertical_scale",
                    "ch1_vertical_scale_ratio",
                    "ch2_attenuation",
                    "ch2_bandwidth",
                    "ch2_clipping",
                    "ch2_coupling",
                    "ch2_display_vertical_scale",
                    "ch2_ext_attenuation",
                    "ch2_offset",
                    "ch2_parameters",
                    "ch2_position",
                    "ch2_probe_gain",
                    "ch2_probe_units",
                    "ch2_select",
                    "ch2_trigger_a_level",
                    "ch2_trigger_b_level",
                    "ch2_vertical_position",
                    "ch2_vertical_scale",
                    "ch2_vertical_scale_ratio",
                    "ch3_attenuation",
                    "ch3_bandwidth",
                    "ch3_clipping",
                    "ch3_coupling",
                    "ch3_display_vertical_scale",
                    "ch3_ext_attenuation",
                    "ch3_offset",
                    "ch3_parameters",
                    "ch3_position",
                    "ch3_probe_gain",
                    "ch3_probe_units",
                    "ch3_select",
                    "ch3_trigger_a_level",
                    "ch3_trigger_b_level",
                    "ch3_vertical_position",
                    "ch3_vertical_scale",
                    "ch3_vertical_scale_ratio",
                    "ch4_attenuation",
                    "ch4_bandwidth",
                    "ch4_clipping",
                    "ch4_coupling",
                    "ch4_display_vertical_scale",
                    "ch4_ext_attenuation",
                    "ch4_offset",
                    "ch4_parameters",
                    "ch4_position",
                    "ch4_probe_gain",
                    "ch4_probe_units",
                    "ch4_select",
                    "ch4_trigger_a_level",
                    "ch4_trigger_b_level",
                    "ch4_vertical_position",
                    "ch4_vertical_scale",
                    "ch4_vertical_scale_ratio",
                    "current_settings",
                    "data_encoding",
                    "data_source",
                    "data_source_available",
                    "data_start",
                    "data_stop",
                    "data_width",
                    "date",
                    "display_select_source",
                    "display_select_view",
                    "display_spectrum_source",
                    "flag_when_complete",
                    "horizontal_delay_mode",
                    "horizontal_delay_time",
                    "horizontal_divisions",
                    "horizontal_duration",
                    "horizontal_increment",
                    "horizontal_mode",
                    "horizontal_position",
                    "horizontal_sample_rate",
                    "horizontal_scale",
                    "identity",
                    "operation_complete",
                    # "reset",
                    # "sample_rate",
                    "sweep_points",
                    "time",
                    "trace_data",
                    "trace_format",
                    "trace_parameters",
                    "trace_source",
                    "trigger_zero",
                    "vertical_offset",
                    "vertical_position",
                    "vertical_scale",
                )
                self._tick = 0

        self.logger.debug(f"Returning request for next poll: {attribute_request}")
        return attribute_request

    def poll_succeeded(self, poll_response: AttributeResponse) -> None:
        """
        Handle the receipt of new polling values.

        This is a hook called by the poller when values have been read
        during a poll.

        :param poll_response: response to the pool, including any
            values read.
        """
        super().poll_succeeded(poll_response)
        values = poll_response.responses
        self.logger.info(f"Handing results of successful poll: {values}.")

        if "device_error" in values:
            self._device_error = values["device_error"]
        if "autoattenuation" in values:
            self._autoattenuation = values["autoattenuation"]
        if "attenuation" in values:
            self._nonzero_attenuation = values["attenuation"] != 0

        fault = bool(self._device_error)
        self.logger.debug(f"Calculated fault status is {fault}")

        if "identity" in values:
            if self._check_identity(values["identity"]):
                self.logger.debug(f"Identity established: {values['identity']}.")
                self._identified = True
            else:
                self.logger.error(f"Wrong identity: {values['identity']}.")
                fault = True

        if self._reset_status == TaskStatus.IN_PROGRESS:
            self.logger.debug("Reset command has been executed.")
            self._reset_status = None
            if self._reset_callback is not None:
                self._reset_callback(status=TaskStatus.COMPLETED)

        self.logger.debug("Check operation_complete")
        operation_complete = values.pop("operation_complete", False)
        # operation_complete = values["operation_complete"]
        if operation_complete and self._acquisition_status == TaskStatus.IN_PROGRESS:
            self.logger.debug("Acquisition operation has completed.")
            self._acquisition_status = TaskStatus.COMPLETED
            if self._acquisition_callback is not None:
                self._acquisition_callback(status=self._acquisition_status)

        self.logger.debug("Pushing updates.")
        # TODO: Always-on device for now.
        self._update_component_state(power=PowerState.ON, fault=fault, **values)

    def _check_identity(self, identity: str) -> bool:
        """
        Check that the instrument model matches our expectations.

        :param identity: the identity reported by the instrument, in the
            form "make,model,serial_number,version".

        :returns: whether the identity of the instrument matches
            our expectations.
        """
        self.logger.info("Check identity %s", identity)
        _, model, _, _ = (s.strip() for s in identity.split(","))
        if model != self._model:
            self.logger.error(
                f"Expected instrument model to be '{self._model}' but "
                f"it is '{model}'. Polling cannot proceed until this is "
                "corrected."
            )
            return False
        return True

    def polling_stopped(self) -> None:
        """
        Respond to polling having stopped.

        This is a hook called by the poller when it stops polling.
        """
        self.logger.debug("Polling has stopped.")
        self._identified = False
        # Set to max here so that if/when polling restarts, an update is
        # requested as soon as possible.
        self._tick = self._max_tick
        super().polling_stopped()

    def send_receive(self, attribute_request: AttributeRequest) -> AttributeResponse:
        """
        Send an attribute request, and receive an attribute response.

        :param attribute_request: details of the attribute request to be sent.

        :returns: details of the attribute response.
        """
        # pylint: disable-next=protected-access
        scpi_request = self._attribute_client._marshall_request(attribute_request)
        # pylint: disable-next=protected-access
        scpi_response = self._attribute_client._scpi_client.send_receive(scpi_request)
        self.logger.debug(f"scpi_response: {scpi_response.responses}")
        # pylint: disable-next=protected-access
        attribute_response = self._attribute_client._unmarshall_response(scpi_response)
        self.logger.debug(f"attribute_response: {attribute_response.responses}")
        return attribute_response
