# -*- coding: utf-8 -*-
#
# (c) 2022 CSIRO.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""This module provides a Tango device for an oscilloscope."""

import logging
import os
import socket
from typing import Any

import tango
import tango.server
from ska_control_model import ResultCode
from ska_tango_base.commands import SubmittedSlowCommand

from ska_ser_test_equipment.base import TestEquipmentBaseDevice
from ska_ser_test_equipment.interface_definitions import InterfaceDefinitionFactory
from ska_ser_test_equipment.mixins import SCPIWriteQueryMixin
from ska_ser_test_equipment.oscilloscope.oscilloscope_component_manager import (
    OscilloscopeComponentManager,
)

__all__ = ["OscilloscopeDevice", "main"]

CommandReturnType = tuple[list[ResultCode], list[str]]

log_level_str = os.getenv("LOG_LEVEL", "WARNING").upper()
if log_level_str == "DEBUG":
    LOG_LEVEL = logging.DEBUG
elif log_level_str == "INFO":
    LOG_LEVEL = logging.INFO
elif log_level_str == "WARNING":
    LOG_LEVEL = logging.WARNING
elif log_level_str == "ERROR":
    LOG_LEVEL = logging.ERROR
else:
    LOG_LEVEL = logging.WARNING
logging.basicConfig(level=LOG_LEVEL)
_module_logger = logging.getLogger(__name__)
INTERACTIVE = False


class OscilloscopeDevice(TestEquipmentBaseDevice, SCPIWriteQueryMixin):
    """A Tango device to monitor and control an oscilloscope."""

    # --------------
    # Initialization
    # --------------
    def create_component_manager(self) -> OscilloscopeComponentManager:
        """
        Create and return a component manager for this device.

        :returns: a component manager for this device.
        """
        if INTERACTIVE:
            # pylint: disable-next=invalid-name
            self.Protocol = os.getenv("SIMULATOR_PROTOCOL", "tcp").lower()
            # pylint: disable-next=invalid-name
            self.Model = os.getenv("SIMULATOR_MODEL", "MSO64B").upper()
            # pylint: disable-next=invalid-name
            self.Host = os.getenv("SIMULATOR_HOST", socket.gethostname())
            # pylint: disable-next=invalid-name
            self.Port = int(os.getenv("SIMULATOR_PORT", "0"))
        self.logger.info(
            "Start %s device for %s on %s:%d",
            self.Protocol,
            self.Model,
            self.Host,
            self.Port,
        )
        # pylint: disable-next=attribute-defined-outside-init
        self._interface_definition = InterfaceDefinitionFactory()(self.Model)
        return OscilloscopeComponentManager(
            self._interface_definition,
            self.Protocol,  # tcp or telnet
            self.Host,
            self.Port,
            self.logger,
            self._communication_state_changed,
            self._component_state_changed,
            update_rate=self.UpdateRate,
        )

    def init_command_objects(self) -> None:
        """Register command objects (handlers) for this device's commands."""
        self.logger.info("Register command objects")
        super().init_command_objects()
        self.register_command_object(
            "Acquire",
            SubmittedSlowCommand(
                "Acquire",
                self._command_tracker,
                self.component_manager,
                "acquire",
                logger=None,
            ),
        )

    @tango.server.command(dtype_out="DevVarLongStringArray")
    @tango.DebugIt()
    def Acquire(self) -> CommandReturnType:  # pylint: disable=invalid-name
        """
        Play the configured waveform on the configured outputs of the AWG.

        :returns: Tuple of command result and a message
        """
        handler = self.get_command_object("Acquire")
        result_code, message = handler()
        self.logger.debug("Acquire message %s", message)
        return [result_code], [message]

    @tango.server.command()
    @tango.DebugIt()
    def FindPeak(self) -> None:  # pylint: disable=invalid-name
        """Move the marker to the frequency where power is maximum."""
        self.logger.debug("Move marker to peak")
        self.component_manager.write_command("marker_find_peak")


def main(args: Any = None, **kwargs: Any) -> int:
    """
    Launch an `OscilloscopeDevice` server instance.

    :param args: arguments to the oscilloscope device.
    :param kwargs: keyword arguments to the tango device server.

    :returns: exit code of the Tango server.
    """
    if _module_logger.isEnabledFor(logging.DEBUG):
        _module_logger.warning("Log level is DEBUG")
    elif _module_logger.isEnabledFor(logging.INFO):
        _module_logger.warning("Log level is INFO")
    elif _module_logger.isEnabledFor(logging.WARNING):
        _module_logger.warning("Log level is WARNING")
    else:
        pass
    return tango.server.run((OscilloscopeDevice,), args=args, **kwargs)


if __name__ == "__main__":
    print("*** Tango device for oscilloscope ***")
    INTERACTIVE = True
    main()
