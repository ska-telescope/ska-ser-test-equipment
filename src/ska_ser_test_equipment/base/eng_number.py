"""Display number in engineering format."""
import decimal
from typing import Optional


def eng_number(val: int | float | None, unit: Optional[str] = "") -> str:
    """
    Convert number to engineering format.

    :param val: input value
    :param unit: unit added to end of string

    :returns: formatted string
    """
    if val is None:
        return ""
    f_str: str = decimal.Decimal(f"{val:.4E}").normalize().to_eng_string()
    f_str = f_str.replace("E-12", "p")
    f_str = f_str.replace("E-9", "n")
    f_str = f_str.replace("E-6", "u")
    f_str = f_str.replace("E-3", "m")
    f_str = f_str.replace("E+3", "k")
    f_str = f_str.replace("E+6", "M")
    f_str = f_str.replace("E+9", "G")
    f_str = f_str.replace("E+12", "T")
    f_str += unit  # type: ignore[operator]
    return f_str
