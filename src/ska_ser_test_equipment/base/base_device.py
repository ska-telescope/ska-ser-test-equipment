# -*- coding: utf-8 -*-
#
# (c) 2022 CSIRO.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""This module provides a base Tango device for test equipment."""
import logging
from typing import Any, Dict, Optional, Tuple

import tango
import tango.server
from ska_control_model import PowerState, ResultCode
from ska_tango_base import SKABaseDevice
from ska_tango_base.commands import DeviceInitCommand

from ska_ser_test_equipment import __version__

__all__ = ["TestEquipmentBaseDevice"]

_module_logger = logging.getLogger(__name__)


# pylint: disable-next=abstract-method
class TestEquipmentBaseDevice(SKABaseDevice):
    """A Tango device for monitor and control of test equipment."""

    # ----------
    # Properties
    # ----------
    Model = tango.server.device_property(dtype=str)
    Protocol = tango.server.device_property(dtype=str)
    Host = tango.server.device_property(dtype=str)
    Port = tango.server.device_property(dtype=int)
    UpdateRate = tango.server.device_property(dtype=float, default_value=15.0)

    class InitCommand(DeviceInitCommand):
        """Initialisation command class for this base device."""

        # pylint: disable=protected-access
        def do(self, *args: Any, **kwargs: Any) -> Tuple[ResultCode, str]:
            """
            Initialise the attributes of this TestEquipmentBaseDevice.

            :param args: additional positional arguments; unused here.
            :param kwargs: additional keyword arguments; unused here.

            :returns: a resultcode, message tuple.
            """
            self._device._version_id = __version__
            self._device._setup_instrument_attributes()

            message = "TestEquipmentBaseDevice init complete."
            self._device.logger.info(message)
            self._completed()
            return (ResultCode.OK, message)

    # ----------
    # Attributes
    # ----------
    def _setup_instrument_attributes(self) -> None:
        """Set up attributes for the this device."""
        # pylint: disable-next=attribute-defined-outside-init
        self._instrument_state: Dict[str, Any] = {}

        for name, attr_spec in self._interface_definition["attributes"].items():
            _module_logger.info("Set attribute %s spec %s", name, attr_spec)
            writable = "write" in attr_spec or "read_write" in attr_spec
            spec = list(attr_spec.values())[0]
            if "field_type" not in spec:
                continue  # not an attribute

            self._instrument_state[name] = None
            kwargs = self._get_attribute_kwargs(name, spec)

            # some defaults
            attribute_type = self._assign_attribute_type(spec)
            dformat = tango.AttrDataFormat.SCALAR
            max_dim_x = 1

            if spec["field_type"] == "arbitrary_block":
                dformat = tango.AttrDataFormat.SPECTRUM
                max_dim_x = spec["max_dim_x"]

            write_type = (
                tango.AttrWriteType.READ_WRITE if writable else tango.AttrWriteType.READ
            )

            attr = tango.server.attribute(
                name=name,
                dtype=attribute_type,
                dformat=dformat,
                access=write_type,
                label=name,
                max_dim_x=max_dim_x,
                fread="_read_instrument_attribute",
                fwrite="_write_instrument_attribute" if writable else None,
                **kwargs,
            ).to_attr()

            self.add_attribute(
                attr,
                self._read_instrument_attribute,
                self._write_instrument_attribute if writable else None,
                None,
            )

            if "absolute_resolution" in spec:
                self.set_change_event(name, True)
            else:
                self.set_change_event(name, True, False)

    # pylint: disable-next=no-self-use
    def _assign_attribute_type(self, spec: dict[str, Any]) -> str:
        attribute_type = attribute_type = spec["field_type"]
        if spec["field_type"] == "bit":
            attribute_type = "bool"
        elif spec["field_type"] == "arbitrary_block":
            attribute_type = spec["block_data_type"]
        elif spec["field_type"] == "packet_item":
            attribute_type = "float"
        return attribute_type

    # pylint: disable-next=unused-argument, no-self-use
    def _get_attribute_kwargs(self, name: str, spec: dict[str, Any]) -> dict[str, Any]:
        kwargs = {}
        if "absolute_resolution" in spec:
            kwargs["abs_change"] = spec["absolute_resolution"]
        if "min_value" in spec:
            kwargs["min_value"] = spec["min_value"]
        if "max_value" in spec:
            kwargs["max_value"] = spec["max_value"]
        if "unit" in spec:
            kwargs["unit"] = spec["unit"]
            kwargs["display_unit"] = spec["unit"]
        return kwargs

    def _read_instrument_attribute(self, attribute: tango.Attribute) -> None:
        name = attribute.get_name()
        value = self._instrument_state[name]
        if value is None:
            _module_logger.warning("Read attribute %s has no value", name)
            attribute.set_quality(tango.AttrQuality.ATTR_INVALID)
        else:
            _module_logger.info("Read attribute %s value '%s'", name, value)
            attribute.set_value(value)
            attribute.set_quality(tango.AttrQuality.ATTR_VALID)

    def _write_instrument_attribute(self, attribute: tango.Attribute) -> None:
        name = attribute.get_name()
        value = attribute.get_write_value()
        _module_logger.info("Write attribute %s value '%s'", name, value)

        # TODO: implement an abstract component manager for this
        self.component_manager.write_attribute(  # type:ignore[attr-defined]
            **{name: value}
        )

    # ----------
    # Callbacks
    # ----------
    def is_Reset_allowed(self) -> bool:
        """
        Return whether `Reset` command may be called in current device state.

        Here we override the ska-tango-base implementation, which only
        allows Reset() in FAULT state. That's silly.

        :returns: whether the command may be called in the current
            device state.
        """
        return self.get_state() in [tango.DevState.ON, tango.DevState.FAULT]

    def _component_state_changed(
        self,
        fault: Optional[bool] = None,
        power: Optional[PowerState] = None,
        **kwargs: Any,
    ) -> None:
        _module_logger.info("Component state change %s", kwargs)
        super()._component_state_changed(fault=fault, power=power)
        for name, value in kwargs.items():
            if self._instrument_state[name] != value:
                self._instrument_state[name] = value
                self.push_change_event(name, value)
