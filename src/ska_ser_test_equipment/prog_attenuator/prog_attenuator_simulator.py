#!/usr/bin/env python
"""Implementation of a simple SCPI simulator for the programmable attenuator."""
# import asyncio
import logging
import os
import socket

# import telnetlib3
import threading
from typing import Dict

from ska_ser_devices.client_server import TcpServer

# from ska_ser_devices.client_server import Telnet3Server
from ska_ser_scpi import ScpiSimulator, SupportedAttributeType

from ska_ser_test_equipment.interface_definitions import InterfaceDefinitionFactory

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class ProgAttenuatorSimulator(ScpiSimulator):
    """A programmable attenuator simulator TCP server."""

    def __init__(
        self,
        model: str,
        **kwargs: SupportedAttributeType,
    ) -> None:
        """
        Initialise a new programmable attenuator simulator.

        :param model: the model identifier to use.
        :param kwargs: initial values for simulator attributes; where an
            initial value is not provided for an attribute, a default
            value will be used.
        """
        self._lock = threading.Lock()

        interface_definition = InterfaceDefinitionFactory()(model)

        self._initial_channels: Dict[str, float] = {
            attr: 0.0
            for attr in interface_definition["attributes"].keys()
            if "channel" in attr
        }

        initial_values = kwargs
        for key, value in self._initial_channels.items():
            initial_values.setdefault(key, value)

        super().__init__(
            interface_definition,
            initial_values,
            argument_separator=interface_definition["argument_separator"],
        )

    # pylint: disable-next=no-self-use
    def request(self) -> None:
        """Nothing to see here."""
        return

    def reset(self) -> None:
        """Reset to factory default values."""
        for key, value in self._initial_channels.items():
            self.set_attribute(key, value)


# TODO this does not do anything
# async def tserver_shell(reader, writer) -> None:
#     """
#     Callback function for telnet server
#     """
#     writer.write("\r\nSend? ")
#     inp = await reader.readline()
#     if inp:
#         writer.echo(str(inp))
#         writer.write("\r\nWhatever.\r\n")
#         writer.drain()
#     writer.close()


# TODO this does not work
def main_telnet() -> None:
    """Run the telnet server main loop."""
    model = os.getenv("SIMULATOR_MODEL", "RC4DAT6G95").upper()
    # model = os.getenv("SIMULATOR_MODEL", "RC1DAT800030").upper()
    host = os.getenv("SIMULATOR_HOST", socket.gethostname())
    port = int(os.getenv("SIMULATOR_PORT", "23"))
    logger.info("Start simulator for %s on %s:%d", model, host, port)

    # simulator = ProgAttenuatorSimulator(model)

    # tloop = asyncio.new_event_loop()
    # tcoro = telnetlib3.create_server(host, port, shell=tserver_shell)
    # tserver = tloop.run_until_complete(tcoro)
    # tloop.run_until_complete(tserver.wait_closed())


def main() -> None:
    """Run the TCP socket server main loop."""
    _protocol = os.getenv("SIMULATOR_PROTOCOL", "tcp").lower()  # noqa: F841
    # model = os.getenv("SIMULATOR_MODEL", "RC4DAT6G95").upper()
    model = os.getenv("SIMULATOR_MODEL", "RC1DAT800030").upper()
    host = os.getenv("SIMULATOR_HOST", socket.gethostname())
    port = int(os.getenv("SIMULATOR_PORT", "23"))
    logger.info("Start simulator for %s on %s:%d", model, host, port)

    simulator = ProgAttenuatorSimulator(model)
    server = TcpServer(host, port, simulator)
    with server:
        server.serve_forever()


# TODO: Implement a TelnetServer in ska-ser-devices to accurately
# simulate real life behaviour: https://jira.skatelescope.org/browse/LOW-509
if __name__ == "__main__":
    print("*** Programmable attenuator simulator ***")
    logger.setLevel(logging.DEBUG)
    try:
        main()
    except KeyboardInterrupt:
        logger.error("Interrupted")
