# -*- coding: utf-8 -*-
#
# (c) 2022 CSIRO.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""This module provides a Tango device for a programmable attenuator."""
import logging
import os
import socket
from typing import Any

import tango
import tango.server

from ska_ser_test_equipment.base.base_device import TestEquipmentBaseDevice
from ska_ser_test_equipment.mixins import SCPIWriteQueryMixin

if __package__ is None or __package__ == "":
    from ska_ser_test_equipment.interface_definitions import InterfaceDefinitionFactory

    # pylint: disable-next=line-too-long
    from ska_ser_test_equipment.prog_attenuator.prog_attenuator_component_manager import (  # noqa: E501
        ProgAttenuatorComponentManager,
    )
else:
    from ..interface_definitions import InterfaceDefinitionFactory
    from .prog_attenuator_component_manager import ProgAttenuatorComponentManager

__all__ = ["ProgAttenuatorDevice", "main"]

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
INTERACTIVE = False


class ProgAttenuatorDevice(TestEquipmentBaseDevice, SCPIWriteQueryMixin):
    """A Tango device for monitor and control of a programmable attenuator."""

    # --------------
    # Initialization
    # --------------

    def create_component_manager(self) -> ProgAttenuatorComponentManager:
        """
        Create and return a component manager for this device.

        :returns: a component manager for this device.
        """
        # pylint: disable-next=attribute-defined-outside-init
        self.logger = logger
        self.logger.info("Start device for %s", self.Model)
        if INTERACTIVE:
            # pylint: disable-next=invalid-name
            self.Protocol = os.getenv("SIMULATOR_PROTOCOL", "tcp").lower()
            # model = os.getenv("SIMULATOR_MODEL", "RC4DAT6G95").upper()
            # pylint: disable-next=invalid-name
            self.Model = os.getenv("SIMULATOR_MODEL", "RC1DAT800030").upper()
            # pylint: disable-next=invalid-name
            self.Host = os.getenv("SIMULATOR_HOST", socket.gethostname())
            # pylint: disable-next=invalid-name
            self.Port = int(os.getenv("SIMULATOR_PORT", "23"))
        self.logger.info(
            "Start %s device for %s on %s:%d",
            self.Protocol,
            self.Model,
            self.Host,
            self.Port,
        )
        # pylint: disable-next=attribute-defined-outside-init
        self._interface_definition = InterfaceDefinitionFactory()(self.Model)
        return ProgAttenuatorComponentManager(
            self._interface_definition,
            self.Protocol,
            self.Host,
            self.Port,
            self.logger,
            self._communication_state_changed,
            self._component_state_changed,
            update_rate=self.UpdateRate,
        )


# ----------
# Run server
# ----------
def main(args: Any = None, **kwargs: Any) -> int:
    """
    Launch a `ProgAttenuatorDevice` server instance.

    :param args: arguments to the programmable attenuator device.
    :param kwargs: keyword arguments to the server.

    :returns: the Tango server exit code.
    """
    return tango.server.run((ProgAttenuatorDevice,), args=args, **kwargs)


if __name__ == "__main__":
    INTERACTIVE = True
    main()
