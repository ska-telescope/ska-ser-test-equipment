# -*- coding: utf-8 -*-
#
# (c) 2022 CSIRO.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""This module provides monitoring and control of a programmable attenuator."""
from __future__ import annotations

import logging
import threading
from typing import Any, Callable, Dict, Final, Optional, Tuple

from ska_control_model import PowerState, TaskStatus
from ska_ser_scpi import (
    AttributeClient,
    AttributeRequest,
    AttributeResponse,
    InterfaceDefinitionType,
    ScpiBytesClientFactory,
    ScpiClient,
    SupportedProtocol,
)
from ska_tango_base.poller import PollingComponentManager

from ska_ser_test_equipment.mixins import SCPISendReceiveMixin


# pylint: disable-next=too-many-instance-attributes
class ProgAttenuatorComponentManager(
    PollingComponentManager[AttributeRequest, AttributeResponse], SCPISendReceiveMixin
):
    """A component manager for a programmable attenuator."""

    # pylint: disable=too-many-arguments
    def __init__(
        self,
        interface_definition: InterfaceDefinitionType,
        protocol: SupportedProtocol,
        host: str,
        port: int,
        logger: logging.Logger,
        communication_state_callback: Callable,
        component_state_callback: Callable,
        update_rate: float = 5.0,
    ) -> None:
        """
        Initialise a new instance.

        :param interface_definition: definition of the programmable attenuator's
            SCPI interface.
        :param protocol: the network protocol to be used to communicate
            with the programmable attenuator.
        :param host: the host name or IP address of the programmable attenuator.
        :param port: the port of the programmable attenuator.
        :param logger: a logger for this component manager to use for
            logging.
        :param communication_state_callback: callback to be called when
            the status of communications between the component manager
            and its component changes.
        :param component_state_callback: callback to be called when the
            state of the component changes.
        :param update_rate: how often updates to attribute values should
            be provided. This is not necessarily the same as the rate at
            which the instrument is polled. For example, the instrument
            may be polled every 0.1 seconds, thus ensuring that any
            invoked commands or writes will be executed promptly.
            However, if the `update_rate` is 5.0, then routine reads of
            instrument values will only occur every 50th poll (i.e.
            every 5 seconds).
        """
        bytes_client = ScpiBytesClientFactory().create_client(
            protocol,
            host,
            port,
            interface_definition["timeout"],
            interface_definition["sentinel_string"],
            logger=logger,
        )
        scpi_client = ScpiClient(
            bytes_client,
            chain=interface_definition["supports_chains"],
            argument_separator=interface_definition["argument_separator"],
            return_response=interface_definition["return_response"],
        )
        self._attribute_client = AttributeClient(
            scpi_client, interface_definition["attributes"]
        )

        self._model = interface_definition["model"]
        self._max_tick: Final = int(update_rate / interface_definition["poll_rate"])

        # We'll count ticks upwards, but start at the maximum so that
        # our initial update request occurs as soon as possible.
        self._tick = self._max_tick
        self._identified = False
        self._marked = False

        self._reset_status: Optional[TaskStatus] = None
        self._reset_callback: Optional[Callable] = None

        self._write_lock = threading.Lock()
        self._attributes_to_write: Dict[str, Any] = {}

        self._channel_attrs: list[str] = [
            key for key in interface_definition["attributes"] if "channel" in key
        ]

        super().__init__(
            logger,
            communication_state_callback,
            component_state_callback,
            interface_definition["poll_rate"],
            model_name=None,
            **{channel: None for channel in self._channel_attrs},
        )

        self.logger.info(
            "Set up %s client for %s on %s:%d",
            protocol.upper(),
            self._model,
            host,
            port,
        )

        self.logger.debug(
            f"Initialising programmable attenuator component manager: "
            f"Update rate is {update_rate}. "
            f"Poll rate is {interface_definition['poll_rate']}. "
            f"Attributes will be updated roughly each {self._max_tick} polls."
        )

    def off(self, task_callback: Optional[Callable] = None) -> Tuple[TaskStatus, str]:
        """
        Turn the component off.

        :param task_callback: callback to be called when the status of
            the command changes.

        :raises NotImplementedError: because this command is not yet
            implemented.
        """
        raise NotImplementedError("The device cannot be turned off or on.")

    def standby(
        self, task_callback: Optional[Callable] = None
    ) -> Tuple[TaskStatus, str]:
        """
        Put the component into low-power standby mode.

        :param task_callback: callback to be called when the status of
            the command changes.

        :raises NotImplementedError: because this command is not yet
            implemented.
        """
        raise NotImplementedError("The device cannot be put into standby mode.")

    def on(self, task_callback: Optional[Callable] = None) -> Tuple[TaskStatus, str]:
        """
        Turn the component on.

        :param task_callback: callback to be called when the status of
            the command changes.

        :raises NotImplementedError: because this command is not yet
            implemented.
        """
        raise NotImplementedError("The device cannot be turned off or on.")

    def reset(self, task_callback: Optional[Callable] = None) -> Tuple[TaskStatus, str]:
        """
        Reset the component (from fault state).

        :param task_callback: callback to be called when the status of
            the command changes.

        :returns: the task status and a message.
        """
        self.logger.debug("reset method called; setting status to QUEUED")
        self._reset_status = TaskStatus.QUEUED
        self._reset_callback = task_callback

        if task_callback is not None:
            task_callback(status=TaskStatus.QUEUED)

        return (
            TaskStatus.QUEUED,
            "Reset command will be executed at next poll",
        )

    def write_attribute(self, **kwargs: Any) -> None:
        """
        Update programmable attenuator attribute value(s).

        This doesn't actually immediately write to the programmable attenuator.
        It only stores the details of the requested write where it will
        be picked up by the next iteration of the polling loop.

        :param kwargs: keyword arguments specifying attributes to be
            written along with their corresponding value.
        """
        self.logger.debug(f"Registering attribute writes for next poll: {kwargs}")

        with self._write_lock:
            # TODO: this is just for testing the lock
            self._attributes_to_write.update(kwargs)

    def poll(self, poll_request: AttributeRequest) -> AttributeResponse:
        """
        Poll the hardware.

        Connect to the hardware, write any values that are to be
        written, and then read all values.

        :param poll_request: specification of the reads and writes to be
            performed in this poll.

        :returns: responses to queries in this poll.
        """
        self.logger.debug("Poller is initiating next poll.")
        poll_response = self._attribute_client.send_receive(poll_request)
        self.logger.debug("Poller is returning result of next poll.")
        return poll_response

    def get_request(self) -> AttributeRequest:
        """
        Return the reads and writes to be executed in the next poll.

        :returns: reads and writes to be executed in the next poll.
        """
        self._tick += 1
        self.logger.debug("Constructing request poll %d", self._tick)

        attribute_request = AttributeRequest()
        if not self._identified:
            self.logger.debug("Adding model name query.")
            attribute_request.set_queries("model_name")
        elif self._reset_status == TaskStatus.QUEUED:
            self.logger.debug("Adding reset setops.")
            #     # TODO: Ideally, this sequence of things that we need to do
            #     # to reset the instrument would be part of the interface
            #     # definition. Discuss what a reset on the programmable attenuator
            #     # really means.
            for channel in self._channel_attrs:
                attribute_request.add_setop(channel, 0.0)

            self._reset_status = TaskStatus.IN_PROGRESS
            if self._reset_callback is not None:
                self._reset_callback(status=TaskStatus.IN_PROGRESS)
        else:
            with self._write_lock:
                for name, value in self._attributes_to_write.items():
                    self.logger.debug(f"Adding write request setop: {name}={value}.")
                    attribute_request.add_setop(name, value)
                self._attributes_to_write.clear()
                self._tick = self._max_tick

            if self._tick >= self._max_tick:
                self.logger.debug(f"Tick {self._tick} >= {self._max_tick}.")
                self.logger.debug("Adding queries.")
                attribute_request.set_queries(
                    *self._channel_attrs,
                )
                self._tick = 0

        self.logger.debug("Returning request for next poll.")
        return attribute_request

    def poll_succeeded(self, poll_response: AttributeResponse) -> None:
        """
        Handle the receipt of new polling values.

        This is a hook called by the poller when values have been read
        during a poll.

        :param poll_response: response to the pool, including any
            values read.
        """
        super().poll_succeeded(poll_response)
        values = poll_response.responses
        self.logger.debug(f"Handing results of successful poll: {values}.")

        fault = False
        if "model_name" in values:
            if self._check_model_name(values["model_name"]):
                self.logger.debug(f"Identity established: {values['model_name']}.")
                self._identified = True
            else:
                self.logger.error(f"Wrong identity: {values['model_name']}.")
                fault = True

        if self._reset_status == TaskStatus.IN_PROGRESS:
            self.logger.debug("Reset command has been executed.")
            self._reset_status = None
            if self._reset_callback is not None:
                self._reset_callback(status=TaskStatus.COMPLETED)

        # self.logger.debug("Pushing updates.")
        # TODO: Always-on device for now.
        self._update_component_state(power=PowerState.ON, fault=fault, **values)

    def _check_model_name(self, model_name: str) -> bool:
        """
        Check that the instrument model matches our expectations.

        :param model_name: the model_name reported by the instrument, in the
            form "MN_[model_name]".

        :returns: whether the identity of the instrument matches
            our expectations.
        """
        model = model_name.split("=")[1]
        if model != self._model:
            self.logger.error(
                f"Expected instrument model to be {self._model},  but "
                f"it is {model}. Polling cannot proceed until this is "
                "corrected."
            )
            return False
        return True

    def polling_stopped(self) -> None:
        """
        Respond to polling having stopped.

        This is a hook called by the poller when it stops polling.
        """
        self.logger.debug("Polling has stopped.")
        self._identified = False
        # Set to max here so that if/when polling restarts, an update is
        # requested as soon as possible.
        self._tick = self._max_tick
        super().polling_stopped()
