#!/usr/bin/env python
"""Implementation of a simple SCPI simulator for the switch matrix."""
import os
import socket
import threading
from typing import Dict

from ska_ser_devices.client_server import TcpServer
from ska_ser_scpi import ScpiSimulator, SupportedAttributeType

from ska_ser_test_equipment.interface_definitions import InterfaceDefinitionFactory


class SPDTSimulator(ScpiSimulator):
    """A switch matrix simulator TCP server."""

    def __init__(
        self,
        model: str,
        **kwargs: SupportedAttributeType,
    ) -> None:
        """
        Initialise a new switch matrix simulator.

        :param model: the model identifier to use.
        :param kwargs: initial values for simulator attributes; where an
            initial value is not provided for an attribute, a default
            value will be used.
        """
        self._lock = threading.Lock()

        interface_definition = InterfaceDefinitionFactory()(model)

        self._initial_switches: Dict[str, bool] = {
            attr: False
            for attr in interface_definition["attributes"].keys()
            if "switch" in attr
        }

        initial_values = kwargs
        for key, value in self._initial_switches.items():
            initial_values.setdefault(key, value)

        super().__init__(
            interface_definition,
            initial_values,
            argument_separator=interface_definition["argument_separator"],
        )

    def reset(self) -> None:
        """Reset to factory default values."""
        for key, value in self._initial_switches.items():
            self.set_attribute(key, value)


def main() -> None:
    """Run the socketserver main loop."""
    model = os.getenv("SIMULATOR_MODEL", "ZTRC4SPDTA18").upper()
    host = os.getenv("SIMULATOR_HOST", socket.gethostname())
    port = int(os.getenv("SIMULATOR_PORT", "5025"))

    simulator = SPDTSimulator(model)
    # TODO: Implement a TelnetServer in ska-ser-devices to accurately
    # simulate real life behaviour: https://jira.skatelescope.org/browse/LOW-509
    server = TcpServer(host, port, simulator)
    with server:
        server.serve_forever()
