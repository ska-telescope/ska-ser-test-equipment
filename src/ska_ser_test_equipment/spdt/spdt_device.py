# -*- coding: utf-8 -*-
#
# (c) 2022 CSIRO.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""This module provides a Tango device for a switch matrix."""
from typing import Any

import tango
import tango.server

from ska_ser_test_equipment.base.base_device import TestEquipmentBaseDevice
from ska_ser_test_equipment.mixins import SCPIWriteQueryMixin

from ..interface_definitions import InterfaceDefinitionFactory
from .spdt_component_manager import SPDTComponentManager

__all__ = ["SPDTDevice", "main"]


class SPDTDevice(TestEquipmentBaseDevice, SCPIWriteQueryMixin):
    """A Tango device for monitor and control of a switch matrix."""

    # --------------
    # Initialization
    # --------------

    def create_component_manager(self) -> SPDTComponentManager:
        """
        Create and return a component manager for this device.

        :returns: a component manager for this device.
        """
        # pylint: disable-next=attribute-defined-outside-init
        self._interface_definition = InterfaceDefinitionFactory()(self.Model)
        return SPDTComponentManager(
            self._interface_definition,
            self.Protocol,
            self.Host,
            self.Port,
            self.logger,
            self._communication_state_changed,
            self._component_state_changed,
            update_rate=self.UpdateRate,
        )

    def _assign_attribute_type(self, spec: dict[str, Any]) -> str:
        attribute_type = super()._assign_attribute_type(spec)
        if attribute_type == "bool":
            attribute_type = "str"
        return attribute_type


# ----------
# Run server
# ----------
def main(args: Any = None, **kwargs: Any) -> int:
    """
    Launch a `SwitchMatrixDevice` server instance.

    :param args: arguments to the switch matrix device.
    :param kwargs: keyword arguments to the server.

    :returns: the Tango server exit code.
    """
    return tango.server.run((SPDTDevice,), args=args, **kwargs)


if __name__ == "__main__":
    main()
