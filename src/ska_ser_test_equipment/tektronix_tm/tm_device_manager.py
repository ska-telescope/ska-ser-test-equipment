"""
Tektronix stuff from the interwebs.

See https://tm-devices.readthedocs.io/stable/
"""

# pylint: disable=useless-suppression
# pylint: disable=bad-option-value
# pylint: disable=useless-option-value

import decimal
import logging
from typing import Any, Optional

import pyvisa
from tm_devices import DeviceManager

CSV_FILE = "example_curve_query.csv"


def eng_number(val: int | float, unit: Optional[str] = "") -> str:
    """
    Convert number to engineering format.

    :param val: input value
    :param unit: unit added to end of string

    :returns: formatted string
    """
    f_str = decimal.Decimal(f"{val:.4E}").normalize().to_eng_string()
    f_str = f_str.replace("E-12", "p")
    f_str = f_str.replace("E-9", "n")
    f_str = f_str.replace("E-6", "u")
    f_str = f_str.replace("E-3", "m")
    f_str = f_str.replace("E+3", "k")
    f_str = f_str.replace("E+6", "M")
    f_str = f_str.replace("E+9", "G")
    f_str = f_str.replace("E+12", "T")
    if unit:
        f_str += unit  # type: ignore[operator]
    return f_str


class TektronixDeviceManager:
    """Use Tektronix API to manage devices."""

    dev_mgr: DeviceManager

    def __init__(
        self,
        logger: logging.Logger,
        dev_verbose: bool,
        # pylint: disable-next=unused-argument
        dev_addr: str | None = None,
        # pylint: disable-next=unused-argument
        dev_alias: str | None = None,
    ):
        """
        Initialize Tektronix API and friends.

        :param logger: logging handle
        :param dev_verbose: extra logging
        :param dev_addr: IP address
        :param dev_alias: device alias string
        """
        self.logger = logger
        self.dev_mgr = DeviceManager(verbose=dev_verbose)
        self.instrument: Any = None
        self.id = ""

    def _get_command(self, cmd_descr: str, scpi_cmd: str) -> str:
        """
        Run SCPI command.

        :param cmd_descr: description
        :param scpi_cmd: command to run
        :returns: reply from instrument
        """
        try:
            reply = self.instrument.query(scpi_cmd)
            self.logger.debug("Read %s: %s", cmd_descr, reply)
        # pylint: disable-next=bare-except
        except:  # noqa: E722
            self.logger.error("Could not read %s (%s)", cmd_descr, scpi_cmd)
            reply = ""
        return reply

    def _get_command_float(self, cmd_descr: str, scpi_cmd: str) -> float:
        """
        Run SCPI command and convert to float value.

        :param cmd_descr: description
        :param scpi_cmd: command to run
        :returns: float value from instrument
        """
        ret_val: float
        try:
            reply = self.instrument.query(scpi_cmd)
            self.logger.debug("Read %s: %s", cmd_descr, reply)
            ret_val = float(reply)
        # pylint: disable-next=bare-except
        except:  # noqa: E722
            self.logger.error("Could not read %s (%s)", cmd_descr, scpi_cmd)
            ret_val = 0.0
        return ret_val

    def _get_command_int(self, cmd_descr: str, scpi_cmd: str) -> int:
        """
        Run SCPI command and convert to float value.

        :param cmd_descr: description
        :param scpi_cmd: command to run
        :returns: integer value from instrument
        """
        ret_val: int
        try:
            reply = self.instrument.query(scpi_cmd)
            self.logger.debug("Read %s: %s", cmd_descr, reply)
            ret_val = int(reply)
        # pylint: disable-next=bare-except
        except:  # noqa: E722
            self.logger.error("Could not read %s (%s)", cmd_descr, scpi_cmd)
            ret_val = 0
        return ret_val

    def _set_command(self, cmd_descr: str, scpi_cmd: str) -> None:
        """
        Run SCPI command.

        :param cmd_descr: description
        :param scpi_cmd: command to run
        """
        try:
            self.logger.debug("Write %s: %s", cmd_descr, scpi_cmd)
            self.instrument.write(scpi_cmd)
        # pylint: disable-next=bare-except
        except:  # noqa: E722
            self.logger.error("Could not write %s (%s)", cmd_descr, scpi_cmd)

    def command(self, scpi_cmd: str) -> None:
        """
        Run SCPI command.

        :param scpi_cmd: command to run
        """
        if "?" in scpi_cmd:
            self.logger.info("Read: %s", scpi_cmd)
            try:
                reply = self.instrument.query(scpi_cmd)
                print(reply)
            # pylint: disable-next=broad-exception-caught
            except Exception as cmd_err:
                self.logger.error("Could not read '%s' : %s", scpi_cmd, cmd_err)
        else:
            self.logger.info("Write: %s", scpi_cmd)
            try:
                self.instrument.write(scpi_cmd)
            # pylint: disable-next=broad-exception-caught
            except Exception as cmd_err:
                self.logger.error("Could not write '%s' : %s", scpi_cmd, cmd_err)

    def turn_channel_on(self, channel: int) -> None:
        """
        Turn channel on.

        :param channel: channel number
        """
        self.instrument.turn_channel_on(f"CH{channel}")

    def turn_channel_off(self, channel: int) -> None:
        """
        Turn channel off.

        :param channel: channel number
        """
        self.instrument.turn_channel_off(f"CH{channel}")

    def turn_all_channels_off(self) -> None:
        """Turn all channel off."""
        self.instrument.turn_all_channels_off()

    def turn_all_channels_on(self) -> None:
        """Turn all channel on."""
        self.instrument.turn_all_channels_on()

    def read_query_str(
        self,
        descrptn: str,
        scpi_cmd: str,
        # pylint: disable-next=unused-argument
        unit: str = "",
    ) -> str | None:
        """
        Write SCPI command to instrument and read reply as string.

        :param descrptn: description of what is happening
        :param scpi_cmd: command sent to instrument
        :param unit: units of value
        :returns: string value returned by instrument
        """
        self.logger.debug("VISA [%s] '%s'", descrptn, scpi_cmd)
        try:
            rval = self.instrument.query(scpi_cmd).strip()
            self.logger.debug("Got reply '%s'", rval)
        except pyvisa.errors.VisaIOError as v_err:
            self.logger.error("Could not read: %s", v_err)
            return None
        return rval

    def read_query_int(
        self,
        descrptn: str,
        scpi_cmd: str,
        # pylint: disable-next=unused-argument
        unit: str = "",
    ) -> int | None:
        """
        Write SCPI command to instrument and read reply as string.

        :param descrptn: description of what is happening
        :param scpi_cmd: command sent to instrument
        :param unit: units of value
        :returns: string value returned by instrument
        """
        rval: Any
        self.logger.debug("VISA [%s] '%s'", descrptn, scpi_cmd)
        try:
            rval = self.instrument.query(scpi_cmd).strip()
            self.logger.debug("Got reply '%s'", rval)
        except pyvisa.errors.VisaIOError as v_err:
            self.logger.error("Could not read: %s", v_err)
            return None
        return int(rval)

    def read_query_float(
        self,
        descrptn: str,
        scpi_cmd: str,
        # pylint: disable-next=unused-argument
        unit: str = "",
    ) -> float:
        """
        Write SCPI command to instrument and read reply as string.

        :param descrptn: description of what is happening
        :param scpi_cmd: command sent to instrument
        :param unit: units of value
        :returns: string value returned by instrument
        """
        rval: str
        self.logger.debug("VISA [%s] '%s'", descrptn, scpi_cmd)
        try:
            rval = self.instrument.query(scpi_cmd).strip()
            self.logger.debug("Got reply '%s'", rval)
        except pyvisa.errors.VisaIOError as v_err:
            self.logger.error("Could not read: %s", v_err)
            return float("nan")
        return float(rval)

    # pylint: disable-next=no-self-use
    def wait(self) -> None:
        """Wait for something to happen."""
        return

    def write_query_str(
        self,
        descrptn: str,
        scpi_cmd: str,
        value: str,
        # pylint: disable-next=unused-argument
        unit: str = "",
    ) -> str | None:
        """
        Change setting to specified value.

        :param descrptn: description of what is happening
        :param scpi_cmd: command sent to instrument
        :param value: new value
        :param unit: units of value
        :returns: floating point number as returned by instrument
        """
        self.write_command(descrptn, f"{scpi_cmd} {value}")
        self.wait()
        new_val: str | None = self.read_query_str(descrptn, f"{scpi_cmd}?")
        if new_val is None:
            self.logger.error("Could not value for %s", descrptn)
            return None
        if new_val != value:
            # Check for substring, e.g. TRI instead of TRIANGLE
            if new_val == value[0 : len(new_val)]:
                self.logger.info("Set value for %s to %s", descrptn, new_val)
            else:
                self.logger.error(
                    "Could not set value for %s, got %s", descrptn, new_val
                )
                return None
        return new_val

    def write_query_state(
        self,
        descrptn: str,
        scpi_cmd: str,
        value: str,
    ) -> str | None:
        """
        Change setting to specified value.

        :param descrptn: description of what is happening
        :param scpi_cmd: command sent to instrument
        :param value: new value
        :returns: floating point number as returned by instrument
        """
        self.write_command(descrptn, f"{scpi_cmd} {value}")
        self.wait()
        new_val: str | None = self.read_query_str(descrptn, f"{scpi_cmd}?")
        if new_val is None:
            self.logger.error("Could not value for %s", descrptn)
            return None
        if new_val != value:
            # Check for substring, e.g. TRI instead of TRIANGLE
            if value == "ON" and new_val == "1":
                self.logger.info("Set value for %s to %s", descrptn, new_val)
            elif value == "OFF" and new_val == "0":
                self.logger.info("Set value for %s to %s", descrptn, new_val)
            elif new_val == value[0 : len(new_val)]:
                self.logger.info("Set value for %s to %s", descrptn, new_val)
            else:
                self.logger.error(
                    "Could not set value for %s, got %s", descrptn, new_val
                )
                return None
        return new_val

    def write_query_int(
        self, descrptn: str, scpi_cmd: str, value: int, unit: str = ""
    ) -> int | None:
        """
        Change setting to specified value.

        :param descrptn: description of what is happening
        :param scpi_cmd: command sent to instrument
        :param value: new value
        :param unit: units of value
        :returns: floating point number as returned by instrument
        """
        self.write_command(descrptn, f"{scpi_cmd} {value}")
        self.wait()
        new_val: int | None = self.read_query_int(descrptn, f"{scpi_cmd}?")
        if new_val is None:
            self.logger.error("Could not value for %s", descrptn)
            return None
        if new_val != value:
            self.logger.error("Could not set value for %s", descrptn)
            return None
        new_str: str = eng_number(new_val, unit)
        print(f"{descrptn: 45} : {new_str}")
        return new_val

    def write_query_float(
        self, descrptn: str, scpi_cmd: str, value: float, unit: str = ""
    ) -> float | None:
        """
        Change setting to specified value.

        :param descrptn: description of what is happening
        :param scpi_cmd: command sent to instrument
        :param value: new value
        :param unit: units of value
        :returns: floating point number as returned by instrument
        """
        self.write_command(descrptn, f"{scpi_cmd} {value}")
        self.wait()
        new_val: float | None = self.read_query_float(descrptn, f"{scpi_cmd}?")
        if new_val is None:
            self.logger.error("Could not value for %s", descrptn)
            return None
        if new_val != value:
            self.logger.error(
                "Could not set value for %s, got %s", descrptn, str(new_val)
            )
            return None
        new_str: str = eng_number(new_val, unit)
        print(f"{descrptn:45} : {new_str}")
        return new_val

    def write_command(self, descrptn: str, scpi_cmd: str) -> None:
        """
        Write SCPI command to instrument without waiting for reply.

        :param descrptn: description of what is happening
        :param scpi_cmd: command sent to instrument
        """
        if self.instrument is None:
            print(f"/* {descrptn} */\n{scpi_cmd}\n")
        else:
            self.logger.debug("Send %s command '%s'", descrptn, scpi_cmd)
            self.instrument.write(scpi_cmd)
            self.instrument.write("*WAI")

    def __del__(self) -> None:
        """Destruct this thing."""
        try:
            self.dev_mgr.close()
        except AttributeError:
            pass
