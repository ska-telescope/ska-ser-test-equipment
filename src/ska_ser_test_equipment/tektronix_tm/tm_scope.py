"""
Tektronix stuff from the interwebs.

See https://tm-devices.readthedocs.io/stable/
"""

# pylint: disable=useless-suppression
# pylint: disable=invalid-name

import logging
from typing import Any, List

from tm_devices.drivers.pi.scopes.tekscope.mso6b import MSO6B

from ska_ser_test_equipment.tektronix_tm.tm_device_manager import TektronixDeviceManager

CSV_FILE = "example_curve_query.csv"


# pylint: disable-next=too-many-instance-attributes
class ScopeDevMan(TektronixDeviceManager):
    """Control and read Tektronix osciloscope."""

    instrument: MSO6B

    def __init__(
        self,
        logger: logging.Logger,
        scope_verbose: bool,
        scope_addr: str | None = None,
        scope_alias: str | None = None,
    ):
        """
        Rock and roll.

        :param logger: logging handle
        :param scope_verbose: talkativity flag
        :param scope_addr: IP address
        :param scope_alias: alias
        """
        super().__init__(logger, scope_verbose)
        if not scope_alias:
            scope_alias = f"SCOPE@{scope_alias}"
        if scope_addr is not None:
            self.instrument = self.dev_mgr.add_scope(
                scope_addr, alias=scope_alias
            )  # type: ignore[assignment]
        else:
            self.instrument = None  # type: ignore[assignment]
        self.id: str = f"SCOPE {scope_alias}: "
        self.id += self.instrument.query("*IDN?")
        self._sweep_points: int = 0
        self._vertical_divisions: float = 10.0
        self._vertical_scale1: float = 0.0
        self._vertical_scale2: float = 0.0
        self._vertical_scale3: float = 0.0
        self._vertical_scale4: float = 0.0
        self._horizontal_scale: float = 0.0
        self._horizontal_delay_time: float = 0.0
        self._horizontal_divisions: float = 0.0
        self.logger.debug("Scope %s: %s", scope_addr, self.instrument)
        self._trigger_a_edge_source: str = ""
        self._trigger_b_edge_source: str = ""

    def __repr__(self) -> str:
        """
        Set up the string representation.

        :returns: string thing
        """
        return f"{self.instrument}"

    def print(self) -> None:
        """Print ID value."""
        print(self.id)

    @property
    def vertical_divisions(self) -> float:
        """
        Query vertical divisions.

        :return: vertical divisions
        """
        return self._vertical_divisions

    @vertical_divisions.getter
    def vertical_divisions(self) -> float:
        """
        Query vertical divisions.

        :return: vertical divisions
        """
        vdivs: float
        self.logger.info("Vertical: %s", self.instrument.commands.vertical.query())
        vdivs = self._get_command_float("vertical_divisions", "VERTICAL:DIVISIONS?")
        if vdivs == 0.0:
            vdivs = 10.0
        self._vertical_divisions = vdivs
        return vdivs

    def _read_vertical_scale(self, ch_num: int) -> float:
        """
        Query vertical scale for specified analog channel.

        :param ch_num: channel number
        :returns: Volt per division
        """
        try:
            vs = self.instrument.query(f"CH{ch_num}:SCALE?")
        # pylint: disable-next=bare-except
        except:  # noqa: E722
            self.logger.error("Could not read vertical scale for channel %d", ch_num)
            return 0.0
        div: float = float(vs)
        return div

    def _set_vertical_scale(self, ch_num: int, value: float) -> None:
        """
        Set vertical scale for specified analog channel.

        :param ch_num: channel number
        :param value: vertical scale in Volt per division
        """
        self.instrument.write(f"CH{ch_num}:SCALE {value}")

    @property
    def vertical_scale1(self) -> float:
        """
        Query vertical scale for analog channel 1.

        :returns: Volt per division
        """
        return self._vertical_scale1

    @vertical_scale1.getter
    def vertical_scale1(self) -> float:
        """
        Query vertical scale for analog channel 1.

        :returns: Volt per division
        """
        self._vertical_scale1 = self._read_vertical_scale(1)
        return self._vertical_scale1

    @vertical_scale1.setter
    def vertical_scale1(self, value: float) -> None:
        """
        Set vertical scale for analog channel 1.

        :param value: new value
        """
        self._vertical_scale1 = value
        self._set_vertical_scale(1, value)

    @property
    def vertical_scale2(self) -> float:
        """
        Query vertical scale for analog channel 2.

        :returns: Volt per division
        """
        return self._vertical_scale2

    @vertical_scale2.getter
    def vertical_scale2(self) -> float:
        """
        Get vertical scale for analog channel 2.

        :returns: Volt per division
        """
        self._vertical_scale2 = self._read_vertical_scale(2)
        return self._vertical_scale2

    @vertical_scale2.setter
    def vertical_scale2(self, value: float) -> None:
        """
        Set vertical scale for analog channel 2.

        :param value: Volt per division
        """
        self._vertical_scale2 = value
        self._set_vertical_scale(2, value)

    @property
    def vertical_scale3(self) -> float:
        """
        Query vertical scale for analog channel 1.

        :returns: Volt per division
        """
        return self._vertical_scale3

    @vertical_scale3.getter
    def vertical_scale3(self) -> float:
        """
        Query vertical scale for analog channel 3.

        :returns: Volt per division
        """
        self._vertical_scale3 = self._read_vertical_scale(3)
        return self._vertical_scale3

    @vertical_scale3.setter
    def vertical_scale3(self, value: float) -> None:
        """
        Set vertical scale for analog channel 3.

        :param value: new value
        """
        self._vertical_scale3 = value
        self._set_vertical_scale(3, value)

    @property
    def vertical_scale4(self) -> float:
        """
        Query vertical scale for analog channel 4.

        :returns: Volt per division
        """
        return self._vertical_scale4

    @vertical_scale4.getter
    def vertical_scale4(self) -> float:
        """
        Get vertical scale for analog channel 4.

        :returns: Volt per division
        """
        self._vertical_scale3 = self._read_vertical_scale(4)
        return self._vertical_scale4

    @vertical_scale4.setter
    def vertical_scale4(self, value: float) -> None:
        """
        Set vertical scale for analog channel 4.

        :param value: new value
        """
        self._vertical_scale4 = value
        self._set_vertical_scale(4, value)

    @property
    def horizontal_scale(self) -> float:
        """
        Get time per division.

        :returns: horizontal scale
        """
        return self._horizontal_scale

    @horizontal_scale.getter
    def horizontal_scale(self) -> float:
        """
        Query horizontal scale.

        :returns: time per division
        """
        try:
            hs = self.instrument.query("HORIZONTAL:SCALE?")
            hdiv: float = float(hs)
        # pylint: disable-next=bare-except
        except:  # noqa: E722
            self.logger.error("Could not read horizontal scale")
            hdiv = 0.0
        self._horizontal_scale = hdiv
        return hdiv

    @horizontal_scale.setter
    def horizontal_scale(self, value: float) -> None:
        """
        Get horizontal scale.

        :param value: horizontal scale in seconds per division
        """
        self._horizontal_scale = value
        self.instrument.write(f"HORIZONTAL:SCALE {value}")

    @property
    def horizontal_delay_time(self) -> float:
        """
        Get horizontal delay time.

        :returns: horizontal delay time
        """
        return self._horizontal_delay_time

    @horizontal_delay_time.getter
    def horizontal_delay_time(self) -> float:
        """
        Query horizontal delay time.

        :returns: time per division
        """
        try:
            hs = self.instrument.query("HORIZONTAL:DELAY:TIME?")
            hdiv: float = float(hs)
        # pylint: disable-next=bare-except
        except:  # noqa: E722
            self.logger.error("Could not read horizontal scale")
            hdiv = 0.0
        self._horizontal_scale = hdiv
        return hdiv

    @property
    def horizontal_divisions(self) -> float:
        """
        Get horizontal divisions.

        :returns: number of horizontal divisions
        """
        return self._horizontal_divisions

    @horizontal_divisions.getter
    def horizontal_divisions(self) -> float:
        """
        Get horizontal divisions.

        :returns: number of horizontal divisions
        """
        hd: str
        try:
            hd = self.instrument.query("HORIZONTAL:DIVISIONS?")
        # pylint: disable-next=bare-except
        except:  # noqa: E722
            self.logger.error("Could not read horizontal scale for channel %d")
            hd = "0.0"
        hdivs: float = float(hd)
        self._horizontal_divisions = hdivs
        return hdivs

    @property
    def sweep_points(self) -> int:
        """
        Get number of sweep points.

        :returns: number of sweep points
        """
        return self._sweep_points

    @sweep_points.getter
    def sweep_points(self) -> int:
        """
        Get number of sweep points.

        :returns: number of sweep points
        """
        sp: str
        try:
            sp = self.instrument.query("WFMOUTPRE:NR_PT?")
        # pylint: disable-next=bare-except
        except:  # noqa: E722
            sp = "0"
        self._sweep_points = int(sp)
        return self._sweep_points

    @property
    def trigger_a_edge_source(self) -> str:
        """
        Get trigger A edge source.

        :returns: trigger A edge source
        """
        return self._trigger_a_edge_source

    @trigger_a_edge_source.getter
    def trigger_a_edge_source(self) -> str:
        """
        Get trigger A edge source.

        :returns: trigger A edge source
        """
        src = self._get_command("trigger_a_edge_source", "TRIGGER:A:EDGE:SOURCE?")
        self._trigger_a_edge_source = src
        return src

    def command(self, scpi_cmd: str) -> None:
        """
        Send SCPI command.

        :param scpi_cmd: command to send
        """
        if "?" in scpi_cmd:
            reply = self.instrument.query(scpi_cmd)
            print(reply)
        else:
            self.instrument.write(scpi_cmd)

    def write_curve_file(self, curve_file: str) -> List[Any]:
        """
        Perform curve query and save results to CSV file.

        :param curve_file: file name to write
        :returns: curve data
        """
        curve_returned = self.instrument.curve_query(1, output_csv_file=curve_file)
        return curve_returned

    def read_curve(self) -> tuple[list, list]:
        """
        Perform curve query and save results to CSV file.

        :returns: curve data
        """
        curve_read = self.instrument.curve_query(1)

        value: float
        t_start = 0.0
        t_stop = self.horizontal_scale * self.horizontal_divisions
        t_step = (t_stop - t_start) / (len(curve_read) + 1)
        n = 0
        x_vals = [t_start]
        t_curr = t_start
        for n in range(1, len(curve_read) - 1):
            t_curr += t_step
            x_vals.append(t_curr)
        x_vals.append(t_stop)

        y_vals = []
        scale_factor: float = self.vertical_scale1 / self.vertical_divisions
        # pylint: disable-next=consider-using-enumerate
        for n in range(0, len(curve_read)):
            value = curve_read[n] * scale_factor
            y_vals.append(value)
        # self.logger.info()
        return x_vals, y_vals
