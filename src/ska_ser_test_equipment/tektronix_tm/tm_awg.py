"""
Tektronix stuff from the interwebs.

Test & Measurement Device Management:
See https://tm-devices.readthedocs.io/stable/
"""

# pylint: disable=useless-suppression
# pylint: disable=bad-option-value
# pylint: disable=too-many-arguments,too-many-positional-arguments

# mypy: disable-error-code=attr-defined

import logging
from typing import Any, Literal, Optional, Sequence, Tuple

import numpy as np
from tm_devices import DeviceManager
from tm_devices.drivers.pi.signal_generators.awgs.awg5200 import AWG5200
from tm_devices.helpers import PYVISA_PY_BACKEND
from tm_devices.helpers.enums import (
    SignalGeneratorFunctionsAWG,
    SignalGeneratorOutputPathsBase,
)

from ska_ser_test_equipment.tektronix_tm.tm_device_manager import TektronixDeviceManager

AWG_MODES = ["AWG", "FGEN"]
AWG_RUN_STATES = ["STOPPED", "WAITING FOR TRIGGER", "RUNNING"]
AWG_TYPES = [
    "SINE",
    "SQUARE",
    "TRIANGLE",
    "NOISE",
    "DC",
    "GAUSSIAN",
    "EXPRISE",
    "EXPDECAY",
    "NONE",
]
AWG_COUPLED_STATES = [
    "NONE",
    "ALL",
    "PAIR",
]
SIGNAL_PATHS = [
    "DCHB",  # DC High Bandwidth
    "ACDIRECT",
    "ACAMPLIFIED",
]


# pylint: disable-next=too-many-public-methods
class AwgDevMan(TektronixDeviceManager):
    """Manage AWG device."""

    instrument: AWG5200
    dev_mgr: DeviceManager

    def __init__(
        self,
        logger: logging.Logger,
        awg_verbose: bool,
        awg_addr: str | None = None,
        awg_alias: str | None = None,
    ):
        """
        Get the show on the road.

        :param logger: logging handle
        :param awg_verbose: verbosity flag
        :param awg_addr: IP address
        :param awg_alias: alternate name
        """
        super().__init__(logger, awg_verbose)
        self.logger.info("Connect to %s", awg_addr)
        if not awg_alias:
            awg_alias = f"AWG@{awg_addr}"
        if awg_addr is not None:
            self.instrument = self.dev_mgr.add_awg(
                awg_addr, alias=awg_alias
            )  # type: ignore[assignment]
            self.instrument.visa_timeout = 50000
        else:
            self.instrument = None  # type: ignore[assignment]
        self.id: str = f"AWG {awg_alias}: "
        self.id += self.instrument.query("*IDN?")
        print(f"{self.instrument}")

    def __repr__(self) -> str:
        """
        Do the string thing.

        :returns: string representation
        """
        return f"IDN: {self.id}"

    def asset_name(self, ch_num: int) -> Any:
        """
        Read asset name (waveform or sequence) assigned to specified channel.

        :param ch_num: channel number
        :returns: asset name and type
        """
        a_name: str | None = self.read_query_str(
            f"ch{ch_num} type", f"SOURCE{ch_num}:CASSET?"
        )
        a_type: str | None = self.read_query_str(
            f"ch{ch_num} type", f"SOURCE{ch_num}:CASSET:TYPE?"
        )
        self.logger.info("Channel %d asset is %s (%s)", ch_num, a_name, a_type)
        return a_name, a_type

    def channel_off(self, channel: int) -> None:
        """
        Turn channel off.

        :param channel: channel number
        """
        self.logger.info("Turn channel %d off", channel)
        self.instrument.write(f"OUTPUT{channel}:STATE 0")

    def channel_on(self, channel: int) -> None:
        """
        Turn channel on.

        :param channel: channel number
        """
        self.logger.info("Turn channel %d on", channel)
        self.instrument.write(f"OUTPUT{channel}:STATE 1")

    def coupled_state(self, ch_state: str | None = None) -> str | None:
        """
        Get coupled state of the channel’s Analog and Marker output controls.

        NONE - Disables coupling of all channel’s analog and marker output settings.
        ALL - Couples all channel’s analog and marker output settings. The initial
        settings are derived from channel 1.
        After the initial coupling of the settings, changes made to any channel settings
        affect all channels.
        PAIR - Couples the analog channel and marker output settings in pairs. Initial
        settings are derived from the odd numbered channel of each pair. (For example,
        CH1 to CH2, CH3 to CH4, etc. for all available channels.)
        After the initial coupling of the settings, changes made to either channel
        settings in the pair affect both channels.

        :param ch_state: new value to be set, None if only reading
        :returns: coupled state
        """
        o_state: str | None = self.read_query_str(
            "coupled state", "INSTRUMENT:COUPLE:SOURCE?"
        )
        if ch_state is None:
            self.logger.info("coupled state is %s", o_state)
            return o_state
        if ch_state not in AWG_COUPLED_STATES:
            self.logger.error("Invalid value for coupled state %s", ch_state)
            return None
        self.logger.info("Set coupled state to %s", ch_state)
        o_state = self.write_query_str(
            "coupled state", "INSTRUMENT:COUPLE:SOURCE", ch_state
        )
        self.logger.info("coupled state set to %s", o_state)
        return o_state

    def dac_resolution(self, ch_num: int) -> int | None:
        """
        Read the DAC resolution.

        :param ch_num: channel number
        :returns: number of bits
        """
        o_res: int | None = self.read_query_int(
            f"ch{ch_num} resolution", f"SOURCE{ch_num}:DAC:RESOLUTION?"
        )
        self.logger.info(f"ch{ch_num} DAC resolution is {o_res}")
        return o_res

    def dc_level(self, ch_num: int, ch_dc: float | None = None) -> float | None:
        """
        Set DC level.

        :param ch_num: channel number
        :param ch_dc: new value to be set, None if only reading
        :returns: current or new DC level
        """
        # self.print_query(f"ch{ch_num} offset", f"FGEN:CHANNEL{ch_num}:OFFSET?", "s")
        o_dc: float | None = self.read_query_float(
            f"ch{ch_num} DC level", f"FGEN:CHANNEL{ch_num}:DCLEVEL?"
        )
        if ch_dc is None:
            self.logger.info(f"ch{ch_num} DC level is {o_dc} V")
            return o_dc
        self.logger.info("Set channel %d DC level to %f", ch_num, ch_dc)
        o_dc = self.write_query_float(
            f"ch{ch_num} DC level", f"FGEN:CHANNEL{ch_num}:DCLEVEL", ch_dc, "V"
        )
        self.logger.info(f"ch{ch_num} DC level set to {o_dc} Hz")
        return o_dc

    def error_count(self) -> int | None:
        """
        Read error count.

        :returns: number of errors
        """
        err_count: int | None = self.read_query_int(
            "error count", "SYSTEM:ERROR:COUNT?"
        )
        self.logger.info("Error queue contains %d entries", err_count)
        return err_count

    def frequency(self, ch_num: int, ch_freq: float | None = None) -> float | None:
        """
        Set frequency.

        :param ch_num: channel number
        :param ch_freq: new value to be set, None if only reading
        :returns: current or new frequency
        """
        # self.print_query(f"ch{ch_num} period", f"FGEN:CHANNEL{ch_num}:PERIOD?", "s")
        o_hertz: float | None = self.read_query_float(
            f"ch{ch_num} frequency", f"FGEN:CHANNEL{ch_num}:FREQUENCY?"
        )
        if ch_freq is None:
            self.logger.info(f"ch{ch_num} frequency is {o_hertz} Hz")
            return o_hertz
        self.logger.info("Set channel %d frequency to %f", ch_num, ch_freq)
        o_hertz = self.write_query_float(
            f"ch{ch_num} frequency", f"FGEN:CHANNEL{ch_num}:FREQUENCY", ch_freq, "Hz"
        )
        self.logger.info(f"ch{ch_num} frequency set to {o_hertz} Hz")
        return o_hertz

    def generate_sine(
        self,
        frequency: float,
        amplitude: float,
        offset: float,
        channel: str,
        # pylint: disable-next=unused-argument
        output_signal_path: Optional[SignalGeneratorOutputPathsBase] = None,
        # pylint: disable-next=unused-argument
        termination: Literal["FIFTY", "HIGHZ"] = "FIFTY",
        # pylint: disable-next=unused-argument
        duty_cycle: float = 50.0,
        # pylint: disable-next=unused-argument
        polarity: Literal["NORMAL", "INVERTED"] = "NORMAL",
        # pylint: disable-next=unused-argument
        symmetry: float = 50.0,
    ) -> None:
        """
        Generate a sine wave.

        :param frequency: Hertz
        :param amplitude: Volt
        :param offset: Volt
        :param channel: number
        :param output_signal_path: path to write file
        :param termination: 50 ohm or 1000 ohm
        :param duty_cycle: Defaults to 50.0%
        :param polarity: "NORMAL" or "INVERTED"
        :param symmetry: Defaults to 50.0%
        """
        self.logger.info(f"Generate sine wave {amplitude}V {frequency}Hz to {channel}")
        self.instrument.generate_function(
            frequency,
            SignalGeneratorFunctionsAWG.SIN,
            amplitude,
            offset,
            channel,
        )

    def generate_wave(
        self,
        function: str,
        frequency: float,
        amplitude: float,
        offset: float,
        ch_num: int,
        # pylint: disable-next=unused-argument
        output_signal_path: Optional[SignalGeneratorOutputPathsBase] = None,
        # pylint: disable-next=unused-argument
        termination: Literal["FIFTY", "HIGHZ"] = "FIFTY",
        # pylint: disable-next=unused-argument
        duty_cycle: float = 50.0,
        # pylint: disable-next=unused-argument
        polarity: Literal["NORMAL", "INVERTED"] = "NORMAL",
        # pylint: disable-next=unused-argument
        symmetry: float = 50.0,
    ) -> None:
        """
        Generate waveform.

        :param function: waveform type
        :param frequency: Hertz
        :param amplitude: Volt
        :param offset: Volt
        :param ch_num: channel number
        :param output_signal_path: path to write file
        :param termination: 50 ohm or 1000 ohm
        :param duty_cycle: Defaults to 50.0%
        :param polarity: "NORMAL" or "INVERTED"
        :param symmetry: Defaults to 50.0%
        """
        wave: SignalGeneratorFunctionsAWG

        channel: str = f"SOURCE{ch_num}"
        self.logger.info(
            "Generate %s wave %fV %fHz to %d", function, amplitude, frequency, channel
        )
        function = function.lower()
        if function == "sine":
            wave = SignalGeneratorFunctionsAWG.SIN
        elif function == "triangle":
            wave = SignalGeneratorFunctionsAWG.TRIANGLE
        elif function == "square":
            wave = SignalGeneratorFunctionsAWG.SQUARE
        else:
            self.logger.error("Invalid waveform '%s'", function)
            return
        self.instrument.generate_function(
            frequency,
            wave,
            amplitude,
            offset,
            channel,
        )

    def high_low(self, ch_num: int) -> Tuple[float | None, float | None]:
        """
        Set DC voltage.

        :param ch_num: channel number
        :returns: current high and low voltages
        """
        h_volt: float | None = self.read_query_float(
            f"ch{ch_num} DC voltage", f"FGEN:CHANNEL{ch_num}:HIGH?"
        )
        self.logger.info(f"ch{ch_num} high voltage{' ':24} : {h_volt}")
        l_volt: float | None = self.read_query_float(
            f"ch{ch_num} DC voltage", f"FGEN:CHANNEL{ch_num}:LOW?"
        )
        self.logger.info(f"ch{ch_num} low voltage{' ':25} : {l_volt}")
        return h_volt, l_volt

    def instrument_mode(self, inst_mode: str | None = None) -> str | None:
        """
        Read instrument mode.

        :param inst_mode: instrument mode
        :returns: AWG or FGEN
        """
        o_mode: str | None = self.read_query_str("instrument_mode", "INSTRUMENT:MODE?")
        if inst_mode is None:
            self.logger.info(f"instrument mode is {o_mode}")
            return o_mode
        if inst_mode not in AWG_MODES:
            self.logger.error("Invalid value for instrument mode %s", inst_mode)
            return None
        self.logger.info("Set instrument mode to %s", inst_mode)
        o_state = self.write_query_str("instrument mode", "INSTRUMENT:MODE", inst_mode)
        self.logger.info("instrument mode set to %s", o_state)
        return o_mode

    def offset_voltage(
        self, ch_num: int, ch_voltage: float | None = None
    ) -> float | None:
        """
        Set offset voltage.

        :param ch_num: channel number
        :param ch_voltage: new value to be set, None if only reading
        :returns: current or new offset voltage
        """
        volt: float | None = self.read_query_float(
            f"ch{ch_num} offset voltage", f"FGEN:CHANNEL{ch_num}:OFFSET?"
        )
        if ch_voltage is None:
            self.logger.info(f"ch{ch_num} offset voltage is {volt}")
            return volt
        self.logger.info("Set channel %d output voltage to %f", ch_num, ch_voltage)
        volt = self.write_query_float(
            f"ch{ch_num} offset voltage",
            f"FGEN:CHANNEL{ch_num}:OFFSET",
            ch_voltage,
            "V",
        )
        self.logger.info(f"ch{ch_num} offset voltage set to {volt}")
        return volt

    def output_power(self, ch_num: int, ch_pwr: float | None = None) -> float | None:
        """
        Set output power in dBm.

        :param ch_num: channel number
        :param ch_pwr: new value to be set, None if only reading
        :returns: current or new output power in dBm
        """
        o_pwr: float | None = self.read_query_float(
            f"ch{ch_num} power", f"FGEN:CHANNEL{ch_num}:AMPLITUDE:POWER?"
        )
        if ch_pwr is None:
            self.logger.info(f"ch{ch_num} output power is {o_pwr}")
            return o_pwr
        self.logger.info("Set channel %d output power to %s", ch_num, ch_pwr)
        o_pwr = self.write_query_float(
            f"ch{ch_num} output power",
            f"FGEN:CHANNEL{ch_num}:AMPLITUDE:POWER",
            ch_pwr,
            "dBm",
        )
        self.logger.info(f"ch{ch_num} output power set to {o_pwr}")
        return o_pwr

    def output_state(self, channel: int) -> int | None:
        """
        Set output state.

        :param channel: channel number

        :returns: zero or one
        """
        rval: int | None = self.read_query_int(
            "output_state", f"OUTPUT{channel}:STATE?"
        )
        return rval

    def output_voltage(
        self, ch_num: int, ch_voltage: float | None = None
    ) -> float | None:
        """
        Set output voltage.

        :param ch_num: channel number
        :param ch_voltage: new value to be set, None if only reading
        :returns: current or new output voltage
        """
        volt: float | None = self.read_query_float(
            f"ch{ch_num} output voltage", f"FGEN:CHANNEL{ch_num}:AMPLITUDE:VOLTAGE?"
        )
        if ch_voltage is None:
            self.logger.info(f"ch{ch_num} output voltage is {volt}")
            return volt
        self.logger.info("Set channel %d output voltage to %s", ch_num, ch_voltage)
        volt = self.write_query_float(
            f"ch{ch_num} output voltage",
            f"FGEN:CHANNEL{ch_num}:AMPLITUDE:VOLTAGE",
            ch_voltage,
            "V",
        )
        self.logger.info(f"ch{ch_num} output voltage set to {volt}")
        return volt

    def period(self, ch_num: int, ch_period: float | None = None) -> float | None:
        """
        Set period.

        :param ch_num: channel number
        :param ch_period: new value
        :returns: current or new DC voltage
        """
        perd: float | None = self.read_query_float(
            f"ch{ch_num} DC voltage", f"FGEN:CHANNEL{ch_num}:PERIOD?"
        )
        if ch_period is None:
            self.logger.info(f"ch{ch_num} Period is {perd} s")
            return perd
        self.logger.info("Set channel %d period to %f", ch_num, ch_period)
        perd = self.write_query_float(
            f"ch{ch_num} period",
            f"FGEN:CHANNEL{ch_num}:PERIOD",
            ch_period,
            "s",
        )
        self.logger.info(f"ch{ch_num} Period set to {perd} s")
        return perd

    def phase(self, ch_num: int, ch_phase: float | None = None) -> float | None:
        """
        Set phase angle.

        :param ch_num: channel number
        :param ch_phase: new value
        :returns: current or new phase angle
        """
        o_phase: float | None = self.read_query_float(
            f"ch{ch_num} phase", f"FGEN:CHANNEL{ch_num}:PHASE?"
        )
        if ch_phase is None:
            self.logger.info(f"ch{ch_num} phase is {o_phase} degrees")
            return o_phase
        self.logger.info("Set channel %d phase to %f", ch_num, ch_phase)
        o_phase = self.write_query_float(
            f"ch{ch_num} offset voltage",
            f"FGEN:CHANNEL{ch_num}:PHASE",
            ch_phase,
            "degrees",
        )
        self.logger.info(f"ch{ch_num} phase set to {o_phase} degrees")
        return o_phase

    def play_sine(self) -> None:
        """Compile sine waveform, name waveform and play it."""
        # Disable resetting devices when connecting and closing
        self.dev_mgr.setup_cleanup_enabled = False
        self.dev_mgr.teardown_cleanup_enabled = False

        # Use PyVISA-py backend
        self.dev_mgr.visa_library = PYVISA_PY_BACKEND

        # Create Scope driver object by providing ip address.
        # scope: AWG5200 = self.dev_mgr.add_awg("127.0.0.1")

        # Set Basic Waveform editor plug-in waveform type.
        self.instrument.commands.bwaveform.function.write("SINE")
        # Set peak-to-peak Amplitude value for waveform created by Basic Waveform
        # editor plug-in.
        self.instrument.commands.bwaveform.frequency.write(10e6)
        self.instrument.commands.bwaveform.amplitude.write(250.0e-3)
        # Set Sample Rate for waveform created by Basic Waveform editor plug-in.
        self.instrument.commands.bwaveform.srate.write(1.0e9)
        # Set name of Basic Waveform editor plug-in compiled waveform.
        self.instrument.commands.bwaveform.compile.name.write("Basic_Wfm_sine")

        # self.awg_dev.commands.awgcontrol.

        # Enable state of Basic Waveform editor to either immediately play the waveform
        # after compile or just compile
        self.instrument.commands.bwaveform.compile.play.write("ON")

    def raw_data(self) -> None:
        """Write raw data."""
        record_length = 500
        # sample_rate = 5e6
        # name2 = "pulse"
        pulse_data = np.empty(record_length)
        pulse_data[: int(record_length / 2)] = 1
        pulse_data[int(record_length / 2) :] = -1
        self.logger.debug("Pulse data: %s", str(pulse_data))

        self.instrument.write('wlist:waveform:new "{name2}", {record_length}')

    def run_mode(self, ch_num: int) -> str | None:
        """
        Read the run mode.

        :param ch_num: channel number
        :returns: number of bits
        """
        o_res: str | None = self.read_query_str(
            f"ch{ch_num} run mode", f"SOURCE{ch_num}:RMODE?"
        )
        self.logger.info(f"ch{ch_num} run mode is {o_res}")
        return o_res

    def run_state(self) -> int | None:
        """
        Read run state.

        :returns: run state, i.e. one or zero
        """
        o_mode: int | None = self.read_query_int("run state", "AWGCONTROL:RSTATE?")
        return o_mode

    # pylint: disable-next=too-many-locals,too-many-statements
    def sequencer_phase(self) -> None:  # noqa: C901
        """Do the sequencer thing."""
        self.instrument.timeout = 25000  # type: ignore[attr-defined]
        self.instrument.encoding = "latin_1"  # type: ignore[attr-defined]
        # self.instrument.write_termination = "\n"
        # self.instrument.read_termination = "\n"
        # self._logger.debug("Get ID")
        # print(self.instrument.query("*idn?"))
        self.instrument.write("*rst")
        self.instrument.write("*cls")

        record_length = 500
        # sample_rate = 5e6

        # name1 = "deadTime"
        dead_time_data = np.zeros(record_length)
        self.logger.debug("Dead time data: %s", str(dead_time_data))

        name2 = "pulse"
        pulse_data: Sequence[Any] = np.empty(record_length)  # type: ignore[assignment]
        pulse_data[: int(record_length / 2)] = 1  # type: ignore[index]
        pulse_data[int(record_length / 2) :] = -1  # type: ignore[index]
        self.logger.debug("Pulse data: %s", str(pulse_data))

        self.instrument.write("wlist:waveform:del all")

        seq_name = "Simple Sequence"
        self.instrument.write(f'slist:seq:delete "{seq_name}"')
        # 2 steps, 1 track
        self.instrument.write(f'slist:seq:new "{seq_name}", 2, 2')

        # rcount1 = "once"
        rcount2 = "once"

        # jump1 = "next"
        jump2 = "first"

        transfer_list = [
            # [name1, deadTimeData, rCount1, jump1],
            [name2, pulse_data, rcount2, jump2],
        ]

        i = 1
        for name, data, count, jump in transfer_list:
            self.logger.debug("Waveform %s", name)
            self.instrument.write('wlist:waveform:new "{name}", {record_length}')
            string_arg = f'wlist:waveform:data "{name}", 0, {record_length}, '
            self.logger.debug("Write data %s: %s", string_arg, str(data))
            try:
                # pylint: disable-next=protected-access
                self.instrument._visa_resource.write_binary_values(string_arg, data)
            except BrokenPipeError:
                self.logger.warning("Could not write data")
            try:
                self.instrument.write("*WAI")
            # pylint: disable-next=bare-except
            except:  # noqa: E722
                self.logger.warning("Wait failed")
            try:
                self.instrument.write("*opc?")
            # pylint: disable-next=bare-except
            except:  # noqa: E722
                self.logger.warning("Could not read operation complete")
            self.command(f'slist:seq:step{i}:tasset1:wav "{seq_name}", "{name}"')
            self.command(f'slist:seq:step{i}:tasset2:wav "{seq_name}", "{name}"')
            self.command(f'slist:seq:step{i}:rcount "{seq_name}", {count}')
            self.command(f'slist:seq:step{i}:ejinput "{seq_name}", atrigger')
            self.command(f'slist:seq:step{i}:ejump "{seq_name}", {jump}')
            self.command(f'slist:seq:step{i}:goto "{seq_name}", {jump}')
            i += 1

        self.command("clock:srate {sample_rate}")
        self.command('source1:casset:sequence "{seq_name}", 1')
        self.command('source2:casset:sequence "{seq_name}", 2')
        self.command("output1:state on")
        self.command("output2:state on")
        self.command("awgcontrol:run:immediate")
        self.command("*opc?")

        delay = ["100ps"]

        for dly in delay:
            self.instrument.write(f"source1:skew {dly}")
            self.instrument.write("trigger:immediate atrigger")

        try:
            self.logger.info(self.instrument.query("system:error:all?"))
        # pylint: disable-next=bare-except
        except:  # noqa: E722
            self.logger.warning("Could not read system errors")
        self.instrument.close()

    def signal_path(self, ch_num: int, ch_path: str | None = None) -> str | None:
        """
        Set waveform type.

        :param ch_num: channel number
        :param ch_path: new value to be set, None if only reading
        :returns: current or new signal path
        """
        o_path: str | None = self.read_query_str(
            f"ch{ch_num} type", f"FGEN:CHANNEL{ch_num}:PATH?"
        )
        if ch_path is None:
            self.logger.info("Channel %d type is %s", ch_num, ch_path)
            return o_path
        self.logger.debug("Set channel %d signal path to %s", ch_num, ch_path)
        if ch_path.upper() not in SIGNAL_PATHS:
            self.logger.error("Invalid path %s", ch_path)
            return None
        o_path = self.write_query_str(
            f"ch{ch_num} type", f"FGEN:CHANNEL{ch_num}:PATH", ch_path
        )
        self.logger.info("Channel %d signal path set to %s", ch_num, o_path)
        return o_path

    def start(self) -> None:
        """Start playback."""
        self.logger.info("Start")
        # self.instrument.commands.awgcontrol.run._immediate
        self.instrument.write("AWGCONTROL:START:IMMEDIATE")

    # pylint: disable-next=too-many-locals,too-many-statements
    def status(self, ch_nums: list) -> int:
        """
        Read and display states of AWG.

        :param ch_nums: channel numbers
        :return: zero
        """
        self.logger.info("Status of channel %s", ch_nums)
        plugins = self.read_query_str("plugins", "WPLUGIN:PLUGINS?")
        print(f"{'plugins':40} : {plugins}")
        r_state = self.run_state()
        print(f"{'run state':40} : {AWG_RUN_STATES[r_state]}")  # type: ignore[index]
        e_count = self.error_count()
        print(f"{'error count':40} : {e_count}")
        o_state = self.instrument_mode()
        print(f"{'instrument mode':40} : {o_state}")
        o_state = self.coupled_state()
        print(f"coupled state{' ':27} : {o_state}")
        s_count = self.read_query_int("sequences", "SLIST:SIZE?")
        print(f"{'sequences':40} : {s_count}")

        self.logger.info("Check channels %s", ch_nums)
        for ch_num in ch_nums:
            # Output state
            a_name, a_type = self.asset_name(ch_num)
            print(f"ch{ch_num} {'asset':36} : {a_name} ({a_type})")
            if len(a_name) > 2:
                a_len = self.read_query_int(
                    "asset length", f"WLIST:WAVEFORM:LENGTH? {a_name}"
                )
                print(f"waveform length {a_name:24} : {a_len}")
            o_state = self.output_state(ch_num)  # type: ignore[assignment]
            if o_state:
                print(f"ch{ch_num} {'state':36} : ON")
            else:
                print(f"ch{ch_num} {'state':36} : OFF")
            o_res = self.dac_resolution(ch_num)
            print(f"ch{ch_num} {'resolution':36} : {o_res} bits")
            o_rmode = self.run_mode(ch_num)
            print(f"ch{ch_num} {'run mode':36} : {o_rmode}")
            # Output power
            o_pwr = self.output_power(ch_num)
            print(f"ch{ch_num} {'output power':36} : {o_pwr} dBm")
            # Output voltage
            volt = self.output_voltage(ch_num)
            print(f"ch{ch_num} {'output voltage':36} : {volt} V")
            # DC level
            volt = self.dc_level(ch_num)
            print(f"ch{ch_num} {'DC level':36} : {volt} V")
            # Frequency
            o_hertz = self.frequency(ch_num)
            print(f"ch{ch_num} {'frequency':36} : {o_hertz} Hz")
            # High and low voltages
            h_volt, l_volt = self.high_low(ch_num)
            print(f"ch{ch_num} {'high voltage':36} : {h_volt} V")
            print(f"ch{ch_num} {'low voltage':36} : {l_volt} V")
            # Offset voltage
            volt = self.offset_voltage(ch_num)
            print(f"ch{ch_num} {'offset voltage':36} : {volt} V")
            # Signal path
            o_path = self.signal_path(ch_num)
            print(f"ch{ch_num} {'signal path':36} : {o_path}")
            # Period
            o_perd = self.period(ch_num)
            print(f"ch{ch_num} {'period':36} : {o_perd}")
            # Phase angle
            o_phase = self.phase(ch_num)
            print(f"ch{ch_num} {'phase':36} : {o_phase} degrees")
            # Symmetry
            o_sym = self.symmetry(ch_num)
            print(f"ch{ch_num} {'symmetry':36} : {o_sym} %")
            # Waveform type
            o_typ = self.waveform_function(ch_num)
            print(f"ch{ch_num} {'type':36} : {o_typ}")

        trig_a = self.read_query_int("trig_imp_a", "TRIGGER:IMPEDANCE? ATRIGGER")
        print(f"{'Trigger impedance A':40} : {trig_a} ohm")
        trig_b = self.read_query_int("trig_imp_a", "TRIGGER:IMPEDANCE? ATRIGGER")
        print(f"{'Trigger impedance B':40} : {trig_b} ohm")
        return 0

    def stop(self) -> None:
        """Stop playback."""
        self.logger.info("Stop")
        self.instrument.write("AWGControl:STOP")

    def symmetry(self, ch_num: int, ch_sym: float | None = None) -> float | None:
        """
        Set waveform symmetry.

        :param ch_num: channel number
        :param ch_sym: symmetry percentage
        :returns: current or new symmetry percentage
        """
        o_sym: float | None = self.read_query_int(
            f"ch{ch_num} symmetry", f"FGEN:CHANNEL{ch_num}:SYMMETRY?"
        )
        if ch_sym is None:
            self.logger.info(f"ch{ch_num} symmetry is {o_sym}")
            return o_sym
        self.logger.info("Set channel %d symmetry to %d", ch_num, ch_sym)
        o_sym = self.write_query_float(
            f"ch{ch_num} DC voltage",
            f"FGEN:CHANNEL{ch_num}:SYMMETRY",
            ch_sym,
            "V",
        )
        self.logger.info(f"ch{ch_num} symmetry set to {o_sym}")
        return o_sym

    def waveform_function(self, ch_num: int, ch_type: str | None = None) -> str | None:
        """
        Set waveform type.

        :param ch_num: channel number
        :param ch_type: new value to be set, None if only reading
        :returns: current or new waveform type
        """
        self.logger.debug("Set channel %d waveform to %s", ch_num, ch_type)
        o_typ: str | None = self.read_query_str(
            f"ch{ch_num} type", f"FGEN:CHANNEL{ch_num}:TYPE?"
        )
        if ch_type is None:
            self.logger.info("Channel %d type is %s", ch_num, ch_type)
            return o_typ
        if ch_type.upper() not in AWG_TYPES:
            self.logger.error("Invalid type %s", ch_type)
            return None
        o_typ = self.write_query_str(
            f"ch{ch_num} type", f"FGEN:CHANNEL{ch_num}:TYPE", ch_type
        )
        self.logger.info("Channel %d type set to %s", ch_num, o_typ)
        return o_typ

    def upload_sine_wave(self, rec_len: int = 50000, check_err: bool = False) -> None:
        """
        Upload sine wave data.

        :param rec_len: record length
        :param check_err: flag to check errors
        """
        # rm = pyvisa.ResourceManager('@py')
        # awg = rm.open_resource('TCPIP::10.165.3.3::INSTR')
        self.logger.info("Upload sine waves with %d points", rec_len)

        awg = self.instrument
        awg.timeout = 50000
        awg.encoding = "latin_1"
        awg.write_termination = "\n"
        awg.read_termination = "\n"
        print(awg.query("*idn?"))
        awg.write("*rst")
        awg.write("*cls")

        # recordLength = 50000
        sample_rate = 2.5e9

        name1 = "SineTest"
        name2 = "SineTestLow"

        # self.logger.info("Delete waveforms")
        # awg.write('wlist:waveform:del all')

        dur = 50000
        f0 = 26e6
        fs = 2.5e9
        t = np.arange(dur)
        sin_freq = f0 / fs

        self.logger.info("Sine wave frequency %f (%d samples)", sin_freq, rec_len)
        sinusoid = np.sin(2 * np.pi * t * sin_freq)
        sinusoid2 = 0.1 * np.sin(2 * np.pi * t * sin_freq)

        awg.write(f"clock:srate {sample_rate}")

        self.logger.info("Write %s", name1)
        awg.write(f'wlist:waveform:new "{name1}", {rec_len}')
        awg.write_binary_values(f'WLISt:WAVeform:DATA "{name1}",', sinusoid)

        self.logger.info("Write %s", name2)
        awg.write(f'wlist:waveform:new "{name2}", {rec_len}')
        awg.write_binary_values(f'WLISt:WAVeform:DATA "{name2}",', sinusoid2)

        awg.write(f'source1:casset:WAVeform "{name1}"')
        awg.write("output1:state on")

        awg.write(f'source2:casset:WAVeform "{name2}"')
        awg.write("output2:state on")

        self.logger.info("Run")
        awg.write("awgcontrol:run:immediate")

        if check_err:
            self.logger.info("Check errors")
            print(awg.query("system:error:all?"))
        awg.close()
