"""Generate waveform data for use with AWG e.a."""

# pylint: disable=useless-suppression
# pylint: disable=bad-option-value
# pylint: disable=invalid-name,too-many-arguments,too-many-positional-arguments

import logging
from typing import Any

import matplotlib.pyplot as plt
import numpy as np

logging.getLogger("pyvisa").setLevel(logging.WARNING)
logging.basicConfig(level=logging.WARNING)
_module_logger = logging.getLogger("awg_ctl")
_module_logger.setLevel(logging.INFO)


# pylint: disable-next=too-many-locals
def sine_wave(
    w_ampl: float,
    w_freq: float,
    w_delay: float,
    w_delta: float,
    w_total: float,
    w_offset: float,
) -> tuple[Any, Any]:
    """
    Generate a sine wave.

    :param w_ampl: amplitude
    :param w_freq: frequency
    :param w_delay: delay
    :param w_delta: timestep
    :param w_total: total time
    :param w_offset: Y-axis offset
    :returns: X and Y axis data
    """
    a = w_ampl
    phi = 0

    n_samples = int(w_total / w_delta)  # number of samples

    w_t1 = np.linspace(0.0, w_total, n_samples)
    _module_logger.info("Time data from %f to %f (%d)", w_t1[0], w_t1[-1], len(w_t1))
    c_time = w_total
    c_end = w_total + 4 * w_delay
    w_sine1 = a * np.sin(2 * np.pi * w_freq * w_t1 + phi) + w_offset
    while c_time <= c_end:
        c_time += w_delta
        np.append(w_t1, [c_time])
        np.append(w_sine1, [0.0])
    _module_logger.info("Generated %d of %s : %s", len(w_sine1), type(w_sine1), w_sine1)

    w_time = np.linspace(0.0, w_total * 2, n_samples * 2)
    w_signal = np.empty((n_samples * 2,))
    for n in range(0, n_samples):
        w_signal[n] = w_sine1[n]

    for n in range(n_samples, n_samples * 2):
        w_signal[n] = 0.0

    return w_time, w_signal


# pylint: disable-next=too-many-locals
def square_wave(
    w_ampl: float,
    w_freq: float,
    w_delay: float,
    w_delta: float,
    w_total: float,
    w_offset: float,
) -> tuple[Any, Any]:
    """
    Generate a sine wave.

    :param w_ampl: amplitude
    :param w_freq: frequency
    :param w_delay: delay
    :param w_delta: timestep
    :param w_total: total time
    :param w_offset: Y-axis offset
    :returns: X and Y axis data
    """
    a = w_ampl
    phi = 0

    n_samples = int(w_total / w_delta)  # number of samples

    w_t1 = np.linspace(0.0, w_total, n_samples)
    _module_logger.info("Time data from %f to %f (%d)", w_t1[0], w_t1[-1], len(w_t1))
    c_time = w_total
    c_end = w_total + 4 * w_delay
    w_square1 = a * np.sin(2 * np.pi * w_freq * w_t1 + phi) + w_offset
    # + 0.3333 * a * np.sin(6 * w_ampl * np.pi * w_freq * w_t1 + phi) + w_offset \
    # + 0.2 * a * np.sin(10 * w_ampl * np.pi * w_freq * w_t1 + phi) + w_offset \
    # + 0.1429 * a * np.sin(14 * w_ampl * np.pi * w_freq * w_t1 + phi) + w_offset
    for n in range(1, 7):
        w_square1 += (
            a * np.sin(2 * (2 * n + 1) * np.pi * w_freq * w_t1 + phi) / (2 * n + 1)
            + w_offset
        )
    while c_time <= c_end:
        c_time += w_delta
        np.append(w_t1, [c_time])
        np.append(w_square1, [0.0])
    _module_logger.info(
        "Generated %d of %s : %s", len(w_square1), type(w_square1), w_square1
    )

    w_time = np.linspace(0.0, w_total * 2, n_samples * 2)
    w_signal = np.empty((n_samples * 2,))
    for n in range(0, n_samples):
        w_signal[n] = w_square1[n]

    for n in range(n_samples, n_samples * 2):
        w_signal[n] = 0.0

    return w_time, w_signal


# pylint: disable-next=too-many-locals
def noisy_sine_wave(
    w_ampl: float,
    w_freq: float,
    w_delay: float,
    w_delta: float,
    w_total: float,
    w_offset: float,
) -> tuple[Any, Any]:
    """
    Generate a noisy sine wave.

    :param w_ampl: amplitude
    :param w_freq: frequency
    :param w_delay: delay
    :param w_delta: timestep
    :param w_total: total time
    :param w_offset: Y-axis offset
    :returns: X and Y axis data
    """
    phi = 0

    n_ampl = w_ampl / 3
    # number of samples
    n_samples = int(w_total / w_delta)

    w_t1 = np.linspace(0.0, w_total, n_samples)
    _module_logger.info("Time data from %f to %f (%d)", w_t1[0], w_t1[-1], len(w_t1))
    c_time = w_total
    c_end = w_total + 4 * w_delay
    w_sine1 = (
        w_ampl * np.sin(2 * np.pi * w_freq * w_t1 + phi)
        + w_offset
        + n_ampl * np.random.normal(0, 0.3, n_samples)
    )
    while c_time <= c_end:
        c_time += w_delta
        np.append(w_t1, [c_time])
        np.append(w_sine1, [0.0])
    _module_logger.info("Generated %d of %s : %s", len(w_sine1), type(w_sine1), w_sine1)

    w_time = np.linspace(0.0, w_total * 2, n_samples * 2)
    w_signal = np.empty((n_samples * 2,))
    for n in range(0, n_samples):
        w_signal[n] = w_sine1[n]

    for n in range(n_samples, n_samples * 2):
        w_signal[n] = 0.0

    return w_time, w_signal


def wave_delay(
    w_wave: Any,
    w_delay: float,
    w_delta: float,
    w_offset: float,
) -> Any:
    """
    Generate a delayed wave.

    :param w_wave: wave data
    :param w_delay: delay
    :param w_delta: timestep
    :param w_offset: Y-axis offset
    :returns: X and Y axis data
    """
    n_total = len(w_wave)
    n_start = int(w_delay / w_delta)
    _module_logger.info("Delay starts at %d of %d", n_start, n_total)

    w_signal = np.empty((n_total,))

    _module_logger.info("Zero from %d to %d", 0, n_start)
    for n in range(0, n_start):
        w_signal[n] = w_offset
    _module_logger.info("Wave from %d to %d", n_start, n_total)
    for n in range(n_start, n_total):
        w_signal[n] = w_wave[n - n_start] + w_offset
    #     np.append(w_signal1, w_sinel1[n])
    # for val in w_sinel1:
    #     np.append(w_signal1, val)
    _module_logger.info(
        "Generated %d of %s : %s", len(w_signal), type(w_signal), w_signal
    )

    # for n in range(0, n_total):
    #     if w_time[n] < w_delay:
    #         w_signal
    return w_signal


def plot_wave(total: float, time: float, plot_waves: Any) -> None:
    """
    Plot waves.

    :param total: total thing
    :param time: X-axis data
    :param plot_waves: Y-axis data
    """
    _module_logger.info("PLOT total: %f", total)
    for signal in plot_waves:
        plt.plot(time, signal)
    plt.xlim(0, total)
    plt.xlabel("Time / s")
    plt.ylabel("Amplitude")
    plt.show()


def generate_waves() -> None:
    """Generate waveforms."""
    w_freq = 1e6  # frequency
    w_perd = 1 / w_freq  # period
    delta_t = w_perd / 2500  # timestep
    t_total = 5 * w_perd  # total
    delay1 = 0.1 * w_perd
    delay2 = 0.2 * w_perd
    delay3 = 0.3 * w_perd

    waves = []

    t1, wave1 = square_wave(1, w_freq, 0.0, delta_t, t_total, 0)
    # t1, wave1 = sine_wave(2, f, 0.0, dt, Ttot, 0)
    # t1, wave1 = noisy_sine_wave(1, f, 0.0, dt, Ttot, 0)
    waves.append(wave1)

    wave2 = wave_delay(wave1, delay1, delta_t, -2)
    waves.append(wave2)

    wave3 = wave_delay(wave1, delay2, delta_t, -4)
    waves.append(wave3)

    wave4 = wave_delay(wave1, delay3, delta_t, -6)
    waves.append(wave4)

    plot_wave(t_total * 2, t1, waves)


if __name__ == "__main__":
    generate_waves()
