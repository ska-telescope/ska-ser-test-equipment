# -*- coding: utf-8 -*-
#
# (c) 2022 CSIRO.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""A Tango device for an arbitrary waveform generator (AWG)."""
from __future__ import annotations

import logging
import os
import socket
import sys
from typing import Any

import tango
from ska_control_model import ResultCode
from tango.server import command

from ska_ser_test_equipment.base.base_device import TestEquipmentBaseDevice
from ska_ser_test_equipment.mixins import SCPIWriteQueryMixin

try:
    from ..interface_definitions import InterfaceDefinitionFactory
except ImportError:
    from ska_ser_test_equipment.interface_definitions import InterfaceDefinitionFactory

try:
    from .awg_component_manager import AwgComponentManager
except ImportError:
    from ska_ser_test_equipment.awg.awg_component_manager import AwgComponentManager

DevVarLongStringArrayType = tuple[list[ResultCode], list[str]]

__all__ = ["AwgDevice", "main"]

log_level_str = os.getenv("LOG_LEVEL", "WARNING").upper()
if log_level_str == "DEBUG":
    LOG_LEVEL = logging.DEBUG
elif log_level_str == "INFO":
    LOG_LEVEL = logging.INFO
elif log_level_str == "WARNING":
    LOG_LEVEL = logging.WARNING
elif log_level_str == "ERROR":
    LOG_LEVEL = logging.ERROR
else:
    LOG_LEVEL = logging.WARNING
logging.basicConfig(level=LOG_LEVEL)
_module_logger = logging.getLogger(__name__)
INTERACTIVE = False


class AwgDevice(TestEquipmentBaseDevice, SCPIWriteQueryMixin):
    """A Tango device for an arbitrary waveform generator (AWG)."""

    # --------------
    # Initialization
    # --------------

    PowerLimit = tango.server.device_property(
        dtype=float, default_value=sys.float_info.max
    )

    # --------------
    # Commands
    # --------------

    @command()
    @tango.DebugIt()
    def play(self) -> None:
        """Play the configured waveform on the configured outputs of the AWG."""
        self.component_manager.write_command("play")

    @command()
    @tango.DebugIt()
    def stop(self) -> None:
        """Stop playing the configured waveform on the configured outputs of the AWG."""
        self.component_manager.write_command("stop")

    def create_component_manager(self) -> AwgComponentManager:
        """
        Create and return a component manager for this device.

        :returns: a component manager for this device.
        """
        if INTERACTIVE:
            # pylint: disable-next=invalid-name
            self.Protocol = os.getenv("SIMULATOR_PROTOCOL", "tcp").lower()
            # pylint: disable-next=invalid-name
            self.Model = os.getenv("SIMULATOR_MODEL", "AWG5208").upper()
            # pylint: disable-next=invalid-name
            self.Host = os.getenv("SIMULATOR_HOST", socket.gethostname())
            # pylint: disable-next=invalid-name
            self.Port = int(os.getenv("SIMULATOR_PORT", "9100"))
        self.logger.info(
            "Start %s device for %s on %s:%d",
            self.Protocol,
            self.Model,
            self.Host,
            self.Port,
        )
        # pylint: disable-next=attribute-defined-outside-init
        self._interface_definition = InterfaceDefinitionFactory()(self.Model)
        return AwgComponentManager(
            self._interface_definition,
            self.Protocol,
            self.Host,
            self.Port,
            self.logger,
            self._communication_state_changed,
            self._component_state_changed,
            update_rate=self.UpdateRate,
        )

    def _get_attribute_kwargs(self, name: str, spec: dict[str, Any]) -> dict[str, Any]:
        kwargs = super()._get_attribute_kwargs(name, spec)
        if name == "power_dbm":
            kwargs["max_value"] = min(
                kwargs.get("max_value", sys.float_info.max), self.PowerLimit
            )
        return kwargs


# ----------
# Run server
# ----------
def main(args: Any = None, **kwargs: Any) -> int:
    """
    Launch a `AwgDevice` server instance.

    :param args: arguments to the AWG device.
    :param kwargs: keyword arguments to the server.

    :returns: the Tango server exit code.
    """
    if _module_logger.isEnabledFor(logging.DEBUG):
        _module_logger.warning("Log level is DEBUG")
    elif _module_logger.isEnabledFor(logging.INFO):
        _module_logger.warning("Log level is INFO")
    elif _module_logger.isEnabledFor(logging.WARNING):
        _module_logger.warning("Log level is WARNING")
    else:
        pass
    return tango.server.run((AwgDevice,), args=args, **kwargs)


if __name__ == "__main__":
    print("*** Tango device for AWG ***")
    INTERACTIVE = True
    main()
