"""Mixins for component managers."""
import threading
from typing import Protocol

from ska_ser_scpi import AttributeClient
from ska_ser_scpi.scpi_payload import ScpiRequest, ScpiResponse


class ComponentManagerProtocol(Protocol):  # pylint: disable=too-few-public-methods
    """
    Protocol of component manager which allows the scpi mixin to be mixed in.

    We supply types here for the used attributes.
    """

    _write_lock: threading.Lock
    _attribute_client: AttributeClient


class SCPISendReceiveMixin:  # pylint: disable-next=too-few-public-methods
    """Allow a component manager to send arbitrary scpi commands."""

    def send_receive_scpi(
        self: ComponentManagerProtocol, scpi_request: ScpiRequest
    ) -> ScpiResponse:
        """
        Send a scpi request, and receive an scpi response.

        :param scpi_request: details of the scpi request to be
            sent.

        :returns: details of the scpi response.
        """
        with self._write_lock:
            # pylint: disable-next=protected-access
            scpi_response = self._attribute_client._scpi_client.send_receive(
                scpi_request
            )

        return scpi_response
