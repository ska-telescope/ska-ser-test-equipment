"""Mixins for devices."""

from typing import Any

import tango
from ska_ser_scpi.scpi_payload import ScpiRequest, ScpiResponse
from tango.server import command


class SCPIWriteQueryMixin:
    """
    Enables raw scpi commands/query execution on any device.

    NB. The device's component manager needs to mix in SCPISendReceiveMixin
    also to add the send_receive_scpi method.
    """

    component_manager: Any

    @command(dtype_in=str)
    @tango.DebugIt()
    def Write(self, scpi_cmd: str) -> None:  # pylint: disable=invalid-name
        """
        Run an arbitrary scpi command (not expecting a return value).

        :param scpi_cmd: - the scpi command to run.

        """
        scpi_request = ScpiRequest()
        scpi_request.add_setop(scpi_cmd)
        self.component_manager.send_receive_scpi(scpi_request)

    @command(dtype_in=str, dtype_out=str)
    @tango.DebugIt()
    def Query(self, scpi_cmd: str) -> str:  # pylint: disable=invalid-name
        """
        Run an arbitrary scpi query.

        Values returned are alway strings which must converted by the client.

        :param scpi_cmd: The scpi query to run.
        :returns: The result of the query as a string.

        """
        if scpi_cmd.endswith("?"):
            scpi_cmd = scpi_cmd[:-1]

        scpi_request = ScpiRequest()
        scpi_request.add_query(scpi_cmd)
        scpi_response: ScpiResponse = self.component_manager.send_receive_scpi(
            scpi_request
        )
        return scpi_response.responses[scpi_cmd].decode()
