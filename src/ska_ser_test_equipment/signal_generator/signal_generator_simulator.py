#!/usr/bin/env python
"""Implementation of a simple SCPI simulator for the signal generator."""
import logging
import os
from socket import gethostname
from typing import Dict, Final

from ska_ser_devices.client_server import TcpServer
from ska_ser_scpi import ScpiSimulator, SupportedAttributeType

try:
    from ..interface_definitions import InterfaceDefinitionFactory
except ImportError:
    from ska_ser_test_equipment.interface_definitions import InterfaceDefinitionFactory

logging.basicConfig(level=logging.DEBUG)
_module_logger = logging.getLogger(__name__)


class SignalGeneratorSimulator(ScpiSimulator):
    """A concrete simulator class."""

    DEFAULTS: Final[Dict[str, SupportedAttributeType]] = {
        "rf_output_on": False,
        "power_dbm": -10.0,
        "frequency": 50000000.0,
        "query_error": False,
        "device_error": False,
        "execution_error": False,
        "command_error": False,
        "power_cycled": False,
    }

    def __init__(
        self,
        model: str,
        **kwargs: SupportedAttributeType,
    ) -> None:
        """
        Initialise a new instance.

        :param model: the model identifier to use.
        :param kwargs: initial values for simulator attributes; where an
            initial value is not provided for an attribute, a default
            value will be used.
        """
        interface_definition = InterfaceDefinitionFactory()(model)
        _module_logger.info("Interface definition %s", interface_definition)

        initial_values = kwargs
        for key, value in self.DEFAULTS.items():
            initial_values.setdefault(key, value)

        super().__init__(interface_definition, initial_values)

    def reset(self) -> None:
        """Reset to factory default values."""
        # TODO: Different models will reset to different values.
        self.set_attribute("frequency", 10000000.0)
        self.set_attribute("power_dbm", 0.0)
        self.set_attribute("rf_output_on", True)


def main() -> None:
    """Run the socketserver main loop."""
    model = os.getenv("SIMULATOR_MODEL", "TSG4104A").upper()
    host = os.getenv("SIMULATOR_HOST", gethostname())
    port = int(os.getenv("SIMULATOR_PORT", "5025"))
    _module_logger.info("Start simulator for %s on %s:%d", model, host, port)

    simulator = SignalGeneratorSimulator(model)
    server = TcpServer(host, port, simulator)
    with server:
        server.serve_forever()


if __name__ == "__main__":
    main()
