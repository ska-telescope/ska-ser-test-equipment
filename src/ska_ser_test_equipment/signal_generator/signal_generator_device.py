# -*- coding: utf-8 -*-
#
# (c) 2022 CSIRO.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""This module provides a Tango device for a signal generator."""
import os
import socket
import sys
from typing import Any

import tango
import tango.server

from ska_ser_test_equipment.base.base_device import TestEquipmentBaseDevice
from ska_ser_test_equipment.mixins import SCPIWriteQueryMixin

try:
    from ..interface_definitions import InterfaceDefinitionFactory
    from .signal_generator_component_manager import SignalGeneratorComponentManager
except ImportError:
    from ska_ser_test_equipment.interface_definitions import InterfaceDefinitionFactory

    # pylint: disable-next=line-too-long
    from ska_ser_test_equipment.signal_generator.signal_generator_component_manager import (  # noqa: E501
        SignalGeneratorComponentManager,
    )

__all__ = ["SignalGeneratorDevice", "main"]


class SignalGeneratorDevice(TestEquipmentBaseDevice, SCPIWriteQueryMixin):
    """A Tango device for monitor and control of a signal generator."""

    # --------------
    # Initialization
    # --------------

    PowerLimit = tango.server.device_property(
        dtype=float, default_value=sys.float_info.max
    )

    def create_component_manager(self) -> SignalGeneratorComponentManager:
        """
        Create and return a component manager for this device.

        :returns: a component manager for this device.
        """
        if self.Model is None:
            # pylint: disable-next=invalid-name
            self.Protocol = os.getenv("SIMULATOR_PROTOCOL", "tcp").lower()
            # pylint: disable-next=invalid-name
            self.Model = os.getenv("SIMULATOR_MODEL", "TSG4104A").upper()
            # pylint: disable-next=invalid-name
            self.Host = os.getenv("SIMULATOR_HOST", socket.gethostname())
            # pylint: disable-next=invalid-name
            self.Port = int(os.getenv("SIMULATOR_PORT", "5025"))
        # pylint: disable-next=attribute-defined-outside-init
        self._interface_definition = InterfaceDefinitionFactory()(self.Model)
        return SignalGeneratorComponentManager(
            self._interface_definition,
            self.Protocol,
            self.Host,
            self.Port,
            self.logger,
            self._communication_state_changed,
            self._component_state_changed,
            update_rate=self.UpdateRate,
        )

    def _get_attribute_kwargs(self, name: str, spec: dict[str, Any]) -> dict[str, Any]:
        kwargs = super()._get_attribute_kwargs(name, spec)
        if name == "power_dbm":
            kwargs["max_value"] = min(
                kwargs.get("max_value", sys.float_info.max), self.PowerLimit
            )
        return kwargs


# ----------
# Run server
# ----------
def main(args: Any = None, **kwargs: Any) -> int:
    """
    Launch a `SignalGeneratorDevice` server instance.

    :param args: arguments to the signal generator device.
    :param kwargs: keyword arguments to the server.

    :returns: the Tango server exit code.
    """
    return tango.server.run((SignalGeneratorDevice,), args=args, **kwargs)


if __name__ == "__main__":
    main()
