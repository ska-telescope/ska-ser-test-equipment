"""This module provides a factory for interface definitions."""
from __future__ import annotations

import pkgutil

import yaml
from ska_ser_scpi import InterfaceDefinitionType, expand_read_write_command


class InterfaceDefinitionFactory:  # pylint: disable=too-few-public-methods
    """Factory that returns a SCPI interface definition for a given model."""

    def __init__(self) -> None:
        """Initialise a new instance."""
        self._interfaces = {
            "AWG5208": "awg/tektronix_awg5208.yaml",
            "AWG5208I": "awg_visa/tektronix_awg5208i.yaml",
            "RC4DAT6G95": "prog_attenuator/mini_circuits_rc4dat_6g_95.yaml",
            "RC1DAT800030": "prog_attenuator/mini_circuits_rc1dat_8000_30.yaml",
            "SMB100A": "signal_generator/rohde_and_schwarz_smb100a.yaml",
            "TGR2051": "signal_generator/aimtti_tgr2051.yaml",
            "TSG4104A": "signal_generator/tektronix_tsg4104a.yaml",
            "SPECMON26B": "spectrum_analyser/tektronix_specmon26b.yaml",
            "MSO54": "oscilloscope/tektronix_mso54.yaml",
            "MSO64B": "oscilloscope/tektronix_mso64b.yaml",
            "MS2090A": "spectrum_analyser_anritsu/anritsu_ms2090a.yaml",
            "RSA5103B": "spectrum_analyser/tektronix_rsa5103b.yaml",
            "ZTRC4SPDTA18": "spdt/mini_circuits_ztrc_4spdt_a18.yaml",
            "ZTRC8SPDTA18": "spdt/mini_circuits_ztrc_8spdt_a18.yaml",
            "RC2SP6TA12": "sp6t/mini_circuits_rc_2sp6t_a12.yaml",
        }

    def __call__(self, model: str) -> InterfaceDefinitionType:
        """
        Get an interface definition for the specified model.

        :param model: the name of the model for which an interface
            definition is required.

        :returns: SCPI interface definition for the model.
        """
        file_name = self._interfaces[model]
        interface_definition_data = pkgutil.get_data(
            "ska_ser_test_equipment", file_name
        )
        assert interface_definition_data is not None  # for the type-checker
        interface_definition: InterfaceDefinitionType = yaml.safe_load(
            interface_definition_data
        )

        interface_definition = expand_read_write_command(interface_definition)
        return interface_definition
