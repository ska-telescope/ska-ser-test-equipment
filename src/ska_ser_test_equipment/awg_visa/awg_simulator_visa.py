#!/usr/bin/env python
"""A simple SCPI simulator for an arbitrary waveform generator."""
import logging
import os
import time
from socket import gethostname
from typing import Dict, Final

# from ska_ser_devices.client_server.tcp import (
#     _TcpServerRequestHandler,
#     DEFAULT_BUFFER_SIZE,
#     TcpServer,
# )
from ska_ser_devices.client_server.tcp import TcpServer
from ska_ser_scpi import ScpiSimulator, SupportedAttributeType

try:
    from ..interface_definitions import InterfaceDefinitionFactory
except ImportError:
    from ska_ser_test_equipment.interface_definitions import InterfaceDefinitionFactory

log_level_str = os.getenv("LOG_LEVEL", "WARNING").upper()
if log_level_str == "DEBUG":
    LOG_LEVEL = logging.DEBUG
elif log_level_str == "INFO":
    LOG_LEVEL = logging.INFO
elif log_level_str == "WARNING":
    LOG_LEVEL = logging.WARNING
elif log_level_str == "ERROR":
    LOG_LEVEL = logging.ERROR
else:
    LOG_LEVEL = logging.WARNING
logging.basicConfig(level=LOG_LEVEL)
_module_logger = logging.getLogger(__name__)

AWG_TYPES = [
    "SINE",
    "SQUARE",
    "TRIANGLE",
    "NOISE",
    "DC",
    "GAUSSIAN",
    "EXPRISE",
    "EXPDECAY",
    "NONE",
]

CLOCK_SOURCES = ["INTERNAL", "EFIXED", "EVARIABLE", "EXTERNAL"]


class AwgSimulatorVisa(ScpiSimulator):
    """A concrete simulator class."""

    DEFAULTS: Final[Dict[str, SupportedAttributeType]] = {
        "instrument_mode": "AWG",
        "clock_source": "INT",
        "clock_sample_rate": 2.5e9,
        "query_error": False,
        "device_error": False,
        "execution_error": False,
        "command_error": False,
        "power_cycled": False,
        "playing": 0,
        **{f"channel{chan}_asset": f"SINE{chan}00" for chan in range(1, 9)},
        **{f"channel{chan}_type": AWG_TYPES[chan - 1] for chan in range(1, 9)},
        **{f"channel{chan}_phase": 0.0 for chan in range(1, 9)},
        **{f"channel{chan}_freq": float(f"{chan}e6") for chan in range(1, 9)},
        **{
            f"channel{chan}_amplitude_power": float(f"0.{chan}") for chan in range(1, 9)
        },
        **{f"channel{chan}_output_state": True for chan in range(1, 9)},
    }

    def __init__(
        self,
        model: str,
        **kwargs: SupportedAttributeType,
    ) -> None:
        """
        Initialise a new instance.

        :param model: the model identifier to use.
        :param kwargs: initial values for simulator attributes; where an
            initial value is not provided for an attribute, a default
            value will be used.
        """
        interface_definition = InterfaceDefinitionFactory()(model)
        _module_logger.info("Interface definition %s", interface_definition)

        initial_values = kwargs
        for key, value in self.DEFAULTS.items():
            initial_values.setdefault(key, value)

        super().__init__(interface_definition, initial_values)

    def reset(self) -> None:
        """Reset to factory default values."""
        _module_logger.info("Reset to default values")
        self.set_attribute("instrument_mode", self.DEFAULTS["instrument_mode"])
        self.set_attribute("clock_source", self.DEFAULTS["clock_source"])
        self.set_attribute("clock_sample_rate", self.DEFAULTS["clock_sample_rate"])
        self.set_attribute("playing", self.DEFAULTS["playing"])

        for i in range(1, 9):
            self.set_attribute(f"channel{i}_type", self.DEFAULTS[f"channel{i}_type"])
            self.set_attribute(f"channel{i}_freq", self.DEFAULTS[f"channel{i}_freq"])
            self.set_attribute(f"channel{i}_phase", self.DEFAULTS[f"channel{i}_phase"])
            self.set_attribute(
                f"channel{i}_amplitude_power",
                self.DEFAULTS[f"channel{i}_amplitude_power"],
            )
            self.set_attribute(
                f"channel{i}_output_state", self.DEFAULTS[f"channel{i}_output_state"]
            )

    def play(self) -> None:
        """Simulate the AWG Play command."""
        _module_logger.debug("Play")
        self.set_attribute("playing", 2)

    def stop(self) -> None:
        """Simulate the AWG Stop command."""
        _module_logger.debug("Stop")
        self.set_attribute("playing", 0)

    def __repr__(self) -> str:
        """
        Represent this thing as a string.

        :returns: string thing
        """
        return f"AWG: {self.get_attribute('identity')}"


def main() -> None:
    """Run the socketserver main loop."""
    model = os.getenv("SIMULATOR_MODEL", "AWG5208I").upper()
    host = os.getenv("SIMULATOR_HOST", gethostname())
    port = int(os.getenv("SIMULATOR_PORT", "4000"))

    _module_logger.info("Start AWG simulator for %s on %s:%d", model, host, port)
    simulator = AwgSimulatorVisa(model)
    n_try = 0
    n_max = 5
    server = None
    while n_try <= n_max:
        n_try += 1
        try:
            server = TcpServer(host, port, simulator)
        except OSError as os_err:
            _module_logger.error(str(os_err))
            _module_logger.warning("Retry %d of %d", n_try, n_max)
            time.sleep(15)
            continue
        break
    if server is not None:
        with server:
            server.serve_forever()


if __name__ == "__main__":
    print("*** Simulator for arbitrary waveform generator ***")
    try:
        main()
    except KeyboardInterrupt:
        _module_logger.error("Interrupted")
