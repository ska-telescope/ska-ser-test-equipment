"""Mixin class for setting up channels on AWG."""

import logging
import math
from typing import Any

from ska_ser_test_equipment.awg_visa.tektronix_awg_definitions import CHANNEL_COUNT


class TektronixChannelsMixin:
    """Mix in the channels."""

    logger: logging.Logger
    _clock_rate: float = float("nan")

    @property
    def clock_sample_rate(self) -> float:
        """
        Read clock rate.

        CLOCK (114)

        This command sets or returns the sample rate for the clock. This command is not
        valid if CLOCk:SOURce is set to EXTernal.

        :returns: clock rate
        """
        self._clock_rate = self.read_query_float(  # type: ignore[attr-defined]
            "clock_rate", "CLOCK:SRATE?"
        )
        return self._clock_rate

    @clock_sample_rate.setter
    def clock_sample_rate(self, value: float) -> None:
        """
        Update clock rate.

        :param value: new value for clock rate
        """
        self._clock_rate = value
        self.write_query_float(  # type: ignore[attr-defined]
            "clock_rate", "CLOCK:SRATE", value, "S/s"
        )

    def channel_asset(self, ch_num: int) -> Any:
        """
        Read name, type and length for asset (waveform or sequence) assigned to channel.

        :param ch_num: channel number
        :returns: asset name, type and length
        """
        if ch_num < 1 or ch_num > CHANNEL_COUNT:
            self.logger.error("Invalid channel number %d", ch_num)
            return "", "", 0
        a_name: str | None = self.read_query_str(  # type: ignore[attr-defined]
            f"ch{ch_num} type", f"SOURCE{ch_num}:CASSET?"
        )
        if a_name is None:
            self.logger.error("Could not read asset name")
            return "", "", 0
        a_name = a_name.replace('"', "")
        a_type: str | None = self.read_query_str(  # type: ignore[attr-defined]
            f"ch{ch_num} type", f"SOURCE{ch_num}:CASSET:TYPE?"
        )
        if a_type is not None:
            a_type = a_type.upper()
        a_len: int | None
        if a_type == "SEQ":
            a_len = 0
        else:
            a_len = self.read_query_int(  # type: ignore[attr-defined]
                f"asset {a_name} length", f'WLIST:WAVEFORM:LENGTH? "{a_name}"'
            )
            if a_len is None:
                a_len = 0
        self.logger.info(
            "Channel %d asset is %s (%s) %d samples", ch_num, a_name, a_type, a_len
        )
        return a_name, a_type, a_len

    def source_asset_name(self, ch_num: int, value: str | None = None) -> Any:
        """
        Get asset name.

        SOURCE (250)

        This command returns the asset name (waveform or sequence) assigned to the
        specified channel.

        :param ch_num: channel/source number
        :param value: change to this
        :returns: string value of asset name
        """
        self.logger.debug("Read source %d asset name", ch_num)
        if ch_num < 1 or ch_num > CHANNEL_COUNT:
            self.logger.error("Invalid source number %d", ch_num)
            return ""
        if value is not None:
            self.write_command(  # type: ignore[attr-defined]
                f"source{ch_num}_asset_name", f"SOURCE{ch_num}:CASSET {value}"
            )
        return self.read_query_str(  # type: ignore[attr-defined]
            f"source{ch_num}_asset_name", f"SOURCE{ch_num}:CASSET?"
        )

    def source_asset_type(self, ch_num: int, value: str | None = None) -> str | None:
        """
        Get asset type.

        SOURCE (253)

        This command returns the type of the asset (waveform or sequence) assigned
        to a channel.

        WAV – a waveform is assigned to the specified channel.
        SEQ – a sequence is assigned to the specified channel.
        NONE – nothing is assigned to the specified channel.

        :param ch_num: channel/source number
        :param value: change to this
        :returns: string value of asset type
        """
        if ch_num < 1 or ch_num > CHANNEL_COUNT:
            self.logger.error("Invalid channel number %d", ch_num)
            return ""
        if value is not None:
            self.write_command(  # type: ignore[attr-defined]
                f"source{ch_num}_asset_type", f"SOURCE{ch_num}:CASSET:TYPE {value}"
            )
        return self.read_query_str(  # type: ignore[attr-defined]
            f"source{ch_num}_asset_type", f"SOURCE{ch_num}:CASSET:TYPE?"
        )

    def channel_output_state(
        self, ch_num: int, ch_state: bool | None = None
    ) -> bool | None:
        """
        Set output state.

        OUTPUT (208)

        This command sets or returns the output state of the specified channel.

        This is a blocking command.

        :param ch_num: channel number
        :param ch_state: new value to be set, None if only reading
        :returns: current or new output state
        """
        if ch_num < 1 or ch_num > CHANNEL_COUNT:
            self.logger.error("Invalid channel number %d", ch_num)
            return None
        o_state: bool | None = self.read_query_state(  # type: ignore[attr-defined]
            f"ch{ch_num} output state", f"OUTPUT{ch_num}:STATE?"
        )
        if ch_state is None:
            self.logger.info(f"ch{ch_num} state is {o_state}")
            return o_state
        if ch_state:
            self.logger.info("Set channel %d state to ON", ch_num)
            scpi_tx = "ON"
        else:
            self.logger.info("Set channel %d state to OFF", ch_num)
            scpi_tx = "OFF"
        w_state = self.write_query_state(  # type: ignore[attr-defined]
            f"ch{ch_num} state", f"OUTPUT{ch_num}:STATE", scpi_tx
        )
        self.wait()  # type: ignore[attr-defined]
        self.logger.info(f"ch{ch_num} state set to {w_state}")
        if w_state in ("1", "ON"):
            return True
        return False

    def source_resolution(self, ch_num: int, value: int | None = None) -> int | None:
        """
        Read the DAC resolution.

        SOURCE (256)

        This command sets or returns the DAC resolution.

        12 indicates 12 bit DAC Resolution + 4 Marker bits.
        13 indicates 13 bit DAC Resolution + 3 Marker bit.
        14 indicates 14 bit DAC Resolution + 2 Marker bits.
        15 indicates 15 bit DAC Resolution + 1 Marker bits.
        16 indicates 16 bit DAC Resolution + 0 Marker bits.

        :param ch_num: channel number
        :param value: change to this
        :returns: number of bits - 12|13|14|15|16
        """
        if ch_num < 1 or ch_num > CHANNEL_COUNT:
            self.logger.error("Invalid channel number %d", ch_num)
            return -1
        if value is not None:
            self.write_command(  # type: ignore[attr-defined]
                f"ch{ch_num} resolution", f"SOURCE{ch_num}:DAC:RESOLUTION {value}"
            )
        o_res: int | None = self.read_query_int(  # type: ignore[attr-defined]
            f"ch{ch_num} resolution", f"SOURCE{ch_num}:DAC:RESOLUTION?"
        )
        self.logger.info(f"ch{ch_num} DAC resolution is {o_res}")
        # if o_res is None:
        #     return -1
        return o_res

    def source_run_mode(self, ch_num: int, value: str | None = None) -> str | None:
        """
        Get run mode.

        SOURCE (267)

        This command sets or returns the run mode of the specified channel.

        CONTinuous sets the Run Mode to Continuous (not waiting for trigger).
        TRIGgered sets the Run Mode to Triggered, waiting for a trigger event. One
        waveform play out cycle completes, then play out stops, waiting for the next
        trigger event.

        TCONtinuous sets the Run Mode to Triggered Continuous, waiting for a trigger.
        Once a trigger is received, the waveform plays out continuously.

        GATed sets the Run Mode to only playout a waveform while the trigger is enabled.
        [n] determines the channel number. If omitted, interpreted as 1.

        RST sets this to CONT.

        :param ch_num: channel/source number
        :param value: set to this - CONTinuous|TRIGgered|TCONtinuous|GATed
        :returns: string value of
        """
        if ch_num < 1 or ch_num > CHANNEL_COUNT:
            self.logger.error("Invalid channel number %d", ch_num)
            return ""
        if value is not None:
            self.write_command(  # type: ignore[attr-defined]
                f"source{ch_num}_run_mode", f"SOURCE{ch_num}:RMODE {value}"
            )
        return self.read_query_str(  # type: ignore[attr-defined]
            f"source{ch_num}_run_mode", f"SOURCE{ch_num}:RMODE?"
        )

    def source_skew(self, ch_num: int, ch_skew: float | None = None) -> float:
        """
        Set skew, i.e. relative timing of the analog output.

        SOURCE (270)

        This command sets or returns the skew (relative timing of the analog output) for
        the waveform associated with the specified channel.

        Range: –2 ns to 2 ns. Minimum increments is 0.5 ps.

        RST sets this to 0.

        :param ch_num: channel/source number
        :param ch_skew: new value to be set, None if only reading
        :returns: current or new skew time
        """
        if ch_num < 1 or ch_num > CHANNEL_COUNT:
            self.logger.error("Invalid channel number %d", ch_num)
            return float("nan")
        o_skew: float = self.read_query_float(  # type: ignore[attr-defined]
            f"ch{ch_num} skew", f"SOURCE{ch_num}:SKEW?"
        )
        if math.isnan(o_skew):
            self.logger.error("Could not read channel %d skew", ch_num)
            return o_skew
        if ch_skew is None:
            return o_skew
        self.logger.info("Set channel %d DC skew to %f", ch_num, ch_skew)
        o_skew = self.write_query_float(  # type: ignore[attr-defined]
            f"ch{ch_num} skew", f"SOURCE{ch_num}:SKEW", ch_skew, "s"
        )
        self.logger.info(f"Set ch{ch_num} skew to {o_skew} Hz")
        return float(o_skew)
