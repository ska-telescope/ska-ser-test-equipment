"""Things related to AWG waveforms are implemented here."""

import json
import logging
from typing import Any

from ska_control_model import AdminMode
from tango.server import attribute


class Awg5208DeviceWaveformsMixin:
    """Mixin for the waves."""

    tektronix: Any
    logger: logging.Logger
    _admin_mode: AdminMode
    _channel_nr: int
    _waveform_maximum_length: int = -1
    _source_level_bias: float = float("nan")
    _source_amplitude_power: float = float("nan")
    _source_amplitude_voltage: float = float("nan")
    _source_amplitude_high: float = float("nan")
    _source_amplitude_low: float = float("nan")
    _source_level_offset: float = float("nan")

    @attribute(dtype=float)
    def source_amplitude_power(self) -> float:
        """
        Read value for source amplitude power.

        :returns: current value in dBm
        """
        # TODO this fails silently
        # if self._admin_mode:
        #     return 0.0, TimeVal.totime(TimeVal.now()), AttrQuality.ATTR_INVALID
        self._source_amplitude_power = self.tektronix.source_amplitude_power(
            self._channel_nr
        )
        return self._source_amplitude_power

    @source_amplitude_power.is_allowed
    # pylint: disable-next=unused-argument
    def source_amplitude_power_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: operation can proceed
        """
        if self._channel_nr == 0:
            self.logger.error("Channel number is not set")
            return False
        return not self._admin_mode

    @source_amplitude_power.write  # type: ignore[no-redef]
    def source_amplitude_power_write(self, value: float) -> None:
        """
        Write value for source amplitude power.

        :param value: changed value in dBm
        """
        # TODO this fails silently
        # if self._admin_mode:
        #     return False
        self.logger.debug(
            "Set source %d amplitude power to %f", self._channel_nr, value
        )
        self._source_amplitude_power = value
        self.tektronix.source_amplitude_power(self._channel_nr, value)

    @attribute(dtype=float)
    def source_amplitude_voltage(self) -> float:
        """
        Read value for source amplitude voltage.

        SCPI string: FGEN:CHANNEL1:AMPLITUDE:VOLTAGE

        :returns: current value in Volt
        """
        self._source_amplitude_voltage = self.tektronix.source_amplitude_voltage(
            self._channel_nr
        )
        return self._source_amplitude_voltage

    @source_amplitude_voltage.is_allowed
    def source_amplitude_voltage_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: operation can proceed
        """
        self.logger.info("Check source_amplitude_voltage_is_allowed")
        return self._check_instrument(  # type: ignore[attr-defined]
            "FGEN", request_type
        )

    @source_amplitude_voltage.write  # type: ignore[no-redef]
    def source_amplitude_voltage(self, value: float) -> None:
        """
        Write value for source amplitude voltage.

        :param value: changed value
        """
        self.logger.debug(
            "Set source %d amplitude voltage to %f", self._channel_nr, value
        )
        self._source_amplitude_voltage = value
        self.tektronix.source_amplitude_voltage(self._channel_nr, value)

    @attribute(dtype=float)
    def source_amplitude_high(self) -> float:
        """
        Read value for source amplitude high.

        SCPI string: FGEN:CHANNEL1:AMPLITUDE:VOLTAGE

        :returns: current value in Volt
        """
        self._source_level_bias = self.tektronix.source_amplitude_high(self._channel_nr)
        return self._source_amplitude_high

    @source_amplitude_high.is_allowed
    def source_amplitude_high_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: operation can proceed
        """
        self.logger.info("Check source amplitude high is allowed")
        return self._check_instrument(  # type: ignore[attr-defined]
            "FGEN", request_type
        )

    @source_amplitude_high.write  # type: ignore[no-redef]
    def source_amplitude_high(self, value: float) -> None:
        """
        Write value for source amplitude high.

        :param value: changed value
        """
        self.logger.debug("Set source %d amplitude high to %f", self._channel_nr, value)
        self._source_amplitude_high = value
        self.tektronix.source_amplitude_high(self._channel_nr, value)

    @attribute(dtype=float)
    def source_amplitude_low(self) -> float:
        """
        Read value for source amplitude low.

        :returns: current value in Volt
        """
        self._source_level_bias = self.tektronix.source_amplitude_low(self._channel_nr)
        return self._source_level_bias

    @source_amplitude_low.is_allowed
    def source_amplitude_low_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: operation can proceed
        """
        self.logger.info("Check source amplitude low is allowed")
        return self._check_instrument(  # type: ignore[attr-defined]
            "FGEN", request_type
        )

    @source_amplitude_low.write  # type: ignore[no-redef]
    def source_amplitude_low(self, value: float) -> None:
        """
        Write value for source amplitude low.

        :param value: changed value
        """
        self.logger.debug("Set source %d amplitude low to %f", self._channel_nr, value)
        self._source_amplitude_low = value
        self.tektronix.source_amplitude_low(self._channel_nr, value)

    @attribute(dtype=float)
    def source_level_bias(self) -> float:
        """
        Read value for source amplitude bias.

        :returns: current value in Volt
        """
        self._source_level_bias = self.tektronix.source_level_bias(self._channel_nr)
        return self._source_level_bias

    @source_level_bias.is_allowed
    def source_level_bias_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: operation can proceed
        """
        self.logger.info("Check source_amplitude_voltage_is_allowed")
        return self._check_instrument(  # type: ignore[attr-defined]
            "FGEN", request_type
        )

    @source_level_bias.write  # type: ignore[no-redef]
    def source_level_bias(self, value: float) -> None:
        """
        Write value for source amplitude bias.

        :param value: changed value
        """
        self.logger.debug("Set source %d amplitude bias to %f", self._channel_nr, value)
        self._source_level_bias = value
        self.tektronix.source_level_bias(self._channel_nr, value)

    @attribute(dtype=float)
    def source_level_offset(self) -> float:
        """
        Read value for source amplitude offset.

        :returns: current value in Volt
        """
        self._source_level_offset = self.tektronix.source_level_offset(self._channel_nr)
        return self._source_level_offset

    @source_level_offset.is_allowed
    def source_level_offset_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: operation can proceed
        """
        self.logger.debug("Check source level offset is allowed")
        return self._check_instrument(  # type: ignore[attr-defined]
            "FGEN", request_type
        )

    @source_level_offset.write  # type: ignore[no-redef]
    def source_level_offset(self, value: float) -> None:
        """
        Write value for source amplitude bias.

        :param value: changed value
        """
        self.logger.debug("Set source %d amplitude bias to %f", self._channel_nr, value)
        self._source_level_offset = value
        self.tektronix.source_level_offset(self._channel_nr, value)

    @attribute
    def waveform_maximum_length(self) -> int:
        """
        Read maximum waveform maximum length.

        :returns: length
        """
        return self.tektronix.waveform_maximum_length  # type: ignore[attr-defined]

    @waveform_maximum_length.is_allowed
    # pylint: disable-next=unused-argument
    def waveform_maximum_length_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return not self._admin_mode

    @waveform_maximum_length.write  # type: ignore[no-redef]
    def waveform_maximum_length(self, value: float) -> None:
        """
        Write waveform maximum length.

        :param value: change to this length
        """
        self.tektronix.waveform_maximum_length = value

    @attribute
    def waveform_minimum_length(self) -> int:
        """
        Read maximum waveform minimum length.

        :returns: length
        """
        return self.tektronix.waveform_minimum_length  # type: ignore[attr-defined]

    @waveform_minimum_length.is_allowed
    # pylint: disable-next=unused-argument
    def waveform_minimum_length_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return not self._admin_mode

    @waveform_maximum_length.write  # type: ignore[no-redef]
    def waveform_maximum_length(self, value: float) -> None:
        """
        Write waveform minimum length.

        :param value: change to this length
        """
        self.tektronix.waveform_minimum_length = value

    @attribute(dtype=str)
    def waveform_names(self) -> str:
        """
        Read all waveform names.

        :returns: name
        """
        l_wavs = self.tektronix.list_waves()
        self.logger.debug("Wave names %s", l_wavs)
        return ",".join(l_wavs)

    @attribute(dtype=str)
    def waveform_status(self) -> str:
        """
        Read all waveform names.

        :returns: name
        """
        l_wavs = self.tektronix.dict_waves()
        self.logger.debug("Wave status %s", l_wavs)
        return json.dumps(l_wavs)
