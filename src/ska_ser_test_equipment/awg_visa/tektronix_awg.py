"""
Set up and control Tektronix AWG5208.

The AWG5200 series instruments have several commands that are blocking. A
blocking command does not allow any further commands to be executed until it is
finished performing its task, such as a command that changes a hardware setting.

Blocking commands tend to take a long amount of time to complete. Because of the time
for a blocking command to complete, if a number of blocking commands are run in a
sequence followed by a query, the query could time out because the previous blocking
commands have not finished.
"""

# pylint: disable=useless-suppression
# pylint: disable=bad-option-value
# pylint: disable=too-many-positional-arguments

import logging
from typing import Any

from ska_ser_test_equipment.awg_visa.tektronix_awg_channels import (
    TektronixChannelsMixin,
)
from ska_ser_test_equipment.awg_visa.tektronix_awg_definitions import (
    AWG_COUPLED_STATES,
    AWG_DATA_BLOCK,
    AWG_MODES,
    AWG_RUN_STATES,
)
from ska_ser_test_equipment.awg_visa.tektronix_awg_mode import TektronixAwgModeMixin
from ska_ser_test_equipment.awg_visa.tektronix_basic_waveforms import (
    TektronixBasicWaveformMixin,
)
from ska_ser_test_equipment.awg_visa.tektronix_fgen_mode import TektronixFgenModeMixin
from ska_ser_test_equipment.awg_visa.tektronix_sequences import TektronixSequenceMixin
from ska_ser_test_equipment.awg_visa.tektronix_waveforms import TektronixWaveformMixin
from ska_ser_test_equipment.scpi.virtual_instrument import VirtualInstrument


# pylint: disable-next=too-many-public-methods,too-many-instance-attributes
class TektronixAwg(
    VirtualInstrument,
    TektronixBasicWaveformMixin,
    TektronixAwgModeMixin,
    TektronixChannelsMixin,
    TektronixFgenModeMixin,
    TektronixSequenceMixin,
    TektronixWaveformMixin,
):
    """Connect to arbitrary waveform generator and do things."""

    visa_hw: Any = None
    m_blox = AWG_DATA_BLOCK
    _instrument_mode: str = ""
    _clock_rate: float = 0.0
    _clock_source: str | None = "INT"
    _run_state: int | None = None

    # pylint: disable-next=too-many-arguments
    def __init__(
        self,
        logger: logging.Logger,
        ip_address: str,
        port: int,
        sentinel_string: str,
        timeout: float,
        use_telnet: bool,
    ):
        """
        Instantiate this class.

        :param logger: logger instance
        :param ip_address: IP address or host name
        :param port: port number for telnet (zero means VISA)
        :param sentinel_string: added to end of command in TCP mode
        :param timeout: timeout in seconds
        :param use_telnet: use telnet protocol
        :raises Exception: could not connect to instrument
        """
        self.logger = logger
        self.logger.info(
            "Set up AWG address %s port %s (telnet %s)", ip_address, port, use_telnet
        )
        self.host = ip_address
        self.port = port  # type: ignore[assignment]
        self.sentinel_string = sentinel_string
        self.timeout = timeout
        self.use_telnet = use_telnet
        if self.use_telnet:
            if not self.port:
                # pylint: disable-next=broad-exception-raised
                raise Exception("Port for telnet not specified")
            self.visa_hw = f"{self.host}:{self.port}"  # type: ignore[assignment]
            self.logger.info("Telnet to %s:%d", self.host, self.port)
        elif self.port:
            self.visa_hw = f"{self.host}:{self.port}"  # type: ignore[assignment]
            self.logger.info("Connect to socket %s:%d", self.host, self.port)
        else:
            try:
                super().__init__(
                    logger, ip_address, port, sentinel_string, timeout, use_telnet
                )
            except Exception as visa_err:
                raise visa_err
            self.visa_hw.timeout = 50000
            self.visa_hw.encoding = "latin_1"
            self.visa_hw.write_termination = "\n"
            self.visa_hw.read_termination = "\n"
            print(self.visa_hw.query("*idn?"))

    @property
    def run_state(self) -> int:
        """
        Read run state.

        0 indicates that the AWG has stopped.
        1 indicates that the AWG is waiting for trigger.
        2 indicates that the AWG is running.

        :returns: run state value
        """
        o_mode: str | None = self.read_query_str("run state", "AWGCONTROL:RSTATE?")
        if o_mode is None:
            self._run_state = -1
            return -1
        self._run_state = int(o_mode)
        return self._run_state

    def clear(self) -> None:
        """Clear all event registers and queues."""
        self.logger.info("Clear")
        self.write_command("clear", "*CLS")

    def reset(self) -> None:
        """
        Reset to default state.

        IEEE MANDATED AND OPTIONAL (213)

        This command resets the AWG to its default state. Waveform plug-ins are not
        affected.

        This is a blocking command.
        """
        self.logger.info("Reset")
        self.write_command("reset", "*RST")
        self.wait()

    def pending(self) -> bool:
        """
        Wait for overlapping operation to complete.

        IEEE MANDATED AND OPTIONAL (304)

        The OPC query causes the AWG to sense the internal flag referred to as the
        "No-Operation-Pending" flag (same as the OPC command).

        When the pending operation has completed, a “1” will be returned to the client.
        """
        rval: int | None
        self.logger.info("Pending")
        try:
            rval = self.read_query_int("wait", "*OPC?")
            if rval is None:
                rval = 0
        except ConnectionResetError:
            self.logger.info("Not able to check for pending")
            rval = 0
        return bool(rval)

    def wait(self) -> None:
        """
        Wait for blocking operation to complete.

        IEEE MANDATED AND OPTIONAL (304)

        This command is used to ensure that the previous command is complete before
        the next command is issued.
        """
        self.logger.info("Wait")
        try:
            self.write_command("wait", "*WAI")
        except ConnectionResetError:
            self.logger.info("No more waiting")

    def start(self) -> bool:
        """
        Start playback of waveforms.

        CONTROL (64)

        This command initiates the output of a waveform or sequence. This is equivalent
        to pushing the play button on the front-panel or display. The AWG can be put
        in the run state only when waveforms or sequences are assigned to channels.

        This is an overlapping command.

        :returns: true
        """
        self.write_command("run", "AWGCONTROL:RUN:IMMEDIATE")
        rc = self.pending()
        return rc

    def stop(self) -> bool:
        """
        Stop playback of waveforms.

        CONTROL (64)

        This command stops the output of a waveform or a sequence.

        This is a blocking command.

        :returns: true
        """
        self.write_command("stop", "AWGCONTROL:STOP:IMMEDIATE")
        self.wait()
        return True

    def close(self) -> None:
        """Close it down."""
        self.logger.info("Close")
        self.visa_hw.close()

    @property
    def clock_source(self) -> str:
        """
        Read clock source.

        CLOCK (112)

        :returns: clock source
        """
        self._clock_source = self.read_query_str("clock_source", "CLOCK:SOURCE?")
        return self._clock_source

    @clock_source.setter
    def clock_source(self, value: str) -> None:
        """
        Set clock source.

        :param value: new source
        """
        self._clock_source = value
        self.write_command("clock_source", f"CLOCK:SOURCE {value}")

    def error_count(self) -> int:
        """
        Read number of errors.

        SYSTEM (289)

        This command returns the error and event queue for the number of unread items.

        :returns: number of errors
        """
        err_count = self.read_query_int("error count", "SYSTEM:ERROR:COUNT?")
        if err_count is None:
            self.logger.warning("Could not read error queue")
            return -1
        self.logger.info("Error queue contains %d entries", err_count)
        return err_count

    def error_next(self) -> str:
        """
        Read error message.

        :returns: next error in queue
        """
        o_err: str | None = self.read_query_str("error", "SYSTEM:ERROR:NEXT?")
        if o_err is None:
            return ""
        return o_err

    def error_queue(self) -> int:
        """
        Read error messages.

        SYSTEM (290)

        This command returns data from the error and event queues.

        :returns: number of errors
        """
        err_count = self.read_query_int("error count", "SYSTEM:ERROR:COUNT?")
        if err_count is None:
            return 0
        self.logger.info("Error queue contains %d entries", err_count)
        for _n_err in range(0, err_count):
            o_err: str | None = self.read_query_str("error", "SYSTEM:ERROR:NEXT?")
            print(o_err)
        return err_count

    def instrument_mode(self, inst_mode: str | None = None) -> str:
        """
        Read instrument mode.

        INSTRUMENT (175)

        :param inst_mode: new value for instrument mode, i.e. AWG or FGEN
        :returns: AWG or FGEN
        """
        self._instrument_mode = self.read_query_str(
            "instrument_mode", "INSTRUMENT:MODE?"
        )
        if inst_mode is None:
            return self._instrument_mode
        if inst_mode not in AWG_MODES:
            self.logger.error("Invalid value for instrument mode %s", inst_mode)
            return self._instrument_mode
        self.logger.info("Set instrument mode to %s", inst_mode)
        self._instrument_mode = self.write_query_str(
            "instrument mode", "INSTRUMENT:MODE", inst_mode
        )
        self.logger.info("Instrument mode set to %s", self._instrument_mode)
        return self._instrument_mode

    def coupled_state(self, ch_state: str | None = None) -> str:
        """
        Get coupled state of the channel’s Analog and Marker output controls.

        INSTRUMENT (174)

        This command sets or returns the coupled state of the channel’s Analog and
        Marker output controls.

        NONE - Disables coupling of all channel’s analog and marker output settings.
        ALL - Couples all channel’s analog and marker output settings. The initial
        settings are derived from channel 1.
        After the initial coupling of the settings, changes made to any channel settings
        affect all channels.
        PAIR - Couples the analog channel and marker output settings in pairs. Initial
        settings are derived from the odd numbered channel of each pair. (For example,
        CH1 to CH2, CH3 to CH4, etc. for all available channels.)
        After the initial coupling of the settings, changes made to either channel
        settings in the pair affect both channels.

        :param ch_state: new value to be set, None if only reading
        :returns: coupled state
        """
        o_state: str = self.read_query_str("coupled state", "INSTRUMENT:COUPLE:SOURCE?")
        if ch_state is None:
            self.logger.info("coupled state is %s", o_state)
            if o_state is None:
                return ""
            return o_state
        if ch_state not in AWG_COUPLED_STATES:
            self.logger.error("Invalid value for coupled state %s", ch_state)
            return ""
        self.logger.info("Set coupled state to %s", ch_state)
        o_state = self.write_query_str(
            "coupled state", "INSTRUMENT:COUPLE:SOURCE", ch_state
        )
        self.logger.info("coupled state set to %s", o_state)
        return o_state

    def max_length(self) -> int:
        """
        Read the maximum waveform length.

        WAVEFORM (368)

        This command returns the granularity of sample points required for the waveform
        to be valid. The number of sample points of a single channel instrument must
        be divisible by 2.

        :returns: number of samples
        """
        o_res: int | None = self.read_query_int(
            "maximum length", "WLIST:WAVEFORM:LMAXIMUM?"
        )
        self.logger.info(f"Maximum waveform length is {o_res}")
        if o_res is None:
            return 0
        return o_res

    def run_mode(self, ch_num: int) -> str:
        """
        Read the run mode.

        SOURCE (267)

        This command sets or returns the run mode of the specified channel.

        CONTinuous sets the Run Mode to Continuous (not waiting for trigger).

        TRIGgered sets the Run Mode to Triggered, waiting for a trigger event. One
        waveform play out cycle completes, then play out stops, waiting for the next
        trigger event.

        TCONtinuous sets the Run Mode to Triggered Continuous, waiting for a trigger.

        Once a trigger is received, the waveform plays out continuously.

        GATed sets the Run Mode to only playout a waveform while the trigger is enabled.

        RST sets this to CONT.

        :param ch_num: channel number
        :returns: run mode - CONTinuous|TRIGgered|TCONtinuous|GATed
        """
        o_res: str = self.read_query_str(
            f"ch{ch_num} run mode", f"SOURCE{ch_num}:RMODE?"
        )
        self.logger.info(f"ch{ch_num} run mode is {o_res}")
        if not o_res:
            return ""
        return o_res

    def catalog(self, awg_dir: str) -> None:
        """
        Catalog of directory.

        MASS MEMORY (176)

        This command returns the current contents and state of the mass storage media.

        <NR1>,<NR1> [,<file_entry>]
        The first <NR1> indicates the total amount of storage currently used in bytes.
        The second <NR1> indicates the free space of the mass storage in bytes.
        <file_entry> ::= "<file_name>,<file_type>,<file_size>"
        <file_name> ::= the exact name of the file
        <file_type> ::= is DIR for an entry that is a directory, empty/blank otherwise
        <file_size> ::= <NR1> is the size of the file in bytes. For <file_type> marked
        DIR, the file size will always be 0.

        :param awg_dir: contents of catalog
        """
        self.logger.info("Set directory to %s", awg_dir)
        # o_state = self.write_query_str(
        #     "mass storage media", "MMEMORY:CDIRECTORY", f'"{awg_dir}"'
        # )
        o_cat: str | None = self.read_query_str(
            "mass storage media", "MMEMORY:CATALOG?"
        )
        if o_cat is None:
            return
        o_files: list[str] = o_cat.split(",")[2:]
        self.logger.info("mass storage media is %s (%d)", o_files, len(o_files))
        n_files = int(len(o_files) / 3)
        for n_file in range(0, n_files):
            f_name = o_files[n_file * 3].replace('"', "")
            f_type = o_files[n_file * 3 + 1]
            f_size = int(o_files[n_file * 3 + 2].replace('"', ""))
            print(f"{f_name:60} {f_type:3} {f_size:12}")

    def list_sequences(self) -> list:
        """
        Do the (con)sequential thing.

        SEQUENCE (213)

        :returns: list of sequence names
        """
        rlist: list = []
        slist_size: int | None = self.read_query_int("list_sequences", "SLIST:SIZE?")
        self.wait()
        if not slist_size:
            self.logger.warning("No sequences loaded")
            return rlist
        i: int
        self.logger.info("Found %d sequences", slist_size)
        for i in range(0, slist_size):
            slist_name = self.read_query_str("name", f"SLIST:NAME? {i + 1}")
            self.logger.info("Name %d: '%s'", i + 1, slist_name)
            if slist_name:
                if slist_name[0] == '"' and slist_name[-1] == '"':
                    slist_name = slist_name[1:-1]
                rlist.append(slist_name)
        return rlist

    def dict_sequences(self) -> dict:
        """
        Do the (con)sequential thing.

        SEQUENCE (213)

        :returns: dictionary with information about sequences
        """
        rdict: dict = {}
        awg: Any = self.visa_hw
        slist_size: int = int(awg.query("SLIST:SIZE?"))
        self.write_scpi("*WAI")
        if not slist_size:
            self.logger.warning("No sequences loaded")
            return rdict
        i: int
        self.logger.info("Found %d sequences", slist_size)
        for i in range(0, slist_size):
            sdict: dict = {}
            # Sequence name
            slist_name = self.read_query_str("name", f"SLIST:NAME? {i + 1}")
            if slist_name is not None:
                if slist_name[0] == '"' and slist_name[-1] == '"':
                    slist_name = slist_name[1:-1]
            sdict["name"] = slist_name
            self.logger.info("Sequence %d/%d: %s", i + 1, slist_size, sdict["name"])
            # Memory usage
            # TODO Timeout expired before operation completed.
            # sdict["memory"] = self.read_query_int(
            #     "memory", f'SLIST:SEQUENCE:WMUSAGE? "{slist_name}"'
            # )
            # Number of tracks
            n_tracks = self.read_query_int(
                "tracks", f'SLIST:SEQUENCE:TRACK? "{slist_name}"'
            )
            sdict["tracks"] = n_tracks
            # Amplitude
            sdict["amplitude"] = self.read_query_float(
                "amplitude", f'SLIST:SEQUENCE:AMPLITUDE? "{slist_name}"'
            )
            # Recommended offset of sequence
            sdict["offset"] = self.read_query_float(
                "amplitude", f'SLIST:SEQUENCE:OFFSET? "{slist_name}"'
            )
            # Recommended sampling rate
            sdict["sampling_rate"] = self.read_query_float(
                "sampling_rate", f'SLIST:SEQUENCE:SRATE? "{slist_name}"'
            )
            # Recommended frequency of sequence
            sdict["sampling_rate"] = self.read_query_float(
                "frequency", f'SLIST:SEQUENCE:FREQUENCY? "{slist_name}"'
            )
            # Total number of steps sequence
            sdict["steps"] = self.read_query_int(
                "steps", f'SLIST:SEQUENCE:LENGTH? "{slist_name}"'
            )
            self.logger.info("steps: %d", sdict["steps"])
            for n_step in range(1, sdict["steps"] + 1):
                step_dict: dict = {}
                # number of times assigned waveform plays before proceeding to next step
                step_dict["rcount"] = self.read_query_str(
                    "rcount", f'SLIST:SEQUENCE:STEP{n_step}:RCOUNT? "{slist_name}"'
                )
                self.logger.info("step %d repeat: %s", n_step, step_dict["rcount"])
                # target step for GOTO command of sequencer at specified step
                step_dict["goto"] = self.read_query_str(
                    "goto", f'SLIST:SEQUENCE:STEP{n_step}:GOTO? "{slist_name}"'
                )
                self.logger.info("step %d goto: %s", n_step, step_dict["goto"])
                for m in range(1, n_tracks + 1):  # type: ignore[operator]
                    step_dict[f"asset{m:03d}"] = self.read_query_str(
                        "waveform name",
                        f'SLIST:SEQUENCE:STEP{n_step}:TASSET{m}? "{slist_name}"',
                    )
                sdict[f"step{n_step:03d}"] = step_dict
            rdict[i + 1] = sdict
        return rdict

    # pylint: disable-next=too-many-locals,too-many-statements,too-many-branches
    def print_status(self, ch_nums: list) -> int:
        """
        Read and display states of AWG.

        :param ch_nums: channel numbers
        :return: zero
        """
        c_state: str | None
        o_state: bool | None
        r_state: int | None = self.run_state
        o_res = self.max_length()
        print(f"{'waveform length':40} : {o_res}")
        if r_state is not None:
            print(f"{'run state':40} : {AWG_RUN_STATES[r_state]}")
        else:
            print(f"{'run state':40} : NONE")
        print(f"{'sample rate':40} : {self.clock_sample_rate:.3E}")
        e_count = self.error_count()
        print(f"{'error count':40} : {e_count}")
        i_mode = self.instrument_mode()
        print(f"{'instrument mode':40} : {i_mode}")
        self.coupled_state()
        c_state = self.coupled_state()
        print(f"coupled state{' ':27} : {c_state}")
        w_list = self.list_waves()
        print(f"{'waveform list':40} : {w_list}")
        w_list = self.list_sequences()
        print(f"{'sequences list':40} : {w_list}")
        self.logger.info("Check channels %s", ch_nums)
        for ch_num in ch_nums:
            # Output state
            a_name = self.source_asset_name(ch_num).replace('"', "")
            a_type = self.source_asset_type(ch_num)
            if a_type == "SEQ":
                print(f"ch{ch_num} {'asset':36} : {a_name} ({a_type})")
            elif a_name:
                print(f"ch{ch_num} {'asset':36} : {a_name} ({a_type})")
            else:
                self.logger.info("No asset name for channel %d", ch_num)
            o_state = self.channel_output_state(ch_num)
            if o_state:
                print(f"ch{ch_num} {'state':36} : ON")
            else:
                print(f"ch{ch_num} {'state':36} : OFF")
            ch_res: int | None = self.source_resolution(ch_num)
            print(f"ch{ch_num} {'resolution':36} : {ch_res} bits")
            o_rmode = self.run_mode(ch_num)
            print(f"ch{ch_num} {'run mode':36} : {o_rmode}")
            # Skew
            skew = self.source_skew(ch_num)
            print(f"ch{ch_num} {'skew':36} : {skew:.1E} s")
            if i_mode == "AWG" and a_type == "SEQ":
                pass
            elif i_mode == "AWG" and a_name:
                volt = self.awg_amplitude(a_name)
                print(f"ch{ch_num} {'amplitude':36} : {volt} V")
                volt = self.awg_offset(a_name)
                print(f"ch{ch_num} {'offset':36} : {volt} V")
                hertz = self.awg_frequency(a_name)
                print(f"ch{ch_num} {'frequency':36} : {hertz} V")
            elif i_mode == "AWG":
                self.logger.info("No asset name for channel %d", ch_num)
            elif i_mode == "FGEN":
                # Output power
                o_pwr = self.fgen_output_power(ch_num)
                print(f"ch{ch_num} {'output power':36} : {o_pwr} dBm")
                # Output voltage
                volt = self.fgen_output_voltage(ch_num)
                print(f"ch{ch_num} {'output voltage':36} : {volt} V")
                # DC level
                volt = self.fgen_dc_level(ch_num)
                print(f"ch{ch_num} {'DC level':36} : {volt} V")
                # Frequency
                hertz = self.fgen_frequency(ch_num)
                print(f"ch{ch_num} {'frequency':36} : {hertz} Hz")
                # High and low voltages
                h_volt, l_volt = self.fgen_high_low(ch_num)
                print(f"ch{ch_num} {'high voltage':36} : {h_volt} V")
                print(f"ch{ch_num} {'low voltage':36} : {l_volt} V")
                # Offset voltage
                volt = self.fgen_offset_voltage(ch_num)
                print(f"ch{ch_num} {'offset voltage':36} : {volt} V")
                # Signal path
                o_path = self.fgen_signal_path(ch_num)
                print(f"ch{ch_num} {'signal path':36} : {o_path}")
                # Period
                o_perd = self.fgen_period(ch_num)
                print(f"ch{ch_num} {'period':36} : {o_perd}")
                # Phase angle
                o_phase = self.fgen_phase(ch_num)
                print(f"ch{ch_num} {'phase':36} : {o_phase} degrees")
                # Symmetry
                o_sym = self.fgen_symmetry(ch_num)
                print(f"ch{ch_num} {'symmetry':36} : {o_sym} %")
                # Waveform type
                o_typ = self.fgen_waveform_function(ch_num)
                print(f"ch{ch_num} {'type':36} : {o_typ}")
            else:
                self.logger.warning("Unknown instrument mode %s", i_mode)
        return 0

    # pylint: disable-next=too-many-locals,too-many-statements
    def dict_status(self, ch_nums: list) -> dict:
        """
        Read states of AWG.

        :param ch_nums: channel numbers
        :return: zero
        """
        r_dict: dict = {}
        r_state: int | None = self.run_state
        r_state_str: str
        if r_state is not None:
            r_state_str = AWG_RUN_STATES[r_state]
        else:
            r_state_str = "NONE"
        r_dict["run_state"] = r_state_str
        r_dict["sample_rate"] = self.clock_sample_rate
        r_dict["error_count"] = self.error_count()
        i_mode = self.instrument_mode()
        r_dict["instrument_mode"] = i_mode
        r_dict["coupled_state"] = self.coupled_state()
        r_dict["waveforms"] = self.list_waves()
        r_dict["sequences"] = self.list_sequences()
        self.logger.info("Check channels %s", ch_nums)
        for ch_num in ch_nums:
            ch_str = f"ch{ch_num}"
            r_dict[ch_str] = {}
            # Output state
            try:
                a_name, a_type, r_state = self.channel_asset(ch_num)
            except ValueError:
                self.logger.error("Could not read status of channel %d", ch_num)
                continue
            a_name = a_name.replace('"', "")
            r_dict[ch_str]["asset_name"] = a_name
            r_dict[ch_str]["asset_type"] = a_type
            r_dict[ch_str]["asset_length"] = r_state
            o_state = self.channel_output_state(ch_num)
            if o_state:
                r_dict[ch_str]["state"] = "ON"
            else:
                r_dict[ch_str]["state"] = "OFF"
            r_dict[ch_str]["resolution"] = self.source_resolution(ch_num)
            r_dict[ch_str]["run_mode"] = self.run_mode(ch_num)
            # Skew
            r_dict[ch_str]["skew"] = self.source_skew(ch_num)
            if i_mode == "AWG":
                r_dict[ch_str]["amplitude"] = self.awg_amplitude(a_name)
                r_dict[ch_str]["offset"] = self.awg_offset(a_name)
                r_dict[ch_str]["frequency"] = self.awg_frequency(a_name)
            elif i_mode == "FGEN":
                # Output power
                r_dict[ch_str]["output_power"] = self.fgen_output_power(ch_num)
                # Output voltage
                r_dict[ch_str]["amplitude"] = self.fgen_output_voltage(ch_num)
                # DC level
                r_dict[ch_str]["dc_level"] = self.fgen_dc_level(ch_num)
                # Frequency
                r_dict[ch_str]["frequency"] = self.fgen_frequency(ch_num)
                # High and low voltages
                h_volt, l_volt = self.fgen_high_low(ch_num)
                r_dict[ch_str]["high voltage"] = h_volt
                r_dict[ch_str]["low voltage"] = l_volt
                # Offset voltage
                r_dict[ch_str]["offset"] = self.fgen_offset_voltage(ch_num)
                # Signal path
                r_dict[ch_str]["signal_path"] = self.fgen_signal_path(ch_num)
                # Period
                r_dict[ch_str]["period"] = self.fgen_period(ch_num)
                # Phase angle
                r_dict[ch_str]["phase"] = self.fgen_phase(ch_num)
                # Symmetry
                r_dict[ch_str]["symmetry"] = self.fgen_symmetry(ch_num)
                # Waveform type
                r_dict[ch_str]["waveform_function"] = self.fgen_waveform_function(
                    ch_num
                )
            else:
                self.logger.warning("Unknown instrument mode %s", i_mode)
        return r_dict

    @property
    def memory_catalog(self) -> str:
        """
        Read directory on AWG.

        MASS MEMORY (176)

        This command returns the current contents and state of the mass storage media.

        <NR1>,<NR1> [,<file_entry>]
        The first <NR1> indicates the total amount of storage currently used in bytes.
        The second <NR1> indicates the free space of the mass storage in bytes.
        <file_entry> ::= "<file_name>,<file_type>,<file_size>"
        <file_name> ::= the exact name of the file
        <file_type> ::= is DIR for an entry that is a directory, empty/blank otherwise
        <file_size> ::= <NR1> is the size of the file in bytes. For <file_type> marked
        DIR, the file size will always be 0.

        :returns: listing stuff
        """
        return self.read_query_str("memory_catalog", "MMEMORY:CATALOG?")

    @property
    def memory_current_directory(self) -> str:
        """
        Get current directory on AWG.

        MASS MEMORY (177)

        This command sets or returns the current directory of the file system on the
        AWG. The current directory for the programmatic interface is different from the
        currently selected directory in the Windows Explorer on the AWG.

        :returns: directory name
        """
        self._memory_current_directory = self.read_query_str(
            "memory_current_directory", "MMEMORY:CDIRECTORY?"
        )
        return self._memory_current_directory

    @memory_current_directory.setter
    def memory_current_directory(self, value: str) -> None:
        """
        Set current directory on AWG.

        :param value: new directory name
        """
        self._memory_current_directory = value
        self.write_query_str(
            "memory_current_directory", "MMEMORY:CDIRECTORY", f'"{value}"'
        )

    def status_errors(self) -> int:
        """
        Read number of errors.

        :return: error count
        """
        err_count = self.error_queue()
        return err_count
