"""Mixin for use with AWG in function generator mode."""

# mypy: disable-error-code=attr-defined

import logging
from typing import Tuple

from ska_ser_test_equipment.awg_visa.tektronix_awg_definitions import (
    AWG_TYPES,
    CHANNEL_COUNT,
    SIGNAL_PATHS,
)


# pylint: disable-next=too-many-public-methods
class TektronixFgenModeMixin:
    """Mixin class for FGEN mode."""

    logger: logging.Logger

    def fgen_dc_level(self, ch_num: int, ch_dc: float | None = None) -> float | None:
        """
        Set DC level.

        FUNCTION GENERATOR (163)

        This command sets or returns the DC level of the generated waveform for the
        specified channel.

        If the value exceeds the designated maximum or minimum offset, then the
        respective max/min values are used.

        :param ch_num: channel number
        :param ch_dc: new value to be set, None if only reading
        :returns: current or new DC level
        """
        # self.print_query(f"ch{ch_num} offset", f"FGEN:CHANNEL{ch_num}:OFFSET?", "s")
        o_dc: float | None = self.read_query_float(
            f"ch{ch_num} DC level", f"FGEN:CHANNEL{ch_num}:DCLEVEL?"
        )
        if ch_dc is None:
            self.logger.info(f"ch{ch_num} DC level is {o_dc} V")
            return o_dc
        self.logger.info("Set channel %d DC level to %f", ch_num, ch_dc)
        o_dc = self.write_query_float(
            f"ch{ch_num} DC level", f"FGEN:CHANNEL{ch_num}:DCLEVEL", ch_dc, "V"
        )
        self.logger.info(f"ch{ch_num} DC level set to {o_dc} Hz")
        return o_dc

    def fgen_frequency(self, ch_num: int, ch_freq: float | None = None) -> float | None:
        """
        Set frequency.

        FUNCTION GENERATOR (164)

        This command sets or returns the function generator’s waveform frequency for
        the specified channel.

        All channels use the same frequency setting.

        If the value entered is higher than the designated maximum frequency or lower
        than the designated minimum, then the respective max/min values are used.

        :param ch_num: channel number
        :param ch_freq: new value to be set, None if only reading
        :returns: current or new frequency
        """
        # self.print_query(f"ch{ch_num} period", f"FGEN:CHANNEL{ch_num}:PERIOD?", "s")
        o_hertz: float | None = self.read_query_float(
            f"ch{ch_num} frequency", f"FGEN:CHANNEL{ch_num}:FREQUENCY?"
        )
        if ch_freq is None:
            self.logger.info(f"ch{ch_num} frequency is {o_hertz} Hz")
            return o_hertz
        self.logger.info("Set channel %d frequency to %f", ch_num, ch_freq)
        o_hertz = self.write_query_float(
            f"ch{ch_num} frequency", f"FGEN:CHANNEL{ch_num}:FREQUENCY", ch_freq, "Hz"
        )
        self.logger.info(f"ch{ch_num} frequency set to {o_hertz} Hz")
        return o_hertz

    def fgen_high_low(self, ch_num: int) -> Tuple[float | None, float | None]:
        """
        Set DC voltage.

        FUNCTION GENERATOR (165)

        This command sets or returns the function generator’s waveform high voltage
        value for the specified channel.

        :param ch_num: channel number
        :returns: current high and low voltages
        """
        h_volt: float | None = self.read_query_float(
            f"ch{ch_num} DC voltage", f"FGEN:CHANNEL{ch_num}:HIGH?"
        )
        self.logger.info(f"ch{ch_num} high voltage{' ':24} : {h_volt}")
        l_volt: float | None = self.read_query_float(
            f"ch{ch_num} DC voltage", f"FGEN:CHANNEL{ch_num}:LOW?"
        )
        self.logger.info(f"ch{ch_num} low voltage{' ':25} : {l_volt}")
        return h_volt, l_volt

    def fgen_offset_voltage(
        self, ch_num: int, ch_voltage: float | None = None
    ) -> float | None:
        """
        Set offset voltage.

        FUNCTION GENERATOR (167)

        This command sets or returns the function generator’s waveform offset value for
        the specified channel.

        If the offset value is higher than the designated maximum offset or lower than
        the designated minimum offset, then the respective max/min values are used.

        :param ch_num: channel number
        :param ch_voltage: new value to be set, None if only reading
        :returns: current or new offset voltage
        """
        volt: float | None = self.read_query_float(
            f"ch{ch_num} offset voltage", f"FGEN:CHANNEL{ch_num}:OFFSET?"
        )
        if ch_voltage is None:
            self.logger.info(f"ch{ch_num} offset voltage is {volt}")
            return volt
        self.logger.info("Set channel %d output voltage to %f", ch_num, ch_voltage)
        volt = self.write_query_float(
            f"ch{ch_num} offset voltage",
            f"FGEN:CHANNEL{ch_num}:OFFSET",
            ch_voltage,
            "V",
        )
        self.logger.info(f"ch{ch_num} offset voltage set to {volt}")
        return volt

    def fgen_signal_path(self, ch_num: int, ch_path: str | None = None) -> str | None:
        """
        Set waveform signal path.

        FUNCTION GENERATOR (168)

        This command sets or returns the function generator’s signal path of the
        specified channel.

        This is a blocking command.

        :param ch_num: channel number
        :param ch_path: new value to be set, None if only reading
        :returns: current or new signal path
        """
        o_path: str | None = self.read_query_str(
            f"ch{ch_num} type", f"FGEN:CHANNEL{ch_num}:PATH?"
        )
        if ch_path is None:
            self.logger.info("Channel %d type is %s", ch_num, ch_path)
            return o_path
        self.logger.debug("Set channel %d signal path to %s", ch_num, ch_path)
        if ch_path.upper() not in SIGNAL_PATHS:
            self.logger.error("Invalid path %s", ch_path)
            return None
        o_path = self.write_query_str(
            f"ch{ch_num} type", f"FGEN:CHANNEL{ch_num}:PATH", ch_path
        )
        self.logger.info("Channel %d signal path set to %s", ch_num, o_path)
        self.wait()  # type: ignore[attr-defined]
        return o_path

    def fgen_period(self, ch_num: int) -> float | None:
        """
        Get period for channel.

        FUNCTION GENERATOR (169)

        This command returns the function generator’s waveform period for the specified
        channel.

        :param ch_num: channel number
        :returns: current or new DC voltage
        """
        perd: float | None = self.read_query_float(
            f"ch{ch_num} period", f"FGEN:CHANNEL{ch_num}:PERIOD?"
        )
        self.logger.info(f"ch{ch_num} Period is {perd} s")
        return perd

    def fgen_phase(self, ch_num: int, ch_phase: float | None = None) -> float | None:
        """
        Set phase angle.

        FUNCTION GENERATOR (170)

        This command sets or returns the function generator’s waveform phase value for
        the specified channel.

        If the value is higher than the designated maximum phase or lower than the
        designated minimum, then the respective max/min values are used.

        :param ch_num: channel number
        :param ch_phase: new value
        :returns: current or new phase angle
        """
        o_phase: float | None = self.read_query_float(
            f"ch{ch_num} phase", f"FGEN:CHANNEL{ch_num}:PHASE?"
        )
        if ch_phase is None:
            self.logger.info(f"ch{ch_num} phase is {o_phase} degrees")
            return o_phase
        self.logger.info("Set channel %d phase to %f", ch_num, ch_phase)
        o_phase = self.write_query_float(
            f"ch{ch_num} offset voltage",
            f"FGEN:CHANNEL{ch_num}:PHASE",
            ch_phase,
            "degrees",
        )
        self.logger.info(f"ch{ch_num} phase set to {o_phase} degrees")
        return o_phase

    def fgen_symmetry(self, ch_num: int, ch_sym: float | None = None) -> float:
        """
        Set waveform symmetry.

        FUNCTION GENERATOR (171)

        This command sets or returns the function generator’s triangle waveform
        symmetry value for the specified channel.

        If the value is higher than the designated maximum symmetry value or lower than
        the designated minimum, then the respective max/min values are used.

        :param ch_num: channel number
        :param ch_sym: symmetry percentage
        :returns: current or new symmetry percentage
        """
        o_sym: float = self.read_query_float(
            f"ch{ch_num} symmetry", f"FGEN:CHANNEL{ch_num}:SYMMETRY?"
        )
        if ch_sym is None:
            self.logger.info(f"ch{ch_num} symmetry is {o_sym}")
            return o_sym
        self.logger.info("Set channel %d symmetry to %d", ch_num, ch_sym)
        o_sym = self.write_query_float(
            f"ch{ch_num} DC voltage",
            f"FGEN:CHANNEL{ch_num}:SYMMETRY",
            ch_sym,
            "V",
        )
        self.logger.info(f"ch{ch_num} symmetry set to {o_sym}")
        return o_sym

    def fgen_waveform_function(
        self, ch_num: int, ch_type: str | None = None
    ) -> str | None:
        """
        Set waveform type.

        FUNCTION GENERATOR (172)

        This command sets or returns the function generator’s waveform type (shape) for
        the specified channel.

        SINE – Sinewave
        SQU – Square wave
        TRI – Triangle wave
        NOIS – Noise
        DC – DC
        GAUS – Gaussian
        EXPR – Exponential Rise
        EXPD – Exponential Decay
        NONE

        :param ch_num: channel number
        :param ch_type: new value to be set, None if only reading
        :returns: current or new waveform type - SINE|SQUare|TRIangle|NOISe|DC
        """
        self.logger.debug("Set channel %d waveform to %s", ch_num, ch_type)
        o_typ: str | None = self.read_query_str(
            f"ch{ch_num} type", f"FGEN:CHANNEL{ch_num}:TYPE?"
        )
        if ch_type is None:
            self.logger.info("Channel %d type is %s", ch_num, ch_type)
            return o_typ
        if ch_type.upper() not in AWG_TYPES:
            self.logger.error("Invalid type %s", ch_type)
            return None
        o_typ = self.write_query_str(
            f"ch{ch_num} type", f"FGEN:CHANNEL{ch_num}:TYPE", ch_type
        )
        self.logger.info("Channel %d type set to %s", ch_num, o_typ)
        return o_typ

    def fgen_output_power(
        self, ch_num: int, ch_pwr: float | None = None
    ) -> float | None:
        """
        Set output power in dBm.

        FUNCTION GENERATOR (161)

        This command sets or returns the function generator’s waveform amplitude value
        for the specified channel in units of dBm.

        :param ch_num: channel number
        :param ch_pwr: new value to be set, None if only reading
        :returns: current or new output power in dBm
        """
        o_pwr: float | None = self.read_query_float(
            f"ch{ch_num} power", f"FGEN:CHANNEL{ch_num}:AMPLITUDE:POWER?"
        )
        if ch_pwr is None:
            self.logger.info(f"ch{ch_num} output power is {o_pwr}")
            return o_pwr
        self.logger.info("Set channel %d output power to %s", ch_num, ch_pwr)
        o_pwr = self.write_query_float(
            f"ch{ch_num} output power",
            f"FGEN:CHANNEL{ch_num}:AMPLITUDE:POWER",
            ch_pwr,
            "dBm",
        )
        self.logger.info(f"ch{ch_num} output power set to {o_pwr}")
        return o_pwr

    def fgen_output_voltage(
        self, ch_num: int, ch_voltage: float | None = None
    ) -> float | None:
        """
        Get or set output voltage.

        FUNCTION GENERATOR (162)

        This command sets or returns the function generator’s waveform amplitude value
        for the specified channel in units of volts.

        :param ch_num: channel number
        :param ch_voltage: new value to be set, None if only reading
        :returns: current or new output voltage
        """
        volt: float | None = self.read_query_float(
            f"ch{ch_num} output voltage", f"FGEN:CHANNEL{ch_num}:AMPLITUDE:VOLTAGE?"
        )
        if ch_voltage is None:
            self.logger.info(f"ch{ch_num} output voltage is {volt}")
            return volt
        self.logger.info("Set channel %d output voltage to %s", ch_num, ch_voltage)
        volt = self.write_query_float(
            f"ch{ch_num} output voltage",
            f"FGEN:CHANNEL{ch_num}:AMPLITUDE:VOLTAGE",
            ch_voltage,
            "V",
        )
        self.logger.info(f"ch{ch_num} output voltage set to {volt}")
        return volt

    def channel_amplitude_power(self, ch_num: int, value: float | None = None) -> float:
        """
        Get amplitude power.

        FUNCTION GENERATOR (161)

        This command sets or returns the function generator’s waveform amplitude value
        for the specified channel in units of dBm.

        :param ch_num: channel number
        :param value: change to this
        :returns: float value of amplitude power
        """
        if ch_num < 1 or ch_num > CHANNEL_COUNT:
            self.logger.error("Invalid channel number %d", ch_num)
            return float("nan")
        if value is not None:
            self.write_command(  # type: ignore[attr-defined]
                f"channel{ch_num}_amplitude_power",
                f"FGEN:CHANNEL{ch_num}:AMPLITUDE:POWER {value}",
            )
        return self.read_query_float(  # type: ignore[attr-defined]
            f"channel{ch_num}_amplitude_power", f"FGEN:CHANNEL{ch_num}:AMPLITUDE:POWER?"
        )

    def channel_amplitude_voltage(
        self, ch_num: int, value: float | None = None
    ) -> float | None:
        """
        Get amplitude voltage.

        FUNCTION GENERATOR (162)

        This command sets or returns the function generator’s waveform amplitude value
        for the specified channel in units of volts.

        :param ch_num: channel number
        :param value: change to this
        :returns: float value of amplitude voltage
        """
        if ch_num < 1 or ch_num > CHANNEL_COUNT:
            self.logger.error("Invalid channel number %d", ch_num)
            return float("nan")
        if value is not None:
            self.write_command(  # type: ignore[attr-defined]
                f"channel{ch_num}_amplitude_voltage",
                f"FGEN:CHANNEL{ch_num}:AMPLITUDE:VOLTAGE {value}",
            )
        return self.read_query_float(  # type: ignore[attr-defined]
            f"channel{ch_num}_amplitude_voltage",
            f"FGEN:CHANNEL{ch_num}:AMPLITUDE:VOLTAGE?",
        )

    def channel_dc_level(self, ch_num: int, value: float | None = None) -> float | None:
        """
        Get DC level.

        FUNCTION GENERATOR (163)

        This command sets or returns the DC level of the generated waveform for the
        specified channel.

        :param ch_num: channel number
        :param value: set to this
        :returns: float value of DC level
        """
        if ch_num < 1 or ch_num > CHANNEL_COUNT:
            self.logger.error("Invalid channel number %d", ch_num)
            return float("nan")
        if value is not None:
            self.write_command(  # type: ignore[attr-defined]
                f"channel{ch_num}_dc_level", f"FGEN:CHANNEL{ch_num}:DCLEVEL {value}"
            )
        return self.read_query_float(  # type: ignore[attr-defined]
            f"channel{ch_num}_dc_level", f"FGEN:CHANNEL{ch_num}:DCLEVEL?"
        )

    def channel_frequency(self, ch_num: int, value: float | None = None) -> float:
        """
        Get frequency.

        FUNCTION GENERATOR (164)

        This command sets or returns the function generator’s waveform frequency for
        the specified channel. All channels use the same frequency setting.

        If the value entered is higher than the designated maximum frequency or lower
        than the designated minimum, then the respective max/min values are used.

        Waveform type   Range
        Sine            1 Hz to 1.25 GHz (Option 25) or 2.5 GHz (Option 40)
        Square          1 Hz to 1.25 GHz (Option 25) or 2.5 GHz (Option 40)
        Exp Rise        1 Hz to 1.25 GHz (Option 25) or 2.5 GHz (Option 40)
        Exp Decay       1 Hz to 1.25 GHz (Option 25) or 2.5 GHz (Option 40)
        Gaussian        1 Hz to 1.25 GHz (Option 25) or 2.5 GHz (Option 40)
        Triangle        1 Hz to 625 MHz (Option 25) or 1.25 GHz (Option 40)

        :param ch_num: channel number
        :param value: change to this
        :returns: float value of frequency
        """
        if ch_num < 1 or ch_num > CHANNEL_COUNT:
            self.logger.error("Invalid channel number %d", ch_num)
            return float("nan")
        if value is not None:
            self.write_command(  # type: ignore[attr-defined]
                f"channel{ch_num}_freq", f"FGEN:CHANNEL{ch_num}:FREQUENCY {value}"
            )
        return self.read_query_float(  # type: ignore[attr-defined]
            f"channel{ch_num}_freq", f"FGEN:CHANNEL{ch_num}:FREQUENCY?"
        )

    def channel_high(self, ch_num: int, value: float | None = None) -> float:
        """
        Get high.

        FUNCTION GENERATOR (165)

        This command sets or returns the function generator’s waveform high voltage
        value for the specified channel.

        RST sets this to 1/2 the Amplitude setting.

        :param ch_num: channel number
        :param value: change to this
        :returns: float value of high
        """
        if ch_num < 1 or ch_num > CHANNEL_COUNT:
            self.logger.error("Invalid channel number %d", ch_num)
            return float("nan")
        if value is not None:
            self.write_command(  # type: ignore[attr-defined]
                f"channel{ch_num}_high", f"FGEN:CHANNEL{ch_num}:HIGH {value}"
            )
        return self.read_query_float(  # type: ignore[attr-defined]
            f"channel{ch_num}_high", f"FGEN:CHANNEL{ch_num}:HIGH?"
        )

    def channel_low(self, ch_num: int, value: float | None = None) -> float:
        """
        Get low.

        FUNCTION GENERATOR (166)

        This command sets or returns the function generator’s waveform low voltage
        value for the specified channel.

        RST sets this to minus 1/2 the Amplitude setting.

        :param ch_num: channel number
        :param value: change to this
        :returns: float value of low
        """
        if ch_num < 1 or ch_num > CHANNEL_COUNT:
            self.logger.error("Invalid channel number %d", ch_num)
            return float("nan")
        if value is not None:
            self.write_command(  # type: ignore[attr-defined]
                f"channel{ch_num}_low", f"FGEN:CHANNEL{ch_num}:LOW {value}"
            )
        return self.read_query_float(  # type: ignore[attr-defined]
            f"channel{ch_num}_low", f"FGEN:CHANNEL{ch_num}:LOW?"
        )

    def channel_offset(self, ch_num: int, value: float | None = None) -> float:
        """
        Get offset.

        FUNCTION GENERATOR (167)

        This command sets or returns the function generator’s waveform offset value for
        the specified channel.

        If the offset value is higher than the designated maximum offset or lower than
        the designated minimum offset, then the respective max/min values are used.

        RST sets this to 0.

        :param ch_num: channel number
        :param value: change to this
        :returns: float value of offset
        """
        if ch_num < 1 or ch_num > CHANNEL_COUNT:
            self.logger.error("Invalid channel number %d", ch_num)
            return float("nan")
        if value is not None:
            self.write_command(  # type: ignore[attr-defined]
                f"channel{ch_num}_offset", f"FGEN:CHANNEL{ch_num}:OFFSET {value}"
            )
        return self.read_query_float(  # type: ignore[attr-defined]
            f"channel{ch_num}_offset", f"FGEN:CHANNEL{ch_num}:OFFSET?"
        )

    def channel_path(self, ch_num: int, value: str | None = None) -> str | None:
        """
        Get path i.e. DCHB, ACDirect or ACAMplified.

        FUNCTION GENERATOR

        This command sets or returns the function generator’s signal path of the
        specified channel.

        DCHB sets the signal path to DC High Bandwidth, going directly from the DAC to
        the + and – differential outputs.

        ACDirect sets signal path to go to the channel’s (+) connector (single-ended AC
        output).

        ACAMplified sets signal path to go through the attenuators and amplifiers, then
        to the channel’s (+) connector (single-ended AC output). Option AC is required.

        This is a blocking command.

        :param ch_num: channel number
        :param value: change to this - DCHB|ACDirect|ACAMplified
        :returns: string value of path
        """
        if ch_num < 1 or ch_num > CHANNEL_COUNT:
            self.logger.error("Invalid channel number %d", ch_num)
            return ""
        if value is not None:
            self.write_command(  # type: ignore[attr-defined]
                f"channel{ch_num}_path", f"FGEN:CHANNEL{ch_num}:PATH {value}"
            )
            self.wait()  # type: ignore[attr-defined]
        return self.read_query_str(  # type: ignore[attr-defined]
            f"channel{ch_num}_path", f"FGEN:CHANNEL{ch_num}:PATH?"
        )

    def channel_period(self, ch_num: int) -> float:
        """
        Get period.

        FUNCTION GENERATOR (168)

        This command returns the function generator’s waveform period for the specified
        channel.

        :param ch_num: channel number
        :returns: float value of period
        """
        if ch_num < 1 or ch_num > CHANNEL_COUNT:
            self.logger.error("Invalid channel number %d", ch_num)
            return float("nan")
        return self.read_query_float(  # type: ignore[attr-defined]
            f"channel{ch_num}_period", f"FGEN:CHANNEL{ch_num}:PERIOD?"
        )

    def channel_phase(self, ch_num: int, value: float | None = None) -> float:
        """
        Get phase.

        FUNCTION GENERATOR (170)

        This command sets or returns the function generator’s waveform phase value for
        the specified channel.

        If the value is higher than the designated maximum phase or lower than the
        designated minimum, then the respective max/min values are used.

        Range: –180.0 degrees to +180.0 degrees.

        RST sets this to 0.

        :param ch_num: channel number
        :param value: change to this
        :returns: float value of phase
        """
        if ch_num < 1 or ch_num > CHANNEL_COUNT:
            self.logger.error("Invalid channel number %d", ch_num)
            return float("nan")
        if value is not None:
            self.logger.debug("Set channel %d phase to %f", ch_num, value)
            self.write_command(  # type: ignore[attr-defined]
                f"channel{ch_num}_phase", f"FGEN:CHANNEL{ch_num}:PHASE {value}"
            )
        self.logger.debug("Read channel %d phase", ch_num)
        return self.read_query_float(  # type: ignore[attr-defined]
            f"channel{ch_num}_phase", f"FGEN:CHANNEL{ch_num}:PHASE?"
        )

    def channel_symmetry(self, ch_num: int, value: float | None = None) -> float:
        """
        Get symmetry.

        FUNCTION GENERATOR (171)

        This command sets or returns the function generator’s triangle waveform
        symmetry value for the specified channel.

        If the value is higher than the designated maximum symmetry value or lower than
        the designated minimum, then the respective max/min values are used.

        RST sets this to 100.

        :param ch_num: channel number
        :param value: set to this
        :returns: float value of symmetry
        """
        if ch_num < 1 or ch_num > CHANNEL_COUNT:
            self.logger.error("Invalid channel number %d", ch_num)
            return float("nan")
        if value is not None:
            self.write_command(  # type: ignore[attr-defined]
                f"channel{ch_num}_symmetry", f"FGEN:CHANNEL{ch_num}:SYMMETRY {value}"
            )
        return self.read_query_float(  # type: ignore[attr-defined]
            f"channel{ch_num}_symmetry", f"FGEN:CHANNEL{ch_num}:SYMMETRY?"
        )

    def channel_type(self, ch_num: int, value: str | None = None) -> str | None:
        """
        Get type.

        FUNCTION GENERATOR (172)

        This command sets or returns the function generator’s waveform type (shape) for
        the specified channel.

        RST sets this to SINE.

        :param ch_num: channel number
        :param value: set to this
        :returns: string value of type - SINE|SQUare|TRIangle|NOISe|DC
        """
        if ch_num < 1 or ch_num > CHANNEL_COUNT:
            self.logger.error("Invalid channel number %d", ch_num)
            return ""
        if value is not None:
            self.write_query_str(  # type: ignore[attr-defined]
                f"channel{ch_num}_type", f"FGEN:CHANNEL{ch_num}:TYPE", value
            )
        return self.read_query_str(  # type: ignore[attr-defined]
            f"channel{ch_num}_type", f"FGEN:CHANNEL{ch_num}:TYPE?"
        )
