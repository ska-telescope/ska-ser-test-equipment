"""Tango attributes for AWG channels."""

import logging
import math
from typing import Any

import tango
from ska_control_model import AdminMode
from tango import AttReqType, AttrQuality, TimeVal
from tango.server import attribute

from ska_ser_test_equipment.awg_visa.awg_visa_attributes import MAX_CHANNEL


# pylint: disable-next=too-many-public-methods,too-many-instance-attributes
class Awg5208DeviceChannelsMixin:
    """Here are the attributes."""

    tektronix: Any
    logger: logging.Logger
    _admin_mode: AdminMode
    _instrument_mode: str
    _channel_nr: int = 0
    _channel_amplitude_power: float = float("nan")
    _channel_amplitude_voltage: float = float("nan")
    _channel_dc_level: float = float("nan")
    _source_asset_name: str = ""
    _source_asset_type: str = ""
    _channel_output_state: bool = False
    _source_skew: float = float("nan")

    def _check_instrument(self, xpected: str, request_type: Any) -> bool:
        """
        Check that instrument is in correct mode.

        :param xpected: instrument should be in this mode i.e. "AWG" or "FGEN"
        :param request_type: read or write
        :return: mode is FGEN
        """
        xpected = xpected.upper()
        if self._admin_mode != AdminMode.ONLINE:
            self.logger.warning("Device is in admin mode (%d)", self._admin_mode)
            return False
        if not self._channel_nr:
            self.logger.warning("Channel number is not set")
            return False
        if xpected not in ("AWG", "FGEN"):
            self.logger.warning("Unknown mode %s", xpected)
            return False
        if request_type == AttReqType.READ_REQ:
            self.logger.info("Read is OK")
            return True
        if not self._instrument_mode:
            self._instrument_mode = self.tektronix.instrument_mode
            self.logger.info("Current mode is %s", self._instrument_mode)
        if self._instrument_mode != xpected:
            self.logger.warning(
                "Instrument mode is %s, should be %s", self._instrument_mode, xpected
            )
            return False
        self.logger.info("Mode is %s", xpected)
        return True

    @attribute(dtype=int, min_value=1, max_value=8)
    def channel_nr(self) -> int:
        """
        Get current channel number.

        :returns: channel number
        """
        return self._channel_nr

    @channel_nr.is_allowed
    # pylint: disable-next=unused-argument
    def channel_nr_is_allowed(self, request_type: Any) -> bool:
        """
        Check that attribute can be read or written.

        :param request_type: read or write
        :returns: OK to write
        """
        return True

    @channel_nr.write  # type: ignore[no-redef]
    def channel_nr(self, value: int) -> None:
        """
        Set current channel number.

        :param value: new channel number
        """
        if value < 0 or value > MAX_CHANNEL:
            self.logger.error("Channel number %d is invalid", value)
            return
        self.logger.debug("Set channel number to %d", value)
        self._channel_nr = value

    @attribute(dtype=float)
    def channel_amplitude_power(self) -> float:
        """
        Read value for channel 1 amplitude power.

        :returns: current value in dBm
        """
        # TODO this fails silently
        # if self._admin_mode:
        #     return 0.0, TimeVal.totime(TimeVal.now()), AttrQuality.ATTR_INVALID
        self._channel_amplitude_power = self.tektronix.channel_amplitude_power(
            self._channel_nr
        )
        return self._channel_amplitude_power

    @channel_amplitude_power.is_allowed
    # pylint: disable-next=unused-argument
    def channel_amplitude_power_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: operation can proceed
        """
        if self._channel_nr == 0:
            self.logger.error("Channel number is not set")
            return False
        return not self._admin_mode

    @channel_amplitude_power.write  # type: ignore[no-redef]
    def channel_amplitude_power_write(self, value: float) -> None:
        """
        Write value for channel amplitude power.

        :param value: changed value in dBm
        """
        # TODO this fails silently
        # if self._admin_mode:
        #     return False
        self.logger.debug(
            "Set channel %d amplitude power to %f", self._channel_nr, value
        )
        self._channel_amplitude_power = value
        self.tektronix.channel_amplitude_power(self._channel_nr, value)

    @attribute(dtype=float)
    def channel_amplitude_voltage(self) -> float:
        """
        Read value for channel amplitude voltage.

        :returns: current value in Volt
        """
        self._channel_amplitude_voltage = self.tektronix.channel_amplitude_voltage(
            self._channel_nr
        )
        return self._channel_amplitude_voltage

    @channel_amplitude_voltage.is_allowed
    def channel_amplitude_voltage_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: operation can proceed
        """
        self.logger.info("Check channel_amplitude_voltage_is_allowed")
        return self._check_instrument("FGEN", request_type)

    @channel_amplitude_voltage.write  # type: ignore[no-redef]
    def channel_amplitude_voltage(self, value: float) -> None:
        """
        Write value for channel amplitude voltage.

        :param value: changed value
        """
        self.logger.debug(
            "Set channel %d amplitude voltage to %f", self._channel_nr, value
        )
        self._channel_amplitude_voltage = value
        self.tektronix.channel_amplitude_voltage(self._channel_nr, value)

    @attribute(dtype=str)
    def source_asset_name(self) -> str:
        """
        Read value for channel asset name.

        :returns: current value
        """
        self._source_asset_name = self.tektronix.source_asset_name(self._channel_nr)
        return self._source_asset_name

    @source_asset_name.is_allowed
    def source_asset_name_is_allowed(self, request_type: Any) -> bool:
        """
        Check that attribute can be read or written.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._check_instrument("AWG", request_type)

    @source_asset_name.write  # type: ignore[no-redef]
    def source_asset_name(self, value: str) -> None:
        """
        Write value for channel asset name.

        :param value: changed value
        """
        self.logger.debug("Set channel %d asset name to %s", self._channel_nr, value)
        self._source_asset_name = value
        self.tektronix.source_asset_name(self._channel_nr, value)

    @attribute(dtype=str)
    def source_asset_type(self) -> str:
        """
        Read value for channel asset type.

        :returns: current value - WAV, SEQ or NONE
        """
        self._source_asset_type = self.tektronix.source_asset_type(self._channel_nr)
        return self._source_asset_type

    @source_asset_type.is_allowed
    def source_asset_type_is_allowed(self, request_type: Any) -> bool:
        """
        Check that attribute can be read or written.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._check_instrument("AWG", request_type)

    @source_asset_type.write  # type: ignore[no-redef]
    def source_asset_type(self, value: str) -> None:
        """
        Write value for channel asset type.

        :param value: changed value - WAV, SEQ or NONE
        """
        self.logger.debug("Set channel %d asset type to %s", self._channel_nr, value)
        self._source_asset_type = value
        self.tektronix.source_asset_type(self._channel_nr, value)

    @attribute(dtype=float, min_value=-0.75, max_value=0.75)
    def channel_dc_level(self) -> float:
        """
        Read value for channel DC level.

        SCPI string: FGEN:CHANNEL1:DCLEVEL

        :returns: current value
        """
        awg_val: float = self.tektronix.channel_dc_level(self._channel_nr)
        if math.isnan(awg_val):
            return (  # type: ignore[return-value]
                awg_val,
                TimeVal.totime(TimeVal.now()),
                AttrQuality.ATTR_INVALID,
            )
        self._channel_dc_level = awg_val
        return self._channel_dc_level

    @channel_dc_level.is_allowed
    def is_channel_dc_level_allowed(self, request_type: Any) -> bool:
        """
        Check that attribute can be read or written.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._check_instrument("FGEN", request_type)

    @channel_dc_level.write  # type: ignore[no-redef]
    def channel_dc_level(self, value: float) -> None:
        """
        Write value for channel DC level.

        :param value: changed value
        """
        self.logger.debug("Set channel %d DC level to %f", self._channel_nr, value)
        self.tektronix.channel_dc_level(self._channel_nr, value)

    @attribute(dtype=float, min_value=1, max_value=2.5e9)
    def channel_frequency(self) -> float:
        """
        Read value for channel frequency.

        SCPI string: FGEN:CHANNEL1:FREQUENCY

        :returns: current value
        """
        if self._admin_mode:
            return (  # type: ignore[return-value]
                0.0,
                TimeVal.totime(TimeVal.now()),
                AttrQuality.ATTR_INVALID,
            )
        freq: float = self.tektronix.channel_frequency(self._channel_nr)
        if math.isnan(freq):
            return (  # type: ignore[return-value]
                freq,
                TimeVal.totime(TimeVal.now()),
                AttrQuality.ATTR_INVALID,
            )
        return freq

    @channel_frequency.is_allowed
    def is_channel_frequency_allowed(self, request_type: Any) -> bool:
        """
        Check that attribute can be read or written.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._check_instrument("FGEN", request_type)

    @channel_frequency.write  # type: ignore[no-redef]
    def channel_frequency(self, value: float) -> None:
        """
        Write value for channel frequency.

        :param value: changed value
        """
        self.logger.debug("Set channel %d frequency to %f", self._channel_nr, value)
        self.tektronix.channel_frequency(self._channel_nr, value)

    @attribute(dtype=float)
    def channel_high(self) -> float:
        """
        Read value for channel high level.

        SCPI string: FGEN:CHANNEL1:HIGH

        :returns: current value
        """
        if self._admin_mode:
            return (  # type: ignore[return-value]
                0.0,
                TimeVal.totime(TimeVal.now()),
                AttrQuality.ATTR_INVALID,
            )
        hi_lvl: float = self.tektronix.channel_high(self._channel_nr)
        if math.isnan(hi_lvl):
            return (  # type: ignore[return-value]
                hi_lvl,
                TimeVal.totime(TimeVal.now()),
                AttrQuality.ATTR_INVALID,
            )
        return self.tektronix.channel_high(self._channel_nr)

    @channel_high.is_allowed
    def channel_high_is_allowed(self, request_type: Any) -> bool:
        """
        Check that attribute can be read or written.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._check_instrument("FGEN", request_type)

    @channel_high.write  # type: ignore[no-redef]
    def channel_high(self, value: float) -> None:
        """
        Write value for channel high level.

        :param value: changed value
        """
        self.logger.debug("Set channel %d  high level to %f", self._channel_nr, value)
        self.tektronix.channel_high(self._channel_nr, value)

    @attribute(dtype=float)
    def channel_low(self) -> float:
        """
        Read value for channel low level.

        SCPI string: FGEN:CHANNEL1:LOW

        :returns: current value
        """
        ch_low: float = self.tektronix.channel_low(self._channel_nr)
        if math.isnan(ch_low):
            return (  # type: ignore[return-value]
                ch_low,
                TimeVal.totime(TimeVal.now()),
                AttrQuality.ATTR_INVALID,
            )
        return ch_low

    @channel_low.is_allowed
    def channel_low_is_allowed(self, request_type: Any) -> bool:
        """
        Check that attribute can be read or written.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._check_instrument("FGEN", request_type)

    @channel_low.write  # type: ignore[no-redef]
    def channel_low(self, value: float) -> None:
        """
        Write value for channel low level.

        :param value: changed value
        """
        self.logger.info("Set channel %d low level to %f", self._channel_nr, value)
        self.tektronix.channel_low(self._channel_nr, value)

    @attribute(dtype=float)
    def channel_offset(self) -> float:
        """
        Read value for channel offset.

        :returns: current value
        """
        ch_off: float = self.tektronix.channel_offset(self._channel_nr)
        if math.isnan(ch_off):
            return (  # type: ignore[return-value]
                ch_off,
                TimeVal.totime(TimeVal.now()),
                AttrQuality.ATTR_INVALID,
            )
        return ch_off

    @channel_offset.is_allowed
    def is_channel_offset_allowed(self, request_type: Any) -> bool:
        """
        Check that attribute can be read or written.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._check_instrument("FGEN", request_type)

    @channel_offset.write  # type: ignore[no-redef]
    def channel_offset(self, value: float) -> None:
        """
        Write value for channel offset.

        :param value: changed value
        """
        self.logger.debug("Set channel %d offset to %f", self._channel_nr, value)
        self.tektronix.channel_offset(self._channel_nr, value)

    @attribute(dtype=bool)
    def channel_output_state(self) -> bool:
        """
        Read value for channel output state.

        :returns: current value
        """
        self._channel_output_state = self.tektronix.channel_output_state(
            self._channel_nr
        )
        return self._channel_output_state

    @channel_output_state.is_allowed
    # pylint: disable-next=unused-argument
    def channel_output_state_is_allowed(self, request_type: Any) -> bool:
        """
        Check that attribute can be read or written.

        :param request_type: read or write
        :returns: OK to write
        """
        if self._channel_nr == 0:
            self.logger.error("Channel number is not set")
            return False
        return not self._admin_mode

    @channel_output_state.write  # type: ignore[no-redef]
    def channel_output_state(self, value: bool) -> None:
        """
        Write value for channel output state.

        :param value: changed value
        """
        self.logger.debug("Set channel %d output state to %s", self._channel_nr, value)
        self._channel_output_state = value
        self.tektronix.channel_output_state(self._channel_nr, value)

    @attribute(dtype=str)
    def channel_path(self) -> str:
        """
        Read value for channel path.

        :returns: current value - DCHB|ACDirect|ACAMplified
        """
        return self.tektronix.channel_path(self._channel_nr)

    @channel_path.is_allowed
    def is_channel_path_allowed(self, request_type: Any) -> bool:
        """
        Check that attribute can be read or written.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._check_instrument("FGEN", request_type)

    @channel_path.write  # type: ignore[no-redef]
    def channel_path(self, value: str) -> None:
        """
        Write value for channel path.

        :param value: changed value - DCHB|ACDirect|ACAMplified
        """
        self.logger.debug("Set channel %d path to %s", self._channel_nr, value)
        self.tektronix.channel_path(self._channel_nr, value)

    @attribute(dtype=float)
    def channel_period(self) -> float:
        """
        Read value for channel period.

        :returns: current value
        """
        ch_perd: float = self.tektronix.channel_offset(self._channel_nr)
        if math.isnan(ch_perd):
            return (  # type: ignore[return-value]
                ch_perd,
                TimeVal.totime(TimeVal.now()),
                AttrQuality.ATTR_INVALID,
            )
        return ch_perd

    @channel_period.is_allowed
    def channel_period_is_allowed(self, request_type: Any) -> bool:
        """
        Check that attribute can be read or written.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._check_instrument("FGEN", request_type)

    @attribute(dtype=tango.DevDouble, min_value=-180.0, max_value=180.0)
    def channel_phase_angle(self) -> float:
        """
        Read value for channel phase.

        min value: -180.0, max value: 180.0

        :returns: current value
        """
        ch_phase: float = self.tektronix.channel_phase(self._channel_nr)
        if math.isnan(ch_phase):
            return (  # type: ignore[return-value]
                ch_phase,
                TimeVal.totime(TimeVal.now()),
                AttrQuality.ATTR_INVALID,
            )
        return ch_phase

    @channel_phase_angle.is_allowed
    def is_channel_phase_allowed(self, request_type: Any) -> bool:
        """
        Check that attribute can be written.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._check_instrument("FGEN", request_type)

    @channel_phase_angle.write  # type: ignore[no-redef]
    def channel_phase_angle(self, value: float) -> None:
        """
        Write value for channel phase.

        :param value: changed value
        """
        self.logger.debug("Set channel %d phase to %f", self._channel_nr, value)
        self.tektronix.channel_phase(self._channel_nr, value)
        # try:
        #     self.tektronix.channel_phase(self._channel_nr, value)
        # except tango.API_WAttrOutsideLimit:
        #     self.logger.error("Could not write channel %d phase", self._channel_nr)

    @attribute(dtype=int)
    def source_resolution(self) -> int:
        """
        Read value for channel DAC resolution.

        :returns: current value - 12|13|14|15|16
        """
        return self.tektronix.source_resolution(self._channel_nr)

    @source_resolution.is_allowed
    # pylint: disable-next=unused-argument
    def source_resolution_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to read or write
        """
        if self._channel_nr == 0:
            self.logger.error("Channel number is not set")
            return False
        return not self._admin_mode

    @source_resolution.write  # type: ignore[no-redef]
    def source_resolution(self, value: int) -> None:
        """
        Write value for channel DAC resolution.

        :param value: change to this - 12|13|14|15|16
        """
        self.logger.debug(
            "Set channel %d DAC resolution to %d", self._channel_nr, value
        )
        self.tektronix.source_resolution(self._channel_nr, value)

    @attribute(dtype=str)
    def source_run_mode(self) -> str:
        """
        Read value for run mode.

        SCPI string: SOURCE1:RMODE

        :returns: current value - CONTinuous|TRIGgered|TCONtinuous|GATed
        """
        return self.tektronix.source_run_mode(self._channel_nr)

    @source_run_mode.is_allowed
    # pylint: disable-next=unused-argument
    def source_run_mode_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to read or write
        """
        if self._channel_nr == 0:
            self.logger.error("Channel number is not set")
            return False
        return not self._admin_mode

    @source_run_mode.write  # type: ignore[no-redef]
    def source_run_mode(self, value: str) -> None:
        """
        Write value for channel run mode.

        :param value: change to this - CONTinuous|TRIGgered|TCONtinuous|GATed
        """
        self.logger.debug("Set channel %d run mode to %s", self._channel_nr, value)
        self.tektronix.source_run_mode(self._channel_nr, value)

    @attribute(dtype=float)
    def source_skew(self) -> float:
        """
        Read value for channel skew.

        :returns: current value
        """
        ch_skew: float = self.tektronix.source_skew(self._channel_nr)
        if math.isnan(ch_skew):
            return (  # type: ignore[return-value]
                ch_skew,
                TimeVal.totime(TimeVal.now()),
                AttrQuality.ATTR_INVALID,
            )
        self._source_skew = ch_skew
        return ch_skew

    @source_skew.is_allowed
    def source_skew_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to read or write
        """
        return self._check_instrument("AWG", request_type)

    @source_skew.write  # type: ignore[no-redef]
    def source_skew(self, value: float) -> None:
        """
        Write value for channel skew.

        :param value: change to this
        """
        self.logger.debug("Set channel %d skew to %f", self._channel_nr, value)
        self._source_skew = value
        self.tektronix.source_skew(self._channel_nr, value)

    @attribute(dtype=float)
    def channel_symmetry(self) -> float:
        """
        Read value for channel symmetry.

        SCPI string: FGEN:CHANNEL1:SYMMETRY

        :returns: current value
        """
        ch_sym: float = self.tektronix.channel_offset(self._channel_nr)
        if math.isnan(ch_sym):
            return (  # type: ignore[return-value]
                ch_sym,
                TimeVal.totime(TimeVal.now()),
                AttrQuality.ATTR_INVALID,
            )
        return ch_sym

    @channel_symmetry.is_allowed
    def is_channel_symmetry_allowed(self, request_type: Any) -> bool:
        """
        Check that attribute can be written.

        :param request_type: read or write
        :returns: OK to write
        """
        return self._check_instrument("FGEN", request_type)

    @channel_symmetry.write  # type: ignore[no-redef]
    def channel_symmetry(self, value: float) -> None:
        """
        Write value for channel symmetry.

        :param value: change to this
        """
        self.logger.debug("Set channel %d symmetry to %f", self._channel_nr, value)
        self.tektronix.channel_symmetry(self._channel_nr, value)

    @attribute(dtype=str)
    def channel_type(self) -> str:
        """
        Read value for channel type.

        SCPI string: FGEN:CHANNEL1:TYPE
        Values:
        SINE | SQUare | TRIangle | NOISe | DC | GAUSsian | EXPRise | EXPDecay | NONE

        :returns: current value - SINE|SQUare|TRIangle|NOISe|DC
        """
        return self.tektronix.channel_type(self._channel_nr)

    @channel_type.is_allowed
    def is_channel_type_allowed(self, request_type: Any) -> bool:
        """
        Check that attribute can be written.

        :param request_type: read or write
        :returns: OK to write
        """
        return self._check_instrument("FGEN", request_type)

    @channel_type.write  # type: ignore[no-redef]
    def channel_type(self, ch_type: str) -> None:
        """
        Write value for channel type.

        SINE | SQUare | TRIangle | NOISe | DC | GAUSsian | EXPRise | EXPDecay | NONE

        :param ch_type: change to this - SINE|SQUare|TRIangle|NOISe|DC
        """
        max_val: float
        self.logger.debug("Set channel %d type to %s", self._channel_nr, ch_type)
        ch_type = ch_type.upper()
        if "TRI" in ch_type:
            max_val = 1.25e9
        else:
            max_val = 2.5e9
        self.logger.info("Set maximum frequency for type %s to %e", ch_type, max_val)
        self._set_attribute_config(  # type: ignore[attr-defined]
            "channel_frequency", max_value=max_val
        )
        self.logger.info("Update channel type %d to %s", self._channel_nr, ch_type)
        self.tektronix.channel_type(self._channel_nr, ch_type)

    @attribute(
        dtype=(float,),
        max_dim_x=10000000,
    )
    def channel_waveform_data(self) -> list[float]:
        """
        Read asset data.

        :returns: list of float values
        """
        wave_data = self.tektronix.read_wave_data(self._source_asset_name)
        return wave_data

    @channel_waveform_data.is_allowed
    # pylint: disable-next=unused-argument
    def channel_waveform_data_is_allowed(self, request_type: Any) -> bool:
        """
        Check that attribute can be read.

        :param request_type: read and/or write
        :returns: OK to read
        """
        return self._admin_mode == 0
