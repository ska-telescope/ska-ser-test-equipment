"""Mixin for use with AWG in arnitrary waveform generator mode."""

import logging

# mypy: disable-error-code=attr-defined


class TektronixAwgModeMixin:
    """Mixin class for AWG mode."""

    logger: logging.Logger

    def awg_amplitude(self, asset_name: str, ampl: float | None = None) -> float | None:
        """
        Set output voltage.

        WAVEFORM (359)

        This command sets or returns the Recommended Amplitude (peak-to-peak) of the
        specified waveform in the waveform list.

        If a recommended amplitude is not specified, a query returns the value for Not a
        Number (9.9100E+037).

        :param asset_name: waveform asset name
        :param ampl: new value to be set, None if only reading
        :returns: current or new amplitude
        """
        volt: float | None = self.read_query_float(
            "amplitude", f'WLIST:WAVEFORM:AMPLITUDE? "{asset_name}"'
        )
        if volt is None:
            pass
        elif volt >= 9.9100e37:
            volt = float("nan")
        else:
            pass
        if ampl is None:
            self.logger.info(f"asset {asset_name} amplitude is {volt}")
            return volt
        self.logger.info("Set amplitude for %s to %E", asset_name, ampl)
        volt = self.write_query_str(
            "amplitude",
            "WLIST:WAVEFORM:AMPLITUDE",
            f'"{asset_name}",{ampl:.3E}',
            "V",
        )
        ampl = float(volt)  # type: ignore[arg-type]
        self.logger.info("%s amplitude set to %E", asset_name, ampl)
        return ampl

    def awg_offset(self, asset_name: str, offset: float | None = None) -> float | None:
        """
        Set output offset voltage.

        WAVEFORM (376)

        This command sets or returns the Recommended Offset of the specified waveform
        in the waveform list.

        If a recommended offset is not specified, a query returns the value for Not a
        Number (9.9100E+037).

        :param asset_name: waveform asset name
        :param offset: new value to be set, None if only reading
        :returns: current or new amplitude
        """
        volt: float | None = self.read_query_float(
            "amplitude", f'WLIST:WAVEFORM:OFFSET? "{asset_name}"'
        )
        if volt is not None:
            if volt >= 9.9100e37:
                volt = float("nan")
        if offset is None:
            self.logger.info(f"asset {asset_name} offset is {volt}")
            return volt
        self.logger.info("Set amplitude for %s to %E", asset_name, offset)
        volt = self.write_query_str(
            "offset",
            "WLIST:WAVEFORM:OFFSET",
            f'"{asset_name}",{offset:.3E}',
            "V",
        )
        ampl = float(volt)  # type: ignore[arg-type]
        self.logger.info("%s amplitude set to %E", asset_name, ampl)
        return ampl

    def awg_frequency(self, asset_name: str, freq: float | None = None) -> float | None:
        """
        Set output frequency.

        WAVEFORM (367)

        The command sets or returns the Recommended Center Frequency of the named
        IQ waveform.

        If the Recommended Frequency is not specified, a query will return the value
        for Not a Number: 9.91E+37.

        :param asset_name: waveform asset name
        :param freq: new value to be set, None if only reading
        :returns: current or new amplitude
        """
        hertz: float | None = self.read_query_float(
            "amplitude", f'WLIST:WAVEFORM:FREQUENCY? "{asset_name}"'
        )
        if hertz is not None:
            if hertz >= 9.9100e37:
                hertz = float("nan")
        if freq is None:
            self.logger.info(f"asset {asset_name} frequency is {hertz}")
            return hertz
        self.logger.info("Set amplitude for %s to %E", asset_name, freq)
        hertz = self.write_query_str(
            "amplitude",
            "WLIST:WAVEFORM:FREQUENCY",
            f'"{asset_name}",{freq:.3E}',
            "V",
        )
        freq = float(hertz)  # type: ignore[arg-type]
        self.logger.info("%s amplitude set to %E", asset_name, freq)
        return freq
