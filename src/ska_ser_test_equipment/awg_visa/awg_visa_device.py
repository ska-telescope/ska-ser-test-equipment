#!/usr/bin/env python
"""Tango device for Tektronix AWG5208."""

# pylint: disable=useless-suppression

import json
import logging
import os
import socket
from typing import Any

import tango
import tango.server
from ska_control_model import AdminMode
from ska_tango_base import SKABaseDevice
from tango.server import attribute, device_property

from ska_ser_test_equipment.awg_visa.awg_visa_device_basic_waveforms import (
    Awg5208DeviceBasicWaveformsMixin,
)
from ska_ser_test_equipment.awg_visa.awg_visa_device_channels import (
    Awg5208DeviceChannelsMixin,
)
from ska_ser_test_equipment.awg_visa.awg_visa_device_commands import (
    Awg5208DeviceCommandsMixin,
)
from ska_ser_test_equipment.awg_visa.awg_visa_device_sequences import (
    Awg5208DeviceSequencesMixin,
)
from ska_ser_test_equipment.awg_visa.awg_visa_device_waveforms import (
    Awg5208DeviceWaveformsMixin,
)
from ska_ser_test_equipment.awg_visa.tektronix_awg import TektronixAwg

__all__ = ["AwgVisaDevice", "main"]

log_level_str = os.getenv("LOG_LEVEL", "WARNING").upper()
if log_level_str == "DEBUG":
    LOG_LEVEL = logging.DEBUG
elif log_level_str == "INFO":
    LOG_LEVEL = logging.INFO
elif log_level_str == "WARNING":
    LOG_LEVEL = logging.WARNING
elif log_level_str == "ERROR":
    LOG_LEVEL = logging.ERROR
else:
    LOG_LEVEL = logging.WARNING


class CM:
    """This is the component manager."""

    logger: logging.Logger
    max_executing_tasks = 1
    max_queued_tasks = 0

    def __init__(self) -> None:
        """You can start me up."""

    def start_communicating(self) -> None:
        """Start communication."""
        self.logger.info("Start communicating")

    def stop_communicating(self) -> None:
        """Stop communication."""
        self.logger.info("Stop communicating")


# noqa: E501 pylint: disable-next=too-many-instance-attributes,too-many-public-methods,too-many-ancestors
class AwgVisaDevice(
    SKABaseDevice,
    Awg5208DeviceBasicWaveformsMixin,
    Awg5208DeviceChannelsMixin,
    Awg5208DeviceCommandsMixin,
    Awg5208DeviceSequencesMixin,
    Awg5208DeviceWaveformsMixin,
):
    """The device."""

    host = device_property(dtype=str, default_value="")
    port = device_property(dtype=int, default_value=0)
    data_block_size = device_property(dtype=int, default_value=250000)
    current_directory = device_property(dtype=str, default_value=".")
    model = device_property(dtype=str, default_value="AWG5208I")
    sentinel_string = device_property(dtype=str, default_value="\r\n")
    timeout = device_property(dtype=int, default_value=15)

    use_telnet: bool = False
    device_name: str = ""
    output_file_name: str = ""

    _admin_mode: AdminMode
    _instrument_mode: str = ""
    _clock_rate: float = float("nan")
    _clock_source: str = ""
    _run_state: int = -1
    _memory_current_directory: str = ""
    tektronix: TektronixAwg

    def init_device(self) -> None:
        """
        Initialize the device.

        :raises AttributeError: when YAML file can not be read
        :raises OSError: when YAML file can not be read
        """
        super().init_device()
        self.device_name = self.get_name()
        log_level: str = os.getenv("LOG_LEVEL", "WARNING").upper()
        self.logger.setLevel(log_level)
        self.logger.info("Initialize arbitrary waveform generator %s", self.device_name)
        self.set_status("DISABLE")
        self.set_state(tango.DevState.DISABLE)
        # set device properties
        self.host = os.getenv("SIMULATOR_HOST", socket.gethostname())
        self.model = os.getenv("SIMULATOR_MODEL", "AWG5208").upper()
        self.port = int(os.getenv("SIMULATOR_PORT", "0"))
        if self.port > 0:
            self.use_telnet = True
        # Instantiate AWG handle
        self.logger.info("Connect to %s port %d", self.host, self.port)
        try:
            self.tektronix = TektronixAwg(
                self.logger,
                self.host,
                self.port,
                self.sentinel_string,
                self.timeout,
                self.use_telnet,
            )
        except AttributeError as v_err:
            self.logger.error(v_err)
            raise v_err
        except OSError as v_err:
            self.logger.error(v_err)
            raise v_err
        self._instrument_mode = self.tektronix.instrument_mode()
        # self._clock_rate = self.tektronix.clock_sample_rate
        # self._clock_source = self.tektronix.clock_source
        self._run_state = self.tektronix.run_state

    def create_component_manager(self) -> Any:
        """
        Create a component manager for this device.

        :returns: component manager handle
        """
        # TODO this causes an error in SKABaseDevice
        self.logger.warning("Dummy component manager")
        cm = CM()
        cm.logger = self.logger
        return cm

    @attribute(dtype=str)
    def awg_status(self) -> str:
        """
        Get status.

        :returns: dictionary string with status
        """
        status_dict = self.tektronix.dict_status([self._channel_nr])
        return json.dumps(status_dict)

    @awg_status.is_allowed
    # pylint: disable-next=unused-argument
    def awg_status_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        if self._channel_nr == 0:
            self.logger.error("Channel number is not set")
            return False
        return self._admin_mode == 0

    @attribute(dtype=float)
    def clock_sample_rate(self) -> float:
        """
        Get sample rate.

        :returns: sample rate for clock
        """
        return self.tektronix.clock_sample_rate

    @clock_sample_rate.write  # type: ignore[no-redef]
    def clock_sample_rate(self, value: float) -> None:
        """
        Set iclock rate.

        :param value: change to this
        """
        self.tektronix.clock_sample_rate = value

    @attribute(dtype=str)
    def clock_source(self) -> str:
        """
        Get source of clock.

        :returns: clock source
        """
        return self.tektronix.clock_source

    @clock_source.write  # type: ignore[no-redef]
    def clock_source(self, value: str) -> None:
        """
        Set source of clock.

        :param value: new clock source
        """
        self.tektronix.clock_source = value

    @attribute(dtype=str)
    def coupled_state(self) -> str:
        """
        Get coupled state.

        :returns: coupled_state
        """
        return self.tektronix.coupled_state()

    @coupled_state.write  # type: ignore[no-redef]
    def coupled_state(self, value: str) -> None:
        """
        Set coupled state.

        :param value: new coupled state
        """
        self.tektronix.coupled_state(value)

    @attribute(dtype=str)
    def identity(self) -> str:
        """
        Get instrument identity.

        :returns: identity string, e.g. "TEKTRONIX,AWG5208,F00BAA7,FV:8.1.0266.0"
        """
        return self.tektronix.identity()

    @attribute(dtype=str)
    def instrument_mode(self) -> str:
        """
        Get instrument mode, i.e. AWG or FGEN.

        :returns: AWG or FGEN
        """
        self._instrument_mode = self.tektronix.instrument_mode()
        return self._instrument_mode

    def _set_attribute_config(
        self,
        attr_name: str,
        min_value: float | None = None,
        max_value: float | None = None,
        # TODO for future use
        # min_warning=None,
        # max_warning=None,
        # min_alarm=None,
        # max_alarm=None,
    ) -> None:
        """
        Change attribute configuration.

        :param attr_name: attribute name
        :param min_value: minimum value
        :param max_value: maximum value
        """
        new_val: str
        multi_attr = self.get_device_attr()
        freq_attr = multi_attr.get_attr_by_name(attr_name)
        multi_prop = freq_attr.get_properties()
        self.logger.info("Properties for %s: %s", attr_name, str(multi_prop))
        if min_value is not None:
            new_val = str(min_value)
            self.logger.info("Set min value for %s to %s", attr_name, new_val)
            multi_prop.min_value = str(min_value)
        if max_value is not None:
            new_val = str(max_value)
            self.logger.info("Set max value for %s to %s", attr_name, new_val)
            multi_prop.max_value = new_val
        self.logger.info("Update properties")
        # TODO this causes a core dump!
        # freq_attr.set_properties(multi_prop)

    def _set_frequency_maximum(self, ch_type: str) -> None:
        """
        Set maximum value for channel frequency.

        :param ch_type: channel type i.e. SINE, TRI, etc.
        """
        ch_type = ch_type.upper()
        max_val: float
        if "TRI" in ch_type:
            max_val = 1.25e9
        else:
            max_val = 2.5e9
        self.logger.info("Set frequency maximum for %s to %e", ch_type, max_val)
        self._set_attribute_config("channel_frequency", max_value=max_val)

    @instrument_mode.is_allowed
    # pylint: disable-next=unused-argument
    def instrument_mode_is_allowed(self, request_type: Any) -> bool:
        """
        Check that instrument mode can be changed.

        :param request_type: read or write
        :returns: OK to proceed
        """
        self.logger.debug(
            "Instrument mode allowed: %s", self._admin_mode == AdminMode.ONLINE
        )
        return self._admin_mode == AdminMode.ONLINE

    @instrument_mode.write  # type: ignore[no-redef]
    def instrument_mode(self, new_mode: str) -> None:
        """
        Set instrument mode, i.e. AWG or FGEN.

        :param new_mode: change to this
        """
        self.logger.info("Change instrument mode to %s", new_mode)
        self.tektronix.instrument_mode(new_mode)
        self._instrument_mode = new_mode
        if new_mode == "FGEN":
            ch_type = self.tektronix.channel_type(self._channel_nr)
            if ch_type is None:
                self.logger.error("Could not read channel type")
            else:
                self._set_frequency_maximum(ch_type)

    @attribute(dtype=str)
    def list_waves(self) -> str:
        """
        Read names of waveform assets.

        :returns: list of asset names
        """
        wavs_list = self.tektronix.list_waves()
        return ",".join(wavs_list)

    @list_waves.is_allowed
    # pylint: disable-next=unused-argument
    def list_waves_is_allowed(self, request_type: Any) -> bool:
        """
        Check that waves can be listed.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._admin_mode == 0

    @attribute(dtype=str)
    def memory_current_directory(self) -> str:
        """
        Get the current directory of the file system on the AWG.

        :returns: current directory
        """
        self._memory_current_directory = self.tektronix.memory_current_directory
        return self._memory_current_directory

    @memory_current_directory.is_allowed
    # pylint: disable-next=unused-argument
    def memory_current_directory_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._admin_mode == 0

    @memory_current_directory.write  # type: ignore[no-redef]
    def memory_current_directory(self, value: str) -> None:
        """
        Set the current directory of the file system on the AWG.

        :param value: new directory
        """
        self.tektronix.memory_current_directory = value
        self._memory_current_directory = value

    @attribute(dtype=str)
    def memory_catalog(self) -> str:
        """
        Get the current directory of the file system on the AWG.

        :returns: current directory
        """
        return self.tektronix.memory_catalog

    @memory_catalog.is_allowed
    # pylint: disable-next=unused-argument
    def memory_catalog_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._admin_mode == 0

    @attribute(dtype=str)
    def output_file(self) -> str:
        """
        Get output file name.

        :returns: current name
        """
        return self.output_file_name

    @output_file.write  # type: ignore[no-redef]
    def output_file(self, output_file_name: str) -> None:
        """
        Set output file name.

        :param output_file_name: new name
        """
        self.output_file_name = output_file_name

    @attribute(dtype=int)
    def system_error_count(self) -> int:
        """
        Get number of errors.

        :returns: error count
        """
        return self.tektronix.error_count()

    @system_error_count.is_allowed
    # pylint: disable-next=unused-argument
    def system_error_count_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._admin_mode == 0

    @attribute(dtype=str)
    def system_error_next(self) -> str:
        """
        Read item in system errors.

        :returns: error message
        """
        err: str | None = self.tektronix.error_next()
        if err is None:
            return ""
        return err

    @system_error_next.is_allowed
    # pylint: disable-next=unused-argument
    def system_error_next_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._admin_mode == 0


def main(args: Any = None, **kwargs: Any) -> int:
    """
    Launch an AWG Tango device server instance.

    :param args: arguments to the AWG device.
    :param kwargs: keyword arguments to the tango device server.

    :returns: exit code of the Tango server.
    """
    return tango.server.run((AwgVisaDevice,), args=args, **kwargs)


if __name__ == "__main__":
    print("*** Tango device for AWG ***")
    INTERACTIVE = True
    main()
