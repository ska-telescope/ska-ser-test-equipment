"""Mixin for loading waveforms into AWG."""

# pylint: disable=too-many-arguments,too-many-positional-arguments
# pylint: disable=useless-suppression

# mypy: disable-error-code=attr-defined

import gzip
import logging
import math
import os
import pprint
from typing import Any, Sequence

import matplotlib.pyplot as plt
import numpy as np
import pyvisa

from ska_ser_test_equipment.awg_visa.tektronix_awg_definitions import (
    AWG_AMPL,
    AWG_FREQ,
    AWG_SAMPLE,
)


# pylint: disable-next=too-many-instance-attributes,too-many-public-methods
class TektronixWaveformMixin:
    """Mixin class for waveforms."""

    logger: logging.Logger
    use_telnet: bool
    _waveform_maximum_length: int = 0
    _waveform_minimum_length: int = 0

    @property
    def waveform_maximum_length(self) -> int:
        """
        Get maximum number of waveform sample points allowed.

        WAVEFORM (369)

        This command returns the maximum number of waveform sample points allowed.

        The returned value is dependent on the instrument model, the installed options,
        and sampling rate setting.

        :returns: maximum length
        """
        self._waveform_maximum_length = self.read_query_int(
            "waveform_maximum_length", "WLIST:WAVEFORM:LMAXIMUM?"
        )
        return self._waveform_maximum_length

    @waveform_maximum_length.setter
    def waveform_maximum_length(self, value: int) -> None:
        """
        Set maximum number of waveform sample points allowed.

        :param value: new value
        """
        self.write_query_int(
            "waveform_maximum_length", "WLIST:WAVEFORM:LMAXIMUM", value
        )
        self._waveform_maximum_length = value

    @property
    def waveform_minimum_length(self) -> int:
        """
        Get minimum number of waveform sample points allowed.

        WAVEFORM (370)

        This command returns the minimum number of waveform sample points required
        for a valid waveform. The number of required sample points is dependent on
        the instrument model.

        :returns: minimum length
        """
        self._waveform_minimum_length = self.read_query_int(
            "waveform_minimum_length", "WLIST:WAVEFORM:LMINIMUM?"
        )
        return self._waveform_minimum_length

    @waveform_minimum_length.setter
    def waveform_minimum_length(self, value: int) -> None:
        """
        Set minimum number of waveform sample points allowed.

        :param value: new value
        """
        self.write_query_int(
            "waveform_minimum_length", "WLIST:WAVEFORM:LMINIMUM", value
        )
        self._waveform_minimum_length = value

    def source_amplitude_power(
        self, ch_num: int, pwr_val: float | None = None
    ) -> float:
        """
        Get amplitude in dBm.

        SOURCE (247)

        This command sets or returns the amplitude for the waveform associated with
        the specified channel in units of dBm.

        This is a blocking command.

        Output Path                 Range
        DC High BW                  –28.06 dBm to 1.48 dBm
        (With DC Amplified license) –28.06 dBm to 7.50 dBm
        AC Direct                   –17 dBm to –5 dBm
        AC Amplified                –85 dBm to +10 dBm
        (AC Amplified license required)

        :param ch_num: channel number
        :param pwr_val: new value
        :returns: amplitude in dBm
        """
        pwr = self.read_query_float(
            "source_amplitude_power", f"SOURCE{ch_num}:POWER:LEVEL:IMMEDIATE:AMPLITUDE?"
        )
        if math.isnan(pwr):
            self.logger.error("Could not read source amplitude power")
            return pwr
        if pwr_val is None:
            self.logger.info("Source amplitude power is %e", pwr)
            return pwr
        self.logger.info("Set source amplitude power to %e", pwr_val)
        pwr = self.write_query_float(
            f"ch {ch_num} source amplitude power",
            f"SOURCE{ch_num}:POWER:LEVEL:IMMEDIATE:AMPLITUDE",
            pwr_val,
            "dBm",
        )
        self.wait()
        return pwr

    def source_amplitude_voltage(
        self, ch_num: int, volt_val: float | None = None
    ) -> float:
        """
        Get amplitude in V.

        SOURCE (272)

        This command sets or returns the amplitude for the waveform associated with
        the specified channel in units of volts.

        This is a blocking command.

        Output Path                 Range
        DC High BW                  25 mV to 750 mV
        (With DC Amplified license) 25 mV to 1.5 V
        AC Direct                   89.34 mVpp to 355.7 mVpp
        AC Amplified                35.57 μVpp to 2 V
        (AC Amplified license required)

        :param ch_num: channel number
        :param volt_val: new value
        :returns: amplitude in V
        """
        volt = self.read_query_float(
            f"ch {ch_num} source amplitude voltage",
            f"SOURCE{ch_num}:VOLTAGE:LEVEL:IMMEDIATE:AMPLITUDE?",
        )
        if math.isnan(volt):
            self.logger.error("Could not read source amplitude voltage")
            return volt
        if volt_val is None:
            self.logger.info("Source amplitude voltage is %e", volt)
            return volt
        self.logger.info("Set source amplitude voltage to %e", volt_val)
        volt = self.write_query_float(
            f"ch {ch_num} source amplitude voltage",
            f"SOURCE{ch_num}:VOLTAGE:LEVEL:IMMEDIATE:AMPLITUDE",
            volt_val,
            "V",
        )
        self.wait()  # type: ignore[attr-defined]
        return volt

    def source_level_bias(self, ch_num: int, volt_val: float | None = None) -> float:
        """
        Get bias voltage.

        SOURCE (273)

        This command sets or returns the Bias (for AC output paths) for the waveform
        associated with the specified channel in units of volts.

        Output path must be an AC path.

        This value has no effect unless BIAS is enabled.

        This is a blocking command.

        :param ch_num: channel number
        :param volt_val: new value
        :returns: bias voltage
        """
        volt = self.read_query_float(
            f"ch {ch_num} source amplitude bias",
            f"SOURCE{ch_num}:VOLTAGE:LEVEL:IMMEDIATE:BIAS?",
        )
        if math.isnan(volt):
            self.logger.error("Could not read source amplitude bias")
            return volt
        if volt_val is None:
            self.logger.info("Source amplitude bias is %e", volt)
            return volt
        self.logger.info("Set source amplitude bias to %e", volt_val)
        volt = self.write_query_float(
            f"ch {ch_num} source amplitude bias",
            f"SOURCE{ch_num}:VOLTAGE:LEVEL:IMMEDIATE:BIAS",
            volt_val,
            "V",
        )
        self.wait()  # type: ignore[attr-defined]
        return volt

    def source_level_offset(self, ch_num: int, volt_val: float | None = None) -> float:
        """
        Get offset voltage.

        SOURCE (277)

        This command sets or returns the Offset (for DC output paths) for the waveform
        associated with the specified channel in units of volts.

        This is a blocking command.

        :param ch_num: channel number
        :param volt_val: new value
        :returns: offset voltage
        """
        volt = self.read_query_float(
            f"ch {ch_num} source amplitude offset",
            f"SOURCE{ch_num}:VOLTAGE:LEVEL:IMMEDIATE:OFFSET?",
        )
        if math.isnan(volt):
            self.logger.error("Could not read source amplitude offset")
            return volt
        if volt_val is None:
            self.logger.info("Source amplitude offset is %e", volt)
            return volt
        self.logger.info("Set source amplitude offset to %e", volt_val)
        volt = self.write_query_float(
            f"ch {ch_num} source amplitude offset",
            f"SOURCE{ch_num}:VOLTAGE:LEVEL:IMMEDIATE:OFFSET",
            volt_val,
            "V",
        )
        self.wait()  # type: ignore[attr-defined]
        return volt

    def source_amplitude_high(
        self, ch_num: int, volt_val: float | None = None
    ) -> float:
        """
        Get bias in V.

        SOURCE (275)

        This command sets or returns the high voltage level for the waveform
        associated with the specified channel in units of volts.

        The value is affected by the Offset setting (for DC modes) or the Bias setting
        (for AC modes).

        This is a blocking command.

        :param ch_num: channel number
        :param volt_val: new value
        :returns: high voltage
        """
        volt = self.read_query_float(
            f"ch {ch_num} source amplitude high",
            f"SOURCE{ch_num}:VOLTAGE:LEVEL:IMMEDIATE:HIGH?",
        )
        if math.isnan(volt):
            self.logger.error("Could not read source amplitude high")
            return volt
        if volt_val is None:
            self.logger.info("Source amplitude high is %e", volt)
            return volt
        self.logger.info("Set source amplitude high to %e", volt_val)
        volt = self.write_query_float(
            f"ch {ch_num} source amplitude high",
            f"SOURCE{ch_num}:VOLTAGE:LEVEL:IMMEDIATE:HIGH",
            volt_val,
            "V",
        )
        self.wait()  # type: ignore[attr-defined]
        return volt

    def source_amplitude_low(self, ch_num: int, volt_val: float | None = None) -> float:
        """
        Get bias in V.

        SOURCE (276)

        This command sets or returns the low voltage level for the waveform
        associated with the specified channel in units of volts.

        The value is affected by the Offset setting (for DC modes) or the Bias setting
        (for AC modes).

        This is a blocking command.

        :param ch_num: channel number
        :param volt_val: new value
        :returns: low voltage
        """
        volt = self.read_query_float(
            f"ch {ch_num} source amplitude low",
            f"SOURCE{ch_num}:VOLTAGE:LEVEL:IMMEDIATE::LOW?",
        )
        if math.isnan(volt):
            self.logger.error("Could not read source amplitude low")
            return volt
        if volt_val is None:
            self.logger.info("Source amplitude low is %e", volt)
            return volt
        self.logger.info("Set source amplitude low to %e", volt_val)
        volt = self.write_query_float(
            f"ch {ch_num} source amplitude low",
            f"SOURCE{ch_num}:VOLTAGE:LEVEL:IMMEDIATE:LOW",
            volt_val,
            "V",
        )
        self.wait()  # type: ignore[attr-defined]
        return volt

    def list_waves(self) -> list:
        """
        Read all waveform names in the waveform list.

        :returns: list of waveform names
        """
        o_res: str | None = self.read_query_str("list_waves", "WLIST:LIST?")
        if o_res is None:
            return []
        if o_res[0] == '"' and o_res[-1] == '"':
            o_res = o_res[1:-1]
        o_list: list = o_res.split(",")
        self.logger.info(f"Waveform list is {o_list}")
        return sorted(o_list)

    def dict_waves(self) -> dict:
        """
        Compile dictionary of waves.

        :returns: dictionary with wave data
        """
        rdict: dict = {}
        waves = self.list_waves()
        # maximum number of waveform sample points allowed
        rdict["lmax"] = self.read_query_int("lmax", "WLIST:WAVEFORM:LMAXIMUM?")
        # minimum number of waveform sample points required for a valid waveform
        rdict["lmin"] = self.read_query_int("lmax", "WLIST:WAVEFORM:LMINIMUM?")
        # sets or returns whether the measured Skew will be applied during
        # application of a pre-compensation file (correction coefficients file)
        rdict["skew"] = self.read_query_float("skew", "WLIST:WAVEFORM:ACFILE:SKEW?")
        # sets or returns whether a gaussian filter will be applied during the
        # application of a pre-compensation file (correction coefficients file)
        rdict["gaussian"] = self.read_query_str(
            "gaussian", "WLIST:WAVEFORM:ACFILE:GAUSSIAN?"
        )
        # bandwidth of the gaussian filter that is to be applied during application
        # of a pre-compensation file (correction coefficients file)
        rdict["bandwidth"] = self.read_query_float(
            "bandwidth", "WLIST:WAVEFORM:ACFILE:GAUSSIAN:BANDWIDTH?"
        )
        # granularity of sample points required for the waveform to be valid
        # number of sample points of a single channel instrument must be divisible by 2
        rdict["granularity"] = self.read_query_int(
            "granularity", "WLIST:WAVEFORM:GRANULARITY?"
        )
        self.logger.debug("Read waves %s", waves)
        for wave in waves:
            wave = wave.replace('"', "")
            self.logger.debug("Read wave %s", wave)
            rdict[wave] = {}
            # recommended amplitude (peak-to-peak) of the specified waveform in the
            # waveform list
            rdict[wave]["amplitude"] = self.read_query_float(
                "amplitude", f'WLIST:WAVEFORM:AMPLITUDE? "{wave}"'
            )
            rdict[wave]["offset"] = self.read_query_float(
                "offset", f'WLIST:WAVEFORM:OFFSET? "{wave}"'
            )
            # number of data points for the specified waveform in the waveform list
            rdict[wave]["length"] = self.read_query_int(
                "length", f'WLIST:WAVEFORM:LENGTH? "{wave}"'
            )
            # recommended offset of the specified waveform in the waveform list
            rdict[wave]["offset"] = self.read_query_float(
                "offset", f'WLIST:WAVEFORM:OFFSET? "{wave}"'
            )
            # recommended frequency of the specified waveform in the waveform list
            rdict[wave]["frequency"] = self.read_query_float(
                "frequency", f'WLIST:WAVEFORM:FREQUENCY? "{wave}"'
            )
            # signal format of the specified waveform in the waveform list
            rdict[wave]["sformat"] = self.read_query_str(
                "sformat", f'WLIST:WAVEFORM:SFORMAT? "{wave}"'
            )
            # recommended sampling rate of the specified waveform in the waveform list
            rdict[wave]["srate"] = self.read_query_float(
                "srate", f'WLIST:WAVEFORM:SRATE? "{wave}"'
            )
            # timestamp of the specified waveform in the waveform list
            rdict[wave]["tstamp"] = self.read_query_str(
                "tstamp", f'WLIST:WAVEFORM:TSTAMP? "{wave}"'
            )
        return rdict

    def plot_wave(self, x_data: Any, y_data: Any, wav_name: str) -> None:
        """
        Plot data in graph.

        :param x_data: numpy array to write
        :param y_data: numpy array to write
        :param wav_name: waveform asset name
        """
        wave_len = len(y_data)
        if x_data is None:
            x_data = np.arange(wave_len)
        # Plot the points using matplotlib
        self.logger.info('Plot wave "%s" (%d samples)', wav_name, wave_len)
        plt.title(f"{wav_name}")
        plt.plot(x_data, y_data)
        plt.show()

    # pylint: disable-next=too-many-locals
    def create_wave(  # noqa: C901
        self,
        ch_num: int,
        asset_name: str,
        y_values: Sequence[Any],
        awg_sample: float | None,
        check_err: bool = False,
        q_trail: int = 0,
    ) -> None:
        """
        Upload wave data.

        :param ch_num: channel number
        :param asset_name: name assigned to waveform
        :param y_values: output amplitude
        :param awg_sample: sample rate
        :param check_err: check for errors
        :param q_trail: trailing data hack
        """
        if asset_name is None:
            return
        awg = self.visa_hw
        if awg_sample is None:
            awg_sample = AWG_SAMPLE

        if ch_num:
            # set the output state of the specified channel to off
            self.logger.info("Turn off channel %d", ch_num)
            self.write_scpi(f"OUTPUT{ch_num}:STATE OFF")

        try:
            # delete a single waveform from the waveform list
            self.logger.info('Delete waveform "%s"', asset_name)
            self.write_scpi(f'WLIST:WAVEFORM:DEL "{asset_name}"')
        except pyvisa.errors.InvalidSession:
            self.logger.warning("Could not delete waveform %s", asset_name)

        rec_len = len(y_values)
        self.logger.info(
            'Wave "%s" (%d samples at %.3E per second) +%d',
            asset_name,
            rec_len,
            awg_sample,
            q_trail,
        )

        # set the sample rate for the clock
        self.write_scpi(f"CLOCK:SRATE {awg_sample}")
        # create a new empty waveform in the waveform list of the current setup
        self.write_scpi(f'WLIST:WAVEFORM:NEW "{asset_name}",{rec_len},REAL')

        n_blox: int = int(rec_len / self.m_blox)
        n_rems: int = rec_len % self.m_blox
        self.logger.info(
            "Write wave as %d blocks of %d plus %d", n_blox, self.m_blox, n_rems
        )
        n_end = 0
        for n_blok in range(0, n_blox):
            n_start = n_blok * self.m_blox
            n_end = n_start + self.m_blox
            y_vals = y_values[n_start:n_end]
            self.logger.info(
                "Write wave block %d at %d (%d samples)", n_blok, n_start, len(y_vals)
            )
            # transfer analog waveform data from the external controller into the
            # specified waveform or from a waveform to the external control program
            q_str = f'WLIST:WAVEFORM:DATA "{asset_name}",{n_start},{self.m_blox},'
            self.logger.info("Send %s", q_str)
            self.write_data_binary(q_str, y_vals, q_trail)
            if check_err:
                self.wait()
        if n_rems > 0:
            y_vals = y_values[n_end:]
            self.logger.info(
                "Write wave remainder %d at %d (%d samples)", n_rems, n_end, len(y_vals)
            )
            q_str = f'WLIST:WAVEFORM:DATA "{asset_name}",{n_end},{n_rems},'
            self.logger.info("Send %s", q_str)
            self.write_data_binary(q_str, y_vals, q_trail)
            if check_err:
                self.wait()

        if ch_num:
            # assign a waveform (from the waveform list) to the specified channel
            self.logger.info('Assign asset "%s" to channel %d', asset_name, ch_num)
            self.write_scpi(f'SOURCE{ch_num}:CASSET:WAVEFORM "{asset_name}"')
            self.logger.info("Turn on channel %d", ch_num)
            self.write_scpi(f"OUTPUT{ch_num}:STATE ON")
            self.logger.info("Run")
            self.start()

        if check_err:
            self.logger.info("Check for errors")
            # ensure that previous command is complete before next command is issued
            awg.query("*WAI")
            print(awg.query("SYSTEM:ERROR:ALL?"))

    def assign_wave(self, ch_num: int | None, asset_name: str | None = None) -> str:
        """
        Assign asset to channel.

        :param ch_num: channel number
        :param asset_name: name assigned to waveform
        :returns: asset name
        """
        if ch_num is None:
            return ""
        if asset_name is None:
            # assign a waveform (from the waveform list) to the specified channel
            self.logger.info("Read asset name for channel %d", ch_num)
            asset_name = self.read_query_str(
                "assign_wave", f"SOURCE{ch_num}:CASSET:WAVEFORM?"
            )
            return asset_name
        # assign a waveform (from the waveform list) to the specified channel
        self.logger.info('Assign asset "%s" to channel %d', asset_name, ch_num)
        self.write_scpi(f'SOURCE{ch_num}:CASSET:WAVEFORM "{asset_name}"')
        self.logger.info("Turn on channel %d", ch_num)
        self.write_scpi(f"OUTPUT{ch_num}:STATE ON")
        return asset_name

    def delete_wave(self, asset_name: str | None) -> None:
        """
        Delete waveform.

        :param asset_name: name assigned to waveform
        """
        if asset_name is None:
            return
        try:
            self.logger.info('Delete waveform "%s"', asset_name)
            self.write_scpi(f'WLIST:WAVEFORM:DEL "{asset_name}"')
        except pyvisa.errors.InvalidSession:
            self.logger.warning("Could not delete waveform %s", asset_name)

    def _read_file_data_line(self, line: Any) -> float:
        """

        Read value from line of file.

        :param line: value string
        :returns: float value
        """
        val_str = line.rstrip()
        try:
            vals = val_str.split(",")
            if len(vals) == 2:
                val = float(vals[1])
            elif len(vals) == 5:
                val = float(vals[0])
            else:
                val = float(vals[0])
        except TypeError:
            # self.logger.info("Could not split '%s'", val_str)
            val = float(val_str)
        return val

    # pylint: disable-next=too-many-locals
    def _read_file_data(self, input_file: str | None, block_len: int) -> Any:
        """
        Read data from file.

        :param input_file: file name
        :param block_len: block length
        :returns: numpy float array
        """
        _file_nam, file_ext = os.path.splitext(input_file)  # type: ignore[type-var]
        file_data: list = []
        if ".gz" in input_file:  # type: ignore[operator]
            self.logger.info(
                "Read %s zip file %s",
                file_ext[1:].upper(),  # type: ignore[index]
                input_file,
            )
            with gzip.open(input_file) as file:  # type: ignore[arg-type]
                for line in file:
                    val = self._read_file_data_line(line)
                    file_data.append(val)
            self.logger.info("Read %d records", len(file_data))
            wave_data = np.array(file_data)
        elif file_ext in (".txt", ".csv"):
            self.logger.info("Read %s text file %s", file_ext[1:].upper(), input_file)
            # pylint: disable-next=unspecified-encoding
            with open(input_file) as file:  # type: ignore[arg-type]
                bline: str
                for bline in file:
                    val = self._read_file_data_line(bline)
                    file_data.append(val)
            self.logger.info("Read %d text records", len(file_data))
            wave_data = np.array(file_data)
        else:
            self.logger.info(
                "Read %s binary file %s",
                file_ext[1:].upper(),  # type: ignore[index]
                input_file,
            )
            wave_data = np.fromfile(
                input_file, count=-1, sep="", offset=0  # type: ignore[arg-type]
            )
            self.logger.info("Read %d binary records", len(wave_data))

        if file_data:
            rec_len = len(file_data)
            rec_blocks = int(rec_len / block_len)
            rec_rem = rec_len % block_len
            for n in range(0, rec_blocks):
                self.logger.debug("Process block %d/%d", n + 1, rec_blocks)
                n1 = int(n * block_len)
                n2 = int((n + 1) * block_len)
                self.logger.info("Create wave: %d to %d of %d", n1, n2, rec_len)
                wave_slice = file_data[n1:n2]
                wave_data = np.array(wave_slice)
            if rec_rem:
                self.logger.debug("Process remaining %d", rec_rem)
        return wave_data

    # pylint: disable-next=too-many-locals
    def upload_wave_data(
        self,
        asset_name: str,
        ch_num: int,
        awg_sample: float | None,
        input_file: str | None,
        block_len: int,
        q_trail: int = 0,
    ) -> None:
        """
        Read data from file and write to AWG.

        :param asset_name: waveform asset name
        :param ch_num: channel number
        :param awg_sample: sampling rate
        :param input_file: file name
        :param block_len: length of block
        :param q_trail: trailing data hack
        """
        if input_file is None:
            return
        self.logger.info("Read data file %s", input_file)
        wave_data = self._read_file_data(input_file, block_len)
        self.logger.info("Read %d records +%d", len(wave_data), q_trail)
        self.create_wave(
            ch_num,
            asset_name,
            wave_data,  # type: ignore[arg-type]
            awg_sample,
            q_trail=q_trail,
        )

    # pylint: disable-next=too-many-locals,too-many-branches
    def write_wave_data(  # noqa: C901
        self,
        asset_name: str,
        ch_num: int,
        ch_volt: float | None,
        ch_freq: float | None,
        awg_sample: float | None,
        input_file: str | None,
        rec_len: int,
        rec_blocks: int | None,
        del_wave: bool,
        q_trail: int = 0,
    ) -> None:
        """
        Upload waveform data.

        :param asset_name: waveform name
        :param ch_num: channel number
        :param ch_volt: amplitude in Volt
        :param ch_freq: frequency in Hertz
        :param awg_sample: samples per second
        :param input_file: data input file
        :param rec_len: record length
        :param rec_blocks: record blocks
        :param del_wave: delete waveform
        :param q_trail: trailing data hack
        """
        if del_wave:
            if rec_blocks:
                for n in range(0, rec_blocks):
                    self.delete_wave(f"{asset_name}{n:04d}")
            else:
                self.delete_wave(asset_name)
            return

        if ch_volt is None:
            ch_volt = AWG_AMPL
        if ch_freq is None:
            ch_freq = AWG_FREQ
        if awg_sample is None:
            awg_sample = AWG_SAMPLE
        if input_file:
            try:
                self.upload_wave_data(
                    asset_name, ch_num, awg_sample, input_file, rec_len, q_trail
                )
            except ConnectionResetError as cx_err:
                self.logger.error("Could not upload: %s", cx_err)
        else:
            self.assign_wave(ch_num, asset_name)
        self.close()

    def read_wave_data(self, asset_name: str | None) -> list:  # noqa: C901
        """
        Download waveform data from AWG.

        :param asset_name: waveform asset name
        :returns: list with binary data
        """
        self.logger.info("Read wave data from asset %s", asset_name)
        # size of the specified waveform in the waveform list
        # returned value represents data points (not bytes)
        rec_len = self.read_query_int("len", f'WLIST:WAVEFORM:LENGTH? "{asset_name}"')
        if rec_len is None:
            rec_len = 0
        n_blox: int = int(rec_len / self.m_blox)
        n_rems: int = rec_len % self.m_blox
        self.logger.info(
            'Read data from asset "%s" (length %d: %d*%d+%d)',
            asset_name,
            rec_len,
            n_blox,
            self.m_blox,
            n_rems,
        )
        wave_data: list = []
        for n_blok in range(0, n_blox):
            n_start = n_blok * self.m_blox
            # transfer analog waveform data from the external controller into
            # the specified waveform or from a waveform to the external control program
            q_str = f'WLIST:WAVEFORM:DATA? "{asset_name}",{n_start},{self.m_blox}'
            self.logger.info("Send: %s", q_str)
            if self.use_telnet:
                str_data = self.read_query_str("read_wave_data", q_str)
                for str_val in str_data.split(","):
                    try:
                        wave_data.append(float(str_val))
                    except ValueError:
                        pass
            else:
                binary_data = self.visa_hw.query_binary_values(
                    q_str, datatype="f", is_big_endian=False
                )
                self.logger.info('Read %s: "%s"', type(binary_data), len(binary_data))
                wave_data.extend(binary_data)
        if n_rems > 0:
            n_start = n_blox * self.m_blox
            # transfer waveform data
            q_str = f'WLIST:WAVEFORM:DATA? "{asset_name}",{n_start},{n_rems}'
            self.logger.info("Send: %s", q_str)
            if self.use_telnet:
                str_data = self.read_query_str("read_wave_data", q_str)
                for str_val in str_data.split(","):
                    try:
                        wave_data.append(float(str_val))
                    except ValueError:
                        pass
            else:
                binary_data = self.visa_hw.query_binary_values(
                    q_str, datatype="f", is_big_endian=False
                )
                self.logger.info('Read %s: "%s"', type(binary_data), len(binary_data))
                wave_data.extend(binary_data)
        self.logger.info('Read asset "%s": %d samples', asset_name, len(wave_data))
        return wave_data

    def download_wave_data(  # noqa: C901
        self, asset_name: str | None, output_file: str | None
    ) -> None:
        """
        Download waveform data from AWG and write it to a file.

        :param asset_name: waveform asset name
        :param output_file: name of file to write
        """
        if output_file is None:
            self.logger.error("Input file not specified")
            return
        # number of data points for the specified waveform in the waveform list
        rec_len: int = self.read_query_int(
            "len", f'WLIST:WAVEFORM:LENGTH? "{asset_name}"'
        )
        self.logger.info(
            'Download data from asset "%s" (length %d)', asset_name, rec_len
        )
        if rec_len < self.m_blox:
            q_str = f'WLIST:WAVEFORM:DATA? "{asset_name}"'
            self.logger.info("Send: %s", q_str)
            binary_data = self.visa_hw.query_binary_values(
                q_str, datatype="f", is_big_endian=False
            )
            self.logger.info('Read %s: "%s"', type(binary_data), len(binary_data))
            self.logger.info("Write file %s", output_file)
            # pylint: disable-next=unspecified-encoding
            with open(output_file, "w") as file:
                item: int | str | bytes | os.PathLike[str] | os.PathLike[bytes]
                for item in binary_data:
                    w_data = item.decode(encoding="utf-8")  # type: ignore[union-attr]
                    file.write(f"{w_data:.6e}\n")
        else:
            n_blox: int = int(rec_len / self.m_blox)
            n_rems: int = rec_len % self.m_blox
            self.logger.info(
                "Write file %s in %d blocks of %d, plus %d",
                output_file,
                n_blox,
                self.m_blox,
                n_rems,
            )
            # pylint: disable-next=unspecified-encoding
            with open(output_file, "w") as file:
                for n_blok in range(0, n_blox):
                    n_start = n_blok * self.m_blox
                    q_str = (
                        f'WLIST:WAVEFORM:DATA? "{asset_name}",{n_start},{self.m_blox}'
                    )
                    self.logger.info("Send: %s", q_str)
                    binary_data = self.visa_hw.query_binary_values(
                        q_str, datatype="f", is_big_endian=False
                    )
                    self.logger.info(
                        'Read %s: "%s"', type(binary_data), len(binary_data)
                    )
                    for item in binary_data:
                        w_data = item.decode(  # type: ignore[union-attr]
                            encoding="utf-8"
                        )
                        file.write(f"{w_data:.6e}\n")
                if n_rems > 0:
                    n_start = n_blox * self.m_blox
                    q_str = f'WLIST:WAVEFORM:DATA? "{asset_name}",{n_start},{n_rems}'
                    self.logger.info("Send: %s", q_str)
                    binary_data = self.visa_hw.query_binary_values(
                        q_str, datatype="f", is_big_endian=False
                    )
                    self.logger.info(
                        'Read %s: "%s"', type(binary_data), len(binary_data)
                    )
                    for item in binary_data:
                        w_data = item.decode(  # type: ignore[union-attr]
                            encoding="utf-8"
                        )
                        file.write(f"{w_data:.6e}\n")

    def status_awg_wav(self) -> Any:
        """
        Read names, lengths e.a of waveform(s) from AWG.

        :returns: zero or one
        """
        pp = pprint.PrettyPrinter(depth=4)
        sdict = self.dict_waves()
        self.logger.info("Read waves: %s", sdict)
        for s_nam in sorted(sdict):
            print(f"{s_nam:} :")
            pp.pprint(sdict[s_nam])
        return sdict
