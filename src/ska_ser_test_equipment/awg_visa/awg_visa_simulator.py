#!/usr/bin/env python
"""A simple SCPI simulator for an arbitrary waveform generator."""

import json
import logging
import os
import time
from socket import gethostname
from typing import Dict, Final

# from ska_ser_devices.client_server.tcp import (
#     _TcpServerRequestHandler,
#     DEFAULT_BUFFER_SIZE,
#     TcpServer,
# )
from ska_ser_devices.client_server.tcp import TcpServer
from ska_ser_scpi import ScpiSimulator, SupportedAttributeType

try:
    from ..interface_definitions import InterfaceDefinitionFactory
except ImportError:
    from ska_ser_test_equipment.interface_definitions import InterfaceDefinitionFactory

log_level_str = os.getenv("LOG_LEVEL", "WARNING").upper()
if log_level_str == "DEBUG":
    LOG_LEVEL = logging.DEBUG
elif log_level_str == "INFO":
    LOG_LEVEL = logging.INFO
elif log_level_str == "WARNING":
    LOG_LEVEL = logging.WARNING
elif log_level_str == "ERROR":
    LOG_LEVEL = logging.ERROR
else:
    LOG_LEVEL = logging.WARNING
logging.basicConfig(level=LOG_LEVEL)
_module_logger = logging.getLogger(__name__)

AWG_TYPES = [
    "SINE",
    "SQUARE",
    "TRIANGLE",
    "NOISE",
    "DC",
    "GAUSSIAN",
    "EXPRISE",
    "EXPDECAY",
    "NONE",
]

CLOCK_SOURCES = ["INTERNAL", "EFIXED", "EVARIABLE", "EXTERNAL"]


class AwgVisaSimulator(ScpiSimulator):
    """A concrete simulator class."""

    DEFAULTS: Final[Dict[str, SupportedAttributeType]] = {
        "clock_sample_rate": 2.5e9,
        "clock_source": "INT",
        "command_error": False,
        "coupled_state": "NONE",
        "device_error": False,
        "execution_error": False,
        "instrument_mode": "AWG",
        "list_waves": "Sine100,Sine200,Sine300,Sine400,Sine500,Sine600,Sine700,Sine800",
        "waveform_maximum_length": 2000000000,
        "power_cycled": False,
        "query_error": False,
        "run_state": 0,
        "channel_nr": 1,
        **{f"channel{chan}_amplitude_power": -11.0 for chan in range(1, 9)},
        **{f"channel{chan}_amplitude_voltage": 0.17825 for chan in range(1, 9)},
        **{f"source{chan}_asset_name": f"SINE{chan}00" for chan in range(1, 9)},
        **{f"source{chan}_asset_type": "NONE" for chan in range(1, 9)},
        **{f"channel{chan}_dc_level": 0.0 for chan in range(1, 9)},
        **{f"channel{chan}_frequency": float(f"{chan}e6") for chan in range(1, 9)},
        **{f"channel{chan}_high": 0.25 for chan in range(1, 9)},
        **{f"channel{chan}_low": 0.25 for chan in range(1, 9)},
        **{f"channel{chan}_offset": 0.0 for chan in range(1, 9)},
        **{f"channel{chan}_output_state": True for chan in range(1, 9)},
        **{f"channel{chan}_path": "DHCB" for chan in range(1, 9)},
        **{f"channel{chan}_period": 1 / float(f"{chan}e6") for chan in range(1, 9)},
        **{f"channel{chan}_phase": 0.0 for chan in range(1, 9)},
        **{f"source{chan}_resolution": 16 for chan in range(1, 9)},
        **{f"source{chan}_run_mode": "CONT" for chan in range(1, 9)},
        **{f"source{chan}_skew": 0.25 for chan in range(1, 9)},
        **{f"channel{chan}_symmetry": 100 for chan in range(1, 9)},
        **{f"channel{chan}_type": AWG_TYPES[chan - 1] for chan in range(1, 9)},
    }

    def __init__(
        self,
        model: str,
        **kwargs: SupportedAttributeType,
    ) -> None:
        """
        Initialise a new instance.

        :param model: the model identifier to use.
        :param kwargs: initial values for simulator attributes; where an
            initial value is not provided for an attribute, a default
            value will be used.
        """
        interface_definition = InterfaceDefinitionFactory()(model)
        _module_logger.debug(
            "Interface definition %s", json.dumps(interface_definition, indent=2)
        )

        initial_values = kwargs
        for key, value in self.DEFAULTS.items():
            initial_values.setdefault(key, value)

        super().__init__(interface_definition, initial_values)

    def reset(self) -> None:
        """Reset to factory default values."""
        _module_logger.info("Reset to default values")
        self.set_attribute("instrument_mode", self.DEFAULTS["instrument_mode"])
        self.set_attribute("clock_source", self.DEFAULTS["clock_source"])
        self.set_attribute("clock_sample_rate", self.DEFAULTS["clock_sample_rate"])
        self.set_attribute("playing", self.DEFAULTS["playing"])

        # for i in range(1, 9):
        #     self.set_attribute(f"channel{i}_type", self.DEFAULTS[f"channel{i}_type"])
        #     self.set_attribute(f"channel{i}_freq", self.DEFAULTS[f"channel{i}_freq"])
        #     self.set_attribute(
        #         f"channel{i}_phase", self.DEFAULTS[f"channel{i}_phase"]
        #     )
        #     self.set_attribute(
        #         f"channel{i}_amplitude_power",
        #         self.DEFAULTS[f"channel{i}_amplitude_power"],
        #     )
        #     self.set_attribute(
        #         f"channel{i}_output_state", self.DEFAULTS[f"channel{i}_output_state"]
        #     )
        _module_logger.debug("Reset AWG")
        for key, value in self.DEFAULTS.items():
            self.set_attribute(key, value)

    def run(self) -> None:
        """Simulate the AWG Play command."""
        _module_logger.debug("Run AWG")
        self.set_attribute("run_state", 2)

    def stop(self) -> None:
        """Simulate the AWG Stop command."""
        _module_logger.debug("Stop AWG")
        self.set_attribute("run_state", 0)

    def wait(self) -> None:
        """Wait for something to happen."""
        _module_logger.debug("Wait 0.1 second")
        time.sleep(0.1)

    # def channel1_output_state(self):
    #     _module_logger.debug("Read channel1_output_state: %s")

    def __repr__(self) -> str:
        """
        Represent this thing as a string.

        :returns: string thing
        """
        return f"AWG: {self.get_attribute('identity')}"


def main() -> None:
    """Run the socketserver main loop."""
    model = os.getenv("SIMULATOR_MODEL", "AWG5208I").upper()
    host = os.getenv("SIMULATOR_HOST", gethostname())
    port = int(os.getenv("SIMULATOR_PORT", "4000"))

    _module_logger.info("Start AWG simulator for %s on %s:%d", model, host, port)
    simulator = AwgVisaSimulator(model)
    n_try = 0
    n_max = 5
    server = None
    while n_try <= n_max:
        n_try += 1
        try:
            server = TcpServer(host, port, simulator)
        except OSError as os_err:
            _module_logger.error(str(os_err))
            _module_logger.warning("Retry %d of %d", n_try, n_max)
            time.sleep(15)
            continue
        break
    if server is not None:
        with server:
            server.serve_forever()


if __name__ == "__main__":
    print("*** Simulator for arbitrary waveform generator ***")
    try:
        main()
    except KeyboardInterrupt:
        _module_logger.error("Interrupted")
