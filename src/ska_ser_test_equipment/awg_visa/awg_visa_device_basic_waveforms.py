"""Things related to AWG waveforms are implemented here."""

import logging
import math
from typing import Any

from ska_control_model import AdminMode
from tango import AttrQuality, TimeVal
from tango.server import attribute


# pylint: disable-next=too-many-public-methods
class Awg5208DeviceBasicWaveformsMixin:
    """Mixin for the waves."""

    tektronix: Any
    logger: logging.Logger
    _admin_mode: AdminMode
    _bwaveform_amplitude: float = float("nan")
    _bwaveform_assign: int = -1
    _bwaveform_auto_calculate: str = ""
    _bwaveform_cycles: int = -1
    _bwaveform_frequency: float = float("nan")
    _bwaveform_full_dac_range: bool = False
    _bwaveform_function: str = ""
    _bwaveform_high: float = float("nan")
    _bwaveform_length: int = -1
    _bwaveform_low: float = float("nan")
    _bwaveform_name: str = ""
    _bwaveform_offset: float = float("nan")
    _bwaveform_play: bool = False
    _bwaveform_sample_rate: float = float("nan")

    @attribute
    def bwaveform_amplitude(self) -> float:
        """
        Read waveform amplitude.

        :returns:amplitude
        """
        self._bwaveform_amplitude = self.tektronix.bwaveform_amplitude
        if math.isnan(self._bwaveform_amplitude):
            return (  # type: ignore[return-value]
                self._bwaveform_amplitude,
                TimeVal.totime(TimeVal.now()),
                AttrQuality.ATTR_INVALID,
            )
        return self._bwaveform_amplitude

    @bwaveform_amplitude.is_allowed
    # pylint: disable-next=unused-argument
    def bwaveform_amplitude_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._admin_mode == 0

    @bwaveform_amplitude.write  # type: ignore[no-redef]
    def bwaveform_amplitude(self, value: float) -> None:
        """
        Write waveform amplitude.

        :param value: change to this amplitude
        """
        self.tektronix.bwaveform_amplitude = value

    @attribute(dtype=bool)
    def bwaveform_assign(self) -> bool:
        """
        Read waveform assign.

        This command sets or returns the state (enabled or disabled) of the Basic
        Waveform editor to either compile the waveform and immediately assign it to a
        specified channel (enabled) or just compile the waveform (disabled).

        :returns: assign
        """
        return self.tektronix.bwaveform_assign

    @bwaveform_assign.is_allowed
    # pylint: disable-next=unused-argument
    def bwaveform_assign_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._admin_mode == 0

    @bwaveform_assign.write  # type: ignore[no-redef]
    def bwaveform_assign(self, value: bool) -> None:
        """
        Write waveform assign.

        :param value: change waveform to this
        """
        self.tektronix.bwaveform_assign = value

    @attribute(dtype=str)
    def bwaveform_auto_calculate(self) -> str:
        """
        Read waveform auto calculate (LEN|CYCLE|DUR|FREQ|SR).

        :returns: auto calculate
        """
        return self.tektronix.bwaveform_auto_calculate

    @bwaveform_auto_calculate.is_allowed
    # pylint: disable-next=unused-argument
    def bwaveform_auto_calculate_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._admin_mode == 0

    @bwaveform_auto_calculate.write  # type: ignore[no-redef]
    def bwaveform_auto_calculate(self, value: str) -> None:
        """
        Write auto calculate.

        :param value: change auto calculate to this
        """
        self._bwaveform_auto_calculate = value
        self.tektronix.bwaveform_auto_calculate = value

    @attribute(dtype=float)
    def bwaveform_cycles(self) -> float:
        """
        Read waveform cycles.

        :returns: cycles
        """
        return self.tektronix.bwaveform_cycles

    @bwaveform_cycles.is_allowed
    # pylint: disable-next=unused-argument
    def bwaveform_cycles_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._admin_mode == 0

    @bwaveform_cycles.write  # type: ignore[no-redef]
    def bwaveform_cycles(self, value: float) -> None:
        """
        Write .

        :param value: change cycles to this
        """
        self.tektronix.bwaveform_cycles = value

    @attribute(min_value=1, max_value=2.5e9)
    def bwaveform_frequency(self) -> float:
        """
        Read waveform frequency.

        :returns: frequency
        """
        self._bwaveform_frequency = self.tektronix.bwaveform_frequency
        if math.isnan(self._bwaveform_frequency):
            return (  # type: ignore[return-value]
                self._bwaveform_frequency,
                TimeVal.totime(TimeVal.now()),
                AttrQuality.ATTR_INVALID,
            )
        return self._bwaveform_frequency

    @bwaveform_frequency.is_allowed
    # pylint: disable-next=unused-argument
    def bwaveform_frequency_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._admin_mode == 0

    @bwaveform_frequency.write  # type: ignore[no-redef]
    def bwaveform_frequency(self, value: float) -> None:
        """
        Write waveform frequency.

        :param value: change to this DAC range
        """
        self.tektronix.bwaveform_frequency = value

    @attribute(dtype=bool)
    def bwaveform_full_dac_range(self) -> bool:
        """
        Read waveform full DAC range.

        :returns: DAC range
        """
        return self.tektronix.bwaveform_full_dac_range

    @bwaveform_full_dac_range.is_allowed
    # pylint: disable-next=unused-argument
    def bwaveform_full_dac_range_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._admin_mode == 0

    @bwaveform_full_dac_range.write  # type: ignore[no-redef]
    def bwaveform_full_dac_range(self, value: bool) -> None:
        """
        Write setting for DAC range.

        :param value: change to this DAC range
        """
        self.tektronix.bwaveform_full_dac_range = value

    @attribute(dtype=str)
    def bwaveform_function(self) -> str:
        """
        Read waveform type.

        :returns: type
        """
        return self.tektronix.bwaveform_function

    @bwaveform_function.write  # type: ignore[no-redef]
    def bwaveform_function(self, value: str) -> None:
        """
        Write waveform type.

        :param value: change to this type
        """
        self.tektronix.waveform_function = value

    @attribute(dtype=float)
    def bwaveform_high(self) -> float:
        """
        Read waveform high value.

        :returns: high value
        """
        self._bwaveform_high = self.tektronix.bwaveform_high
        self.logger.debug("Read bwaveform_high: %f", self._bwaveform_high)
        if math.isnan(self._bwaveform_high):
            return (  # type: ignore[return-value]
                self._bwaveform_high,
                TimeVal.totime(TimeVal.now()),
                AttrQuality.ATTR_INVALID,
            )
        return self._bwaveform_high

    @bwaveform_high.is_allowed
    # pylint: disable-next=unused-argument
    def bwaveform_high_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._admin_mode == 0

    @bwaveform_high.write  # type: ignore[no-redef]
    def bwaveform_high(self, value: float) -> None:
        """
        Write waveform high value.

        :param value: change to this high value
        """
        self.tektronix.bwaveform_high = value

    @attribute(dtype=float)
    def bwaveform_length(self) -> float:
        """
        Read waveform length.

        :returns: length
        """
        return self.tektronix.bwaveform_length

    @bwaveform_length.is_allowed
    # pylint: disable-next=unused-argument
    def bwaveform_length_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._admin_mode == 0

    @bwaveform_length.write  # type: ignore[no-redef]
    def bwaveform_length(self, value: float) -> None:
        """
        Write waveform length.

        :param value: change to this length
        """
        self.tektronix.bwaveform_length = value

    @attribute(dtype=float)
    def bwaveform_low(self) -> float:
        """
        Read waveform low value.

        :returns: low value
        """
        self._bwaveform_low = self.tektronix.bwaveform_low
        if math.isnan(self._bwaveform_low):
            return (  # type: ignore[return-value]
                self._bwaveform_low,
                TimeVal.totime(TimeVal.now()),
                AttrQuality.ATTR_INVALID,
            )
        return self._bwaveform_low

    @bwaveform_low.is_allowed
    # pylint: disable-next=unused-argument
    def bwaveform_low_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._admin_mode == 0

    @bwaveform_low.write  # type: ignore[no-redef]
    def bwaveform_low(self, value: float) -> None:
        """
        Write waveform low value.

        :param value: change to this low value
        """
        self._bwaveform_low = value
        self.tektronix.bwaveform_low = value

    @attribute(dtype=float)
    def bwaveform_offset(self) -> float:
        """
        Read waveform offset.

        :returns: offset
        """
        self._bwaveform_offset = self.tektronix.bwaveform_offset
        if math.isnan(self._bwaveform_offset):
            return (  # type: ignore[return-value]
                self._bwaveform_offset,
                TimeVal.totime(TimeVal.now()),
                AttrQuality.ATTR_INVALID,
            )
        return self._bwaveform_offset

    @bwaveform_offset.is_allowed
    # pylint: disable-next=unused-argument
    def bwaveform_offset_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._admin_mode == 0

    @bwaveform_offset.write  # type: ignore[no-redef]
    def bwaveform_offset(self, value: float) -> None:
        """
        Write waveform offset.

        :param value: change to this offset
        """
        self._bwaveform_offset = value
        self.tektronix.bwaveform_offset = value

    @attribute(dtype=bool)
    def bwaveform_play(self) -> bool:
        """
        Read waveform play.

        :returns: play
        """
        return self.tektronix.bwaveform_play

    @bwaveform_play.is_allowed
    # pylint: disable-next=unused-argument
    def bwaveform_play_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._admin_mode == 0

    @bwaveform_play.write  # type: ignore[no-redef]
    def bwaveform_play(self, value: bool) -> None:
        """
        Write waveform play.

        :param value: change to this play
        """
        self.tektronix.bwaveform_play = value

    @attribute(dtype=float)
    def bwaveform_sample_rate(self) -> float:
        """
        Read waveform sample rate.

        :returns: sample rate
        """
        self._bwaveform_sample_rate = self.tektronix.bwaveform_sample_rate
        if math.isnan(self._bwaveform_sample_rate):
            return (  # type: ignore[return-value]
                self._bwaveform_sample_rate,
                TimeVal.totime(TimeVal.now()),
                AttrQuality.ATTR_INVALID,
            )
        return self._bwaveform_sample_rate

    @bwaveform_sample_rate.is_allowed
    # pylint: disable-next=unused-argument
    def bwaveform_sample_rate_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._admin_mode == 0

    @bwaveform_sample_rate.write  # type: ignore[no-redef]
    def bwaveform_sample_rate(self, value: float) -> None:
        """
        Write waveform sample rate.

        :param value: change to this rate
        """
        self.tektronix.bwaveform_sample_rate = value
