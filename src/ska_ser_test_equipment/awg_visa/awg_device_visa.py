#! /usr/bin/env python
# pylint: disable=too-many-lines
# mypy: disable-error-code="union-attr"
"""
Implement a Tango Device for Tektronix AWG.

Includes support for Virtual Instrument Software Architecture.

This is a proof of concept at the moment.

from tango.server import CmdArgType, DevDouble, DevVarFloatArray
    @attribute(dtype=(CmdArgType.DevDouble,),
        max_dim_x=1000,)
    def float_array(self) -> CmdArgType.DevVarFloatArray:
        value = (1.0, 2.0, 3.0)
        return value
"""
import logging
import os
from typing import Dict

import tango
from tango.server import attribute

from ska_ser_test_equipment.scpi.netcat import netcat, netcat_tx
from ska_ser_test_equipment.scpi.scpi_config import (
    POLL_COUNT,
    TANGO_LOG_LEVEL,
    get_host_port,
    get_scpi_config,
    get_scpi_polls,
)
from ska_ser_test_equipment.scpi.scpi_visa import visa_tx, visa_tx_rx

LOG_LEVEL = logging.DEBUG
logging.basicConfig(level=LOG_LEVEL)
_module_logger = logging.getLogger(__name__)

MAX_CHANNELS = 8


def cmd_tx_rx(
    host: str, port: int | None, scpi_tx: str, sentinel_string: str
) -> str | None:
    """
    Send SCPI command to instrument and read reply.

    :param host: instrument IP address or hostname
    :param port: instrument port number
    :param scpi_tx: SCPI command to be sent to instrument
    :param sentinel_string: appended to command

    :returns: message as received
    """
    if port:
        # Use raw TCP
        scpi_rx = netcat(host, port, scpi_tx, sentinel_string)
        _module_logger.info("Read TCP %s, received %s", scpi_tx, scpi_rx)
    else:
        # Use VISA
        scpi_rx = visa_tx_rx(host, scpi_tx, 30)
        _module_logger.info("Read VISA %s, received %s", scpi_tx, scpi_rx)
    return scpi_rx


def cmd_tx(host: str, port: int | None, scpi_tx: str, sentinel_string: str) -> None:
    """
    Send SCPI command to instrument.

    :param host: instrument IP address or hostname
    :param port: instrument port number
    :param scpi_tx: SCPI command to be sent to instrument
    :param sentinel_string: appended to command
    """
    if port:
        # Use raw TCP
        _module_logger.info("Write TCP %s", scpi_tx)
        netcat_tx(host, port, scpi_tx, sentinel_string)
    else:
        # Use VISA
        _module_logger.info("Write VISA %s", scpi_tx)
        visa_tx(host, scpi_tx, 5)


# pylint: disable-next=too-many-instance-attributes,too-many-public-methods
class AwgVisaChannel:
    """Implement channel of AWG."""

    logger: logging.Logger | None = None
    admin_mode: bool = True
    host: str = ""
    port: int | None = 0
    argument_separator: str = " "
    sentinel_string: str = "\n"

    # pylint: disable-next=too-many-arguments
    def __init__(
        self,
        logger: logging.Logger,
        hostnam: str,
        port: int | None,
        argument_separator: str,
        sentinel_string: str,
        ch_num: int,
        scpi_fields: dict,
        scpi_values: dict,
        scpi_mins: dict,
        scpi_maxs: dict,
    ):
        """
        Initialise this thing.

        :param logger: logging handle
        :param hostnam: host name or address
        :param port: port number
        :param argument_separator: goes between command and argument
        :param sentinel_string: added to end of command string
        :param ch_num: channel number
        :param scpi_fields: SCPI commands
        :param scpi_values: default values
        :param scpi_mins: minimum values
        :param scpi_maxs: maximum values
        """
        self.logger = logger
        self.host = hostnam
        self.port = port
        self.ch_num = ch_num
        self.logger.debug("Initialize channel %d", self.ch_num)
        self.admin_mode = True
        self.argument_separator = argument_separator
        self.sentinel_string = sentinel_string
        self.attr_names = {
            "type": f"channel{self.ch_num}_type",
            "phase": f"channel{self.ch_num}_phase",
            "freq": f"channel{self.ch_num}_freq",
            "amplitude_power": f"channel{self.ch_num}_amplitude_power",
            "output_state": f"channel{self.ch_num}_output_state",
        }
        self.scpi_values = {}
        self.scpi_fields = {}
        self.scpi_mins = {}
        self.scpi_maxs = {}
        self.scpi_polls = {}
        # pylint: disable-next=consider-using-dict-items
        for attr_key in self.attr_names:
            attr_name = self.attr_names[attr_key]
            self.scpi_values[attr_key] = scpi_values[attr_name]
            self.scpi_fields[attr_key] = scpi_fields[attr_name]
            if attr_name in scpi_mins:
                self.scpi_mins[attr_key] = scpi_mins[attr_name]
            if attr_name in scpi_maxs:
                self.scpi_maxs[attr_key] = scpi_maxs[attr_name]
            self.scpi_polls[attr_key] = POLL_COUNT

    def set_admin_mode(self, adm_mode: bool) -> None:
        """
        Control device status.

        :param adm_mode: new mode
        """
        self.admin_mode = adm_mode

    def attribute_poll(self, attr_name: str) -> bool:
        """
        Check if instrument should be read.

        :param attr_name: attribute name to be checked
        :returns: flag to indicate read
        """
        self.scpi_polls[attr_name] += 1
        self.logger.debug(
            "Poll %s %d : %d",
            attr_name,
            self.scpi_polls[attr_name],
            self.scpi_values[attr_name],
        )
        if self.scpi_polls[attr_name] < POLL_COUNT:
            return True
        return False

    def read_type(self) -> str:
        """
        Send command FGEN:CHANNEL2:TYPE.

        :returns: channel type
        """
        if self.admin_mode:
            return self.scpi_values["type"]
        if self.attribute_poll("type"):
            return self.scpi_values["type"]
        return self.channel_read_str("type")

    def write_type(self, ch_type: str) -> None:
        """
        Send command SOURCE1:CASSET:WAVEFORM.

        :param ch_type: channel type e.g. SINE
        """
        if self.admin_mode:
            self.logger.warning("Admin mode: cannot write channel %d type", self.ch_num)
            return
        self.logger.info("Set channel type to %s", ch_type)
        self.channel_write_str("type", ch_type)

    def read_state(self) -> bool:
        """
        Send command OUTPUT1:STATE.

        :returns: channel output state
        """
        if self.admin_mode:
            return self.scpi_values["output_state"]
        if self.attribute_poll("output_state"):
            return self.scpi_values["output_state"]
        return self.channel_read_bool("output_state")

    def write_state(self, ch_state: bool) -> None:
        """
        Send command SOURCE1:CASSET:WAVEFORM.

        :param ch_state: turn channel on or off
        """
        if self.admin_mode:
            self.logger.warning(
                "Admin mode: cannot write channel %d output state", self.ch_num
            )
            return
        self.logger.info("Set channel %d state to %s", self.ch_num, ch_state)
        self.channel_write_bool("output_state", ch_state)

    def read_freq(self) -> float:
        """
        Send command FGEN:CHANNEL2:FREQUENCY.

        :returns: channel frequency
        """
        if self.admin_mode:
            return self.scpi_values["freq"]
        if self.attribute_poll("freq"):
            return self.scpi_values["freq"]
        return self.channel_read_float("freq")

    def write_freq(self, ch_freq: float) -> None:
        """
        Set frequency for channel.

        :param ch_freq: frequency in Hz
        """
        if self.admin_mode:
            self.logger.warning("Admin mode: cannot write channel %d freq", self.ch_num)
            return
        if self.minimum_value_check("freq", ch_freq):
            return
        # TODO maximum frequency depends on options fitted
        if self.maximum_value_check("freq", ch_freq):
            return
        self.logger.info("Set channel 2 frequency to %f Hz", ch_freq)
        self.channel_write_float("freq", ch_freq)

    def read_amplitude_power(self) -> float:
        """
        Get amplitude for channel.

        :returns: channel amplitude in dBm
        """
        if self.admin_mode:
            return self.scpi_values["amplitude_power"]
        if self.attribute_poll("amplitude_power"):
            return self.scpi_values["amplitude_power"]
        return self.channel_read_float("amplitude_power")

    def write_amplitude_power(self, ch_power: float) -> None:
        """
        Set amplitude for channel.

        :param ch_power: channel amplitude in dBm
        """
        if self.admin_mode:
            self.logger.warning(
                "Admin mode: cannot write channel ^d amplitude power", self.ch_num
            )
            return
        # TODO add minimum and maximum values to configuration
        if self.minimum_value_check("amplitude_power", ch_power):
            return
        if self.maximum_value_check("amplitude_power", ch_power):
            return
        self.logger.info("Set channel %d amplitude to %f dBm", self.ch_num, ch_power)
        self.channel_write_float("amplitude_power", ch_power)

    def read_phase(self) -> float:
        """
        Get phase for channel.

        :returns: channel phase in degrees
        """
        if self.admin_mode:
            return self.scpi_values["phase"]
        if self.attribute_poll("phase"):
            return self.scpi_values["phase"]
        return self.channel_read_float("phase")

    def write_phase(self, ch_phase: float) -> None:
        """
        Set phase for channel.

        :param ch_phase: channel phase in degrees
        """
        if self.admin_mode:
            self.logger.warning("Admin mode: cannot write phase")
            return
        # TODO add minimum and maximum values to configuration
        if self.minimum_value_check("phase", ch_phase):
            return
        if self.maximum_value_check("phase", ch_phase):
            return
        self.logger.info("Set channel %d phase to %f degrees", self.ch_num, ch_phase)
        self.channel_write_float("phase", ch_phase)

    def minimum_value_check(self, attr_name: str, attr_value: float | int) -> bool:
        """
        Check attribute value.

        :param attr_name: attribute name
        :param attr_value: attribute value
        :returns: true if value is greater than configured minimum
        """
        if attr_name in self.scpi_mins:
            min_val = self.scpi_mins[attr_name]
            if attr_value < min_val:
                self.logger.warning(
                    "Value for %s %s is lower than minimum %s",
                    attr_name,
                    f"{attr_value}",
                    f"{min_val}",
                )
                return True
        else:
            self.logger.warning("No minimum value for %s", attr_name)
        return False

    def maximum_value_check(self, attr_name: str, attr_value: float | int) -> bool:
        """
        Check attribute value.

        :param attr_name: attribute name
        :param attr_value: attribute value
        :returns: true if value is greater than configured maximum
        """
        if attr_name in self.scpi_maxs:
            max_val = self.scpi_maxs[attr_name]
            if attr_value > max_val:
                self.logger.warning(
                    "Value for %s : %s is higher than maximum %s",
                    attr_name,
                    f"{attr_value}",
                    f"{max_val}",
                )
                return True
        else:
            self.logger.warning("No maximum value for %s", attr_name)
        return False

    def channel_read_str(self, attr_name: str) -> str:
        """
        Read attribute value.

        :param attr_name: attribute name

        :returns: attribute value
        """
        self.scpi_polls[attr_name] = 0
        scpi_tx = f"{self.scpi_fields[attr_name]}?"
        scpi_rx = cmd_tx_rx(self.host, self.port, scpi_tx, self.sentinel_string)
        self.logger.debug("Read str %s (%s) %s", attr_name, scpi_tx, scpi_rx)
        if scpi_rx is None:
            rval = "ERROR"
        else:
            rval = str(scpi_rx)
        self.scpi_values[attr_name] = rval
        return rval

    def channel_read_float(self, attr_name: str) -> float:
        """
        Read attribute value.

        :param attr_name: attribute name

        :returns: attribute value
        """
        self.scpi_polls[attr_name] = 0
        scpi_tx = f"{self.scpi_fields[attr_name]}?"
        scpi_rx = cmd_tx_rx(self.host, self.port, scpi_tx, self.sentinel_string)
        self.logger.debug("Read float %s (%s) %s", attr_name, scpi_tx, scpi_rx)
        if scpi_rx is None:
            rval = 0.0
        else:
            rval = float(scpi_rx)
        self.scpi_values[attr_name] = rval
        return rval

    def channel_read_int(self, attr_name: str) -> int:
        """
        Read attribute value.

        :param attr_name: attribute name

        :returns: attribute value
        """
        self.scpi_polls[attr_name] = 0
        scpi_tx = f"{self.scpi_fields[attr_name]}?"
        scpi_rx = cmd_tx_rx(self.host, self.port, scpi_tx, self.sentinel_string)
        self.logger.debug("Read int %s (%s) %s", attr_name, scpi_tx, scpi_rx)
        if scpi_rx is None:
            rval = 0
        else:
            rval = int(scpi_rx)
        self.scpi_values[attr_name] = rval
        return rval

    def channel_read_bool(self, attr_name: str) -> bool:
        """
        Read attribute value.

        :param attr_name: attribute name

        :returns: attribute value
        """
        self.scpi_polls[attr_name] = 0
        scpi_tx = f"{self.scpi_fields[attr_name]}?"
        scpi_rx = cmd_tx_rx(self.host, self.port, scpi_tx, self.sentinel_string)
        self.logger.debug("Read bool %s (%s) %s", attr_name, scpi_tx, scpi_rx)
        if scpi_rx is None:
            rval = False
        elif int(scpi_rx):
            rval = True
        else:
            rval = False
        self.scpi_values[attr_name] = rval
        return rval

    def channel_write_str(self, attr_name: str, attr_value: str) -> None:
        """
        Read attribute value.

        :param attr_name: attribute name
        :param attr_value: attribute value
        """
        if self.admin_mode:
            self.logger.warning("Cannot write %s in admin mode", attr_name)
            return
        scpi_tx = f"{self.scpi_fields[attr_name]}{self.argument_separator}{attr_value}"
        self.logger.debug("Write str %s (%s)", attr_name, scpi_tx)
        cmd_tx(self.host, self.port, scpi_tx, self.sentinel_string)
        self.scpi_values[attr_name] = attr_value

    def channel_write_float(self, attr_name: str, attr_value: float) -> None:
        """
        Read attribute value.

        :param attr_name: attribute name
        :param attr_value: attribute value
        """
        if self.admin_mode:
            self.logger.warning("Cannot write %s in admin mode", attr_name)
            return
        scpi_tx = f"{self.scpi_fields[attr_name]}{self.argument_separator}{attr_value}"
        self.logger.debug("Write float %s (%s)", attr_name, scpi_tx)
        cmd_tx(self.host, self.port, scpi_tx, self.sentinel_string)
        self.scpi_values[attr_name] = attr_value

    def channel_write_int(self, attr_name: str, attr_value: int) -> None:
        """
        Read attribute value.

        :param attr_name: attribute name
        :param attr_value: attribute value
        """
        if self.admin_mode:
            self.logger.warning("Cannot write %s in admin mode", attr_name)
            return
        scpi_tx = f"{self.scpi_fields[attr_name]}{self.argument_separator}{attr_value}"
        self.logger.debug("Write int %s (%s)", attr_name, scpi_tx)
        cmd_tx(self.host, self.port, scpi_tx, self.sentinel_string)
        self.scpi_values[attr_name] = attr_value

    def channel_write_bool(self, attr_name: str, attr_value: bool) -> None:
        """
        Read attribute value.

        :param attr_name: attribute name
        :param attr_value: attribute value
        """
        if self.admin_mode:
            self.logger.warning("Cannot write %s in admin mode", attr_name)
            return
        if attr_value:
            scpi_tx = f"{self.scpi_fields[attr_name]}{self.argument_separator}1"
        else:
            scpi_tx = f"{self.scpi_fields[attr_name]}{self.argument_separator}0"
        self.logger.debug("Write bool %s (%s)", attr_name, scpi_tx)
        cmd_tx(self.host, self.port, scpi_tx, self.sentinel_string)
        self.scpi_values[attr_name] = attr_value

    def channel_poll(self, attr_name: str) -> bool:
        """
        Check if instrument should be read.

        :param attr_name: attribute name to be checked
        :returns: flag to indicate read
        """
        self.scpi_polls[attr_name] += 1
        self.logger.debug(
            "Poll frequency_stop %d : %d",
            self.scpi_polls[attr_name],
            self.scpi_values[attr_name],
        )
        if self.scpi_polls[attr_name] < POLL_COUNT:
            return True
        return False


# pylint: disable-next=too-many-public-methods,too-many-instance-attributes
class AwgDeviceVisa(tango.server.Device):
    """The AWG device."""

    logger: logging.Logger | None = None
    admin_mode: bool = True
    host: str = ""
    port: int | None = 0
    argument_separator: str = " "
    sentinel_string: str = "\n"
    scpi_fields: Dict[str, str] = {}
    scpi_polls: Dict[str, int] = {}
    scpi_values: Dict[str, int | float | str | bool] = {}
    scpi_mins: Dict[str, int | float] = {}
    event_status_register: int = 0

    def init_device(self) -> None:
        """
        Initialize the device.

        :raises ValueError: when YAML file can not be read
        """
        device_name: str = self.get_name()
        # self.logger = logging.getLogger(device_name)
        # self.logger.setLevel(_module_logger.getEffectiveLevel())
        self.logger = _module_logger
        self.logger.info("Initialize spectrum analyser device %s", device_name)
        super().init_device()
        self.set_status("DISABLE")
        self.set_state(tango.DevState.DISABLE)
        model = os.getenv("SIMULATOR_MODEL", "AWG5028I").upper()
        self.host, self.port = get_host_port("awgvisa")
        if not self.port:
            self.logger.warning("No SCPI port, use VISA host %s", self.host)
            # self.host = "127.0.0.1"
            self.port = None
        # Read configuration
        (
            self.scpi_fields,
            self.scpi_values,
            self.scpi_mins,
            self.scpi_maxs,
        ) = get_scpi_config(model)
        self.logger.info("Minimum values %s", self.scpi_mins)
        if not self.scpi_fields:
            # self.logger.error("Could not read SCPI fields")
            raise ValueError(f"No SCPI fields, check {' '.join(os.listdir('/'))}")
        self.scpi_polls = get_scpi_polls(self.scpi_fields)
        self.channels = []
        for ch_num in range(0, MAX_CHANNELS):
            self.logger.debug("Initialize channel %d", ch_num + 1)
            self.channels.append(
                AwgVisaChannel(
                    self.logger,
                    self.host,
                    self.port,
                    self.argument_separator,
                    self.sentinel_string,
                    ch_num + 1,
                    self.scpi_fields,
                    self.scpi_values,
                    self.scpi_mins,
                    self.scpi_maxs,
                )
            )

    def create_component_manager(self) -> None:
        """Create and return a component manager for this device."""
        self.logger.warning("No component manager")

    def init_command_objects(self) -> None:
        """Register command objects (handlers) for this device"s commands."""
        self.logger.warning("No command objects")

    def channel_write_float(self, attr_name: str, attr_value: float) -> None:
        """
        Read attribute value.

        :param attr_name: attribute name
        :param attr_value: attribute value
        """
        if self.admin_mode:
            self.logger.warning("Cannot write %s in admin mode", attr_name)
            return
        scpi_tx = f"{self.scpi_fields[attr_name]}{self.argument_separator}{attr_value}"
        self.logger.debug("Write float %s (%s)", attr_name, scpi_tx)
        cmd_tx(self.host, self.port, scpi_tx, self.sentinel_string)
        self.scpi_values[attr_name] = attr_value

    def channel_write_none(self, attr_name: str) -> None:
        """
        Read attribute value.

        :param attr_name: attribute name
        """
        if self.admin_mode:
            self.logger.warning("Cannot write %s in admin mode", attr_name)
            return
        scpi_tx = f"{self.scpi_fields[attr_name]}"
        self.logger.debug("Write %s (%s)", attr_name, scpi_tx)
        cmd_tx(self.host, self.port, scpi_tx, self.sentinel_string)

    def channel_write_str(self, attr_name: str, attr_value: str) -> None:
        """
        Read attribute value.

        :param attr_name: attribute name
        :param attr_value: attribute value
        """
        if self.admin_mode:
            self.logger.warning("Cannot write %s in admin mode", attr_name)
            return
        scpi_tx = f"{self.scpi_fields[attr_name]}{self.argument_separator}{attr_value}"
        self.logger.debug("Write str %s (%s)", attr_name, scpi_tx)
        cmd_tx(self.host, self.port, scpi_tx, self.sentinel_string)
        self.scpi_values[attr_name] = attr_value

    def minimum_value_check(self, attr_name: str, attr_value: float | int) -> bool:
        """
        Check attribute value.

        :param attr_name: attribute name
        :param attr_value: attribute value
        :returns: true if value is greater than configured minimum
        """
        if attr_name in self.scpi_mins:
            min_val = self.scpi_mins[attr_name]
            if attr_value < min_val:
                self.logger.warning(
                    "Value for %s %s is lower than minimum %s",
                    attr_name,
                    f"{attr_value}",
                    f"{min_val}",
                )
                return True
        else:
            self.logger.warning("No minimum value for %s", attr_name)
        return False

    def maximum_value_check(self, attr_name: str, attr_value: float | int) -> bool:
        """
        Check attribute value.

        :param attr_name: attribute name
        :param attr_value: attribute value
        :returns: true if value is greater than configured maximum
        """
        if attr_name in self.scpi_maxs:
            max_val = self.scpi_maxs[attr_name]
            if attr_value > max_val:
                self.logger.warning(
                    "Value for %s : %s is higher than maximum %s",
                    attr_name,
                    f"{attr_value}",
                    f"{max_val}",
                )
                return True
        else:
            self.logger.warning("No maximum value for %s", attr_name)
        return False

    @attribute(dtype=bool)  # , polling_period=1000)
    # pylint: disable-next=invalid-name
    def adminMode(self) -> bool:
        """
        Control device status.

        :returns: admin mode flag
        """
        self.logger.debug("Read adminMode: %s", self.admin_mode)
        return self.admin_mode

    @adminMode.write  # type: ignore[no-redef]
    # pylint: disable-next=invalid-name
    def adminMode(self, value: bool) -> None:
        """
        Control device status.

        :param value: new mode
        """
        self.admin_mode = value
        if self.admin_mode:
            self.logger.info("Disable device")
            self.set_status("DISABLE")
            self.set_state(tango.DevState.DISABLE)
        else:
            self.logger.info("Enable device")
            self.set_status("ON")
            self.set_state(tango.DevState.ON)
        for ch_num in range(0, MAX_CHANNELS):
            self.channels[ch_num].set_admin_mode(value)

    @attribute(dtype=int, polling_period=1000)
    # pylint: disable-next=invalid-name,too-many-return-statements
    def loggingLevel(self) -> int:
        """
        Get logging level.

        0=OFF
        1=FATAL
        2=ERROR
        3=WARNING
        4=INFO
        5=DEBUG

        :returns: integer value zero to five
        """
        log_lvl = self.logger.getEffectiveLevel()
        self.logger.debug("Effective logging level %d", log_lvl)
        if log_lvl == logging.NOTSET:
            return 0
        if log_lvl == logging.CRITICAL:
            return 1
        if log_lvl == logging.ERROR:
            return 2
        if log_lvl == logging.WARNING:
            return 3
        if log_lvl == logging.INFO:
            return 4
        if log_lvl == logging.DEBUG:
            return 5
        self.logger.error("Invalid logging level %d", log_lvl)
        return 0

    @loggingLevel.write  # type: ignore[no-redef]
    # pylint: disable-next=invalid-name
    def loggingLevel(self, log_lvl: int):
        """
        Set logging level.

        :param log_lvl: integer value zero to five
        """
        if log_lvl == 0:
            self.logger.setLevel(logging.NOTSET)
        elif log_lvl == 1:
            self.logger.setLevel(logging.CRITICAL)
        elif log_lvl == 2:
            self.logger.setLevel(logging.ERROR)
        elif log_lvl == 3:
            self.logger.setLevel(logging.WARNING)
        elif log_lvl == 4:
            self.logger.setLevel(logging.INFO)
        elif log_lvl == 5:
            self.logger.setLevel(logging.DEBUG)
        else:
            self.logger.error("Invalid logging level %d", log_lvl)
            return
        elog_lvl = self.logger.getEffectiveLevel()
        self.logger.warning(
            "Set logging level to %d (%s)", elog_lvl, TANGO_LOG_LEVEL[log_lvl]
        )

    def attribute_poll(self, attr_name: str) -> bool:
        """
        Check if instrument should be read.

        :param attr_name: attribute name to be checked
        :returns: flag to indicate read
        """
        self.scpi_polls[attr_name] += 1
        self.logger.debug(
            "Poll %s %d : %d",
            attr_name,
            self.scpi_polls[attr_name],
            self.scpi_values[attr_name],
        )
        if self.scpi_polls[attr_name] < POLL_COUNT:
            return True
        return False

    @attribute(dtype=str)
    def identity(self) -> str:
        """
        Get manufacturer name, model number, serial number, and firmware package number.

        :returns: identity string
        """
        if self.admin_mode:
            return str(self.scpi_values["identity"])
        scpi_tx = f"{self.scpi_fields['identity']}?"
        scpi_rx = cmd_tx_rx(self.host, self.port, scpi_tx, self.sentinel_string)
        if scpi_rx is None:
            return "ERROR"
        self.logger.debug("Read identity: %s", scpi_rx)
        r_val = str(scpi_rx)
        self.scpi_values["identity"] = r_val
        return r_val

    def reset(self) -> None:
        """Press the reset button."""
        if self.admin_mode:
            self.logger.warning("Cannot do reset in admin mode")
            return
        self.logger.info("Reset")
        scpi_tx = f"{self.scpi_fields['reset']}"
        cmd_tx(self.host, self.port, scpi_tx, self.sentinel_string)

    @attribute(dtype=str)
    def instrument_mode(self) -> str:
        """
        Send command INSTRUMENT:MODE.

        allowed values are AWG|FGEN
        reset puts instrument in AWG mode.

        :returns: instrument mode
        """
        if self.admin_mode:
            return self.scpi_values["instrument_mode"]
        self.scpi_polls["instrument_mode"] = 0
        scpi_tx = f"{self.scpi_fields['instrument_mode']}?"
        scpi_rx = cmd_tx_rx(self.host, self.port, scpi_tx, self.sentinel_string)
        if scpi_rx is None:
            rval = ""
        else:
            self.logger.debug("Read instrument mode: %s", scpi_rx)
            rval = str(scpi_rx)
        self.scpi_values["instrument_mode"] = rval
        return rval

    @instrument_mode.write  # type: ignore[no-redef]
    def instrument_mode(self, inst_mode: str) -> None:
        """
        Set instrument mode.

        :param inst_mode: AWG or FGEN
        """
        if self.admin_mode:
            self.logger.warning("Admin mode: cannot write instrument_mode")
            return
        self.logger.info("Set instrument_mode to %s", inst_mode)
        self.channel_write_str("instrument_mode", inst_mode)

    @attribute(dtype=str)
    def clock_source(self) -> str:
        """
        Send command CLOCK:SOURCE.

        :returns: clock source
        """
        if self.admin_mode:
            return self.scpi_values["clock_source"]
        self.scpi_polls["clock_source"] = 0
        scpi_tx = f"{self.scpi_fields['clock_source']}?"
        scpi_rx = cmd_tx_rx(self.host, self.port, scpi_tx, self.sentinel_string)
        if scpi_rx is None:
            rval = "NONE"
        else:
            self.logger.debug("Read clock source: %s", scpi_rx)
            rval = str(scpi_rx)
        self.scpi_values["clock_source"] = rval
        return rval

    @clock_source.write  # type: ignore[no-redef]
    def clock_source(self, clk_src: str) -> None:
        """
        Set clock source.

        :param clk_src: AWG or FGEN
        """
        if self.admin_mode:
            self.logger.warning("Admin mode: cannot write clock_source")
            return
        self.logger.info("Set clock_source to %s", clk_src)
        self.channel_write_str("clock_source", clk_src)

    @attribute(dtype=float)
    def clock_sample_rate(self) -> float:
        """
        Send command CLOCK:SRATE.

        :returns: clock sample rate
        """
        if self.admin_mode:
            return self.scpi_values["clock_sample_rate"]
        self.scpi_polls["clock_sample_rate"] = 0
        scpi_tx = f"{self.scpi_fields['clock_sample_rate']}?"
        scpi_rx = cmd_tx_rx(self.host, self.port, scpi_tx, self.sentinel_string)
        if scpi_rx is None:
            rval = 0.0
        else:
            rval = float(scpi_rx)
        self.scpi_values["clock_sample_rate"] = rval
        return rval

    @clock_sample_rate.write  # type: ignore[no-redef]
    def clock_sample_rate(self, sampl_rate: float) -> None:
        """
        Set clock sample rate.

        :param sampl_rate: sample rate in Hz
        """
        if self.admin_mode:
            self.logger.warning("Admin mode: cannot write clock_sample_rate")
            return
        if self.minimum_value_check("clock_sample_rate", sampl_rate):
            return
        if self.maximum_value_check("clock_sample_rate", sampl_rate):
            return
        self.logger.info("Set clock_sample_rate to %f", sampl_rate)
        self.channel_write_float("clock_sample_rate", sampl_rate)

    # CHANNEL 1 ATTRIBUTES

    # TODO for future use
    # @attribute(dtype=str)
    # def channel1_asset(self) -> str:
    #     """Send command SOURCE1:CASSET."""
    #     return self.channel_read_str("channel1_asset")

    # TODO for future use
    # @channel1_asset.write  # type: ignore[no-redef]
    # def channel1_asset(self, asset_name: str) -> str:
    #     """Send command SOURCE1:CASSET:WAVEFORM."""
    #     self.channel_write_str("channel1_asset", asset_name)

    @attribute(dtype=str, polling_period=1000)
    def channel1_type(self) -> str:
        """
        Send command FGEN:CHANNEL1:TYPE.

        Valid values are
        SINE | SQUare | TRIangle | NOISe | DC | GAUSsian | EXPRise | EXPDecay | NONE

        :returns: channel type
        """
        return self.channels[0].read_type()

    @channel1_type.write  # type: ignore[no-redef]
    def channel1_type(self, ch_type: str) -> str:
        """
        Send command SOURCE1:CASSET:WAVEFORM.

        :param ch_type: channel type e.g. SINE
        """
        self.channels[0].write_type(ch_type)

    @attribute(dtype=float, polling_period=1000)
    def channel1_phase(self) -> float:
        """
        Send command FGEN:CHANNEL1:PHASE.

        :returns: channel phase angle
        """
        return self.channels[0].read_phase()

    @channel1_phase.write  # type: ignore[no-redef]
    def channel1_phase(self, ch_phase: float) -> float:
        """
        Send command FGEN:CHANNEL1:PHASE.

        :param ch_phase: phase angle in degrees
        """
        self.channels[0].write_phase(ch_phase)

    @attribute(dtype=float, polling_period=1000)
    def channel1_freq(self) -> float:
        """
        Send command FGEN:CHANNEL1:FREQUENCY.

        :returns: channel frequency
        """
        return self.channels[0].read_freq()

    @channel1_freq.write  # type: ignore[no-redef]
    def channel1_freq(self, ch_freq: float) -> None:
        """
        Set frequency for channel.

        :param ch_freq: frequency in Hz
        """
        self.channels[0].write_freq(ch_freq)

    @attribute(dtype=float, polling_period=1000)
    def channel1_amplitude_power(self) -> float:
        """
        Send command FGEN:CHANNEL1:AMPLITUDE:POWER.

        :returns: channel amplitude in dBm
        """
        return self.channels[0].read_amplitude_power()

    @channel1_amplitude_power.write  # type: ignore[no-redef]
    def channel1_amplitude_power(self, ch_power: float) -> None:
        """
        Set amplitude for channel.

        :param ch_power: channel amplitude in dBm
        """
        self.channels[0].write_amplitude_power(ch_power)

    @attribute(dtype=bool, polling_period=1000)
    def channel1_output_state(self) -> bool:
        """
        Send command OUTPUT1:STATE.

        :returns: channel output state
        """
        return self.channels[0].read_state()

    @channel1_output_state.write  # type: ignore[no-redef]
    def channel1_output_state(self, ch_state: bool) -> str:
        """
        Send command SOURCE1:CASSET:WAVEFORM.

        :param ch_state: turn channel on or off
        """
        self.channels[0].write_state(ch_state)

    # CHANNEL 2 ATTRIBUTES

    @attribute(dtype=str, polling_period=1000)
    def channel2_type(self) -> str:
        """
        Send command FGEN:CHANNEL1:TYPE.

        Valid values are
        SINE | SQUare | TRIangle | NOISe | DC | GAUSsian | EXPRise | EXPDecay | NONE

        :returns: channel type
        """
        return self.channels[1].read_type()

    @channel2_type.write  # type: ignore[no-redef]
    def channel2_type(self, ch_type: str) -> str:
        """
        Send command SOURCE1:CASSET:WAVEFORM.

        :param ch_type: channel type e.g. SINE
        """
        self.channels[1].write_type(ch_type)

    @attribute(dtype=float, polling_period=1000)
    def channel2_phase(self) -> float:
        """
        Send command FGEN:CHANNEL1:PHASE.

        :returns: channel phase angle
        """
        return self.channels[1].read_phase()

    @channel2_phase.write  # type: ignore[no-redef]
    def channel2_phase(self, ch_phase: float) -> float:
        """
        Send command FGEN:CHANNEL1:PHASE.

        :param ch_phase: phase angle in degrees
        """
        self.channels[1].write_phase(ch_phase)

    @attribute(dtype=float, polling_period=1000)
    def channel2_freq(self) -> float:
        """
        Send command FGEN:CHANNEL1:FREQUENCY.

        :returns: channel frequency
        """
        return self.channels[1].read_freq()

    @channel2_freq.write  # type: ignore[no-redef]
    def channel2_freq(self, ch_freq: float) -> None:
        """
        Set frequency for channel.

        :param ch_freq: frequency in Hz
        """
        self.channels[1].write_freq(ch_freq)

    @attribute(dtype=float, polling_period=1000)
    def channel2_amplitude_power(self) -> float:
        """
        Send command FGEN:CHANNEL1:AMPLITUDE:POWER.

        :returns: channel amplitude in dBm
        """
        return self.channels[1].read_amplitude_power()

    @channel2_amplitude_power.write  # type: ignore[no-redef]
    def channel2_amplitude_power(self, ch_power: float) -> None:
        """
        Set amplitude for channel.

        :param ch_power: channel amplitude in dBm
        """
        self.channels[1].write_amplitude_power(ch_power)

    @attribute(dtype=bool, polling_period=1000)
    def channel2_output_state(self) -> bool:
        """
        Send command OUTPUT1:STATE.

        :returns: channel output state
        """
        return self.channels[1].read_state()

    @channel2_output_state.write  # type: ignore[no-redef]
    def channel2_output_state(self, ch_state: bool) -> str:
        """
        Send command SOURCE1:CASSET:WAVEFORM.

        :param ch_state: turn channel on or off
        """
        self.channels[1].write_state(ch_state)

    # CHANNEL 3 ATTRIBUTES

    @attribute(dtype=str, polling_period=1000)
    def channel3_type(self) -> str:
        """
        Send command FGEN:CHANNEL1:TYPE.

        Valid values are
        SINE | SQUare | TRIangle | NOISe | DC | GAUSsian | EXPRise | EXPDecay | NONE

        :returns: channel type
        """
        return self.channels[2].read_type()

    @channel3_type.write  # type: ignore[no-redef]
    def channel3_type(self, ch_type: str) -> str:
        """
        Send command SOURCE1:CASSET:WAVEFORM.

        :param ch_type: channel type e.g. SINE
        """
        self.channels[2].write_type(ch_type)

    @attribute(dtype=float, polling_period=1000)
    def channel3_phase(self) -> float:
        """
        Send command FGEN:CHANNEL1:PHASE.

        :returns: channel phase angle
        """
        return self.channels[2].read_phase()

    @channel3_phase.write  # type: ignore[no-redef]
    def channel3_phase(self, ch_phase: float) -> float:
        """
        Send command FGEN:CHANNEL1:PHASE.

        :param ch_phase: phase angle in degrees
        """
        self.channels[2].write_phase(ch_phase)

    @attribute(dtype=float, polling_period=1000)
    def channel3_freq(self) -> float:
        """
        Send command FGEN:CHANNEL1:FREQUENCY.

        :returns: channel frequency
        """
        return self.channels[2].read_freq()

    @channel3_freq.write  # type: ignore[no-redef]
    def channel3_freq(self, ch_freq: float) -> None:
        """
        Set frequency for channel.

        :param ch_freq: frequency in Hz
        """
        self.channels[2].write_freq(ch_freq)

    @attribute(dtype=float, polling_period=1000)
    def channel3_amplitude_power(self) -> float:
        """
        Send command FGEN:CHANNEL1:AMPLITUDE:POWER.

        :returns: channel amplitude in dBm
        """
        return self.channels[2].read_amplitude_power()

    @channel3_amplitude_power.write  # type: ignore[no-redef]
    def channel3_amplitude_power(self, ch_power: float) -> None:
        """
        Set amplitude for channel.

        :param ch_power: channel amplitude in dBm
        """
        self.channels[2].write_amplitude_power(ch_power)

    @attribute(dtype=bool, polling_period=1000)
    def channel3_output_state(self) -> bool:
        """
        Send command OUTPUT1:STATE.

        :returns: channel output state
        """
        return self.channels[2].read_state()

    @channel3_output_state.write  # type: ignore[no-redef]
    def channel3_output_state(self, ch_state: bool) -> str:
        """
        Send command SOURCE1:CASSET:WAVEFORM.

        :param ch_state: turn channel on or off
        """
        self.channels[2].write_state(ch_state)

    # CHANNEL 4 ATTRIBUTES

    @attribute(dtype=str, polling_period=1000)
    def channel4_type(self) -> str:
        """
        Send command FGEN:CHANNEL1:TYPE.

        Valid values are
        SINE | SQUare | TRIangle | NOISe | DC | GAUSsian | EXPRise | EXPDecay | NONE

        :returns: channel type
        """
        return self.channels[3].read_type()

    @channel4_type.write  # type: ignore[no-redef]
    def channel4_type(self, ch_type: str) -> str:
        """
        Send command SOURCE1:CASSET:WAVEFORM.

        :param ch_type: channel type e.g. SINE
        """
        self.channels[3].write_type(ch_type)

    @attribute(dtype=float, polling_period=1000)
    def channel4_phase(self) -> float:
        """
        Send command FGEN:CHANNEL1:PHASE.

        :returns: channel phase angle
        """
        return self.channels[3].read_phase()

    @channel4_phase.write  # type: ignore[no-redef]
    def channel4_phase(self, ch_phase: float) -> float:
        """
        Send command FGEN:CHANNEL1:PHASE.

        :param ch_phase: phase angle in degrees
        """
        self.channels[3].write_phase(ch_phase)

    @attribute(dtype=float, polling_period=1000)
    def channel4_freq(self) -> float:
        """
        Send command FGEN:CHANNEL1:FREQUENCY.

        :returns: channel frequency
        """
        return self.channels[3].read_freq()

    @channel4_freq.write  # type: ignore[no-redef]
    def channel4_freq(self, ch_freq: float) -> None:
        """
        Set frequency for channel.

        :param ch_freq: frequency in Hz
        """
        self.channels[3].write_freq(ch_freq)

    @attribute(dtype=float, polling_period=1000)
    def channel4_amplitude_power(self) -> float:
        """
        Send command FGEN:CHANNEL1:AMPLITUDE:POWER.

        :returns: channel amplitude in dBm
        """
        return self.channels[3].read_amplitude_power()

    @channel4_amplitude_power.write  # type: ignore[no-redef]
    def channel4_amplitude_power(self, ch_power: float) -> None:
        """
        Set amplitude for channel.

        :param ch_power: channel amplitude in dBm
        """
        self.channels[3].write_amplitude_power(ch_power)

    @attribute(dtype=bool, polling_period=1000)
    def channel4_output_state(self) -> bool:
        """
        Send command OUTPUT1:STATE.

        :returns: channel output state
        """
        return self.channels[3].read_state()

    @channel4_output_state.write  # type: ignore[no-redef]
    def channel4_output_state(self, ch_state: bool) -> str:
        """
        Send command SOURCE1:CASSET:WAVEFORM.

        :param ch_state: turn channel on or off
        """
        self.channels[3].write_state(ch_state)

    # CHANNEL 5 ATTRIBUTES

    @attribute(dtype=str, polling_period=1000)
    def channel5_type(self) -> str:
        """
        Send command FGEN:CHANNEL1:TYPE.

        Valid values are
        SINE | SQUare | TRIangle | NOISe | DC | GAUSsian | EXPRise | EXPDecay | NONE

        :returns: channel type
        """
        return self.channels[4].read_type()

    @channel5_type.write  # type: ignore[no-redef]
    def channel5_type(self, ch_type: str) -> str:
        """
        Send command SOURCE1:CASSET:WAVEFORM.

        :param ch_type: channel type e.g. SINE
        """
        self.channels[4].write_type(ch_type)

    @attribute(dtype=float, polling_period=1000)
    def channel5_phase(self) -> float:
        """
        Send command FGEN:CHANNEL1:PHASE.

        :returns: channel phase angle
        """
        return self.channels[4].read_phase()

    @channel5_phase.write  # type: ignore[no-redef]
    def channel5_phase(self, ch_phase: float) -> float:
        """
        Send command FGEN:CHANNEL1:PHASE.

        :param ch_phase: phase angle in degrees
        """
        self.channels[4].write_phase(ch_phase)

    @attribute(dtype=float, polling_period=1000)
    def channel5_freq(self) -> float:
        """
        Send command FGEN:CHANNEL1:FREQUENCY.

        :returns: channel frequency
        """
        return self.channels[4].read_freq()

    @channel5_freq.write  # type: ignore[no-redef]
    def channel5_freq(self, ch_freq: float) -> None:
        """
        Set frequency for channel.

        :param ch_freq: frequency in Hz
        """
        self.channels[4].write_freq(ch_freq)

    @attribute(dtype=float, polling_period=1000)
    def channel5_amplitude_power(self) -> float:
        """
        Send command FGEN:CHANNEL1:AMPLITUDE:POWER.

        :returns: channel amplitude in dBm
        """
        return self.channels[4].read_amplitude_power()

    @channel5_amplitude_power.write  # type: ignore[no-redef]
    def channel5_amplitude_power(self, ch_power: float) -> None:
        """
        Set amplitude for channel.

        :param ch_power: channel amplitude in dBm
        """
        self.channels[4].write_amplitude_power(ch_power)

    @attribute(dtype=bool, polling_period=1000)
    def channel5_output_state(self) -> bool:
        """
        Send command OUTPUT1:STATE.

        :returns: channel output state
        """
        return self.channels[4].read_state()

    @channel5_output_state.write  # type: ignore[no-redef]
    def channel5_output_state(self, ch_state: bool) -> str:
        """
        Send command SOURCE1:CASSET:WAVEFORM.

        :param ch_state: turn channel on or off
        """
        self.channels[4].write_state(ch_state)

    # CHANNEL 6 ATTRIBUTES

    @attribute(dtype=str, polling_period=1000)
    def channel6_type(self) -> str:
        """
        Send command FGEN:CHANNEL1:TYPE.

        Valid values are
        SINE | SQUare | TRIangle | NOISe | DC | GAUSsian | EXPRise | EXPDecay | NONE

        :returns: channel type
        """
        return self.channels[5].read_type()

    @channel6_type.write  # type: ignore[no-redef]
    def channel6_type(self, ch_type: str) -> str:
        """
        Send command SOURCE1:CASSET:WAVEFORM.

        :param ch_type: channel type e.g. SINE
        """
        self.channels[5].write_type(ch_type)

    @attribute(dtype=float, polling_period=1000)
    def channel6_phase(self) -> float:
        """
        Send command FGEN:CHANNEL1:PHASE.

        :returns: channel phase angle
        """
        return self.channels[5].read_phase()

    @channel6_phase.write  # type: ignore[no-redef]
    def channel6_phase(self, ch_phase: float) -> float:
        """
        Send command FGEN:CHANNEL1:PHASE.

        :param ch_phase: phase angle in degrees
        """
        self.channels[5].write_phase(ch_phase)

    @attribute(dtype=float, polling_period=1000)
    def channel6_freq(self) -> float:
        """
        Send command FGEN:CHANNEL1:FREQUENCY.

        :returns: channel frequency
        """
        return self.channels[5].read_freq()

    @channel6_freq.write  # type: ignore[no-redef]
    def channel6_freq(self, ch_freq: float) -> None:
        """
        Set frequency for channel.

        :param ch_freq: frequency in Hz
        """
        self.channels[5].write_freq(ch_freq)

    @attribute(dtype=float, polling_period=1000)
    def channel6_amplitude_power(self) -> float:
        """
        Send command FGEN:CHANNEL1:AMPLITUDE:POWER.

        :returns: channel amplitude in dBm
        """
        return self.channels[5].read_amplitude_power()

    @channel6_amplitude_power.write  # type: ignore[no-redef]
    def channel6_amplitude_power(self, ch_power: float) -> None:
        """
        Set amplitude for channel.

        :param ch_power: channel amplitude in dBm
        """
        self.channels[5].write_amplitude_power(ch_power)

    @attribute(dtype=bool, polling_period=1000)
    def channel6_output_state(self) -> bool:
        """
        Send command OUTPUT1:STATE.

        :returns: channel output state
        """
        return self.channels[5].read_state()

    @channel6_output_state.write  # type: ignore[no-redef]
    def channel6_output_state(self, ch_state: bool) -> str:
        """
        Send command SOURCE1:CASSET:WAVEFORM.

        :param ch_state: turn channel on or off
        """
        self.channels[5].write_state(ch_state)

    # CHANNEL 7 ATTRIBUTES

    @attribute(dtype=str, polling_period=1000)
    def channel7_type(self) -> str:
        """
        Send command FGEN:CHANNEL1:TYPE.

        Valid values are
        SINE | SQUare | TRIangle | NOISe | DC | GAUSsian | EXPRise | EXPDecay | NONE

        :returns: channel type
        """
        return self.channels[6].read_type()

    @channel7_type.write  # type: ignore[no-redef]
    def channel7_type(self, ch_type: str) -> str:
        """
        Send command SOURCE1:CASSET:WAVEFORM.

        :param ch_type: channel type e.g. SINE
        """
        self.channels[6].write_type(ch_type)

    @attribute(dtype=float, polling_period=1000)
    def channel7_phase(self) -> float:
        """
        Send command FGEN:CHANNEL1:PHASE.

        :returns: channel phase angle
        """
        return self.channels[6].read_phase()

    @channel7_phase.write  # type: ignore[no-redef]
    def channel7_phase(self, ch_phase: float) -> float:
        """
        Send command FGEN:CHANNEL1:PHASE.

        :param ch_phase: phase angle in degrees
        """
        self.channels[6].write_phase(ch_phase)

    @attribute(dtype=float, polling_period=1000)
    def channel7_freq(self) -> float:
        """
        Send command FGEN:CHANNEL1:FREQUENCY.

        :returns: channel frequency
        """
        return self.channels[6].read_freq()

    @channel7_freq.write  # type: ignore[no-redef]
    def channel7_freq(self, ch_freq: float) -> None:
        """
        Set frequency for channel.

        :param ch_freq: frequency in Hz
        """
        self.channels[6].write_freq(ch_freq)

    @attribute(dtype=float, polling_period=1000)
    def channel7_amplitude_power(self) -> float:
        """
        Send command FGEN:CHANNEL1:AMPLITUDE:POWER.

        :returns: channel amplitude in dBm
        """
        return self.channels[6].read_amplitude_power()

    @channel7_amplitude_power.write  # type: ignore[no-redef]
    def channel7_amplitude_power(self, ch_power: float) -> None:
        """
        Set amplitude for channel.

        :param ch_power: channel amplitude in dBm
        """
        self.channels[6].write_amplitude_power(ch_power)

    @attribute(dtype=bool, polling_period=1000)
    def channel7_output_state(self) -> bool:
        """
        Send command OUTPUT1:STATE.

        :returns: channel output state
        """
        return self.channels[6].read_state()

    @channel7_output_state.write  # type: ignore[no-redef]
    def channel7_output_state(self, ch_state: bool) -> str:
        """
        Send command SOURCE1:CASSET:WAVEFORM.

        :param ch_state: turn channel on or off
        """
        self.channels[6].write_state(ch_state)

    # CHANNEL 8 ATTRIBUTES

    @attribute(dtype=str, polling_period=1000)
    def channel8_type(self) -> str:
        """
        Send command FGEN:CHANNEL1:TYPE.

        Valid values are
        SINE | SQUare | TRIangle | NOISe | DC | GAUSsian | EXPRise | EXPDecay | NONE

        :returns: channel type
        """
        return self.channels[7].read_type()

    @channel8_type.write  # type: ignore[no-redef]
    def channel8_type(self, ch_type: str) -> str:
        """
        Send command SOURCE1:CASSET:WAVEFORM.

        :param ch_type: channel type e.g. SINE
        """
        self.channels[7].write_type(ch_type)

    @attribute(dtype=float, polling_period=1000)
    def channel8_phase(self) -> float:
        """
        Send command FGEN:CHANNEL1:PHASE.

        :returns: channel phase angle
        """
        return self.channels[7].read_phase()

    @channel8_phase.write  # type: ignore[no-redef]
    def channel8_phase(self, ch_phase: float) -> float:
        """
        Send command FGEN:CHANNEL1:PHASE.

        :param ch_phase: phase angle in degrees
        """
        self.channels[7].write_phase(ch_phase)

    @attribute(dtype=float, polling_period=1000)
    def channel8_freq(self) -> float:
        """
        Send command FGEN:CHANNEL1:FREQUENCY.

        :returns: channel frequency
        """
        return self.channels[7].read_freq()

    @channel8_freq.write  # type: ignore[no-redef]
    def channel8_freq(self, ch_freq: float) -> None:
        """
        Set frequency for channel.

        :param ch_freq: frequency in Hz
        """
        self.channels[7].write_freq(ch_freq)

    @attribute(dtype=float, polling_period=1000)
    def channel8_amplitude_power(self) -> float:
        """
        Send command FGEN:CHANNEL1:AMPLITUDE:POWER.

        :returns: channel amplitude in dBm
        """
        return self.channels[7].read_amplitude_power()

    @channel8_amplitude_power.write  # type: ignore[no-redef]
    def channel8_amplitude_power(self, ch_power: float) -> None:
        """
        Set amplitude for channel.

        :param ch_power: channel amplitude in dBm
        """
        self.channels[7].write_amplitude_power(ch_power)

    @attribute(dtype=bool, polling_period=1000)
    def channel8_output_state(self) -> bool:
        """
        Send command OUTPUT1:STATE.

        :returns: channel output state
        """
        return self.channels[7].read_state()

    @channel8_output_state.write  # type: ignore[no-redef]
    def channel8_output_state(self, ch_state: bool) -> str:
        """
        Send command SOURCE1:CASSET:WAVEFORM.

        :param ch_state: turn channel on or off
        """
        self.channels[7].write_state(ch_state)

    # ATTRIBUTES FOR PLAYBACK

    def play(self) -> None:
        """Send command AWGCONTROL:RUN:IMMEDIATE."""
        if self.admin_mode:
            self.logger.warning("Admin mode: cannot write play")
            return
        self.channel_write_none("play")

    def stop(self) -> None:
        """Send command AWGCONTROL:STOP:IMMEDIATE."""
        if self.admin_mode:
            self.logger.warning("Admin mode: cannot write stop")
            return
        self.channel_write_none("stop")

    @attribute(dtype=int, polling_period=3000)
    def playing(self) -> int:
        """
        Send command AWGCONTROL:RSTATE.

        :returns: playing state
        """
        if self.admin_mode:
            return int(self.scpi_values["playing"])
        if self.attribute_poll("playing"):
            return int(self.scpi_values["playing"])
        return self.channel_read_int("playing")

    # ATTRIBUTES FOR INSTRUMENT STATES AND ERRORS

    @attribute(dtype=bool, polling_period=3000)
    def operation_complete(self) -> bool | None:
        """
        Get operation complete.

        :returns: current flag value
        """
        if self.admin_mode:
            return None
        scpi_tx = "*ESR?"
        scpi_rx = cmd_tx_rx(self.host, self.port, scpi_tx, self.sentinel_string)
        if scpi_rx is None:
            return None
        self.event_status_register = int(scpi_rx)
        self.logger.debug("Read Event Status Register: %s", self.event_status_register)
        # pylint: disable-next=simplifiable-if-statement
        if self.event_status_register & 0x01:
            rval = True
        else:
            rval = False
        # TODO pylint suggest this but mypy will have none of it ;-)
        # rval = self.event_status_register & 0x01
        self.logger.debug("Read operation_complete: %s", rval)
        return rval

    @attribute(dtype=bool, polling_period=8000)
    def query_error(self) -> bool | None:
        """
        Get query error.

        :returns: current flag value
        """
        if self.admin_mode:
            return None
        if self.event_status_register & 0x04:
            rval = True
            self.logger.error(
                "Detected query_error in event_status_register (%d)",
                self.event_status_register,
            )
        else:
            rval = False
            self.logger.debug("Read query_error: %s", rval)
        return rval

    @attribute(dtype=bool, polling_period=8000)
    def device_error(self) -> bool | None:
        """
        Get device error.

        :returns: current flag value
        """
        if self.admin_mode:
            return None
        if self.event_status_register & 0x08:
            rval = True
            self.logger.error(
                "Detected device_error in event_status_register (%d)",
                self.event_status_register,
            )
        else:
            rval = False
            self.logger.debug("Read device_error: %s", rval)
        return rval

    @attribute(dtype=bool, polling_period=8000)
    def execution_error(self) -> bool | None:
        """
        Get execution error.

        :returns: current flag value
        """
        if self.admin_mode:
            return None
        if self.event_status_register & 0x10:
            rval = True
            self.logger.error(
                "Detected execution_error in event_status_register (%d)",
                self.event_status_register,
            )
        else:
            rval = False
            self.logger.debug("Read execution_error: %s", rval)
        return rval

    @attribute(dtype=bool, polling_period=8000)
    def command_error(self) -> bool | None:
        """
        Get command error.

        :returns: current flag value
        """
        if self.admin_mode:
            return None
        if self.event_status_register & 0x20:
            rval = True
            self.logger.error(
                "Detected command_error in event_status_register (%d)",
                self.event_status_register,
            )
        else:
            rval = False
            self.logger.debug("Read command_error: %s", rval)
        return rval

    @attribute(dtype=bool, polling_period=8000)
    def power_cycled(self) -> bool | None:
        """
        Get power cycled.

        :returns: current flag value
        """
        if self.admin_mode:
            return None
        if self.event_status_register & 0x80:
            rval = True
            self.logger.error(
                "Detected power_cycled in event_status_register (%d)",
                self.event_status_register,
            )
        else:
            rval = False
            self.logger.debug("Read power_cycled: %s", rval)
        return rval


def run_device(kwargs: list[str]) -> None:
    """
    Get the main peanut.

    :param kwargs: list of arguments
    """
    _module_logger.info("Run : %s", repr(kwargs))
    AwgDeviceVisa.run_server(args=kwargs)


def main(args: list[str] | None = None, **kwargs: list[str]) -> None:
    """
    Get the main peanut.

    :param args: command line arguments
    :param kwargs: keyword arguments
    """
    # AwgDeviceVisa.run_server(green_mode=GreenMode.Asyncio)
    log_level_str = os.getenv("LOG_LEVEL", "WARNING").upper()
    _module_logger.setLevel(log_level_str)
    elog_lvl = _module_logger.getEffectiveLevel()
    _module_logger.info(
        "Start : args %s : kwargs %s (log level %s %d)",
        repr(args),
        repr(kwargs),
        log_level_str,
        elog_lvl,
    )
    tango.server.run((AwgDeviceVisa,), **kwargs)


if __name__ == "__main__":
    _module_logger.setLevel(logging.DEBUG)
    main()
