"""Constant values for AWG."""

CHANNEL_COUNT: int = 8
AWG_DATA_BLOCK: int = 1000000

AWG_AMPL: float = 500e-3
AWG_OFFSET: float = 0.0
AWG_FREQ: float = 100e6
AWG_SAMPLE: float = 2.5e9
AWG_REC_LEN: int = 9600

AWG_MODES = ["AWG", "FGEN"]
AWG_RUN_STATES = ["STOPPED", "WAITING FOR TRIGGER", "RUNNING"]
AWG_TYPES = [
    "SINE",
    "SQUARE",
    "TRIANGLE",
    "NOISE",
    "DC",
    "GAUSSIAN",
    "EXPRISE",
    "EXPDECAY",
    "NONE",
]
AWG_COUPLED_STATES = [
    "NONE",
    "ALL",
    "PAIR",
]
SIGNAL_PATHS = [
    "DCHB",  # DC High Bandwidth
    "ACDIRECT",
    "ACAMPLIFIED",
]
