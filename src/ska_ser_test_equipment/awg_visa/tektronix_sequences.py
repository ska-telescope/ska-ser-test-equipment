"""Mixin class for loading sequences into AWG."""

# pylint: disable=useless-suppression
# pylint: disable=too-many-arguments,too-many-positional-arguments

# mypy: disable-error-code=attr-defined

import logging
import pprint


# pylint: disable-next=too-many-public-methods
class TektronixSequenceMixin:
    """Mixin class for sequences."""

    logger: logging.Logger
    _sequence_amplitude: float = float("nan")
    _sequence_frequency: float = float("nan")
    _sequence_length: int = -1
    _sequence_list_size: int = -1
    _sequence_offset: float = float("nan")
    _sequence_sampling_rate: float = float("nan")
    _sequence_tracks: int = -1

    def delete_sequence(self, seq_name: str) -> None:
        """
        Delete specified sequence.

        SEQUENCE (215)

        This command deletes a specific sequence or all sequences from the sequence
        list.

        :param seq_name: sequence name
        """
        if not seq_name:
            self.logger.error("Sequence name not set")
            return
        self.logger.info('Delete sequence "%s"', seq_name)
        awg = self.visa_hw
        self.write_scpi(f'SLIST:SEQUENCE:DELETE "{seq_name}"')
        awg.close()

    def sequence_amplitude(self, seq_name: str, value: float | None = None) -> float:
        """
        Set or return the recommended amplitude of the specified sequence.

        SEQUENCE (214)

        The command sets or returns the Recommended Amplitude (peak-to-peak) of
        the specified sequence.

        If a recommended amplitude is not specified, a query returns the value for Not a
        Number (9.9100E+037).

        :param seq_name: sequence name
        :param value: new amplitude
        :returns: current value
        """
        if not seq_name:
            self.logger.error("Sequence name not set")
            return float("nan")
        if value is not None:
            self._sequence_amplitude = value
            self.write_query_float(
                "sequence_amplitude",
                f'SLIST:SEQUENCE:AMPLITUDE "{seq_name}",{value}',
            )
        self._sequence_amplitude = self.read_query_float(
            "sequence_amplitude", f'SLIST:SEQUENCE:AMPLITUDE? "{seq_name}"'
        )
        return self._sequence_amplitude

    def sequence_name_index(self, idx: int) -> str:
        """
        Get name of sequence at specified sequence list index.

        SEQUENCE (213)

        This command returns the name of the sequence at the specified sequence list
        index.

        :param idx: list index
        :returns: name of sequence
        """
        return self.read_query_str("sequence_name_index", f"SLIST:NAME? {idx}")

    def sequence_offset(self, seq_name: str, value: float | None = None) -> float:
        """
        Set or return the recommended offset of the specified sequence.

        SEQUENCE (223)

        The command sets or returns the Recommended Offset of the specified sequence.

        If a recommended offset is not specified, a query returns the value for Not a
        Number (9.9100E+037).

        :param seq_name: sequence name
        :param value: new offset
        :returns: current value
        """
        if not seq_name:
            self.logger.error("Sequence name not set")
            return float("nan")
        if value is not None:
            self._sequence_offset = value
            self.write_query_float(
                "sequence_offset",
                f'SLIST:SEQUENCE:OFFSET "{seq_name}",{value}',
            )
        self._sequence_offset = self.read_query_float(
            "sequence_offset", f'SLIST:SEQUENCE:OFFSET? "{seq_name}"'
        )
        return self._sequence_offset

    def sequence_frequency(self, seq_name: str, value: float | None = None) -> float:
        """
        Set or return the recommended frequency of the specified sequence.

        SEQUENCE (220)

        The command sets or returns the recommended frequency of the specified
        sequence when the sequence contains IQ waveforms.

        If a recommended frequency is not specified, a query returns the value for Not a
        Number (9.9100E+037).

        :param seq_name: sequence name
        :param value: new frequency
        :returns: current value
        """
        if not seq_name:
            self.logger.error("Sequence name not set")
            return float("nan")
        if value is not None:
            self._sequence_frequency = value
            self.write_query_float(
                "sequence_frequency",
                f'SLIST:SEQUENCE:FREQUENCY "{seq_name}",{value}',
            )
        self._sequence_frequency = self.read_query_float(
            "sequence_frequency", f'SLIST:SEQUENCE:FREQUENCY? "{seq_name}"'
        )
        return self._sequence_frequency

    def sequence_length(self, seq_name: str, value: int | None = None) -> int:
        """
        Set or return the length of the specified sequence.

        SEQUENCE (221)

        This command returns the total number of steps in the named sequence.

        :param seq_name: sequence name
        :param value: new length
        :returns: current value
        """
        if not seq_name:
            self.logger.error("Sequence name not set")
            return -1
        if value is not None:
            self._sequence_length = value
            self.write_query_int(
                "sequence_length", f'SLIST:SEQUENCE:LENGTH "{seq_name}",{value}'
            )
        self._sequence_length = self.read_query_int(
            "sequence_length", f'SLIST:SEQUENCE:LENGTH? "{seq_name}"'
        )
        self.logger.debug("Sequence length: %d", self._sequence_length)
        return self._sequence_length

    @property
    def sequence_list_size(self) -> int:
        """
        Read the number of sequences in the sequence list.

        SEQUENCE (245)

        This command returns the number sequences in sequence list.

        :returns: number of sequences
        """
        self._sequence_list_size = self.read_query_int(
            "sequence_list_size", "SLIST:SIZE?"
        )
        return self._sequence_list_size

    def sequence_sampling_rate(
        self, seq_name: str, value: float | None = None
    ) -> float:
        """
        Set or return the recommended sampling rate of the specified sequence.

        SEQUENCE (225)

        The command sets or returns the Recommended Sampling Rate of the specified
        sequence.

        If a recommended sampling rate is not specified, a query returns the value for
        Not a Number (9.9100E+037).

        :param seq_name: sequence name
        :param value: new sampling rate
        :returns: current value
        """
        if not seq_name:
            self.logger.error("Sequence name not set")
            return float("nan")
        if value is not None:
            self._sequence_frequency = value
            self.write_query_float(
                "sequence_sampling_rate",
                f'SLIST:SEQUENCE:SRATE "{seq_name}",{value}',
            )
        self._sequence_sampling_rate = self.read_query_float(
            "sequence_frequency", f'SLIST:SEQUENCE:SRATE? "{seq_name}"'
        )
        return self._sequence_sampling_rate

    def sequence_new(self, seq_name: str, n_steps: int, n_tracks: int) -> None:
        """
        Create new sequence.

        SEQUENCE (222)

        This command .creates new sequence with selected name, number of steps and
        number of tracks.

        If a recommended sampling rate is not specified, a query returns the value for
        Not a Number (9.9100E+037).

        :param seq_name: sequence name
        :param n_steps: number of steps
        :param n_tracks: number of tracks
        """
        if not seq_name:
            self.logger.error("Sequence name not set")
            return
        self.logger.info(
            "Create sequence %s with %d steps and %d tracks",
            seq_name,
            n_steps,
            n_tracks,
        )
        self.write_scpi(f'SLIST:SEQUENCE:NEW "{seq_name}", {n_steps}, {n_tracks}')

    def sequence_asset_waveform(
        self, seq_name: str, step_nr: int, track_no: int, wave_name: str
    ) -> None:
        """
        Assign waveform for specific sequence's step and track.

        SEQUENCE (236)

        This command assigns a waveform for a specific sequence's step and track. This
        waveform is played whenever the playing sequence reaches this step. A track in a
        sequence is assigned to a channel with the command [SOURce[n]]:CASSET:SEQ.

        :param seq_name: sequence name
        :param step_nr: step number
        :param track_no: track number
        :param wave_name: wave name
        """
        if not seq_name:
            self.logger.error("Sequence name not set")
            return
        if step_nr <= 0:
            self.logger.error("Invalid step number for asset waveform: %d", step_nr)
            return
        self.write_scpi(
            f"SLIST:SEQUENCE:STEP{step_nr}:TASSET{track_no}:WAVEFORM"
            f' "{seq_name}","{wave_name}"'
        )

    def sequence_step_goto(
        self, seq_name: str, step_nr: int, goto: str | None = None
    ) -> str:
        """
        Set target step for the GOTO command of sequencer at specified step.

        SEQUENCE (231)

        This command sets or returns the target step for the GOTO command of the
        sequencer at the specified step.

        After generating the waveform(s) specified in a sequence step, the sequencer
        jumps to the step specified as the GOTO step. This is an unconditional jump.
        If the GOTO step is not specified, the sequencer moves to the next step. If the
        Repeat Count is Infinite, the specified GOTO step is not used.

        :param seq_name: sequence name
        :param step_nr: step number
        :param goto: next step
        :returns: next step
        """
        if not seq_name:
            self.logger.error("Sequence name not set")
            return ""
        if step_nr <= 0:
            self.logger.error("Invalid step number for asset waveform: %d", step_nr)
            return ""
        if goto is not None:
            self.write_scpi(f'SLIST:SEQUENCE:STEP{step_nr}:GOTO? "{seq_name}"')
        return self.read_query_str(
            "sequence_step_goto",
            f'SLIST:SEQUENCE:STEP{step_nr}:GOTO "{seq_name}", {goto}',
        )

    def sequence_step_repeat_count(
        self, seq_name: str, step_nr: int, rcount: int | None = None
    ) -> int:
        """
        Set number of times assigned waveform(s) play before proceeding to next step.

        SEQUENCE (232)

        This command sets or returns the repeat count, which is the number of times the
        assigned waveform(s) play before proceeding to the next step in the sequence.

        :param seq_name: sequence name
        :param step_nr: step number
        :param rcount: repeat count
        :returns: repeat count
        """
        if not seq_name:
            self.logger.error("Sequence name not set")
            return -1
        if step_nr <= 0:
            self.logger.error("Invalid step number for asset waveform: %d", step_nr)
            return 0
        if rcount is not None:
            self.write_scpi(
                f'SLIST:SEQUENCE:STEP{step_nr}:RCOUNT "{seq_name}",{rcount}'
            )
        return self.read_query_int(
            "sequence_step_repeat_count",
            f'SLIST:SEQUENCE:STEP{step_nr}:RCOUNT? "{seq_name}"',
        )

    def sequence_tracks(self, seq_name: str) -> int:
        """
        Get number of tracks defined in the specified sequence.

        SEQUENCE (242)

        This command returns the number of tracks defined in the specified sequence.

        :param seq_name: sequence name
        :returns: number of tracks
        """
        if not seq_name:
            self.logger.error("Sequence name not set")
            return -1
        self._sequence_tracks = self.read_query_int(
            "sequence_tracks", f'SLIST:SEQUENCE:TRACK? "{seq_name}"'
        )
        return self._sequence_tracks

    def run_sequence(self, seq_name: str) -> None:
        """
        Assign track of sequence (from sequence list) to specified channels.

        :param seq_name: sequence name
        """
        self.logger.info('Load sequence "%s"', seq_name)
        if not seq_name:
            self.logger.error("Sequence name not set")
            return
        awg = self.visa_hw
        self.write_scpi(f'SOURCE1:CASSET:SEQUENCE "{seq_name}", 1')
        self.write_scpi(f'SOURCE2:CASSET:SEQUENCE "{seq_name}", 2')
        self.write_scpi(f'SOURCE3:CASSET:SEQUENCE "{seq_name}", 3')
        self.write_scpi(f'SOURCE4:CASSET:SEQUENCE "{seq_name}", 4')

        # Turn channel outputs on
        self.write_scpi("OUTPUT1:STATE ON")
        self.write_scpi("OUTPUT2:STATE ON")
        self.write_scpi("OUTPUT3:STATE ON")
        self.write_scpi("OUTPUT4:STATE ON")

        self.logger.info("Run")
        self.write_scpi("AWGCONTROL:RUN:IMMEDIATE")
        awg.close()

    def create_sequence_step(
        self, seq_name: str, step_nr: int, waves: list, goto: str
    ) -> None:
        """
        Assign waveform for specific sequence's step and track.

        :param seq_name: sequence name
        :param step_nr: step number
        :param waves: wavefrom names
        :param goto: next step
        """
        self.logger.info(
            "Assign waveform %s step %d: %s go to %s", seq_name, step_nr, waves, goto
        )
        if not seq_name:
            self.logger.error("Sequence name not set")
            return
        if step_nr <= 0:
            self.logger.error("Invalid step number for asset waveform: %d", step_nr)
            return
        self.write_scpi(f'SLIST:SEQUENCE:STEP:ADD "{seq_name}",{step_nr}')
        self.write_scpi(
            f'SLIST:SEQUENCE:STEP{step_nr}:TASSET1:WAVEFORM "{seq_name}","{waves[0]}"'
        )
        self.write_scpi(
            f'SLIST:SEQUENCE:STEP{step_nr}:TASSET2:WAVEFORM "{seq_name}","{waves[1]}"'
        )
        self.write_scpi(
            f'SLIST:SEQUENCE:STEP{step_nr}:TASSET3:WAVEFORM "{seq_name}","{waves[2]}"'
        )
        self.write_scpi(
            f'SLIST:SEQUENCE:STEP{step_nr}:TASSET4:WAVEFORM "{seq_name}","{waves[3]}"'
        )
        # Number of times assigned waveform(s) play before proceeding to next step
        self.write_scpi(f'SLIST:SEQUENCE:STEP{step_nr}:RCOUNT "{seq_name}",1')
        # Set target step for the GOTO command of sequencer at specified step
        self.write_scpi(f'SLISt:SEQUENCE:STEP{step_nr}:GOTO "{seq_name}", {goto}')
        # lather, rinse, repeat

    def _create_sequence2(self, seq_name: str, zero: str, one: str) -> None:
        """
        Create four channel sequence using two waveforms.

        :param seq_name: sequence name
        :param zero: first wave
        :param one: second wave
        """
        if not seq_name:
            self.logger.error("Sequence name not set")
            return
        self.logger.info("Create sequence with waves %s and %s", seq_name, zero, one)
        self.create_sequence_step(seq_name, 1, [one, zero, zero, zero], "NEXT")
        self.create_sequence_step(seq_name, 2, [one, one, zero, zero], "NEXT")
        self.create_sequence_step(seq_name, 3, [one, one, one, zero], "NEXT")
        self.create_sequence_step(seq_name, 4, [one, one, one, one], "NEXT")
        self.create_sequence_step(seq_name, 5, [one, one, one, one], "NEXT")
        self.create_sequence_step(seq_name, 6, [zero, one, one, one], "NEXT")
        self.create_sequence_step(seq_name, 7, [zero, zero, one, one], "NEXT")
        self.create_sequence_step(seq_name, 8, [zero, zero, zero, one], "NEXT")
        self.create_sequence_step(seq_name, 9, [zero, zero, zero, zero], "FIRST")

    def _create_sequence4(self, seq_name: str, waves: list) -> None:  # noqa: C901
        """
        Create four channel sequence using four waveforms.

        :param seq_name: sequence name
        :param waves: list of wave names
        """
        self.logger.info("Create sequence %s with waves %s", seq_name, waves)
        if not seq_name:
            self.logger.error("Sequence name not set")
            return
        w = waves
        self.create_sequence_step(seq_name, 1, [w[0], w[3], w[2], w[1]], "NEXT")
        self.create_sequence_step(seq_name, 2, [w[1], w[0], w[3], w[2]], "NEXT")
        self.create_sequence_step(seq_name, 3, [w[2], w[1], w[0], w[3]], "NEXT")
        self.create_sequence_step(seq_name, 4, [w[3], w[2], w[1], w[0]], "FIRST")

    def _create_sequence8(self, seq_name: str, waves: list) -> None:
        """
        Create four channel sequence using eight waveforms.

        :param seq_name: sequence name
        :param waves: list of waveform asset names
        """
        self.logger.info("Create sequence %s with waves %s", seq_name, waves)
        if not seq_name:
            self.logger.error("Sequence name not set")
            return
        w = waves
        self.create_sequence_step(seq_name, 1, [w[0], w[7], w[6], w[5]], "NEXT")
        self.create_sequence_step(seq_name, 2, [w[1], w[0], w[7], w[6]], "NEXT")
        self.create_sequence_step(seq_name, 3, [w[2], w[1], w[0], w[7]], "NEXT")
        self.create_sequence_step(seq_name, 4, [w[3], w[2], w[1], w[0]], "NEXT")
        self.create_sequence_step(seq_name, 5, [w[4], w[3], w[2], w[1]], "NEXT")
        self.create_sequence_step(seq_name, 6, [w[5], w[4], w[3], w[2]], "NEXT")
        self.create_sequence_step(seq_name, 7, [w[6], w[5], w[4], w[3]], "NEXT")
        self.create_sequence_step(seq_name, 8, [w[7], w[6], w[5], w[4]], "FIRST")

    def _create_sequencex(self, seq_name: str, wave_name: str, n_waves: int) -> None:
        """
        Create four channel sequence using specified number of waveforms.

        :param seq_name: sequence name
        :param wave_name: wave name
        :param n_waves: number of waves
        """
        self.logger.info('Create sequence "%s" with %d waveforms', seq_name, n_waves)
        if not seq_name:
            self.logger.error("Sequence name not set")
            return
        w = []
        for n in range(0, n_waves):
            w.append(f"{wave_name}{n:04d}")
        w_l = len(w)
        self.logger.info('Create sequence "%s" with %d waves %s', seq_name, w_l, w)
        n0 = 0
        n1 = w_l - 1
        n2 = w_l - 2
        n3 = w_l - 3
        for n in range(0, w_l - 1):
            self.logger.info("Add step %d: %d %d %d %d", n + 1, n0, n1, n2, n3)
            self.create_sequence_step(
                seq_name, n + 1, [w[n0], w[n1], w[n2], w[n3]], "NEXT"
            )
            n0 += 1
            n1 += 1
            if n1 >= w_l:
                n1 = 0
            n2 += 1
            if n2 >= w_l:
                n2 = 0
            n3 += 1
            if n3 >= w_l:
                n3 = 0
        self.logger.info(
            "Add step %d: %d %d %d %d", w_l, w_l - 1, w_l - 2, w_l - 3, w_l - 4
        )
        self.create_sequence_step(
            seq_name, n_waves, [w[n0], w[n1], w[n2], w[n3]], "FIRST"
        )

    def add_sequence_step(
        self,
        seq_name: str,
        step_nr: int,
        waves: list,
        goto: str,
    ) -> None:
        """
        Create new sequence step.

        :param seq_name: sequence name
        :param step_nr: step number
        :param waves: waveforms
        :param goto: next step
        """
        self.logger.info(
            "Assign waveform %s step %d: %s go to %s", seq_name, step_nr, waves, goto
        )
        if not seq_name:
            self.logger.error("Sequence name not set")
            return
        if step_nr <= 0:
            self.logger.error("Invalid step number for asset waveform: %d", step_nr)
            return
        self.write_scpi(f'SLIST:SEQUENCE:STEP:ADD "{seq_name}",{step_nr},1')
        self.write_scpi(
            f'SLIST:SEQUENCE:STEP{step_nr}:TASSET1:WAVEFORM "{seq_name}","{waves[0]}"'
        )
        self.write_scpi(
            f'SLIST:SEQUENCE:STEP{step_nr}:TASSET2:WAVEFORM "{seq_name}","{waves[1]}"'
        )
        self.write_scpi(
            f'SLIST:SEQUENCE:STEP{step_nr}:TASSET3:WAVEFORM "{seq_name}","{waves[2]}"'
        )
        self.write_scpi(
            f'SLIST:SEQUENCE:STEP{step_nr}:TASSET4:WAVEFORM "{seq_name}","{waves[3]}"'
        )
        # Number of times assigned waveform(s) play before proceeding to next step
        self.write_scpi(f'SLIST:SEQUENCE:STEP{step_nr}:RCOUNT "{seq_name}",1')
        # Set target step for the GOTO command of sequencer at specified step
        self.write_scpi(f'SLISt:SEQUENCE:STEP{step_nr}:GOTO "{seq_name}", {goto}')

    # pylint: disable-next=too-many-branches,too-many-statements
    def create_sequence_4track(  # noqa: C901
        self,
        awg_ampl: float | None,
        awg_freq: float | None,
        awg_sample: float | None,
        seq_name: str,
        waves: list,
        check_err: bool = False,
    ) -> None:
        """
        Create new sequence.

        :param awg_ampl: output amplitude
        :param awg_freq: frequency
        :param awg_sample: sample rate
        :param seq_name: sequence name
        :param waves: waveforms
        :param check_err: check for errors
        """
        self.logger.info(
            'Create four track sequence "%s" with tracks %s', seq_name, waves
        )
        if not seq_name:
            self.logger.error("Sequence name not set")
            return
        if len(waves) != 4:
            self.logger.error("List of waves should contain 4 items, got %s", waves)
            return
        n_steps: int = 1
        awg = self.visa_hw
        n_tracks: int = 4

        if awg_ampl is None:
            awg_ampl = 500e-3
        if awg_sample is None:
            awg_sample = 1e9
        if awg_freq is None:
            awg_freq = 100e6
        awg_offset = 0.0

        # Delete previous version of sequence
        self.write_scpi(f'SLIST:SEQUENCE:DELETE "{seq_name}"')

        # Create new sequence with selected name, number of steps and number of tracks.
        self.write_scpi(f'SLIST:SEQUENCE:NEW "{seq_name}",{n_steps},{n_tracks}')

        # Set recommended amplitude (peak-to-peak)
        self.write_scpi(f'SLIST:SEQUENCE:AMPLITUDE "{seq_name}",{awg_ampl}')

        # Set recommended frequency of specified sequence when sequence contains IQ
        # waveforms.
        self.write_scpi(f'SLIST:SEQUENCE:FREQUENCY "{seq_name}",{awg_freq}')

        # Set recommended sampling rate
        self.write_scpi(f'SLIST:SEQUENCE:SRATE "{seq_name}",{awg_sample}')

        # Set recommended offset
        self.write_scpi(f'SLIST:SEQUENCE:OFFSET "{seq_name}",{awg_offset}')

        step_nr = 1
        goto = "NEXT"
        self.add_sequence_step(seq_name, step_nr, waves, goto)

        if check_err:
            self.logger.info("Check for errors")
            awg.query("*WAI")
            print(awg.query("SYSTEM:ERROR:ALL?"))

        awg.close()

    # pylint: disable-next=too-many-branches,too-many-statements
    def create_sequence_2wave(  # noqa: C901
        self,
        awg_ampl: float | None,
        awg_freq: float | None,
        awg_sample: float | None,
        seq_name: str,
        waves: list,
        rec_blocks: int | None,
        check_err: bool = False,
    ) -> None:
        """
        Create four channel sequence using two waveforms.

        :param awg_ampl: output amplitude
        :param awg_freq: frequency
        :param awg_sample: sample rate
        :param seq_name: sequence name
        :param waves: waveforms
        :param rec_blocks: number of blocks
        :param check_err: check for errors
        """
        if not seq_name:
            self.logger.error("Sequence name not set")
            return
        n_steps: int
        awg = self.visa_hw
        n_tracks: int = 4
        if len(waves) == 1:
            if rec_blocks is None:
                self.logger.error("Number of blocks in wave %s not specified", waves[0])
                return
            n_steps = rec_blocks
        elif len(waves) == 2:
            n_steps = 9
        elif len(waves) == 4:
            n_steps = 4
        elif len(waves) == 8:
            n_steps = 8
        else:
            self.logger.error("Sequences with %d waves not supported", len(waves))
            return
        self.logger.info(
            'Create sequence "%s" with %d steps and %d tracks: %s ',
            seq_name,
            n_steps,
            n_tracks,
            waves,
        )

        if awg_ampl is None:
            awg_ampl = 500e-3
        if awg_sample is None:
            awg_sample = 1e9
        if awg_freq is None:
            awg_freq = 100e6
        awg_offset = 0.0
        self.logger.info(
            "Amplitude %f offset %f frequency %f sample rate %f",
            awg_ampl,
            awg_offset,
            awg_freq,
            awg_sample,
        )

        # Turn channel outputs off
        self.write_scpi("OUTPUT1:STATE OFF")
        self.write_scpi("OUTPUT2:STATE OFF")
        self.write_scpi("OUTPUT3:STATE OFF")
        self.write_scpi("OUTPUT4:STATE OFF")

        # Delete previous version of sequence
        self.write_scpi(f'SLIST:SEQUENCE:DELETE "{seq_name}"')

        # Create new sequence with selected name, number of steps and number of tracks.
        self.write_scpi(f'SLIST:SEQUENCE:NEW "{seq_name}", {n_steps}, {n_tracks}')

        # Set recommended amplitude (peak-to-peak)
        self.write_scpi(f'SLIST:SEQUENCE:AMPLITUDE "{seq_name}",{awg_ampl}')

        # Set recommended frequency of specified sequence when sequence contains IQ
        # waveforms.
        self.write_scpi(f'SLIST:SEQUENCE:FREQUENCY "{seq_name}",{awg_freq}')

        # Set recommended sampling rate
        self.write_scpi(f'SLIST:SEQUENCE:SRATE "{seq_name}",{awg_sample}')

        # Set recommended offset
        self.write_scpi(f'SLIST:SEQUENCE:OFFSET "{seq_name}",{awg_offset}')

        # Steps 1 to 9
        if len(waves) == 1:
            self._create_sequencex(seq_name, waves[0], n_steps)
        if len(waves) == 2:
            zero: str = waves[0]
            one: str = waves[1]
            self._create_sequence2(seq_name, zero, one)
        elif len(waves) == 4:
            self._create_sequence4(seq_name, waves)
        elif len(waves) == 8:
            self._create_sequence8(seq_name, waves)
        else:
            pass

        # Assign track of sequence (from sequence list) to specified channel
        self.write_scpi(f'SOURCE1:CASSET:SEQUENCE "{seq_name}", 1')
        self.write_scpi(f'SOURCE2:CASSET:SEQUENCE "{seq_name}", 2')
        self.write_scpi(f'SOURCE3:CASSET:SEQUENCE "{seq_name}", 3')
        self.write_scpi(f'SOURCE4:CASSET:SEQUENCE "{seq_name}", 4')

        # Turn channel outputs on
        self.write_scpi("OUTPUT1:STATE ON")
        self.write_scpi("OUTPUT2:STATE ON")
        self.write_scpi("OUTPUT3:STATE ON")
        self.write_scpi("OUTPUT4:STATE ON")

        self.logger.info("Run")
        self.write_scpi("AWGCONTROL:RUN:IMMEDIATE")

        if check_err:
            self.logger.info("Check for errors")
            awg.query("*WAI")
            print(awg.query("SYSTEM:ERROR:ALL?"))
        awg.close()

    def start_sequence(
        self,
        seq_name: str,
    ) -> None:
        """
        Press play for sequence.

        :param seq_name: sequence name
        """
        self.logger.info('Start sequence "%s"', seq_name)
        if not seq_name:
            self.logger.error("Sequence name not set")
            return
        # Assign track of sequence (from sequence list) to specified channel
        self.write_scpi(f'SOURCE1:CASSET:SEQUENCE "{seq_name}", 1')
        self.write_scpi(f'SOURCE2:CASSET:SEQUENCE "{seq_name}", 2')
        self.write_scpi(f'SOURCE3:CASSET:SEQUENCE "{seq_name}", 3')
        self.write_scpi(f'SOURCE4:CASSET:SEQUENCE "{seq_name}", 4')

        # Turn channel outputs on
        self.write_scpi("OUTPUT1:STATE ON")
        self.write_scpi("OUTPUT2:STATE ON")
        self.write_scpi("OUTPUT3:STATE ON")
        self.write_scpi("OUTPUT4:STATE ON")

        self.logger.info("Run")
        self.write_scpi("AWGCONTROL:RUN:IMMEDIATE")

    # pylint: disable-next=too-many-branches
    def load_sequence_data(
        self,
        seq_name: str,
        seq_len: int,
        new_seq: int,
        run: bool,
        clear: bool,
        asset_name: str | None,
        rec_blocks: int | None,
        ch_volt: float | None,
        ch_freq: float | None,
        awg_sample: float | None,
        step_nr: int,
        goto: str,
    ) -> int:
        """
        Create sequence.

        :param seq_name: sequence name
        :param seq_len: number of steps
        :param new_seq: number of tracks
        :param run: load sequence
        :param clear: delete sequence
        :param asset_name: waveform name
        :param rec_blocks: number of blocks in waveform
        :param ch_volt: amplitude
        :param ch_freq: frequency
        :param awg_sample: sampling rate
        :param step_nr: step number
        :param goto: next step
        :return: zero, usually
        """
        if not seq_name:
            self.logger.error("Sequence name not set")
            return 1
        asset_names: list
        if run:
            self.run_sequence(seq_name)
        elif clear:
            self.delete_sequence(seq_name)
        elif new_seq:
            self.sequence_new(seq_name, seq_len, new_seq)
        elif asset_name is not None:
            if "," in asset_name:
                asset_names = asset_name.split(",")
            elif rec_blocks is not None:
                self.logger.info(
                    'Only one asset name specified in "%s", use %d blocks',
                    asset_name,
                    rec_blocks,
                )
                asset_names = [asset_name]
            else:
                self.logger.error(
                    'One asset name specified in "%s" without blocks', asset_name
                )
                return 1
            if len(asset_names) == 4:
                if step_nr and goto:
                    self.create_sequence_step(seq_name, step_nr, asset_names, goto)
                else:
                    self.create_sequence_4track(
                        ch_volt, ch_freq, awg_sample, seq_name, asset_names
                    )
            else:
                self.create_sequence(
                    ch_volt, ch_freq, awg_sample, seq_name, asset_names, rec_blocks
                )
        else:
            self.logger.error('Waveforms for sequence "%s" not specified', seq_name)
            return 1
        return 0

    def status_awg_seq(self) -> int:
        """
        Read sequence(s) from AWG.

        :returns: zero or one
        """
        sdict = self.dict_sequences()
        self.logger.info("Read sequences: %s", sdict)
        for s_num in sdict:
            # print(f"{s_num:4} : {sdict[s_num]}")
            pp = pprint.PrettyPrinter(depth=4)
            pp.pprint(sdict[s_num])
        return 0
