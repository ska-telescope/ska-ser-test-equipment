"""Generate waveform data."""

# pylint: disable=too-many-arguments,too-many-positional-arguments
# pylint: disable=useless-suppression

import logging
import os
from typing import Any

import matplotlib.pyplot as plt
import numpy as np
from PyAstronomy import pyaC  # type: ignore[no-untyped-def,import-untyped]

from ska_ser_test_equipment.awg_visa.noise_data import NOISE_DATA
from ska_ser_test_equipment.awg_visa.tektronix_awg_definitions import (
    AWG_AMPL,
    AWG_FREQ,
    AWG_OFFSET,
    AWG_SAMPLE,
)


# pylint: disable-next=too-many-instance-attributes
class Wavedata:
    """Generate data for AWG."""

    def __init__(
        self,
        logger: logging.Logger,
        asset_name: str,
        ch_volt: float | None,
        ch_offset: float | None,
        ch_freq: float | None,
        rec_len: int,
        awg_sample: float | None,
        wav_zero: int,
        wav_trunc: int,
        wav_view: bool,
        wav_delay: int,
        data_path: str = ".",
    ) -> None:
        """
        Initialize wave data.

        :param logger: logging handle
        :param asset_name: wave name
        :param ch_volt: amplitude
        :param ch_offset: DC offset
        :param ch_freq: frequency
        :param rec_len: record length
        :param awg_sample: sampling rate
        :param wav_zero: set number of values to zero
        :param wav_trunc: truncate wave at this mark
        :param wav_view: display wave in graphical form
        :param wav_delay: set up delay
        :param data_path: directory where data files reside
        """
        self.logger = logger
        self.asset_name = asset_name
        # Amplitude
        self.ch_volt = ch_volt
        if self.ch_volt is None:
            self.ch_volt = AWG_AMPL
        # Offset
        self.ch_offset = ch_offset
        if self.ch_offset is None:
            self.ch_offset = AWG_OFFSET
        # Frequency
        self.ch_freq = ch_freq
        if self.ch_freq is None:
            self.ch_freq = AWG_FREQ
        # Record length
        self.rec_len = rec_len
        # Sampling rate
        self.awg_sample = awg_sample
        if self.awg_sample is None:
            self.awg_sample = AWG_SAMPLE
        self.logger.info(
            "Process wave with %d samples, %E per second", self.rec_len, self.awg_sample
        )
        self.wav_zero = wav_zero
        self.wav_trunc = wav_trunc
        self.wav_view = wav_view
        self.wav_delay = wav_delay
        self.delim = "\n"
        self.data_path = data_path

    def __repr__(self) -> str:
        """
        Do the string thing.

        :returns: magic string
        """
        return f"Wave {self.asset_name}: {self.ch_volt}V, delay {self.wav_delay}"

    def write_file(self, wave_data: Any, output_file: str | None = None) -> None:
        """
        Write data to text file.

        :param wave_data: numpy array to write
        :param output_file: file name
        """
        if output_file:
            self.logger.info("Write file %s", output_file)
            _file_nam, file_ext = os.path.splitext(output_file)
            if file_ext in (".txt", ".csv"):
                self.logger.info("Write wave data to %s", output_file)
                wave_data.tofile(output_file, sep=self.delim)
                # Add delimiter at the end of file
                # pylint: disable-next=unspecified-encoding
                with open(output_file, "a") as foo_file:
                    foo_file.write(self.delim)
            else:
                self.logger.info("Write binary wave data to %s", output_file)
                wave_data.tofile(output_file, sep="")
        elif self.asset_name:
            file_name = os.path.join(self.data_path, f"{self.asset_name}.txt")
            self.logger.info("Write wave data to file %s", file_name)
            wave_data.tofile(file_name, sep=self.delim)
            # Add delimiter at the end of file
            # pylint: disable-next=unspecified-encoding
            with open(file_name, "a") as foo_file:
                foo_file.write(self.delim)
        else:
            self.logger.info("Nothing to see here")

    def plot(self, x: Any, y: Any, wav_name: str) -> None:
        """
        Write data to graph.

        :param x: numpy array to write
        :param y: numpy array to write
        :param wav_name: waveform asset name
        """
        if self.wav_view:
            # Plot the points using matplotlib
            self.logger.info("Plot wave")
            plt.title(f"{wav_name}")
            plt.plot(x, y)
            plt.show()

    # pylint: disable-next=too-many-locals
    def plot_file(
        self,
        input_file: str,
        x_start: int | None = None,
        x_stop: int | None = None,
        wav_up: int = 0,
    ) -> None:  # noqa: C901
        """
        Read data from text file and plot it.

        :param input_file: file name
        :param x_start: X axis start
        :param x_stop: X axis finish
        :param wav_up: flag for upscaling
        """
        self.logger.info("Plot file %s", input_file)
        file_data: list = []
        # pylint: disable-next=unspecified-encoding
        _file_nam, file_ext = os.path.splitext(input_file)
        if file_ext in (".txt", ".csv"):
            self.logger.info("Read %s text file %s", file_ext[1:].upper(), input_file)
            # pylint: disable-next=unspecified-encoding
            with open(input_file) as file:
                n = 0
                for line in file:
                    val_str = line.rstrip()
                    if val_str:
                        vals = val_str.split(",")
                        if len(vals) == 2:
                            val = float(vals[1])
                        elif len(vals) == 5:
                            val = float(vals[0])
                        else:
                            val = float(vals[0])
                        n += 1
                        # self.logger.debug("%6d --> %.6E", n, val)
                        file_data.append(val)
            if x_start is not None and x_stop is not None:
                self.logger.info("X axis from %d to %d", x_start, x_stop)
                t = np.arange(start=x_start, stop=x_stop)
                wave_data = np.array(file_data[x_start:x_stop])
            else:
                wave_data = np.array(file_data)
                wave_len = len(wave_data)
                t = np.arange(wave_len)
            self.logger.info("Plot %d records from %s", len(file_data), input_file)
        else:
            self.logger.info("Read %s binary file %s", file_ext[1:].upper(), input_file)
            wave_data = np.fromfile(input_file, count=-1, sep="", offset=0)
            wave_len = len(wave_data)
            t = np.arange(wave_len)
        if wav_up:
            self.logger.debug("Upsample wave by %d points", wav_up)
            wave_data = np.kron(wave_data, np.ones((wav_up)))
            wave_len = len(wave_data)
            t = np.arange(wave_len)
        self.plot(t, wave_data, f"File {input_file}")

    # pylint: disable-next=too-many-locals
    def read_files(
        self, input_files: list, output_file: str | None, plot_it: bool = True
    ) -> Any:
        """
        Read data from text file.

        :param input_files: list of file names to read
        :param output_file: file name
        :param plot_it: generate plot
        """
        self.logger.info("Read files %s", input_files)
        file_data: list = []
        for input_file in input_files:
            # pylint: disable-next=unspecified-encoding
            with open(input_file) as file:
                self.logger.info("Read file %s", input_file)
                n = 0
                for line in file:
                    val_str = line.rstrip()
                    if val_str:
                        vals = val_str.split(",")
                        if len(vals) == 2:
                            val = float(vals[1])
                        elif len(vals) == 5:
                            val = float(vals[0])
                        else:
                            val = float(vals[0])
                        n += 1
                        # self.logger.debug("%6d --> %.6E", n, val)
                        file_data.append(val)
        wave_len = len(file_data)
        if self.rec_len > wave_len:
            self.logger.info("Add %d zero samples", self.rec_len - wave_len)
            for _n in range(wave_len, self.rec_len):
                file_data.append(0.0)
            wave_len = len(file_data)
        wave_data = np.array(file_data)
        # Add delay by rolling the wave
        if self.wav_delay:
            self.logger.debug("Roll wave by %d points", self.wav_delay)
            wave_data = np.roll(wave_data, self.wav_delay)
        self.write_file(wave_data, output_file)
        if plot_it:
            t = np.arange(wave_len)
            self.plot(t, wave_data, f"File {output_file} with length {wave_len}")

    # pylint: disable-next=too-many-locals,too-many-branches,too-many-statements
    def read_file(  # noqa: C901
        self,
        input_file: str,
        output_file: str | None,
        plot_it: bool = True,
        wav_up: int = 0,
    ) -> None:
        """
        Read data from text file.

        :param input_file: file name
        :param output_file: file name
        :param plot_it: generate plot
        :param wav_up: flag for upscaling
        """
        self.logger.info("Read file %s", input_file)
        file_data: list = []
        if output_file is None:
            self.logger.error("File name not specified")
            return
        _file_nam, file_ext = os.path.splitext(output_file)
        if file_ext in (".txt", ".csv"):
            self.logger.info("Read text file %s", input_file)
            # pylint: disable-next=unspecified-encoding
            with open(input_file) as file:
                n = 0
                for line in file:
                    val_str = line.rstrip()
                    if val_str:
                        vals = val_str.split(",")
                        if len(vals) == 2:
                            val = float(vals[1])
                        elif len(vals) == 5:
                            val = float(vals[0])
                        else:
                            val = float(vals[0])
                        n += 1
                        # self.logger.debug("%6d --> %.6E", n, val)
                        file_data.append(val)
                self.logger.info("Read %d records from %s", len(file_data), input_file)
            wave_len = len(file_data)
            wave_data = np.array(file_data)
        else:
            self.logger.info("Read %s binary file %s", file_ext[1:].upper(), input_file)
            wave_data = np.fromfile(input_file, count=-1, sep="", offset=0)
            wave_len = len(wave_data)
        if self.ch_volt is not None:
            wave_data *= self.ch_volt
        # Add zero values
        if self.wav_zero < 0:  # type: ignore[operator]
            self.wav_zero *= -1
            self.logger.debug("Set first %d points to zero", self.wav_zero)
            for n in range(0, self.wav_zero):  # type: ignore[arg-type]
                wave_data[n] = 0.0
        elif self.wav_zero > 0:  # type: ignore[operator]
            wave_zero = np.zeros(wave_len, dtype=float)
            self.logger.debug("Add %d zero points", self.wav_zero)
            np.append(wave_data, wave_zero)
            # for n in range(0, self.wav_zero):  # type: ignore[operator]
            #     np.append(wave_data, [0.0])
        else:
            pass
        # Truncate wave
        if self.wav_trunc > 0:
            t = np.arange(wave_len)
            # Get coordinates and indices of zero crossings
            _xc, xidx = pyaC.zerocross1d(t, wave_data, getIndices=True)
            self.logger.debug(f"crossings = {_xc}")
            self.logger.debug(f"indexes = {xidx}")
            wave_len = 0
            # Find zero crossing closest to cutoff point
            for xi in xidx:
                if xi >= self.wav_trunc:
                    wave_len = xi
                    break
            self.logger.info("Mark at %d", wave_len)
            wave_data = wave_data[0:wave_len]
            # Set last value to zero
            wave_data[wave_len - 1] = 0.0
        # Add delay by rolling the wave
        if self.wav_delay:
            self.logger.debug("Roll wave by %d points", self.wav_delay)
            wave_data = np.roll(wave_data, self.wav_delay)
        if wav_up:
            self.logger.debug("Upsample wave by %d points", wav_up)
            wave_data = np.kron(wave_data, np.ones((wav_up)))
            wave_len = len(wave_data)
            t = np.arange(wave_len)
        if self.rec_len > wave_len:
            self.logger.info(
                "Add %d zero samples (from %d to %d)",
                self.rec_len - wave_len,
                wave_len,
                self.rec_len,
            )
            for _n in range(wave_len, self.rec_len):
                self.logger.debug("Append %d", _n)
                wave_data = np.append(wave_data, [0.0])
            wave_len = len(wave_data)
            t = np.arange(wave_len)
            self.logger.info("Length is now %d", wave_len)
        if output_file:
            self.write_file(wave_data, output_file)
        t = np.arange(wave_len)
        if plot_it:
            self.plot(
                t, wave_data, f"Asset {self.asset_name} with delay {self.wav_delay}"
            )

    def sine(self, output_file: str | None, plot_it: bool = True) -> Any:
        """
        Generate sine wave.

        :param output_file: file name
        :param plot_it: generate plot
        """
        self.logger.info(
            "Generate sine wave: %.3E V and %.3E Hz (%d)",
            self.ch_volt,
            self.ch_freq,
            self.rec_len,
        )
        t = np.arange(self.rec_len)
        sin_freq = self.ch_freq / self.awg_sample  # type: ignore[operator]
        wave_data = self.ch_volt * np.sin(2 * np.pi * t * sin_freq)
        if self.ch_volt is not None:
            wave_data *= self.ch_volt
        if self.wav_zero > 0:  # type: ignore[operator]
            self.logger.debug("Set first %d points to zero", self.wav_zero)
            for n in range(0, self.wav_zero):  # type: ignore[arg-type]
                wave_data[n] = 0.0
        elif self.wav_zero < 0:  # type: ignore[operator]
            rec_len = len(wave_data)
            self.logger.debug("Set last %d points to zero", self.wav_zero)
            for n in range(rec_len + self.wav_zero, rec_len):  # type: ignore[operator]
                wave_data[n] = 0.0
        else:
            pass
        if self.wav_delay:
            self.logger.debug("Roll wave by %d points", self.wav_delay)
            wave_data = np.roll(wave_data, self.wav_delay)
        self.write_file(wave_data, output_file)
        if plot_it:
            self.plot(t, wave_data, f"Sine wave {self.asset_name}")

    def noise(self, output_file: str | None, plot_it: bool = True) -> Any:
        """
        Generate noise wave.

        :param output_file: file name
        :param plot_it: generate plot
        """
        wave_data = np.array(NOISE_DATA)
        rec_len = len(wave_data)
        t = np.arange(rec_len)
        if self.ch_volt is not None:
            wave_data *= self.ch_volt
        if self.wav_zero > 0:  # type: ignore[operator]
            self.logger.debug("Set first %d points to zero", self.wav_zero)
            for n in range(0, self.wav_zero):  # type: ignore[arg-type]
                wave_data[n] = 0.0
        elif self.wav_zero < 0:  # type: ignore[operator]
            rec_len = len(wave_data)
            self.logger.debug("Set last %d points to zero", self.wav_zero)
            for n in range(rec_len + self.wav_zero, rec_len):  # type: ignore[operator]
                wave_data[n] = 0.0
        else:
            pass
        if self.wav_delay:
            self.logger.debug("Roll wave by %d points", self.wav_delay)
            wave_data = np.roll(wave_data, self.wav_delay)
        self.write_file(wave_data, output_file)
        if plot_it:
            self.plot(t, wave_data, f"Noise wave {self.asset_name}")

    def noisy_sine(
        self, scale: float, output_file: str | None, plot_it: bool = True
    ) -> None:
        """
        Generate sine wave with noise.

        :param scale: standard deviation spread (or width) of distribution
        :param output_file: file name
        :param plot_it: generate plot
        """
        t = np.arange(self.rec_len)
        sin_freq = self.ch_freq / self.awg_sample  # type: ignore[operator]
        wave_data = np.sin(2 * np.pi * t * sin_freq) + np.random.normal(
            0.0, scale, self.rec_len
        )
        if self.wav_zero > 0:  # type: ignore[operator]
            self.logger.debug("Set first %d points to zero", self.wav_zero)
            for n in range(0, self.wav_zero):  # type: ignore[arg-type]
                wave_data[n] = 0.0
        elif self.wav_zero < 0:  # type: ignore[operator]
            rec_len = len(wave_data)
            self.logger.debug("Set last %d points to zero", self.wav_zero)
            for n in range(rec_len + self.wav_zero, rec_len):  # type: ignore[operator]
                wave_data[n] = 0.0
        else:
            pass
        if self.ch_volt is not None:
            wave_data *= self.ch_volt
        if self.wav_delay:
            self.logger.debug("Roll wave by %d points", self.wav_delay)
            wave_data = np.roll(wave_data, self.wav_delay)
        self.write_file(wave_data, output_file)
        if plot_it:
            self.plot(t, wave_data, f"Noisy sine wave {self.asset_name}")

    def decay_sine(
        self, decay: float, output_file: str | None, plot_it: bool = True
    ) -> None:
        """
        Generate decaying sine wave.

        :param decay: exponential delay
        :param output_file: file name
        :param plot_it: generate plot
        """
        t = np.arange(self.rec_len)
        # A = self.ch_volt
        # f = 1E9
        # n = self.rec_len
        # k = decay   # n * 25
        # T = 1 / f
        # t=[0:T/5:2*n*T];
        sin_freq = self.ch_freq / self.awg_sample  # type: ignore[operator]
        self.logger.info(
            "Generate decayed sine wave: %.3E V and %.3E Hz (%f)",
            self.ch_volt,
            self.ch_freq,
            decay,
        )
        wave_data = self.ch_volt * np.exp(-decay * t) * np.sin(2 * np.pi * sin_freq * t)
        if self.wav_zero > 0:  # type: ignore[operator]
            self.logger.debug("Set first %d points to zero", self.wav_zero)
            for n in range(0, self.wav_zero):  # type: ignore[arg-type]
                wave_data[n] = 0.0
        elif self.wav_zero < 0:  # type: ignore[operator]
            rec_len = len(wave_data)
            self.logger.debug("Set last %d points to zero", self.wav_zero)
            for n in range(rec_len + self.wav_zero, rec_len):  # type: ignore[operator]
                wave_data[n] = 0.0
        else:
            pass
        if self.ch_volt is not None:
            wave_data *= self.ch_volt
        if self.wav_delay:
            self.logger.debug("Roll wave by %d points", self.wav_delay)
            wave_data = np.roll(wave_data, self.wav_delay)
        self.write_file(wave_data, output_file)
        if plot_it:
            self.plot(t, wave_data, f"Decayed sine wave {self.asset_name}")
