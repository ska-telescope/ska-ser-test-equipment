"""Tango attributes for AWG sequences."""

import ast
import logging
import math
from typing import Any

from ska_control_model import AdminMode
from tango import AttrQuality, TimeVal
from tango.server import attribute, command


# pylint: disable-next=too-many-instance-attributes,too-many-public-methods
class Awg5208DeviceSequencesMixin:
    """Mixin with (con)sequences."""

    tektronix: Any
    logger: logging.Logger
    _admin_mode: AdminMode
    _sequence_amplitude: float = float("nan")
    _sequence_frequency: float = float("nan")
    _sequence_length: int = -1
    _sequence_list_size: int = -1
    _sequence_list_index: int = 0
    _sequence_name: str = ""
    _sequence_offset: float = float("nan")
    _sequence_sampling_rate: float = float("nan")
    _sequence_step_nr: int = 0
    _sequence_step_repeat_count: int = -1
    _sequence_tracks: int = -1

    @command
    def sequence_new(self, seq_stuff: str) -> None:
        """
        Write new sequence.

        :param seq_stuff: name of sequence with number of steps and tracks
        """
        kwargs = ast.literal_eval(seq_stuff)
        seq_name: str = kwargs["seq_name"]
        n_steps: int = int(kwargs["n_steps"])
        n_tracks: int = int(kwargs["n_tracks"])
        self.tektronix.sequence_new(seq_name, n_steps, n_tracks)

    @command(dtype_in=str)
    def sequence_asset_waveform(self, seq_stuff: str) -> None:
        """
        Read sequence asset waveform.

        :param seq_stuff: JSON string
        """
        kwargs = ast.literal_eval(seq_stuff)
        seq_name: str = kwargs["seq_name"]
        wave_name: str = kwargs["wave_name"]
        step_no: int = int(kwargs["step_no"])
        track_no: int = int(kwargs["track_no"])
        self.tektronix.sequence_asset_waveform(seq_name, step_no, track_no, wave_name)

    @attribute
    def sequence_amplitude(self) -> float:
        """
        Get sequence amplitude.

        :returns: amplitude
        """
        self._sequence_amplitude = self.tektronix.sequence_amplitude(
            self._sequence_name
        )
        if math.isnan(self._sequence_amplitude):
            return (  # type: ignore[return-value]
                self._sequence_amplitude,
                TimeVal.totime(TimeVal.now()),
                AttrQuality.ATTR_INVALID,
            )
        return self._sequence_amplitude

    @sequence_amplitude.is_allowed
    # pylint: disable-next=unused-argument
    def sequence_amplitude_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        if not self._sequence_name:
            self.logger.error("Sequence name not set")
            return False
        return self._admin_mode == 0

    @sequence_amplitude.write  # type: ignore[no-redef]
    def sequence_amplitude(self, value: float) -> None:
        """
        Set sequence amplitude.

        :param value: new amplitude
        """
        self._sequence_amplitude = value
        self.tektronix.sequence_amplitude(self._sequence_name, value)

    @attribute
    def sequence_frequency(self) -> float:
        """
        Get sequence frequency.

        :returns: frequency
        """
        self._sequence_frequency = self.tektronix.sequence_frequency(
            self._sequence_name
        )
        if math.isnan(self._sequence_frequency):
            return (  # type: ignore[return-value]
                self._sequence_frequency,
                TimeVal.totime(TimeVal.now()),
                AttrQuality.ATTR_INVALID,
            )
        return self._sequence_frequency

    @sequence_frequency.is_allowed
    # pylint: disable-next=unused-argument
    def sequence_frequency_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        if not self._sequence_name:
            self.logger.error("Sequence name not set")
            return False
        return self._admin_mode == 0

    @sequence_frequency.write  # type: ignore[no-redef]
    def sequence_frequency(self, value: float) -> None:
        """
        Set sequence frequency.

        :param value: change to this
        """
        self._sequence_frequency = value
        self.tektronix.sequence_frequency(self._sequence_name, value)

    @attribute(dtype=int)
    def sequence_length(self) -> int:
        """
        Get sequence length.

        :returns: length
        """
        self._sequence_length = self.tektronix.sequence_length(self._sequence_name)
        return self._sequence_length

    @sequence_length.is_allowed
    # pylint: disable-next=unused-argument
    def sequence_length_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        if not self._sequence_name:
            self.logger.error("Sequence name not set")
            return False
        return self._admin_mode == 0

    @sequence_length.write  # type: ignore[no-redef]
    def sequence_length(self, value: int) -> None:
        """
        Set sequence length.

        :param value: change to this
        """
        self._sequence_length = value
        self.tektronix.sequence_length(self._sequence_name, value)

    @attribute(dtype=int)
    def sequence_list_index(self) -> int:
        """
        Get the index number for sequences in sequence list.

        :returns: list index
        """
        return self._sequence_list_index

    @sequence_list_index.is_allowed
    # pylint: disable-next=unused-argument
    def sequence_list_index_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._admin_mode == 0

    @sequence_list_index.write  # type: ignore[no-redef]
    def sequence_list_index(self, value: int) -> None:
        """
        Set the index number for sequences in sequence list.

        :param value: new list index
        """
        self._sequence_list_index = value

    @attribute(dtype=int)
    def sequence_list_size(self) -> int:
        """
        Get the number of sequences in sequence list.

        :returns: list size
        """
        self._sequence_list_size = self.tektronix.sequence_list_size
        return self._sequence_list_size

    @sequence_list_size.is_allowed
    # pylint: disable-next=unused-argument
    def sequence_list_size_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._admin_mode == 0

    @attribute(dtype=str)
    def sequence_names(self) -> str:
        """
        Get all sequence names.

        :returns: name
        """
        l_seqs = self.tektronix.list_sequences()
        self.logger.debug("Sequence names %s", l_seqs)
        return ",".join(l_seqs)

    @attribute(dtype=str)
    def sequence_name(self) -> str:
        """
        Get current sequence name.

        :returns: name
        """
        return self._sequence_name

    @sequence_name.is_allowed
    # pylint: disable-next=unused-argument
    def sequence_name_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._admin_mode == 0

    @sequence_name.write  # type: ignore[no-redef]
    def sequence_name(self, value: str) -> None:
        """
        Set current sequence name.

        :param value: new name
        """
        self._sequence_name = value

    @attribute(dtype=str)
    def sequence_name_index(self) -> str:
        """
        Get current sequence name.

        :returns: name
        """
        return self.tektronix.sequence_name_index(self._sequence_list_index)

    @sequence_name_index.is_allowed
    # pylint: disable-next=unused-argument
    def sequence_name_index_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        if not self._sequence_name:
            self.logger.error("Sequence name not set")
            return False
        return self._admin_mode == 0

    @attribute(dtype=int)
    def sequence_step_nr(self) -> int:
        """
        Get sequence step number.

        :returns: step number
        """
        self.logger.debug("Sequence_step_nr is %d", self._sequence_step_nr)
        return self._sequence_step_nr

    @sequence_step_nr.is_allowed
    # pylint: disable-next=unused-argument
    def sequence_step_nr_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        return self._admin_mode == 0

    @sequence_step_nr.write  # type: ignore[no-redef]
    def sequence_step_nr(self, value: int) -> None:
        """
        Get sequence step number.

        :param value: new step number
        """
        self.logger.debug("Set sequence_step_nr to %d", value)
        self._sequence_step_nr = value

    @attribute(dtype=int)
    def sequence_step_repeat_count(self) -> int:
        """
        Get number of times waveform(s) play before proceeding to next step in sequence.

        :returns: current step count
        """
        self._sequence_step_repeat_count = self.tektronix.sequence_step_repeat_count(
            self._sequence_name, self._sequence_step_nr
        )
        return self._sequence_step_repeat_count

    @sequence_step_repeat_count.is_allowed
    # pylint: disable-next=unused-argument
    def sequence_step_repeat_count_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        if not self._sequence_name:
            self.logger.error("Sequence name not set")
            return False
        if self._sequence_step_nr <= 0:
            self.logger.error("Sequence step number not set")
            return False
        return self._admin_mode == 0

    @sequence_step_repeat_count.write  # type: ignore[no-redef]
    def sequence_step_repeat_count(self, value: int) -> None:
        """
        Set number of times waveform(s) play before proceeding to next step in sequence.

        :param value: new step count
        """
        self._sequence_step_repeat_count = value
        self.tektronix.sequence_step_repeat_count(
            self._sequence_name, self._sequence_step_nr, value
        )

    @attribute(dtype=int)
    def sequence_tracks(self) -> int:
        """
        Read sequence tracks.

        :returns: tracks
        """
        return self.tektronix.sequence_tracks(self._sequence_name)

    @sequence_tracks.is_allowed
    # pylint: disable-next=unused-argument
    def sequence_tracks_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        if not self._sequence_name:
            self.logger.error("Sequence name not set")
            return False
        return self._admin_mode == 0

    @attribute
    def sequence_offset(self) -> float:
        """
        Read sequence offset.

        :returns: offset
        """
        self._sequence_offset = self.tektronix.sequence_offset(self._sequence_name)
        if math.isnan(self._sequence_frequency):
            return (  # type: ignore[return-value]
                self._sequence_offset,
                TimeVal.totime(TimeVal.now()),
                AttrQuality.ATTR_INVALID,
            )
        return self._sequence_offset

    @sequence_offset.is_allowed
    # pylint: disable-next=unused-argument
    def sequence_offset_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        if not self._sequence_name:
            self.logger.error("Sequence name not set")
            return False
        return self._admin_mode == 0

    @sequence_offset.write  # type: ignore[no-redef]
    def sequence_offset(self, value: float) -> None:
        """
        Write sequence offset.

        :param value: change to this
        """
        self.tektronix.sequence_offset(self._sequence_name, value)

    @attribute
    def sequence_sampling_rate(self) -> float:
        """
        Read sequence sampling rate.

        :returns: sampling rate
        """
        self._sequence_sampling_rate = self.tektronix.sequence_sampling_rate(
            self._sequence_name
        )
        if math.isnan(self._sequence_sampling_rate):
            return (  # type: ignore[return-value]
                self._sequence_sampling_rate,
                TimeVal.totime(TimeVal.now()),
                AttrQuality.ATTR_INVALID,
            )
        return self._sequence_sampling_rate

    @sequence_sampling_rate.is_allowed
    # pylint: disable-next=unused-argument
    def sequence_sampling_rate_is_allowed(self, request_type: Any) -> bool:
        """
        Check that this operation can be done.

        :param request_type: read or write
        :returns: OK to proceed
        """
        if not self._sequence_name:
            self.logger.error("Sequence name not set")
            return False
        return self._admin_mode == 0

    @sequence_sampling_rate.write  # type: ignore[no-redef]
    def sequence_sampling_rate(self, value: float) -> None:
        """
        Write sequence sampling rate.

        :param value: change to this
        """
        self._sequence_sampling_rate = value
        self.tektronix.sequence_sampling_rate(self._sequence_name, value)
