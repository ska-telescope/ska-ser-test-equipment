"""Mixin for loading waveforms into AWG."""

# pylint: disable=too-many-arguments,too-many-positional-arguments
# pylint: disable=useless-suppression

# mypy: disable-error-code=attr-defined

import logging

from ska_ser_test_equipment.awg_visa.tektronix_awg_definitions import (
    AWG_AMPL,
    AWG_FREQ,
    AWG_REC_LEN,
    AWG_SAMPLE,
)


# pylint: disable-next=too-many-instance-attributes,too-many-public-methods
class TektronixBasicWaveformMixin:
    """Mixin class for basic waveforms."""

    logger: logging.Logger
    _bwaveform_amplitude: float = float("nan")
    _bwaveform_assign: bool = False
    _bwaveform_auto_calculate: str = ""
    _bwaveform_cycles: float = 0
    _bwaveform_frequency: float = float("nan")
    _bwaveform_full_dac_range: bool = True
    _bwaveform_function: str = ""
    _bwaveform_high: float = float("nan")
    _bwaveform_length: float = 0
    _bwaveform_low: float = float("nan")
    _bwaveform_name: str = ""
    _bwaveform_offset: float = float("nan")
    _bwaveform_play: bool = False
    _bwaveform_sample_rate: float = float("nan")

    @property
    def bwaveform_amplitude(self) -> float:
        """
        Get waveform amplitude.

        BASIC WAVEFORM EDITOR (69)

        This command sets or returns the peak-to-peak Amplitude value for the waveform
        created by the Basic Waveform editor plug-in.

        This setting can be affected if the system is set to use the full DAC range.
        Using the full DAC range is the default setting. Refer to the command
        BWAVeform:FDRange for more information.

        The amplitude settings for Offset, High, and Low voltage values can be affected
        by the Amplitude setting.

        :returns: amplitude
        """
        self._bwaveform_amplitude = self.read_query_float(
            "bwaveform_amplitude", "BWAVEFORM:AMPLITUDE?"
        )
        return self._bwaveform_amplitude

    @bwaveform_amplitude.setter
    def bwaveform_amplitude(self, value: float) -> None:
        """
        Set waveform amplitude.

        :param value: new value for amplitude
        """
        self._bwaveform_amplitude = value
        self.write_query_float("bwaveform_amplitude", "BWAVEFORM:AMPLITUDE", value, "V")

    @property
    def bwaveform_assign(self) -> bool:
        """
        Get waveform assignment.

        BASIC WAVEFORM EDITOR (71)

        This command sets or returns the state (enabled or disabled) of the Basic
        Waveform editor to either compile the waveform and immediately assign it to a
        specified channel (enabled) or just compile the waveform (disabled).

        False sets the Basic Waveform editor to compile only.

        True sets the Basic Waveform editor to compile and assign the waveform to a
         channel.

        :returns: assignment
        """
        self._bwaveform_assign = self.read_query_state(
            "bwaveform_assign", "BWAVEFORM:COMPILE:CASSIGN?"
        )
        return self._bwaveform_assign

    @bwaveform_assign.setter
    def bwaveform_assign(self, value: bool) -> None:
        """
        Set waveform assignment.

        :param value: new value for assignment
        """
        self._bwaveform_assign = value
        if value:
            self.write_query_state("bwaveform_assign", "BWAVEFORM:COMPILE:CASSIGN", "1")
        else:
            self.write_query_state("bwaveform_assign", "BWAVEFORM:COMPILE:CASSIGN", "0")

    @property
    def bwaveform_auto_calculate(self) -> str:
        """
        Get waveform auto calculate (LEN|CYCLE|DUR|FREQ|SR).

        BASIC WAVEFORM EDITOR (70)

        This command sets or returns the Basic Waveform editor plug-in Auto Calculate
        setting, determining which value is automatically calculated.

        When the function is set to DC or Noise the options are: Length, Duration,
        or Sample Rate.

        When the function is set to Sine, Square, Triangle, or Ramp the options are:
        Length, Cycle, Frequency, or Sample Rate.

        :returns: setting for auto calculate
        """
        self._bwaveform_auto_calculate = self.read_query_str(
            "bwaveform_auto_calculate", "BWAVEFORM:AUTO?"
        )
        return self._bwaveform_auto_calculate

    @bwaveform_auto_calculate.setter
    def bwaveform_auto_calculate(self, value: str) -> None:
        """
        Set waveform auto calculate.

        :param value: new value for auto calculate
        """
        self._bwaveform_auto_calculate = value
        self.write_query_str("bwaveform_auto_calculate", "BWAVEFORM:AUTO", value)

    @property
    def bwaveform_cycles(self) -> float:
        """
        Get waveform auto cycles.

        BASIC WAVEFORM EDITOR (75)

        This command sets or returns the Cycle value (number of times the waveform
        repeats) for the waveform created by the Basic Waveform editor plug-in.

        This command has no effect if "Cycle" is set to auto-calculate.

        :returns: cycles
        """
        self._bwaveform_cycles = self.read_query_float(
            "bwaveform_cycles", "BWAVEFORM:CYCLE?"
        )
        return self._bwaveform_cycles

    @bwaveform_cycles.setter
    def bwaveform_cycles(self, value: float) -> None:
        """
        Set waveform cycles.

        :param value: new value for cycles
        """
        self._bwaveform_cycles = value
        self.write_query_float("bwaveform_cycles", "BWAVEFORM:CYCLE", value)

    @property
    def bwaveform_frequency(self) -> float:
        """
        Get waveform frequency.

        BASIC WAVEFORM EDITOR (77)

        This command sets or returns the Frequency for the waveform created by the
        Basic Waveform editor plug-in.

        This command has no effect if "Frequency" is set to auto-calculate.

        :returns: frequency
        """
        self._bwaveform_frequency = self.read_query_float(
            "bwaveform_frequency", "BWAVEFORM:FREQUENCY?"
        )
        return self._bwaveform_frequency

    @bwaveform_frequency.setter
    def bwaveform_frequency(self, value: float) -> None:
        """
        Set waveform frequency.

        :param value: new value for frequency
        """
        self._bwaveform_frequency = value
        self.write_query_float(
            "bwaveform_frequency", "BWAVEFORM:FREQUENCY", value, "Hz"
        )

    @property
    def bwaveform_full_dac_range(self) -> bool:
        """
        Get waveform full DAC range.

        BASIC WAVEFORM EDITOR (76)

        This command sets or returns the state (enabled or disabled) of the Basic
        Waveform editor plug-in to use or not use the full DAC range during compile.
        Using the full DAC range when compiling waveforms results in waveforms with
        the best resolution.

        When enabled, if the selected offset and amplitude are within the range of the
        instrument’s hardware, then the compiled waveform is compiled using the full
        DAC range and the compiled waveform’s recommended amplitude and offset
        properties are set to the requested amplitude and offset values. If the selected
        offset and amplitude will result in a compiled waveform that does not take
        advantage of the full DAC range, the instrument adjusts the compiled waveform’s
        recommended amplitude and offset values to use the full DAC range. If the
        system cannot achieve the full DAC range, a warning message is displayed.

        When disabled, the waveform is compiled using the specified amplitude and
        offset values and the compiled waveform’s recommended amplitude is set to
        the maximum value and the recommended offset is set to 0. The control is not
        available for a DC waveform.

        :returns: flag
        """
        self._bwaveform_full_dac_range = self.read_query_state(
            "bwaveform_full_dac_range", "BWAVEFORM:FDRANGE?"
        )
        return self._bwaveform_full_dac_range

    @bwaveform_full_dac_range.setter
    def bwaveform_full_dac_range(self, value: bool) -> None:
        """
        Set waveform full DAC range.

        :param value: new value for flag
        """
        self._bwaveform_full_dac_range = value
        self.write_query_state("bwaveform_full_dac_range", "BWAVEFORM:FDRANGE", value)

    @property
    def bwaveform_function(self) -> str:
        """
        Get waveform function.

        BASIC WAVEFORM EDITOR (78)

        This command sets or returns the Basic Waveform editor plug-in waveform type.

        :returns: setting for function - sine|square|triangle|ramp|noise|DC
        """
        self._bwaveform_function = self.read_query_str(
            "bwaveform_function", "BWAVEFORM:FUNCTION?"
        )
        return self._bwaveform_function

    @bwaveform_function.setter
    def bwaveform_function(self, value: str) -> None:
        """
        Set waveform function (sine|square|triangle|ramp|noise|DC).

        This command sets or returns the Basic Waveform editor plug-in waveform type.

        :param value: new value for function - sine|square|triangle|ramp|noise|DC
        """
        self._bwaveform_function = value
        self.write_query_str("bwaveform_function", "BWAVEFORM:FUNCTION", value)

    @property
    def bwaveform_high(self) -> float:
        """
        Get waveform high amplitude.

        BASIC WAVEFORM EDITOR (79)

        This command sets or returns the High voltage value for the waveform created
        by the Basic Waveform editor plug-in.

        The High and Low values are initially one half the amplitude of the waveform
        (with an offset of 0 V). Changing these values causes the Amplitude value to
        adjust. Changing the High and Low to uneven values cause a change to the
        Offset value.

        :returns: amplitude
        """
        self._bwaveform_high = self.read_query_float(
            "bwaveform_high", "BWAVEFORM:HIGH?"
        )
        self.logger.debug("Read bwaveform_high: %f", self._bwaveform_high)
        return self._bwaveform_high

    @bwaveform_high.setter
    def bwaveform_high(self, value: float) -> None:
        """
        Set waveform high amplitude.

        :param value: new value for amplitude
        """
        self._bwaveform_high = value
        self.logger.debug("Write bwaveform_high: %f", self._bwaveform_high)
        self.write_query_float("bwaveform_high", "BWAVEFORM:HIGH", value, "V")

    @property
    def bwaveform_length(self) -> float:
        """
        Get waveform length.

        BASIC WAVEFORM EDITOR (80)

        This command sets or returns the Length for the waveform created by the Basic
        Waveform editor plug-in.

        This command has no effect if "Length" is set to auto-calculate.

        :returns: length
        """
        self._bwaveform_length = self.read_query_float(
            "bwaveform_length", "BWAVEFORM:LENGTH?"
        )
        return self._bwaveform_length

    @bwaveform_length.setter
    def bwaveform_length(self, value: float) -> None:
        """
        Set waveform length.

        :param value: new value for length
        """
        self._bwaveform_length = value
        self.write_query_float("bwaveform_length", "BWAVEFORM:LENGTH", value)

    @property
    def bwaveform_low(self) -> float:
        """
        Get waveform low amplitude.

        BASIC WAVEFORM EDITOR (81)

        This command sets or returns the Low voltage value for the waveform created
        by the Basic Waveform editor plug-in.

        The High and Low values are initially one half the amplitude of the waveform
        (with an offset of 0 V). Changing these values causes the Amplitude value to
        adjust. Changing the High and Low to uneven values cause a change to the
        Offset value.

        :returns: amplitude
        """
        self._bwaveform_low = self.read_query_float("bwaveform_low", "BWAVEFORM:LOW?")
        self.logger.info("Read bwaveform_low: %f", self._bwaveform_low)
        return self._bwaveform_low

    @bwaveform_low.setter
    def bwaveform_low(self, value: float) -> None:
        """
        Set waveform low amplitude.

        :param value: new value for amplitude
        """
        self._bwaveform_low = value
        self.write_query_float("bwaveform_low", "BWAVEFORM:LOW", value, "V")

    @property
    def bwaveform_name(self) -> str:
        """
        Get waveform name.

        BASIC WAVEFORM EDITOR (73)

        This command sets or returns the name of the Basic Waveform editor plug-in
        compiled waveform.

        If the name already exists in the Waveform List, the name is appended with an
        underscore suffix such as “Waveform_1”.

        :returns: setting for name
        """
        self._bwaveform_name = self.read_query_str(
            "bwaveform_name", "BWAVEFORM:COMPILE:NAME?"
        )
        return self._bwaveform_name

    @bwaveform_name.setter
    def bwaveform_name(self, value: str) -> None:
        """
        Set waveform name.

        :param value: new name
        """
        self._bwaveform_name = value
        self.write_query_str("bwaveform_name", "BWAVEFORM:COMPILE:NAME", value)

    @property
    def bwaveform_offset(self) -> float:
        """
        Get waveform offset.

        BASIC WAVEFORM EDITOR (82)

        This command sets or returns the Offset voltage value for the waveform created
        by the Basic Waveform editor plug-in.

        This setting can be affected if the system is set to use the full DAC range.
        Using the full DAC range is the default setting. Refer to the command
        BWAVeform:FDRange for more information.

        The amplitude settings for High and Low voltage values can be affected by the
        Offset setting.

        :returns: offset
        """
        self._bwaveform_offset = self.read_query_float(
            "bwaveform_offset", "BWAVEFORM:OFFSET?"
        )
        return self._bwaveform_offset

    @bwaveform_offset.setter
    def bwaveform_offset(self, value: float) -> None:
        """
        Set waveform offset.

        :param value: new value for offset
        """
        self._waveform_offset = value
        self.write_query_float("bwaveform_offset", "BWAVEFORM:OFFSET", value, "V")

    @property
    def bwaveform_play(self) -> bool:
        """
        Get waveform play flag.

        BASIC WAVEFORM EDITOR (74)

        This command sets or returns the state (enabled or disabled) of the Basic
        Waveform editor to either immediately play the waveform after compile or just
        compile.

        This command requires that the compiled waveform is assigned to a channel.

        :returns: flag
        """
        self._bwaveform_play = self.read_query_state(
            "bwaveform_play", "BWAVEFORM:COMPILE:PLAY?"
        )
        return self._bwaveform_play

    @bwaveform_play.setter
    def bwaveform_play(self, value: bool) -> None:
        """
        Set waveform play flag.

        :param value: new value for flag
        """
        self._bwaveform_play = value
        self.write_query_state("bwaveform_play", "BWAVEFORM:COMPILE:PLAY", value)

    @property
    def bwaveform_sample_rate(self) -> float:
        """
        Get waveform sample rate.

        BASIC WAVEFORM EDITOR (83)

        This command sets or returns the Sample Rate for the waveform created by the
        Basic Waveform editor plug-in.

        This command has no effect if "Sample Rate" is set to auto-calculate.

        :returns: sample rate
        """
        self._bwaveform_sample_rate = self.read_query_float(
            "bwaveform_sample_rate", "BWAVEFORM:SRATE?"
        )
        return self._bwaveform_sample_rate

    @bwaveform_sample_rate.setter
    def bwaveform_sample_rate(self, value: float) -> None:
        """
        Set waveform sample rate.

        :param value: new value for sample rate
        """
        self._bwaveform_sample_rate = value
        self.write_query_float("bwaveform_sample_rate", "BWAVEFORM:SRATE", value, "V")

    def create_bwaveform(
        self,
        reset_bwe: bool,
        func: str,
        asset_name: str,
        awg_sample: float | None,
        rec_len: int | None,
        ampl: float | None,
        freq: float | None,
        output_file: str | None = None,
    ) -> None:
        """
        Create a waveform using the basic waveform editor.

        :param reset_bwe: reset basic waveform editor to default
        :param func: waveform type - sine or noise (others to be done)
        :param asset_name: waveform name
        :param awg_sample: sample rate
        :param rec_len: number of samples
        :param ampl: amplitude
        :param freq: frequency
        :param output_file: read data and write to this file
        """
        if reset_bwe:
            # Reset the basic waveform editor plug-in to its default values
            self.write_scpi("BWAVEFORM:RESET")
        self.delete_wave(asset_name)
        if func == "noise":
            self.create_bwaveform_noise(asset_name, awg_sample, rec_len, ampl)
        else:
            self.create_bwaveform_wave(
                func, asset_name, awg_sample, rec_len, ampl, freq
            )
        if output_file is not None:
            # TODO write to file
            pass

    def create_bwaveform_noise(
        self,
        asset_name: str,
        awg_sample: float | None,
        rec_len: int | None,
        ampl: float | None,
    ) -> None:
        """
        Create noise waveform with basic waveform editor.

        :param asset_name: waveform name
        :param awg_sample: sample rate
        :param rec_len: number of samples
        :param ampl: amplitude
        """
        # Set name of waveform
        self.write_scpi(f'BWAVEFORM:COMPILE:NAME "{asset_name}"')
        # Set plug-in waveform type
        self.write_scpi("BWAVEFORM:FUNCTION noise")
        # Set peak-to-peak amplitude value
        if ampl is None:
            ampl = AWG_AMPL
        self.write_scpi(f"BWAVEFORM:AMPLITUDE {ampl:.3E}")
        # Set the sample rate for the waveform created
        if awg_sample is None:
            awg_sample = AWG_SAMPLE
        self.write_scpi(f"BWAVEFORM:SRATE {awg_sample:.3E}")
        # Set the offset voltage value for the waveform created
        self.write_scpi("BWAVEFORM:OFFSET 0.0")
        # Set the length for the waveform created
        if rec_len is None:
            rec_len = AWG_REC_LEN
        self.write_scpi(f"BWAVEFORM:LENGTH {rec_len}")
        # Number of times the waveform repeats
        self.write_scpi("BWAVEFORM:CYCLE 1")
        # Auto calculate duration
        self.write_scpi("BWAVEFORM:AUTO DUR")
        # Use the full DAC range during compile
        self.write_scpi("BWAVEFORM:FDRANGE 1")
        # Disable playing the waveform after compile
        self.write_scpi("BWAVEFORM:COMPILE:PLAY 0")
        # Just compile the waveform, do not assign it to a channel
        self.write_scpi("BWAVEFORM:COMPILE:CASSIGN 0")
        self.logger.info(
            'Compile noise wave "%s": %E V (%d samples at %E S/s)',
            asset_name,
            ampl,
            rec_len,
            awg_sample,
        )
        # Initiate the Basic Waveform editor plug-in compile process
        self.write_scpi("BWAVEFORM:COMPILE")

    def create_bwaveform_wave(
        self,
        func: str,
        asset_name: str,
        awg_sample: float | None,
        rec_len: int | None,
        ampl: float | None,
        freq: float | None,
    ) -> None:
        """
        Create sine, square, triangle or ramp waveform with basic waveform editor.

        :param func: waveform type - sine or noise (others to be done)
        :param asset_name: waveform name
        :param awg_sample: sample rate
        :param rec_len: number of samples
        :param ampl: amplitude
        :param freq: frequency
        """
        # Set name of waveform
        self.write_scpi(f'BWAVEFORM:COMPILE:NAME "{asset_name}"')
        # Set plug-in waveform type
        if func not in ("sine", "square", "triangle", "ramp"):
            self.logger.error("Invalid wave function %s", func)
            return
        cmd = f"BWAVEFORM:FUNCTION {func}"
        self.write_scpi(cmd)
        # Set peak-to-peak amplitude value
        if ampl is None:
            ampl = AWG_AMPL
        self.write_scpi(f"BWAVEFORM:AMPLITUDE {ampl:.3E}")
        # Set the sample rate for the waveform created
        if awg_sample is None:
            awg_sample = AWG_SAMPLE
        self.write_scpi(f"BWAVEFORM:SRATE {awg_sample:.3E}")
        # Set the offset voltage value for the waveform created
        self.write_scpi("BWAVEFORM:OFFSET 0.0")
        # Set the frequency for the waveform created
        if freq is None:
            freq = AWG_FREQ
        self.write_scpi(f"BWAVEFORM:FREQUENCY {freq:.3E}")
        # Set the Length for the waveform created
        if rec_len is None:
            rec_len = AWG_REC_LEN
        self.write_scpi(f"BWAVEFORM:LENGTH {rec_len}")
        # Number of times the waveform repeats
        self.write_scpi("BWAVEFORM:CYCLE 1")
        # Auto calculate sample rate
        self.write_scpi("BWAVEFORM:AUTO SR")
        # Use the full DAC range during compile
        self.write_scpi("BWAVEFORM:FDRANGE 1")
        # Disable playing the waveform after compile
        self.write_scpi("BWAVEFORM:COMPILE:PLAY 0")
        # Just compile the waveform, do not assign it to a channel
        self.write_scpi("BWAVEFORM:COMPILE:CASSIGN 0")
        self.logger.info(
            'Compile %s wave "%s": %E Hz, %E V (%d samples at %E S/s)',
            func,
            asset_name,
            ampl,
            freq,
            rec_len,
            awg_sample,
        )
        # Initiate the Basic Waveform editor plug-in compile process
        self.write_scpi("BWAVEFORM:COMPILE")
