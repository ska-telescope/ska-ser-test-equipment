"""Commands for Tekronix AWG Tango device."""

import ast
import logging
from typing import Any

import tango
from ska_control_model import AdminMode
from tango.server import command

from ska_ser_test_equipment.awg_visa.tektronix_awg_definitions import AWG_DATA_BLOCK
from ska_ser_test_equipment.awg_visa.wave_data import Wavedata

# mypy: disable-error-code=attr-defined


class Awg5208DeviceCommandsMixin:
    """These are the commands."""

    tektronix: Any
    logger: logging.Logger
    data_block_size: int
    _admin_mode: AdminMode
    _channel_nr: int

    @command(dtype_in=str)
    def device_status(self, args: str) -> str:
        """
        Read AWG hardware status.

        :param args: JSON string with arguments
        :returns: dictionary string
        """
        kwargs = ast.literal_eval(args)
        ch_num: int = int(kwargs["ch_num"])
        ch_nums = [ch_num]
        self.logger.debug("Read status for channels %s", ch_nums)
        status_dict = self.tektronix.dict_status(ch_nums)
        return f"{status_dict}"

    @command(dtype_in=str)
    def assign_wave(self, args: str) -> None:
        """
        Assign waveform asset to channel.

        :param args: JSON string with arguments
        """
        kwargs = ast.literal_eval(args)
        ch_num: int = int(kwargs["ch_num"])
        asset_name: str = kwargs["asset_name"]
        self.tektronix.assign_wave(ch_num, asset_name)

    @command(dtype_in=str)
    def delete_wave(self, asset_name: str) -> None:
        """
        Delete waveform asset.

        :param asset_name: wavefrom asset name
        """
        self.tektronix.delete_wave(asset_name)

    @command(dtype_in=str)
    def download_wave(self, args: str) -> None:
        """
        Generate new file with waveform data.

        :param args: JSON string with arguments
        """
        kwargs = ast.literal_eval(args)
        asset_name: str = kwargs["asset_name"]
        ch_volt: float = kwargs["ch_volt"]
        input_file: str = kwargs["input_file"]
        wav_zero: int = kwargs["wav_zero"]
        wav_delay: int = kwargs["wav_delay"]
        wav_trunc: int = 0

        if not asset_name:
            self.logger.error("Wave name not specified")
            return
        ch_offset = None
        ch_freq = None
        rec_len = AWG_DATA_BLOCK
        awg_sample = None
        wav_view = False

        wav2 = Wavedata(
            self.logger,
            asset_name,
            ch_volt,
            ch_offset,
            ch_freq,
            rec_len,
            awg_sample,
            wav_zero,
            wav_trunc,
            wav_view,
            wav_delay,
        )
        wav2.read_file(input_file, None, False)

    @command(dtype_in=str, dtype_out=["float"])
    # pylint: disable-next=useless-suppression,too-many-locals
    def read_data_file(self, args: str) -> list:
        """
        Create waveform asset and write to file.

        :param args: JSON string with arguments
        :returns: list of values read from file
        """
        kwargs = ast.literal_eval(args)
        file_name: str = kwargs["file_name"]
        # asset_name: str = kwargs["asset_name"]
        # w_type: str = kwargs["w_type"]
        # ch_volt: float = kwargs["ch_volt"]
        # rec_len: int = kwargs["rec_len"]
        # ch_offset: float = 0.0
        # ch_freq: float | None = None
        # awg_sample: float = 1e9
        # wav_zero: int = 100
        # wav_delay: int = 0
        # wav_trunc: int = 0
        # awg_decay: float = 0.01

        self.logger.info("Read text file %s", file_name)
        file_data: list = []
        # pylint: disable-next=unspecified-encoding
        with open(file_name) as file:  # type: ignore[arg-type]
            bline: str
            for bline in file:
                # val = self._read_file_data_line(bline)
                val = bline.split(",")[0]
                if val:
                    file_data.append(float(val))

        self.logger.info("Read %d text records", len(file_data))
        # wave_data = np.array(file_data)
        # time.sleep(3.0)
        return file_data

    @command()
    def reset(self) -> None:
        """Press reset button."""
        self.logger.info("Reset")
        self.set_status("RUNNING")
        self.set_state(tango.DevState.RUNNING)
        self.tektronix.reset()

    @command()
    def start(self) -> None:
        """Press play button."""
        self.logger.info("Start")
        self._channel_nr = 1
        # self.write_attribute("channel_nr", 1)
        self.set_status("RUNNING")
        self.set_state(tango.DevState.RUNNING)
        self.tektronix.start()

    @command()
    def stop(self) -> None:
        """Press stop button."""
        self.logger.info("Stop")
        self.set_status("OFF")
        self.set_state(tango.DevState.OFF)
        self.tektronix.stop()

    @command()
    def wait(self) -> None:
        """Press pause button."""
        self.logger.info("Wait")
        self.set_status("STANDBY")
        self.set_state(tango.DevState.STANDBY)
        self.tektronix.wait()
        self.set_state(tango.DevState.RUNNING)

    @command(dtype_in=str)
    # pylint: disable-next=too-many-locals
    def write_wave_file(self, args: str) -> None:
        """
        Create waveform asset and write to file.

        :param args: JSON string with arguments
        """
        kwargs = ast.literal_eval(args)
        asset_name: str = kwargs["asset_name"]
        w_type: str = kwargs["w_type"]
        ch_volt: float = kwargs["ch_volt"]
        rec_len: int = kwargs["rec_len"]
        ch_offset: float = 0.0
        ch_freq: float | None = None
        awg_sample: float = 1e9
        wav_zero: int = 100
        wav_view: bool = True
        wav_delay: int = 0
        wav_trunc: int = 0
        awg_decay: float = 0.01

        self.logger.info(
            "Create wave asset file %s.txt amplitude %.3E V (%d samples at %.1E S/s)",
            asset_name,
            ch_volt,
            rec_len,
            awg_sample,
        )

        wav = Wavedata(
            self.logger,
            asset_name,
            ch_volt,
            ch_offset,
            ch_freq,
            rec_len,
            awg_sample,
            wav_zero,
            wav_trunc,
            wav_view,
            wav_delay,
        )

        file_name: str = f"{asset_name}.csv"
        if w_type == "sine":
            wav.sine(file_name, False)
        elif w_type == "noise":
            wav.noise(file_name, False)
        elif w_type == "decayed sine":
            wav.decay_sine(awg_decay, file_name, False)
        elif w_type == "noisy sine":
            wav.noisy_sine(awg_decay, file_name, False)
        else:
            self.logger.error("Type %s not supported yet", w_type)

    @command(dtype_in=str)
    def upload_wave_data(self, args: str) -> None:
        """
        Write wave data to file.

        :param args: JSON string with arguments
        :raises Exception: error
        """
        kwargs = ast.literal_eval(args)
        try:
            input_file: str = kwargs["input_file"]
            asset_name: str = kwargs["asset_name"]
            ch_num: int = kwargs["ch_num"]
        except Exception as kerr:
            self.logger.error("Could not read arguments: %s", kerr)
            raise kerr
        awg_sample: float = self.tektronix.clock_sample_rate
        self.logger.debug("Read asset %s data from file %s", asset_name, input_file)
        try:
            self.tektronix.upload_wave_data(
                asset_name, ch_num, awg_sample, input_file, self.data_block_size, 0
            )
        # pylint: disable-next=broad-exception-caught
        except Exception as uerr:
            self.logger.error("Could not upload wave data: %s", uerr)

    @command(dtype_in=str, dtype_out=["float"])
    def download_wave_data(self, args: str) -> list:
        """
        Read wave data from instrument.

        :param args: JSON string with arguments
        :raises Exception: error
        :returns: list of values downloaded from instrument
        """
        output_file: str
        kwargs = ast.literal_eval(args)
        try:
            asset_name: str = kwargs["asset_name"]
        # pylint: disable-next=broad-exception-caught
        except Exception as kerr:
            self.logger.error("Could not read arguments: %s", kerr)
            raise kerr
        try:
            output_file = kwargs["output_file"]
        # pylint: disable-next=broad-exception-caught,useless-suppression
        except Exception:
            output_file = ""
        wave_data = self.tektronix.read_wave_data(asset_name)
        if output_file:
            self.logger.debug(
                "Download asset %s data to file %s", asset_name, output_file
            )
            # pylint: disable-next=unspecified-encoding
            with open(output_file, "w") as outf:
                for dat in wave_data:
                    outf.write(f"{dat}\n")
        else:
            self.logger.debug("Download asset %s data", asset_name)
        return wave_data

    @command(dtype_in=str)
    def create_bwaveform(self, args: str) -> None:
        """
        Create basic waveform.

        :param args: JSON string with arguments
        :raises Exception: error
        """
        kwargs = ast.literal_eval(args)
        try:
            reset_bwe: bool = kwargs["reset_bwe"]
            func: str = kwargs["func"]
            asset_name: str = kwargs["asset_name"]
            awg_sample: float | None = kwargs["awg_sample"]
            rec_len: int | None = kwargs["rec_len"]
            ampl: float | None = kwargs["ampl"]
            freq: float | None = kwargs["freq"]
            output_file: str | None = kwargs["output_file"]
        except Exception as kerr:
            self.logger.error("Could not read arguments: %s", kerr)
            raise kerr
        self.tektronix.create_bwaveform(
            reset_bwe,
            func,
            asset_name,
            awg_sample,
            rec_len,
            ampl,
            freq,
            output_file,
        )

    @command(dtype_in=str)
    def start_sequence(self, args: str) -> None:
        """
        Start running sequence.

        :param args: JSON string with arguments
        :raises Exception: error
        """
        kwargs = ast.literal_eval(args)
        try:
            seq_name = kwargs["seq_name"]
        except Exception as kerr:
            self.logger.error("Could not read arguments: %s", kerr)
            raise kerr
        self.tektronix.start_sequence(seq_name)

    @command(dtype_in=str)
    def write_wave_data(self, args: str) -> None:
        """
        Read wave data from instrument.

        :param args: JSON string with arguments
        :raises Exception: error
        """
        kwargs = ast.literal_eval(args)
        try:
            asset_name = kwargs["asset_name"]
            ch_num = kwargs["ch_num"]
            ch_volt = kwargs["ch_volt"]
            ch_freq = kwargs["ch_freq"]
            awg_sample = kwargs["awg_sample"]
            input_file = kwargs["input_file"]
            rec_len = kwargs["rec_len"]
            rec_blocks = kwargs["rec_blocks"]
            clear_esr = kwargs["clear_esr"]
        except Exception as kerr:
            self.logger.error("Could not read arguments: %s", kerr)
            raise kerr
        self.info("Write wave data for asset %s", asset_name)
        self.tektronix.write_wave_data(
            asset_name,
            ch_num,
            ch_volt,
            ch_freq,
            awg_sample,
            input_file,
            rec_len,
            rec_blocks,
            clear_esr,
        )

    @command(dtype_in=str)
    def load_sequence_data(self, args: str) -> None:
        """
        Load  sequence.

        :param args: JSON string with arguments
        :raises Exception: error
        """
        kwargs = ast.literal_eval(args)
        try:
            seq_name = kwargs["seq_name"]
        except Exception as kerr:
            self.logger.error("Could not read arguments: %s", kerr)
            raise kerr
        self.logger.info("Load sequence %s", seq_name)
