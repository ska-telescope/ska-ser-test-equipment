#!/usr/bin/env python
"""Implementation of a simple SCPI simulator for the spectrum analyser."""
import logging
import os
import socket
import threading
from typing import Dict, Final, Optional, cast

import numpy as np
from ska_ser_devices.client_server import TcpServer
from ska_ser_scpi import ScpiSimulator, SupportedAttributeType

try:
    from ..interface_definitions import InterfaceDefinitionFactory
except ImportError:
    from ska_ser_test_equipment.interface_definitions import InterfaceDefinitionFactory

logging.basicConfig(level=logging.DEBUG)
_module_logger = logging.getLogger(__name__)


class SpectrumAnalyserSimulator(ScpiSimulator):
    """A spectrum analyser simulator TCP server."""

    DEFAULTS: Final[Dict[str, SupportedAttributeType]] = {
        "autoattenuation": True,
        "attenuation": 0.0,
        "continuous": False,
        "frequency_start": 1.0e9,  # 1 GHz
        "frequency_stop": 2.0e9,  # 2 GHz
        "marker_frequency": 1.5e9,  # 1.5 GHz
        "marker_power": -10.0,
        "query_error": False,
        "device_error": False,
        "execution_error": False,
        "command_error": False,
        "power_cycled": False,
        "trace1": list(
            np.frombuffer(
                b'edge cases "\r\n" and ";" encoded as 32-bit floats',
                dtype=np.float32,
            )
        ),
        "trace1_detection": "POS",
        "trace1_function": "NONE",
        "trace1_average_count": 10,
        "rbw": 200000.0,
        "rbw_actual": 200000.0,
        "rbw_auto": True,
        "rbw_enabled": True,
        "vbw": 200000.0,
        "vbw_enabled": False,
        "reference_level": 0.0,
        "preamp_enabled": False,
    }

    def __init__(
        self,
        model: str,
        **kwargs: SupportedAttributeType,
    ) -> None:
        """
        Initialise a new spectrum analyser simulator.

        :param model: the model identifier to use.
        :param kwargs: initial values for simulator attributes; where an
            initial value is not provided for an attribute, a default
            value will be used.
        """
        self._lock = threading.Lock()

        self._num_markers = 0

        self._tracing = False
        self._flag_when_complete = False
        self._command_complete = False

        interface_definition = InterfaceDefinitionFactory()(model)

        initial_values = kwargs
        for key, value in self.DEFAULTS.items():
            initial_values.setdefault(key, value)

        super().__init__(interface_definition, initial_values)

    def clear_markers(self) -> None:
        """Clear all markers."""
        self._num_markers = 0

    def add_marker(self) -> None:
        """Add a marker."""
        self._num_markers += 1

    def get_trace(self) -> None:
        """Get a trace."""
        # TODO: simulator currently doesn't bother generating a trace.
        # It only spends half a second pretending to do so.

        self._tracing = True

        def _simulate_completion() -> None:
            self.set_attribute(
                "trace1",
                list(np.random.default_rng().standard_normal(101, dtype=np.float32)),
            )
            self._tracing = False

        threading.Timer(0.5, _simulate_completion).start()

    def marker_find_peak(self) -> None:
        """Set marker_frequency and marker_power based on the current trace's peak."""
        if self._num_markers >= 1:
            trace = cast(list[np.float32], self.get_attribute("trace1"))
            freq_a = cast(float, self.get_attribute("frequency_start"))
            freq_b = cast(float, self.get_attribute("frequency_stop"))

            peak_power, peak_sample = max((x, i) for i, x in enumerate(trace))
            sample_width = (freq_b - freq_a) / (len(trace) - 1)
            peak_freq = freq_a + sample_width * peak_sample

            self.set_attribute("marker_frequency", peak_freq)
            self.set_attribute("marker_power", float(peak_power))

    def marker_frequency(self) -> Optional[float]:
        """
        Return the frequency of the peak.

        This method overrides the default behaviour (simply reading the
        attribute value) to ensure that the number of markers is checked
        first.

        :returns: the frequency of the peak, or None if the marker has
            not been added.
        """
        if self._num_markers < 1:
            # TODO: The real device also puts an error message in the system
            # error buffer.
            return None
        return cast(float, self.get_attribute("marker_frequency"))

    def marker_power(self) -> Optional[float]:
        """
        Return the power at the peak.

        This method overrides the default behaviour (simply reading the
        attribute value) to ensure that the number of markers is checked
        first.

        :returns: the power at the peak, or None if the marker has not
            been added.
        """
        if self._num_markers < 1:
            # TODO: The real device also puts an error message in the system
            # error buffer.
            return None
        return cast(float, self.get_attribute("marker_power"))

    def reset(self) -> None:
        """Reset to factory default values."""
        self._num_markers = 0
        self.set_attribute("autoattenuation", True)
        self.set_attribute("attenuation", 0.0)
        self.set_attribute("continuous", True)
        self.set_attribute("frequency_start", 1.4175e9)  # 1.4175 GHz
        self.set_attribute("frequency_stop", 1.5825e9)  # 1.5825 GHz

    def flag_when_complete(self) -> None:
        """Raise a flag when command is complete."""
        self._flag_when_complete = True

    def operation_complete(self) -> bool:
        """
        Return whether all previous operations are complete.

        This only returns true if :py:meth:`flag_when_complete` was
        called while the operation was running, and the operation is now
        complete.

        :returns: whether all previous operations are complete.
        """
        complete = self._flag_when_complete and not self._tracing
        if complete:
            self._flag_when_complete = False
        return complete


def main() -> None:
    """Run the socketserver main loop."""
    model = os.getenv("SIMULATOR_MODEL", "SPECMON26B").upper()
    host = os.getenv("SIMULATOR_HOST", socket.gethostname())
    port = int(os.getenv("SIMULATOR_PORT", "5025"))

    _module_logger.info("Start simulator for %s on %s:%d", model, host, port)
    simulator = SpectrumAnalyserSimulator(model)
    server = TcpServer(host, port, simulator)
    with server:
        server.serve_forever()


if __name__ == "__main__":
    main()
