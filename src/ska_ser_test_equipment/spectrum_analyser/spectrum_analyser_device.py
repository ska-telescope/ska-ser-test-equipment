# -*- coding: utf-8 -*-
#
# (c) 2022 CSIRO.
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement
# See LICENSE.txt for more info.

"""This module provides a Tango device for a spectrum analyser."""
import os
import socket
from typing import Any

import tango
import tango.server
from ska_control_model import ResultCode
from ska_tango_base.commands import SubmittedSlowCommand

from ska_ser_test_equipment.base import TestEquipmentBaseDevice
from ska_ser_test_equipment.mixins import SCPIWriteQueryMixin

try:
    from ..interface_definitions import InterfaceDefinitionFactory
    from .spectrum_analyser_component_manager import SpectrumAnalyserComponentManager
except ImportError:
    from ska_ser_test_equipment.interface_definitions import InterfaceDefinitionFactory

    # pylint: disable-next=line-too-long
    from ska_ser_test_equipment.spectrum_analyser.spectrum_analyser_component_manager import (  # noqa: E501
        SpectrumAnalyserComponentManager,
    )

__all__ = ["SpectrumAnalyserDevice", "main"]

CommandReturnType = tuple[list[ResultCode], list[str]]


class SpectrumAnalyserDevice(TestEquipmentBaseDevice, SCPIWriteQueryMixin):
    """A Tango device for monitor and control of a spectrum analyser."""

    # --------------
    # Initialization
    # --------------
    def create_component_manager(self) -> SpectrumAnalyserComponentManager:
        """
        Create and return a component manager for this device.

        :returns: a component manager for this device.
        """
        if self.Model is None:
            # pylint: disable-next=invalid-name
            self.Protocol = os.getenv("SIMULATOR_PROTOCOL", "tcp").lower()
            # pylint: disable-next=invalid-name
            self.Model = os.getenv("SIMULATOR_MODEL", "SPECMON26B").upper()
            # pylint: disable-next=invalid-name
            self.Host = os.getenv("SIMULATOR_HOST", socket.gethostname())
            # pylint: disable-next=invalid-name
            self.Port = int(os.getenv("SIMULATOR_PORT", "9100"))
        # pylint: disable-next=attribute-defined-outside-init
        self._interface_definition = InterfaceDefinitionFactory()(self.Model)
        return SpectrumAnalyserComponentManager(
            self._interface_definition,
            self.Protocol,  # tcp or telnet
            self.Host,
            self.Port,
            self.logger,
            self._communication_state_changed,
            self._component_state_changed,
            update_rate=self.UpdateRate,
        )

    def init_command_objects(self) -> None:
        """Register command objects (handlers) for this device's commands."""
        super().init_command_objects()
        self.register_command_object(
            "Acquire",
            SubmittedSlowCommand(
                "Acquire",
                self._command_tracker,
                self.component_manager,
                "acquire",
                logger=None,
            ),
        )

    @tango.server.command(dtype_out="DevVarLongStringArray")
    @tango.DebugIt()
    def Acquire(self) -> CommandReturnType:  # pylint: disable=invalid-name
        """
        Play the configured waveform on the configured outputs of the AWG.

        :returns: Tuple of command result and a message
        """
        handler = self.get_command_object("Acquire")
        result_code, message = handler()
        return [result_code], [message]

    @tango.server.command()
    @tango.DebugIt()
    def FindPeak(self) -> None:  # pylint: disable=invalid-name
        """Move the marker to the frequency where power is maximum."""
        self.component_manager.write_command("marker_find_peak")


def main(args: Any = None, **kwargs: Any) -> int:
    """
    Launch a `SpectrumAnalyserDevice` server instance.

    :param args: arguments to the spectrum analyser device.
    :param kwargs: keyword arguments to the tango device server.

    :returns: exit code of the Tango server.
    """
    return tango.server.run((SpectrumAnalyserDevice,), args=args, **kwargs)


if __name__ == "__main__":
    main()
