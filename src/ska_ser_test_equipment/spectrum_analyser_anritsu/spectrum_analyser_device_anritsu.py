#! /usr/bin/env python
# pylint: disable=too-many-lines
# mypy: disable-error-code="union-attr"
"""
Implement a Tango Device for Anritsu MS2090A.

This is a proof of concept at the moment.
"""
import logging
import os
from typing import Dict

import tango
from tango.server import attribute

from ska_ser_test_equipment.scpi.netcat import netcat, netcat_tx
from ska_ser_test_equipment.scpi.scpi_config import (
    POLL_COUNT,
    TANGO_LOG_LEVEL,
    get_host_port,
    get_scpi_config,
    get_scpi_polls,
)

LOG_LEVEL = logging.WARNING
logging.basicConfig(level=LOG_LEVEL)
_module_logger = logging.getLogger(__name__)


# pylint: disable-next=too-many-public-methods,too-many-instance-attributes
class SpectrumAnalyserDeviceAnritsu(tango.server.Device):
    """The device."""

    logger: logging.Logger | None = None
    admin_mode = True
    host: str = ""
    port: int = 0
    argument_separator: str = " "
    sentinel_string: str = "\n"
    scpi_fields: Dict[str, str] = {}
    scpi_polls: Dict[str, int] = {}
    scpi_values: Dict[str, int | float | str | bool] = {}
    event_status_register: int = 0

    def init_device(self) -> None:
        """
        Initialize the device.

        :raises ValueError: when YAML file can not be read
        """
        device_name: str = self.get_name()
        self.logger = logging.getLogger(device_name)
        self.logger.setLevel(logging.INFO)
        # self.logger.setLevel(_module_logger.getEffectiveLevel())
        self.logger.info("Initialize spectrum analyser device")
        super().init_device()
        self.set_status("DISABLE")
        self.set_state(tango.DevState.DISABLE)
        model = os.getenv("SIMULATOR_MODEL", "MS2090A").upper()
        self.host, self.port = get_host_port("spectrumanalyseranritsu")
        if not self.port:
            # raise ValueError(
            #     f"No SCPI port, check {' '.join(os.listdir('/app/charts'))}"
            # )
            self.logger.warning(
                "No SCPI port, check %s", f"{' '.join(os.listdir('/app/charts'))}"
            )
            self.host = "127.0.0.1"
            self.port = 9001
        (
            self.scpi_fields,
            self.scpi_values,
            self.scpi_mins,
            self.scpi_maxs,
        ) = get_scpi_config(model)
        if not self.scpi_fields:
            # self.logger.error("Could not read SCPI fields")
            raise ValueError(f"No SCPI fields, check {' '.join(os.listdir('/'))}")
        self.scpi_polls = get_scpi_polls(self.scpi_fields)

    def create_component_manager(self) -> None:
        """Create and return a component manager for this device."""
        self.logger.warning("No component manager")

    def attribute_poll(self, attr_name: str) -> bool:
        """
        Check if instrument should be read.

        :param attr_name: attribute name to be checked
        :returns: flag to indicate polling in progress
        """
        self.scpi_polls[attr_name] += 1
        self.logger.debug(
            "Poll frequency_stop %d : %s",
            self.scpi_polls[attr_name],
            str(self.scpi_values[attr_name]),
        )
        if self.scpi_polls[attr_name] < POLL_COUNT:
            return True
        return False

    def init_command_objects(self) -> None:
        """Register command objects (handlers) for this device's commands."""
        self.logger.warning("No command objects")

    def minimum_value_check(self, attr_name: str, attr_value: float | int) -> bool:
        """
        Check attribute value.

        :param attr_name: attribute name
        :param attr_value: attribute value
        :returns: true if value is less than configured minimum
        """
        if attr_name in self.scpi_mins:
            min_val = self.scpi_mins[attr_name]
            if attr_value < min_val:
                self.logger.warning(
                    "Value for %s : %s is lower than minimum %s",
                    attr_name,
                    f"{attr_value}",
                    f"{min_val}",
                )
                return True
        else:
            self.logger.warning("No minimum value for %s", attr_name)
        return False

    def maximum_value_check(self, attr_name: str, attr_value: float | int) -> bool:
        """
        Check attribute value.

        :param attr_name: attribute name
        :param attr_value: attribute value
        :returns: true if value is greater than configured maximum
        """
        if attr_name in self.scpi_maxs:
            max_val = self.scpi_maxs[attr_name]
            if attr_value > max_val:
                self.logger.warning(
                    "Value for %s : %s is higher than maximum %s",
                    attr_name,
                    f"{attr_value}",
                    f"{max_val}",
                )
                return True
        else:
            self.logger.warning("No maximum value for %s", attr_name)
        return False

    @attribute(dtype=bool)  # , polling_period=1000)
    # pylint: disable-next=invalid-name
    def adminMode(self) -> bool:
        """
        Control device status.

        :returns: admin mode flag
        """
        self.logger.info("Read adminMode: %s", self.admin_mode)
        return self.admin_mode

    @adminMode.write  # type: ignore[no-redef]
    # pylint: disable-next=invalid-name
    def adminMode(self, value: bool) -> None:
        """
        Control device status.

        :param value: new mode
        """
        self.admin_mode = value
        if self.admin_mode:
            self.logger.info("Disable device")
            self.set_status("DISABLE")
            self.set_state(tango.DevState.DISABLE)
            self.scpi_values["attenuation"] = -999
            self.scpi_values["frequency_stop"] = 0
            self.scpi_values["frequency_start"] = 0
        else:
            self.logger.info("Enable device")
            self.set_status("ON")
            self.set_state(tango.DevState.ON)

    @attribute(dtype=int, polling_period=1000)
    # pylint: disable-next=invalid-name,too-many-return-statements
    def loggingLevel(self) -> int:
        """
        Get logging level.

        0=OFF
        1=FATAL
        2=ERROR
        3=WARNING
        4=INFO
        5=DEBUG

        :returns: integer value zero to five
        """
        log_lvl = self.logger.getEffectiveLevel()
        self.logger.debug("Effective logging level %d", log_lvl)
        if log_lvl == logging.NOTSET:
            return 0
        if log_lvl == logging.CRITICAL:
            return 1
        if log_lvl == logging.ERROR:
            return 2
        if log_lvl == logging.WARNING:
            return 3
        if log_lvl == logging.INFO:
            return 4
        if log_lvl == logging.DEBUG:
            return 5
        self.logger.error("Invalid logging level %d", log_lvl)
        return 0

    @loggingLevel.write  # type: ignore[no-redef]
    # pylint: disable-next=invalid-name
    def loggingLevel(self, log_lvl: int):
        """
        Set logging level.

        :param log_lvl: integer value zero to five
        """
        if log_lvl == 0:
            self.logger.setLevel(logging.NOTSET)
        elif log_lvl == 1:
            self.logger.setLevel(logging.CRITICAL)
        elif log_lvl == 2:
            self.logger.setLevel(logging.ERROR)
        elif log_lvl == 3:
            self.logger.setLevel(logging.WARNING)
        elif log_lvl == 4:
            self.logger.setLevel(logging.INFO)
        elif log_lvl == 5:
            self.logger.setLevel(logging.DEBUG)
        else:
            self.logger.error("Invalid logging level %d", log_lvl)
            return
        elog_lvl = self.logger.getEffectiveLevel()
        self.logger.warning(
            "Set logging level to %d (%s)", elog_lvl, TANGO_LOG_LEVEL[log_lvl]
        )

    @attribute(dtype=str)
    # pylint: disable-next=invalid-name
    def hostInfo(self) -> str:
        """
        Get information about host.

        :returns: host name and port number
        """
        return f"{self.host}:{self.port}"

    # TODO for future use
    # @attribute(dtype=bool)
    # def add_marker(self) -> bool:
    #     """
    #     Get status of marker.
    #
    #     :returns: on or off
    #     """
    #     if self.admin_mode:
    #         return None
    #     scpi_tx = f"{self.scpi_fields['add_marker']}?"
    #     scpi_rx = netcat(self.host, self.port, scpi_tx, self.sentinel_string)
    #     if scpi_rx is None:
    #         return None
    #     self.logger.debug("Read add_marker: %s", scpi_rx)
    #     if scpi_rx == "1":
    #         return True
    #     return False

    # TODO for future use
    # @add_marker.write
    # def add_marker(self, value: bool) -> None:
    #     """
    #     Set status of marker.
    #
    #     :param value: new status
    #     """
    #     if self.admin_mode:
    #         self.logger.info("Cannot write add_marker in admin mode")
    #         return
    #     self.logger.info("Write add_marker: %s", value)
    #     scpi_tx = f"{self.scpi_fields['add_marker"]}{self.argument_separator}"
    #     if value:
    #         scpi_tx += "1"
    #     else:
    #         scpi_tx += "0"
    #     netcat_tx(self.host, self.port, scpi_tx, self.sentinel_string)

    @attribute(dtype=int, polling_period=1000)
    def attenuation(self) -> int:
        """
        Get value for Input Attenuation.

        :returns: attenuation in dBm
        """
        if self.admin_mode:
            return int(self.scpi_values["attenuation"])
        if self.attribute_poll("attenuation"):
            return self.scpi_values["attenuation"]
        scpi_tx = f"{self.scpi_fields['attenuation']}?"
        scpi_rx = netcat(self.host, self.port, scpi_tx, self.sentinel_string)
        if scpi_rx is None:
            self.scpi_values["attenuation"] = -999
        else:
            self.logger.debug("Read attenuation: %s", scpi_rx)
            self.scpi_values["attenuation"] = int(scpi_rx)
        return self.scpi_values["attenuation"]

    @attenuation.write  # type: ignore[no-redef]
    def attenuation(self, value: int) -> None:
        """
        Set value for Input Attenuation.

        :param value: new attenuation in dBm
        """
        if self.admin_mode:
            self.logger.info("Cannot write attenuation in admin mode")
            return
        if self.minimum_value_check("attenuation", value):
            return
        if self.maximum_value_check("attenuation", value):
            return
        self.logger.info("Write attenuation: %d", value)
        scpi_tx = f"{self.scpi_fields['attenuation']}{self.argument_separator}"
        scpi_tx += f"{value}"
        netcat_tx(self.host, self.port, scpi_tx, self.sentinel_string)
        self.scpi_values["attenuation"] = value

    @attribute(dtype=bool)
    def autoattenuation(self) -> bool:
        """
        Get flag to couple input attenuation to reference level.

        :returns: current value of flag
        """
        if self.admin_mode:
            return self.scpi_values["autoattenuation"]
        scpi_tx = f"{self.scpi_fields['autoattenuation']}?"
        scpi_rx = netcat(self.host, self.port, scpi_tx, self.sentinel_string)
        if scpi_rx is None:
            self.scpi_values["autoattenuation"] = None
            return None
        self.logger.debug("Read autoattenuation: %s", scpi_rx)
        if scpi_rx == "1":
            self.scpi_values["autoattenuation"] = True
            return True
        self.scpi_values["autoattenuation"] = False
        return False

    @autoattenuation.write  # type: ignore[no-redef]
    def autoattenuation(self, value: bool) -> None:
        """
        Set status of automatic attenuation.

        :param value: new status
        """
        if self.admin_mode:
            self.logger.info("Cannot write autoattenuation in admin mode")
            return
        self.logger.info("Write autoattenuation: %s", value)
        scpi_tx = f"{self.scpi_fields['autoattenuation']}{self.argument_separator}"
        if value:
            scpi_tx += "1"
        else:
            scpi_tx += "0"
        netcat_tx(self.host, self.port, scpi_tx, self.sentinel_string)
        self.scpi_values["autoattenuation"] = value

    @attribute()
    def clear_markers(self) -> None:
        """Set all markers to off."""
        if self.admin_mode:
            self.logger.info("Cannot write clear_markers in admin mode")
            return
        self.logger.info("Write clear_markers")
        scpi_tx = f"{self.scpi_fields['clear_markers']}"
        netcat_tx(self.host, self.port, scpi_tx, self.sentinel_string)

    # @attribute(dtype=bool, polling_period=1000)
    @attribute(dtype=bool)
    def continuous(self) -> bool:
        """
        Specify whether the sweep/measurement is triggered continuously.

        :returns: continuous sweep flag
        """
        if self.admin_mode:
            return self.scpi_values["continuous"]
        if self.attribute_poll("continuous"):
            return self.scpi_values["continuous"]
        scpi_tx = f"{self.scpi_fields['continuous']}?"
        scpi_rx = netcat(self.host, self.port, scpi_tx, self.sentinel_string)
        if scpi_rx is None:
            self.scpi_values["continuous"] = None
            return None
        self.logger.debug("Read continuous: %s", scpi_rx)
        if scpi_rx == "1":
            self.scpi_values["continuous"] = True
            return True
        self.scpi_values["continuous"] = False
        return False

    @continuous.write  # type: ignore[no-redef]
    def continuous(self, value: bool) -> None:
        """
        Specify whether the sweep/measurement is triggered continuously.

        :param value: new value
        """
        if self.admin_mode:
            self.logger.info("Cannot write continuous in admin mode")
            return
        self.logger.info("Write continuous: %s", value)
        scpi_tx = f"{self.scpi_fields['continuous']}{self.argument_separator}"
        if value:
            scpi_tx += "1"
        else:
            scpi_tx += "0"
        netcat_tx(self.host, self.port, scpi_tx, self.sentinel_string)
        self.scpi_values["continuous"] = value

    @attribute(dtype=bool, polling_period=1000)
    def flag_when_complete(self) -> bool:
        """
        Set OPC bit of Standard Event Status Register on next transition of No Op.

        :returns: pending flag
        """
        if self.admin_mode:
            return self.scpi_fields["flag_when_complete"]
        if self.attribute_poll("flag_when_complete"):
            return self.scpi_fields["flag_when_complete"]
        scpi_tx = f"{self.scpi_fields['flag_when_complete']}?"
        scpi_rx = netcat(self.host, self.port, scpi_tx, self.sentinel_string)
        if scpi_rx is None:
            self.scpi_values["flag_when_complete"] = None
            return None
        self.logger.debug("Read flag_when_complete: %s", scpi_rx)
        if scpi_rx == "1":
            self.scpi_values["flag_when_complete"] = True
            return True
        self.scpi_values["flag_when_complete"] = False
        return False

    @flag_when_complete.write  # type: ignore[no-redef]
    def flag_when_complete(self, value: bool) -> None:
        """
        Set OPC bit of Standard Event Status Register on next transition of No Op.

        :param value: new value
        """
        if self.admin_mode:
            self.logger.info("Cannot write flag_when_complete in admin mode")
            return
        self.logger.info("Write flag_when_complete: %s", value)
        scpi_tx = f"{self.scpi_fields['flag_when_complete']}{self.argument_separator}"
        if value:
            scpi_tx += "1"
        else:
            scpi_tx += "0"
        netcat_tx(self.host, self.port, scpi_tx, self.sentinel_string)
        self.scpi_values["flag_when_complete"] = value

    @attribute(dtype=int, polling_period=1000)
    def frequency_start(self) -> int:
        """
        Set Start Frequency.

        :returns: frequency in Hz
        """
        if self.admin_mode:
            return self.scpi_values["frequency_start"]
        if self.attribute_poll("frequency_start"):
            return self.scpi_values["frequency_start"]
        scpi_tx = f"{self.scpi_fields['frequency_start']}?"
        scpi_rx = netcat(self.host, self.port, scpi_tx, self.sentinel_string)
        if scpi_rx is None:
            self.scpi_values["frequency_start"] = 0
        else:
            self.logger.debug("Read frequency_start: %s", scpi_rx)
            self.scpi_values["frequency_start"] = int(scpi_rx)
        return self.scpi_values["frequency_start"]

    @frequency_start.write  # type: ignore[no-redef]
    def frequency_start(self, value: int) -> None:
        """
        Get Start Frequency.

        :param value: new value
        """
        if self.admin_mode:
            self.logger.info("Cannot write frequency_start in admin mode")
            return
        if self.minimum_value_check("frequency_start", value):
            return
        if self.maximum_value_check("frequency_start", value):
            return
        self.logger.info("Write frequency_start: %d", value)
        scpi_tx = f"{self.scpi_fields['frequency_start']}{self.argument_separator}"
        scpi_tx += f"{value}"
        netcat_tx(self.host, self.port, scpi_tx, self.sentinel_string)
        self.scpi_values["frequency_start"] = value

    @attribute(dtype=int, polling_period=1000)
    def frequency_stop(self) -> int:
        """
        Get Stop Frequency.

        :returns: frequency in Hz
        """
        if self.admin_mode:
            return self.scpi_values["frequency_stop"]
        if self.attribute_poll("frequency_stop"):
            return self.scpi_values["frequency_stop"]
        scpi_tx = f"{self.scpi_fields['frequency_stop']}?"
        scpi_rx = netcat(self.host, self.port, scpi_tx, self.sentinel_string)
        if scpi_rx is None:
            self.scpi_values["frequency_stop"] = 0
        else:
            self.logger.debug("Read frequency_stop: %s", scpi_rx)
            self.scpi_values["frequency_stop"] = int(scpi_rx)
        return self.scpi_values["frequency_stop"]

    @frequency_stop.write  # type: ignore[no-redef]
    def frequency_stop(self, value: int) -> None:
        """
        Set Stop Frequency.

        :param value: new value
        """
        if self.admin_mode:
            self.logger.info("Cannot write frequency_stop in admin mode")
            return
        if self.minimum_value_check("frequency_stop", value):
            return
        if self.maximum_value_check("frequency_stop", value):
            return
        self.logger.info("Write frequency_stop: %d", value)
        scpi_tx = f"{self.scpi_fields['frequency_stop']}{self.argument_separator}"
        scpi_tx += f"{value}"
        netcat_tx(self.host, self.port, scpi_tx, self.sentinel_string)
        self.scpi_values["frequency_stop"] = value

    # @attribute()
    # def get_trace(self) -> None:
    #     """Get the show on the road."""
    #     if self.admin_mode:
    #         self.logger.info("Cannot write get_trace in admin mode")
    #         return
    #     self.logger.info("Write get_trace")
    #     scpi_tx = f"{self.scpi_fields['get_trace']}"
    #     netcat_tx(self.host, self.port, scpi_tx, self.sentinel_string)

    @attribute(dtype=str)
    def identity(self) -> str:
        """
        Get manufacturer name, model number, serial number, and firmware package number.

        :returns: identity string
        """
        if self.admin_mode:
            return str(self.scpi_values["identity"])
        scpi_tx = f"{self.scpi_fields['identity']}?"
        scpi_rx = netcat(self.host, self.port, scpi_tx, self.sentinel_string)
        if scpi_rx is None:
            return "ERROR"
        self.logger.debug("Read identity: %s", scpi_rx)
        r_val = str(scpi_rx)
        self.scpi_values["identity"] = r_val
        return r_val

    @attribute(dtype=int)
    def marker_find_peak(self) -> None:
        """Move X value to point in marker's assigned trace that has highest peak."""
        if self.admin_mode:
            self.logger.info("Cannot write marker_find_peak in admin mode")
            return
        self.logger.info("Write marker_find_peak")
        scpi_tx = f"{self.scpi_fields['marker_find_peak']}"
        netcat_tx(self.host, self.port, scpi_tx, self.sentinel_string)

    @attribute(dtype=int)
    def marker_frequency(self) -> int:
        """
        Get Marker Position.

        :returns: marker frequency in Hz
        """
        if self.admin_mode:
            return self.scpi_values["marker_frequency"]
        if self.scpi_polls["marker_frequency"] < POLL_COUNT:
            return self.scpi_values["marker_frequency"]
        scpi_tx = f"{self.scpi_fields['marker_frequency']}?"
        scpi_rx = netcat(self.host, self.port, scpi_tx, self.sentinel_string)
        self.logger.debug("Read marker_frequency: %s", scpi_rx)
        if scpi_rx is None:
            self.scpi_values["marker_frequency"] = 0
        else:
            try:
                self.scpi_values["marker_frequency"] = int(scpi_rx)
            except ValueError:
                self.scpi_values["marker_frequency"] = 0
        return self.scpi_values["marker_frequency"]

    @marker_frequency.write  # type: ignore[no-redef]
    def marker_frequency(self, value: int) -> None:
        """
        Set marker frequency.

        :param value: marker frequency in Hz
        """
        if self.admin_mode:
            self.logger.info("Cannot write marker_frequency in admin mode")
            return
        if self.minimum_value_check("marker_frequency", value):
            return
        if self.maximum_value_check("marker_frequency", value):
            return
        self.logger.info("Write marker_frequency: %d", value)
        scpi_tx = f"{self.scpi_fields['marker_frequency']}{self.argument_separator}"
        scpi_tx += f"{value}"
        netcat_tx(self.host, self.port, scpi_tx, self.sentinel_string)
        self.scpi_values["marker_frequency"] = value

    @attribute(dtype=float)
    def marker_power(self) -> float:
        """
        Get Marker Value.

        :returns: amplitude value in dBm
        """
        if self.admin_mode:
            return self.scpi_values["marker_power"]
        scpi_tx = f"{self.scpi_fields['marker_power']}?"
        scpi_rx = netcat(self.host, self.port, scpi_tx, self.sentinel_string)
        self.logger.debug("Read marker_power: %s", scpi_rx)
        if scpi_rx is None:
            self.scpi_values["marker_power"] = -250.0
        else:
            self.scpi_values["marker_power"] = float(scpi_rx)
        return self.scpi_values["marker_power"]

    @marker_power.write  # type: ignore[no-redef]
    def marker_power(self, value: float) -> None:
        """
        Set amplitude value.

        :param value: new amplitude value in dBm
        """
        if self.admin_mode:
            self.logger.info("Cannot write marker_power in admin mode")
            return
        if self.minimum_value_check("marker_power", value):
            return
        if self.maximum_value_check("marker_power", value):
            return
        self.logger.info("Write marker_power: %d", value)
        scpi_tx = f"{self.scpi_fields['marker_power']}{self.argument_separator}"
        scpi_tx += f"{value}"
        netcat_tx(self.host, self.port, scpi_tx, self.sentinel_string)
        self.scpi_values["marker_power"] = value

    # @attribute(dtype=bool, polling_period=1000)
    @attribute(dtype=bool)
    def preamp_enabled(self) -> bool:
        """
        Get state of the preamp.

        :returns: state of the preamp
        """
        if self.admin_mode:
            return self.scpi_values["preamp_enabled"]
        if self.attribute_poll("preamp_enabled"):
            return self.scpi_values["preamp_enabled"]
        scpi_tx = f"{self.scpi_fields['preamp_enabled']}?"
        scpi_rx = netcat(self.host, self.port, scpi_tx, self.sentinel_string)
        if scpi_rx is None:
            # self.scpi_values["preamp_enabled"] = None
            return None
        self.logger.debug("Read preamp_enabled: %s", scpi_rx)
        if scpi_rx == "1":
            self.scpi_values["preamp_enabled"] = True
            return True
        self.scpi_values["preamp_enabled"] = False
        return False

    @preamp_enabled.write  # type: ignore[no-redef]
    def preamp_enabled(self, value: bool) -> None:
        """
        Set state of the preamp.

        :param value: new value
        """
        if self.admin_mode:
            self.logger.info("Cannot write preamp_enabled in admin mode")
            return
        self.logger.info("Write preamp_enabled: %s", value)
        scpi_tx = f"{self.scpi_fields['preamp_enabled']}{self.argument_separator}"
        if value:
            scpi_tx += "1"
        else:
            scpi_tx += "0"
        netcat_tx(self.host, self.port, scpi_tx, self.sentinel_string)
        self.scpi_values["preamp_enabled"] = value

    @attribute(dtype=float)
    def rbw(self) -> float:
        """
        Get resolution bandwidth.

        :returns: resolution bandwidth in Hz
        """
        if self.admin_mode:
            return self.scpi_values["rbw"]
        if self.attribute_poll("rbw"):
            return self.scpi_values["rbw"]
        scpi_tx = f"{self.scpi_fields['rbw']}?"
        scpi_rx = netcat(self.host, self.port, scpi_tx, self.sentinel_string)
        if scpi_rx is None:
            return 0
        self.logger.debug("Read rbw: %s", scpi_rx)
        r_val = float(scpi_rx)
        self.scpi_values["rbw"] = r_val
        return r_val

    @rbw.write  # type: ignore[no-redef]
    def rbw(self, value: float) -> None:
        """
        Set resolution bandwidth.

        :param value: resolution bandwidth in Hz
        """
        if self.admin_mode:
            self.logger.info("Cannot write rbw in admin mode")
            return
        if self.minimum_value_check("rbw", value):
            return
        if self.maximum_value_check("rbw", value):
            return
        self.logger.info("Write rbw: %f", value)
        scpi_tx = f"{self.scpi_fields['rbw']}{self.argument_separator}"
        scpi_tx += f"{value}"
        netcat_tx(self.host, self.port, scpi_tx, self.sentinel_string)
        self.scpi_values["rbw"] = value

    @attribute(dtype=bool)
    def rbw_auto(self) -> bool:
        """
        Get state of the coupling of the resolution bandwidth to the frequency span.

        :returns: flag value
        """
        if self.admin_mode:
            return self.scpi_values["rbw_auto"]
        if self.attribute_poll("rbw_auto"):
            return self.scpi_values["rbw_auto"]
        scpi_tx = f"{self.scpi_fields['rbw_auto']}?"
        scpi_rx = netcat(self.host, self.port, scpi_tx, self.sentinel_string)
        if scpi_rx is None:
            self.scpi_values["rbw_auto"] = None
            return None
        self.logger.debug("Read rbw_auto: %s", scpi_rx)
        if scpi_rx == "1":
            self.scpi_values["rbw_auto"] = True
            return True
        self.scpi_values["rbw_auto"] = False
        return False

    @rbw_auto.write  # type: ignore[no-redef]
    def rbw_auto(self, value: bool) -> None:
        """
        Set state of the coupling of the resolution bandwidth to the frequency span.

        :param value: new value
        """
        if self.admin_mode:
            self.logger.info("Cannot write rbw_auto in admin mode")
            return
        self.logger.info("Write rbw_auto: %s", value)
        scpi_tx = f"{self.scpi_fields['rbw_auto']}{self.argument_separator}"
        if value:
            scpi_tx += "1"
        else:
            scpi_tx += "0"
        netcat_tx(self.host, self.port, scpi_tx, self.sentinel_string)
        self.scpi_values["rbw_auto"] = value

    # @attribute(dtype=int, polling_period=1000)
    @attribute(dtype=int)
    def reference_level(self) -> int:
        """
        Get Reference Level.

        :returns: Reference Level in dBm
        """
        if self.admin_mode:
            return self.scpi_values["reference_level"]
        if self.attribute_poll("reference_level"):
            return self.scpi_values["reference_level"]
        scpi_tx = f"{self.scpi_fields['reference_level']}?"
        scpi_rx = netcat(self.host, self.port, scpi_tx, self.sentinel_string)
        if scpi_rx is None:
            return -99
        self.logger.debug("Read reference_level: %s", scpi_rx)
        r_val = int(scpi_rx)
        self.scpi_values["reference_level"] = r_val
        return r_val

    @reference_level.write  # type: ignore[no-redef]
    def reference_level(self, value: int) -> None:
        """
        Set Reference Level.

        :param value: new value
        """
        if self.admin_mode:
            self.logger.info("Cannot write reference_level in admin mode")
            return
        if self.minimum_value_check("reference_level", value):
            return
        if self.maximum_value_check("reference_level", value):
            return
        self.logger.info("Write reference_level: %d", value)
        scpi_tx = f"{self.scpi_fields['reference_level']}{self.argument_separator}"
        scpi_tx += f"{value}"
        netcat_tx(self.host, self.port, scpi_tx, self.sentinel_string)
        self.scpi_values["reference_level"] = value

    def reset(self) -> None:
        """Press the reset button."""
        if self.admin_mode:
            self.logger.warning("Cannot do reset in admin mode")
            return
        self.logger.info("Reset")
        scpi_tx = f"{self.scpi_fields['reset']}"
        netcat_tx(self.host, self.port, scpi_tx, self.sentinel_string)

    # @attribute(dtype=int, polling_period=1000)
    @attribute(dtype=int)
    def sweep_points(self) -> int:
        """
        Get number of display points that instrument currently measures.

        :returns: number of display points
        """
        if self.admin_mode:
            return self.scpi_values["sweep_points"]
        if self.attribute_poll("sweep_points"):
            return self.scpi_values["sweep_points"]
        scpi_tx = f"{self.scpi_fields['sweep_points']}?"
        scpi_rx = netcat(self.host, self.port, scpi_tx, self.sentinel_string)
        if scpi_rx is None:
            self.scpi_values["sweep_points"] = 0
            return 0
        self.logger.debug("Read sweep_points: %s", scpi_rx)
        r_val = int(scpi_rx)
        self.scpi_values["sweep_points"] = r_val
        return r_val

    @sweep_points.write  # type: ignore[no-redef]
    def sweep_points(self, value: int) -> None:
        """
        Set number of display points.

        :param value: new value for number of display points
        """
        if self.admin_mode:
            self.logger.info("Cannot write sweep_points in admin mode")
            return
        if self.minimum_value_check("sweep_points", value):
            return
        if self.maximum_value_check("sweep_points", value):
            return
        self.logger.info("Write sweep_points: %d", value)
        scpi_tx = f"{self.scpi_fields['sweep_points']}{self.argument_separator}"
        scpi_tx += f"{value}"
        netcat_tx(self.host, self.port, scpi_tx, self.sentinel_string)
        self.scpi_values["sweep_points"] = value

    @attribute(
        dtype=(float,),
        max_dim_x=4001,
    )
    def trace1_bin(self) -> list[float]:
        """
        Get trace data.

        :returns: list of float values
        """
        trace_vals: list[float] = []
        if self.admin_mode:
            return trace_vals
        scpi_tx = f"{self.scpi_fields['trace1']}"
        scpi_rx = netcat(self.host, self.port, scpi_tx, self.sentinel_string)
        if scpi_rx is None:
            return trace_vals
        try:
            n_chk = int(scpi_rx[1])
            n_len = int(scpi_rx[2 : 2 + n_chk])
        except IndexError:
            return trace_vals
        if len(scpi_rx) != 2 + n_chk + n_len:
            self.logger.warning(
                "Invalid trace data %d bytes : %s %d %d : %s",
                len(scpi_rx),
                scpi_rx[0],
                n_chk,
                n_len,
                scpi_rx[2 + n_chk :],
            )
            return trace_vals
        self.logger.debug(
            "Trace data %d bytes : %s %d %d : %s",
            len(scpi_rx),
            scpi_rx[0],
            n_chk,
            n_len,
            scpi_rx[2 + n_chk :],
        )
        for str_val in scpi_rx[2 + n_chk :].split(","):
            try:
                trace_vals.append(float(str_val))
            except ValueError:
                self.logger.error("Could not process value '%s'", str_val)
        return trace_vals

    @attribute(dtype=str, polling_period=3000)
    def trace1(self) -> str:
        """
        Get trace data.

        Trace data uses SCPI standard (IEEE 488.2) block data format. The data format
        is '#AXD', where D is a comma separated list of amplitudes (in ASCII), X is one
        or more ASCII digits specifying the number of bytes in D, and A is a single
        ASCII digit specifying the number of digits in X.

        :returns: list of float values
        """
        if self.admin_mode:
            return "#10"
        scpi_tx = f"{self.scpi_fields['trace1']}"
        scpi_rx = netcat(self.host, self.port, scpi_tx, self.sentinel_string)
        if scpi_rx is None:
            return "#10"
        try:
            n_chk = int(scpi_rx[1])
            n_len = int(scpi_rx[2 : 2 + n_chk])
        except IndexError:
            return f"ERROR: {scpi_rx}"
        if len(scpi_rx) == 2 + n_chk + n_len:
            self.logger.debug(
                "Trace data %d bytes : %s %d %d : %s",
                len(scpi_rx),
                scpi_rx[0],
                n_chk,
                n_len,
                scpi_rx[2 + n_chk :],
            )
        else:
            self.logger.warning(
                "Invalid trace data %d bytes : %s %d %d : %s",
                len(scpi_rx),
                scpi_rx[0],
                n_chk,
                n_len,
                scpi_rx[2 + n_chk :],
            )
        return scpi_rx

    # @attribute(dtype=str, polling_period=1000)
    @attribute(dtype=str)
    def trace_format(self) -> str:
        """
        Get trace format.

        :returns: trace format, i.e. string or float
        """
        if self.admin_mode:
            return self.scpi_values["trace_format"]
        if self.attribute_poll("trace_format"):
            return self.scpi_values["trace_format"]
        scpi_tx = f"{self.scpi_fields['trace_format']}?"
        scpi_rx = netcat(self.host, self.port, scpi_tx, self.sentinel_string)
        if scpi_rx is None:
            return "ERROR"
        self.logger.debug("Read trace_format: %s", scpi_rx)
        r_val = str(scpi_rx)
        self.scpi_values["trace_format"] = r_val
        return r_val

    @trace_format.write  # type: ignore[no-redef]
    def trace_format(self, value: str) -> None:
        """
        Set format in which data is returned.

        Valid values are <ASCii|INTeger|REAL>,[<32|64>]

        :param value: new value
        """
        if self.admin_mode:
            self.logger.info("Cannot write trace_format in admin mode")
            return
        self.logger.info("Write trace_format: %s", value)
        scpi_tx = f"{self.scpi_fields['trace_format']}{self.argument_separator}"
        scpi_tx += f"{value}"
        netcat_tx(self.host, self.port, scpi_tx, self.sentinel_string)
        self.scpi_values["trace_format"] = value

    # @attribute(dtype=float, polling_period=1000)
    @attribute(dtype=float)
    def vbw(self) -> tango.DevFloat:
        """
        Get video bandwidth.

        :returns: video bandwidth in Hz
        """
        if self.admin_mode:
            return self.scpi_values["vbw"]
        if self.attribute_poll("vbw"):
            return self.scpi_values["vbw"]
        scpi_tx = f"{self.scpi_fields['vbw']}?"
        scpi_rx = netcat(self.host, self.port, scpi_tx, self.sentinel_string)
        if scpi_rx is None:
            # self.scpi_values["vbw"] = 0.0
            return 0.0
        self.logger.debug("Read vbw %s : %s", scpi_tx, scpi_rx)
        r_val = float(scpi_rx)
        self.scpi_values["vbw"] = r_val
        return r_val

    @vbw.write  # type: ignore[no-redef]
    def vbw(self, value: float) -> None:
        """
        Set video bandwidth.

        :param value: new value for video bandwidth in Hz
        """
        if self.admin_mode:
            self.logger.info("Cannot write vbw in admin mode")
            return
        if self.minimum_value_check("vbw", value):
            return
        if self.maximum_value_check("vbw", value):
            return
        scpi_tx = f"{self.scpi_fields['vbw']}{self.argument_separator}"
        scpi_tx += f"{value}"
        self.logger.info("Write vbw: %s", scpi_tx)
        netcat_tx(self.host, self.port, scpi_tx, self.sentinel_string)
        self.scpi_values["vbw"] = value

    # @attribute(dtype=bool, polling_period=1000)
    @attribute(dtype=bool)
    def vbw_auto(self) -> bool:
        """
        Get state of coupling of video bandwidth to resolution bandwidth.

        :returns: flag value
        """
        if self.admin_mode:
            return self.scpi_values["vbw_auto"]
        if self.attribute_poll("vbw_auto"):
            return self.scpi_values["vbw_auto"]
        scpi_tx = f"{self.scpi_fields['vbw_auto']}?"
        scpi_rx = netcat(self.host, self.port, scpi_tx, self.sentinel_string)
        if scpi_rx is None:
            self.scpi_values["vbw_auto"] = None
            return None
        self.logger.debug("Read vbw_auto: %s", scpi_rx)
        if scpi_rx == "1":
            self.scpi_values["vbw_auto"] = True
            return True
        self.scpi_values["vbw_auto"] = False
        return False

    @vbw_auto.write  # type: ignore[no-redef]
    def vbw_auto(self, value: bool) -> None:
        """
        Set state of coupling of video bandwidth to resolution bandwidth.

        :param value: new value
        """
        if self.admin_mode:
            self.logger.info("Cannot write vbw_auto in admin mode")
            return
        self.logger.info("Write vbw_auto: %s", value)
        scpi_tx = f"{self.scpi_fields['vbw_auto']}{self.argument_separator}"
        if value:
            scpi_tx += "1"
        else:
            scpi_tx += "0"
        netcat_tx(self.host, self.port, scpi_tx, self.sentinel_string)
        self.scpi_values["vbw_auto"] = value

    @attribute(dtype=bool, polling_period=3000)
    def operation_complete(self) -> bool | None:
        """
        Get operation complete.

        :returns: current flag value
        """
        if self.admin_mode:
            return None
        scpi_tx = "*ESR?"
        scpi_rx = netcat(self.host, self.port, scpi_tx, self.sentinel_string)
        if scpi_rx is None:
            return None
        self.event_status_register = int(scpi_rx)
        self.logger.debug("Read Event Status Register: %s", self.event_status_register)
        # pylint: disable-next=simplifiable-if-statement
        if self.event_status_register & 0x01:
            rval = True
        else:
            rval = False
        # TODO pylint suggest this but mypy will have none of it ;-)
        # rval = self.event_status_register & 0x01
        self.logger.debug("Read operation_complete: %s", rval)
        return rval

    @attribute(dtype=bool, polling_period=8000)
    def query_error(self) -> bool | None:
        """
        Get query error.

        :returns: current flag value
        """
        if self.admin_mode:
            return None
        if self.event_status_register & 0x04:
            rval = True
            self.logger.error(
                "Detected query_error in event_status_register (%d)",
                self.event_status_register,
            )
        else:
            rval = False
            self.logger.debug("Read query_error: %s", rval)
        return rval

    @attribute(dtype=bool, polling_period=8000)
    def device_error(self) -> bool | None:
        """
        Get device error.

        :returns: current flag value
        """
        if self.admin_mode:
            return None
        if self.event_status_register & 0x08:
            rval = True
            self.logger.error(
                "Detected device_error in event_status_register (%d)",
                self.event_status_register,
            )
        else:
            rval = False
            self.logger.debug("Read device_error: %s", rval)
        return rval

    @attribute(dtype=bool, polling_period=8000)
    def execution_error(self) -> bool | None:
        """
        Get execution error.

        :returns: current flag value
        """
        if self.admin_mode:
            return None
        if self.event_status_register & 0x10:
            rval = True
            self.logger.error(
                "Detected execution_error in event_status_register (%d)",
                self.event_status_register,
            )
        else:
            rval = False
            self.logger.debug("Read execution_error: %s", rval)
        return rval

    @attribute(dtype=bool, polling_period=8000)
    def command_error(self) -> bool | None:
        """
        Get command error.

        :returns: current flag value
        """
        if self.admin_mode:
            return None
        if self.event_status_register & 0x20:
            rval = True
            self.logger.error(
                "Detected command_error in event_status_register (%d)",
                self.event_status_register,
            )
        else:
            rval = False
            self.logger.debug("Read command_error: %s", rval)
        return rval

    @attribute(dtype=bool, polling_period=8000)
    def power_cycled(self) -> bool | None:
        """
        Get power cycled.

        :returns: current flag value
        """
        if self.admin_mode:
            return None
        if self.event_status_register & 0x80:
            rval = True
            self.logger.error(
                "Detected power_cycled in event_status_register (%d)",
                self.event_status_register,
            )
        else:
            rval = False
            self.logger.debug("Read power_cycled: %s", rval)
        return rval


def run_device(kwargs: list[str]) -> None:
    """
    Get the main peanut.

    :param kwargs: list of arguments
    """
    _module_logger.info("Run : %s", repr(kwargs))
    SpectrumAnalyserDeviceAnritsu.run_server(args=kwargs)


def main(args: list[str] | None = None, **kwargs: list[str]) -> None:
    """
    Get the main peanut.

    :param args: command line arguments
    :param kwargs: keyword arguments
    """
    # SpectrumAnalyserDeviceAnritsu.run_server(green_mode=GreenMode.Asyncio)
    log_level_str = os.getenv("LOG_LEVEL", "WARNING").upper()
    _module_logger.setLevel(log_level_str)
    elog_lvl = _module_logger.getEffectiveLevel()
    _module_logger.info(
        "Start : args %s : kwargs %s (log level %s %d)",
        repr(args),
        repr(kwargs),
        log_level_str,
        elog_lvl,
    )
    tango.server.run((SpectrumAnalyserDeviceAnritsu,), **kwargs)


if __name__ == "__main__":
    _module_logger.setLevel(logging.DEBUG)
    main()
