#! /usr/bin/env python
"""
Generate trace data based on settings read from signal generator.

Needs an active Tango device.
"""
import decimal
import logging
import random
import sys
import time
from typing import Optional, Tuple

from tango import DeviceProxy

TRACE1_DATA_FILE = "/tmp/spectana_data.txt"

LOG_LEVEL = logging.WARNING
logging.basicConfig(level=LOG_LEVEL)
_module_logger = logging.getLogger(__name__)

SIGGEN_DEV = "tango://127.0.0.1:45451/mid-itf/siggen/1#dbase=no"
SPECTANA_DEV = "tango://127.0.0.1:45450/mid-itf/spectana/1#dbase=no"


def eng_number(val: int | float, unit: Optional[str] = "") -> str:
    """
    Convert number to engineering format.

    :param val: input value
    :param unit: unit added to end of string

    :returns: formatted string
    """
    f_str = decimal.Decimal(f"{val:.4E}").normalize().to_eng_string()
    f_str = f_str.replace("E-12", "p")
    f_str = f_str.replace("E-9", "n")
    f_str = f_str.replace("E-6", "u")
    f_str = f_str.replace("E-3", "m")
    f_str = f_str.replace("E+3", "k")
    f_str = f_str.replace("E+6", "M")
    f_str = f_str.replace("E+9", "G")
    f_str = f_str.replace("E+12", "T")
    if unit:
        f_str += unit  # type: ignore[operator]
    return f_str


def read_siggen(dp_str: str) -> Tuple[float, int]:
    """
    Read data from signal generator.

    :param dp_str: Tango device proxy connect string
    :returns: power and frequency
    """
    _module_logger.debug("Connect to %s", dp_str)
    siggen = DeviceProxy(dp_str)

    # Query admin mode
    if siggen.adminmode:
        siggen.adminmode = 0
        time.sleep(5)

    siggen_power_dbm: float = siggen.power_dbm
    siggen_frequency: int = int(siggen.frequency)

    _module_logger.debug("power_dbm : %f", siggen_power_dbm)
    _module_logger.debug("frequency : %s", eng_number(siggen_frequency, "Hz"))

    return siggen_power_dbm, siggen_frequency


def read_spectana(dp_str: str) -> Tuple[float, int, int, int]:
    """
    Read data from spectrum analyser.

    :param dp_str: Tango device proxy connect string
    :returns: power, stop frequency, start frequency and sweep points
    """
    _module_logger.debug("Connect to %s", dp_str)
    spectana = DeviceProxy(dp_str)
    spectana.set_timeout_millis(5000)
    if spectana.adminmode:
        spectana.adminmode = 0
        time.sleep(5)

    spectana_att: float = spectana.attenuation
    spectana_freq_start: int = spectana.frequency_start
    spectana_freq_stop: int = spectana.frequency_stop
    sweep_points: int = spectana.sweep_points

    _module_logger.debug("attenuation     : %f dBm", spectana_att)
    _module_logger.debug("frequency_start : %s", eng_number(spectana_freq_start, "Hz"))
    _module_logger.debug("frequency_stop  : %s", eng_number(spectana_freq_stop, "Hz"))
    _module_logger.debug("sweep_points    : %d", sweep_points)

    return spectana_att, spectana_freq_start, spectana_freq_stop, sweep_points


# pylint: disable-next=too-many-locals
def make_trace(
    sig_ampl: float,
    sig_freq: int,
    sweep_points: int,
    freq_start: int,
    freq_stop: int,
) -> str:
    """
    Generate trace data.

    :param sig_ampl: signal amplitude
    :param sig_freq: signal frequency
    :param sweep_points: number of data points
    :param freq_start: start frequency
    :param freq_stop: stop frequency
    :returns: trace data string
    """
    x_points = []
    y_points = []
    sig_freqs = []
    sig_freqs.append(sig_freq)
    sig_freqs.append(sig_freq * 2)
    sig_freqs.append(sig_freq * 3)
    _module_logger.debug("Signal frequencies %s", sig_freqs)
    sig_ampls = []
    sig_ampls.append(sig_ampl)
    sig_ampls.append(sig_ampl - 45)
    if sig_ampls[1] < -85.0:
        sig_ampls[1] = -85.0
    sig_ampls.append(sig_ampl - 60)
    if sig_ampls[2] < -85.0:
        sig_ampls[2] = -85.0
    _module_logger.debug("Signal amplitudes %s", sig_ampls)
    n_mark: int = 0
    n_data_len: int = sweep_points - 1
    f_step = (freq_stop - freq_start) / n_data_len
    _module_logger.debug(
        "Frequency from %s to %s, step %f Hz",
        eng_number(freq_start, "Hz"),
        eng_number(freq_stop, "Hz"),
        f_step,
    )
    n_data_len += 1
    n_freq: float = float(freq_start)
    ampl: float = float(random.randint(-850, -800) / 10)
    _module_logger.debug("%9s = %.2fdBm", eng_number(n_freq, "Hz"), ampl)
    n_data: int = 1
    n_freq += f_step
    while n_data < n_data_len:
        if n_mark < 3:
            if n_freq >= sig_freqs[n_mark]:
                ampl = sig_ampls[n_mark]
                _module_logger.debug("Set frequency %f amplitude %f", n_freq, ampl)
                n_mark += 1
            else:
                ampl = float(random.randint(-850, -800) / 10)
            # sig_freq = sys.float_info.max
        else:
            ampl = float(random.randint(-850, -800) / 10)
        _module_logger.debug("%9s = %.2fdBm", eng_number(n_freq, "Hz"), ampl)
        x_points.append(float(n_freq))
        y_points.append(ampl)
        n_freq += f_step
        n_data += 1
    ret_str: str = f"#4xxxx{y_points[0]}"
    for y_point in y_points[1:]:
        ret_str += f",{y_point}"
    # pylint: disable-next=consider-using-f-string
    ret_len = "%04d" % (len(ret_str) - 6)
    return ret_str.replace("xxxx", ret_len)


def write_trace_file(out_file: str | None, siggen_dev: str, spectana_dev: str) -> None:
    """
    Write trace data.

    :param out_file: file name
    :param siggen_dev: Tango connect string for signal generator
    :param spectana_dev: Tango connect string for spectrum analyser
    """
    siggen_dbm, siggen_freq = read_siggen(siggen_dev)
    _spectana_att, spectana_start, spectana_stop, sweep_points = read_spectana(
        spectana_dev,
    )
    trace1 = make_trace(
        siggen_dbm,
        siggen_freq,
        sweep_points,
        spectana_start,
        spectana_stop,
    )
    if out_file:
        _module_logger.debug("Write file %s", out_file)
        # pylint: disable-next=unspecified-encoding
        with open(out_file, "w") as trc_file:
            trc_file.write(trace1)
    else:
        print(f"{trace1}")


def main() -> None:
    """Read output file name and write trace data to file or stdout."""
    try:
        out_file = sys.argv[1]
        if out_file == ".":
            out_file = TRACE1_DATA_FILE
    except IndexError:
        out_file = None
    write_trace_file(out_file, SIGGEN_DEV, SPECTANA_DEV)


if __name__ == "__main__":
    _module_logger.setLevel(logging.DEBUG)
    main()
