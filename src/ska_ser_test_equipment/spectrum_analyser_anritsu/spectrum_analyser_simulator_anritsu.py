#!/usr/bin/env python
"""Implementation of a simple SCPI simulator for the spectrum analyser."""
import logging
import os
import socket
import threading
import time

# pylint: disable-next=unused-import,useless-suppression
from typing import Dict, Final, Optional, cast  # noqa: E501,F401

# import numpy as np
from ska_ser_devices.client_server import TcpServer
from ska_ser_scpi import ScpiSimulator, SupportedAttributeType

from ska_ser_test_equipment.interface_definitions import InterfaceDefinitionFactory

# pylint: disable-next=line-too-long
from ska_ser_test_equipment.spectrum_analyser_anritsu.spectrum_analyser_data_anritsu import (  # noqa: E501
    FREQUENCY_START_MS2090A,
    FREQUENCY_STOP_MS2090A,
    IDENTITY_MS2090A,
    MARKER_FREQUENCY_MS2090A,
    MARKER_POWER_MS2090A,
    SUPPORTED_MODELS,
    SWEEP_POINTS_MS2090A,
    TRACE_DATA_1_MS2090A,
)

# pylint: disable-next=line-too-long
from ska_ser_test_equipment.spectrum_analyser_anritsu.spectrum_analyser_trace_anritsu import (  # noqa: E501
    TRACE1_DATA_FILE,
)

logging.basicConfig(level=logging.DEBUG)
_module_logger = logging.getLogger(__name__)


class SpectrumAnalyserSimulatorAnritsu(ScpiSimulator):
    """A spectrum analyser simulator TCP server."""

    DEFAULTS: Final[Dict[str, SupportedAttributeType]] = {
        "add_marker": False,
        "attenuation": 10,
        "autoattenuation": True,
        "command_error": False,
        "continuous": True,
        "device_error": False,
        "execution_error": False,
        "flag_when_complete": True,
        "frequency_start": FREQUENCY_START_MS2090A,
        "frequency_stop": FREQUENCY_STOP_MS2090A,
        "identity": IDENTITY_MS2090A,
        "marker_frequency": MARKER_FREQUENCY_MS2090A,
        "marker_power": MARKER_POWER_MS2090A,
        "operation_complete": True,
        "power_cycled": False,
        "preamp_enabled": False,
        "query_error": False,
        "rbw": 200000,
        "rbw_auto": True,
        "reference_level": 10,
        "sweep_points": SWEEP_POINTS_MS2090A,
        "trace1": TRACE_DATA_1_MS2090A,
        "trace_format": "ASC,8",
        "vbw": 200000.0,
        "vbw_auto": True,
    }

    def __init__(
        self,
        model: str,
        **kwargs: SupportedAttributeType,
    ) -> None:
        """
        Initialise a new spectrum analyser simulator.

        :param model: the model identifier to use.
        :param kwargs: initial values for simulator attributes; where an
            initial value is not provided for an attribute, a default
            value will be used.
        """
        self._lock = threading.Lock()

        self._num_markers = 0

        self._tracing = False
        self._flag_when_complete = False
        self._command_complete = False

        interface_definition = InterfaceDefinitionFactory()(model)
        self.model = model
        _module_logger.info("Interface definition %s", interface_definition)

        # TODO for future use
        data_fn = os.getenv("SIMULATOR_DATA")
        if data_fn is not None:
            _module_logger.info("Read data from %s", data_fn)
            with open(data_fn, encoding="utf-8") as d_fh:
                trace_data = d_fh.read()
            self.DEFAULTS["trace1"] = trace_data

        self._flag_when_complete = True

        initial_values = kwargs
        for key, value in self.DEFAULTS.items():
            initial_values.setdefault(key, value)

        super().__init__(interface_definition, initial_values)
        self.set_attribute("marker_frequency", MARKER_FREQUENCY_MS2090A)
        self.set_attribute("marker_power", float(MARKER_POWER_MS2090A))
        self._num_markers = 1

    def clear_markers(self) -> None:
        """Clear all markers."""
        self._num_markers = 0

    def add_marker(self, mk_flag: bool | None = None) -> None:
        """
        Add a marker.

        :param mk_flag: marker flag
        """
        _module_logger.info("Add marker (%s)", mk_flag)
        self._num_markers = 1

    def trace1(self) -> str:
        """
        Read trace data.

        :returns: trace data buffer
        """
        if os.path.isfile(TRACE1_DATA_FILE):
            _module_logger.debug("Get trace1 from %s", TRACE1_DATA_FILE)
            # pylint: disable-next=unspecified-encoding
            with open(TRACE1_DATA_FILE, "r") as trace1_file:
                trace1_data = trace1_file.read()
        else:
            _module_logger.debug("Get trace1 default")
            trace1_data = str(self.DEFAULTS["trace1"])
        return trace1_data

    def get_trace(self) -> None:
        """Get a trace."""
        # TODO: simulator currently doesn't bother generating a trace.
        # It only spends half a second pretending to do so.
        _module_logger.info("Get trace for %s", self.model)
        self._tracing = True

        def _simulate_completion() -> None:
            self.set_attribute("trace1", self.DEFAULTS["trace1"])
            self._tracing = False

        threading.Timer(0.5, _simulate_completion).start()

    def marker_find_peak(self) -> None:
        """Set marker_frequency and marker_power based on the current trace's peak."""
        if self._num_markers >= 1:
            _module_logger.info("Find peak amplitude and frequency")
            trace_data = str(self.get_attribute("trace1"))
            freq_a = cast(float, self.get_attribute("frequency_start"))
            freq_b = cast(float, self.get_attribute("frequency_stop"))

            if trace_data[0] != "#":
                _module_logger.error(
                    "Trace data should start with '#' not '%s'", trace_data[0]
                )
                return
            n_len = int(trace_data[1])
            trace_data_len = int(trace_data[2 : 2 + n_len])
            _module_logger.debug(
                "Read %d = 1+%d+%d bytes", len(trace_data), n_len, trace_data_len
            )
            trace = []
            for ampl in trace_data[2 + n_len :].split(","):
                trace.append(float(ampl))

            peak_power, peak_sample = max((x, i) for i, x in enumerate(trace))
            sample_width = (freq_b - freq_a) / (len(trace) - 1)
            peak_freq = freq_a + sample_width * peak_sample

            self.set_attribute("marker_frequency", peak_freq)
            self.set_attribute("marker_power", float(peak_power))
        else:
            _module_logger.warning("No markers available")

    def marker_frequency(self, mark_freq: float | None = None) -> Optional[float]:
        """
        Return the frequency of the peak.

        This method overrides the default behaviour (simply reading the
        attribute value) to ensure that the number of markers is checked
        first.

        :param mark_freq: marker frequency in Hz
        :returns: the frequency of the peak, or None if the marker has
            not been added.
        """
        _module_logger.debug("Marker frequency %s", str(mark_freq))
        if mark_freq is not None:
            self.set_attribute("marker_frequency", mark_freq)
            self._num_markers += 1
        if self._num_markers < 1:
            # TODO: The real device also puts an error message in the system
            # error buffer.
            return 0
        return cast(float, self.get_attribute("marker_frequency"))

    def marker_power(self, mark_pwr: float | None = None) -> Optional[float]:
        """
        Return the power at the peak.

        This method overrides the default behaviour (simply reading the
        attribute value) to ensure that the number of markers is checked
        first.

        :param mark_pwr: marker power in dBm
        :returns: the power at the peak, or None if the marker has not
            been added.
        """
        _module_logger.debug("Marker power %s", str(mark_pwr))
        if mark_pwr is not None:
            self.set_attribute("marker_power", mark_pwr)
            self._num_markers += 1
        if self._num_markers < 1:
            # TODO: The real device also puts an error message in the system
            # error buffer.
            return -250.0
        return cast(float, self.get_attribute("marker_power"))

    def reset(self) -> None:
        """Reset to factory default values."""
        self._num_markers = 0
        self.set_attribute("autoattenuation", True)
        self.set_attribute("attenuation", 0.0)
        self.set_attribute("continuous", True)
        self.set_attribute("frequency_start", 1.4175e9)  # 1.4175 GHz
        self.set_attribute("frequency_stop", 1.5825e9)  # 1.5825 GHz

    def flag_when_complete(self, c_flag: bool | None = None) -> bool:
        """
        Raise a flag when command is complete.

        :param c_flag: value to set
        :returns: current value
        """
        _module_logger.debug("Flag completed (%s)", str(c_flag))
        if c_flag is not None:
            self._flag_when_complete = c_flag
        return self._flag_when_complete

    # pylint: disable-next=no-self-use
    def operation_complete(self) -> bool:
        """
        Return whether all previous operations are complete.

        This only returns true if :py:meth:`flag_when_complete` was
        called while the operation was running, and the operation is now
        complete.

        :returns: whether all previous operations are complete.
        """
        # TODO get this working
        # complete = self._flag_when_complete and not self._tracing
        # if complete:
        #     self._flag_when_complete = False
        # _module_logger.info("Operation complete %s", complete)
        # return complete
        complete = True
        _module_logger.debug("Operation complete %s", str(complete))
        return complete


def main() -> None:
    """Run the socketserver main loop."""
    if _module_logger.isEnabledFor(logging.DEBUG):
        _module_logger.warning("Log level is DEBUG")
    elif _module_logger.isEnabledFor(logging.INFO):
        _module_logger.warning("Log level is INFO")
    elif _module_logger.isEnabledFor(logging.WARNING):
        _module_logger.warning("Log level is WARNING")
    else:
        pass
    model = os.getenv("SIMULATOR_MODEL", "MS2090A").upper()
    if model not in SUPPORTED_MODELS:
        _module_logger.error(
            "Model %s should be one of: %s", model, " ".join(SUPPORTED_MODELS)
        )
        return
    host = os.getenv("SIMULATOR_HOST", socket.gethostname())
    port = int(os.getenv("SIMULATOR_PORT", "9100"))
    _module_logger.info("Start simulator for %s on %s:%d", model, host, port)

    simulator = SpectrumAnalyserSimulatorAnritsu(model)
    n_try = 0
    n_max = 5
    server = None
    while n_try <= n_max:
        n_try += 1
        try:
            server = TcpServer(host, port, simulator)
        except OSError as os_err:
            _module_logger.error(str(os_err))
            _module_logger.warning("Retry %d of %d", n_try, n_max)
            time.sleep(15)
            continue
        break
    if server is not None:
        _module_logger.info("Start simulator for instrument on %s:%d", host, port)
        with server:
            server.serve_forever()
    _module_logger.warning("Exit")
    return


if __name__ == "__main__":
    print("*** Simulator for Anritsu MS2090A spectrum analyser ***")
    try:
        main()
    except KeyboardInterrupt:
        _module_logger.error("Interrupted")
