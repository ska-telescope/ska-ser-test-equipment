"""Test harness for Tektronix AWG tests."""
import logging
import os
import subprocess
from typing import Iterator

import pytest
import tango
import tango.test_context

from ska_ser_test_equipment.scpi.scpi_config import find_files

LOG_LEVEL = logging.DEBUG
logging.basicConfig(level=LOG_LEVEL)
caplog = logging.getLogger(__name__)

SIM_PROC = None
DEV_PROC = None
SIMULATOR_HOST = "127.0.0.1"
SIMULATOR_PORT = "4000"
SIMULATOR_MODEL = "AWG5208I"
TANGO_HOST = "tango-databaseds:10000"
TANGO_PORT = "45450"
SIM_PY = "/app/src/ska_ser_test_equipment/awg_visa/awg_simulator_visa.py"
DEV_PY = "/app/src/ska_ser_test_equipment/awg_visa/awg_device_visa.py"


@pytest.fixture()
def awg_visa_device() -> Iterator[tango.DeviceProxy]:
    """
    Get a proxy to the AWG Tango device.

    :yields: a proxy to a AWG device
    """
    try:
        if DEV_PROC is not None:
            # Connect to local Tango device without database
            os.environ["TANGO_HOST"] = ""
            dp_conn = f"tango://{SIMULATOR_HOST}:{TANGO_PORT}/mid-itf/awg/1#dbase=no"
            caplog.info("Get Tango device proxy %s", dp_conn)
            dev = tango.DeviceProxy(dp_conn)
        else:
            dev = None
    # pylint: disable-next=broad-except
    except Exception as t_err:
        caplog.warning("Could not connect: %s", repr(t_err))
        # Connect to Tango device using database (for future use)
        os.environ["TANGO_HOST"] = TANGO_HOST
        dp_conn = "mid-itf/awg/1"
        caplog.info("Get Tango device proxy %s", dp_conn)
        dev = tango.DeviceProxy(dp_conn)
    yield dev


@pytest.fixture()
def awg_visa_simulator_proc() -> subprocess.Popen[bytes] | None:
    """
    Get process for running AWG simulator.

    :returns: process for AWG simulator.
    """
    # pylint: disable-next=global-statement
    global SIM_PROC, SIM_PY
    if SIM_PROC is None:
        if not os.path.exists(SIM_PY):
            caplog.warning("File %s does not exist", SIM_PY)
            sim_files = find_files(".", os.path.basename(SIM_PY))
            if sim_files:
                SIM_PY = sim_files[0]
                caplog.info("Found %s", SIM_PY)
            else:
                caplog.error("Could not find %s in '.'", SIM_PY)
                return None
        caplog.info("Start AWG simulator")
        sim_env = {
            "PYTHONPATH": str(os.getenv("PYTHONPATH")),
            "SIMULATOR_HOST": f"{SIMULATOR_HOST}",
            "SIMULATOR_PORT": f"{SIMULATOR_PORT}",
            "SIMULATOR_MODEL": f"{SIMULATOR_MODEL}",
        }
        caplog.info("Simulator environment %s", sim_env)
        sim_cmd = [SIM_PY]
        caplog.info("Simulator command: %s", " ".join(sim_cmd))
        # pylint: disable-next=consider-using-with
        SIM_PROC = subprocess.Popen(args=sim_cmd, env=sim_env)
        caplog.info("Process for AWG simulator started")
    return SIM_PROC


@pytest.fixture()
def awg_visa_device_proc() -> subprocess.Popen[bytes] | None:
    """
    Get process for running AWG Tango device.

    :returns: process for AWG device.
    """
    # pylint: disable-next=global-statement
    global DEV_PROC, DEV_PY
    if DEV_PROC is None:
        if not os.path.exists(DEV_PY):
            caplog.warning("File %s does not exist", DEV_PY)
            sim_files = find_files(".", os.path.basename(DEV_PY))
            if sim_files:
                DEV_PY = sim_files[0]
                caplog.info("Found %s", DEV_PY)
            else:
                caplog.error("Could not find %s in '.'", DEV_PY)
                return None
        caplog.info("Start AWG device")
        tango_host = os.getenv("TANGO_HOST", "")
        dev_env = {
            "PYTHONPATH": str(os.getenv("PYTHONPATH")),
            "SIMULATOR_HOST": f"{SIMULATOR_HOST}",
            "SIMULATOR_PORT": f"{SIMULATOR_PORT}",
            "SIMULATOR_MODEL": f"{SIMULATOR_MODEL}",
            "TANGO_HOST": tango_host,
        }
        caplog.info("Tango environment %s", dev_env)
        if tango_host:
            # Parameters for Tango device with database (for future use)
            dev_cmd = [
                DEV_PY,
                "awg",
                "-ORBendPoint",
                f"giop:tcp:0.0.0.0:{TANGO_PORT}",
            ]
        else:
            # Parameters for Tango device without database
            dev_cmd = [
                DEV_PY,
                "awg",
                "-v2",
                "-host",
                f"{SIMULATOR_HOST}",
                "-port",
                f"{TANGO_PORT}",
                "-nodb",
                "-dlist",
                "mid-itf/awg/1",
            ]
        caplog.info("Tango command: %s", " ".join(dev_cmd))
        # pylint: disable-next=consider-using-with
        DEV_PROC = subprocess.Popen(args=dev_cmd, env=dev_env)
        caplog.info("Process for AWG device started")
    else:
        pass
    return DEV_PROC
