"""Test harness for Tektronix AWG tests."""
import logging
import os
import socket
import subprocess
import time
from typing import Any, Dict

import psutil
import pytest
import tango
import tango.test_context
from ska_control_model import AdminMode

from ska_ser_test_equipment.awg_visa import AwgSimulatorVisa
from ska_ser_test_equipment.interface_definitions import InterfaceDefinitionFactory
from ska_ser_test_equipment.scpi.scpi_config import get_host_port, get_scpi_config

ATTRIBUTES_BUILTIN = {
    "AWG5208I": [
        # "versionId",
        # "buildState",
        "adminMode",
        # "loggingLevel",
        # "loggingTargets",
        # "healthState",
        # "controlMode",
        # "simulationMode",
        # "testMode",
        # "longRunningCommandsInQueue",
        # "longRunningCommandIDsInQueue",
        # "longRunningCommandStatus",
        # "longRunningCommandProgress",
        # "longRunningCommandResult",
        "State",
        "Status",
    ]
}
ATTRIBUTES_TESTED = {
    "AWG5208I": [
        "channel1_amplitude_power",
        # "channel1_asset",
        "channel1_freq",
        "channel1_output_state",
        "channel1_phase",
        "channel1_type",
        "channel2_amplitude_power",
        "channel2_freq",
        "channel2_output_state",
        "channel2_phase",
        "channel2_type",
        "channel3_amplitude_power",
        "channel3_freq",
        "channel3_output_state",
        "channel3_phase",
        "channel3_type",
        "channel4_amplitude_power",
        "channel4_freq",
        "channel4_output_state",
        "channel4_phase",
        "channel4_type",
        "channel5_amplitude_power",
        "channel5_freq",
        "channel5_output_state",
        "channel5_phase",
        "channel5_type",
        "channel6_amplitude_power",
        "channel6_freq",
        "channel6_output_state",
        "channel6_phase",
        "channel6_type",
        "channel7_amplitude_power",
        "channel7_freq",
        "channel7_output_state",
        "channel7_phase",
        "channel7_type",
        "channel8_amplitude_power",
        "channel8_freq",
        "channel8_output_state",
        "channel8_phase",
        "channel8_type",
        "clock_sample_rate",
        "clock_source",
        # "command_error",
        "device_error",
        # "execution_error",
        "instrument_mode",
        # "operation_complete",
        "play",
        "playing",
        # "power_cycled",
        # "query_error",
        "reset",
        "stop",
    ],
}
ATTRIBUTES_SKIP = {
    "AWG5208I": [
        "identity",
        "reset",
        "command_error",
        "device_error",
        "execution_error",
        "query_error",
        "power_cycled",
        "operation_complete",
    ],
}
ATTRIBUTES_IDENTITY = {
    "AWG5208I": [
        "TEKTRONIX,AWG5208,F00BAA7,FV:8.1.0266.0",
        "TEKTRONIX,AWG5208,B040881,FV:8.1.0266.0",
    ]
}
SIMULATOR_HOST = "127.0.0.1"
SIMULATOR_PORT = 4000
TANGO_HOST = "127.0.0.1"
TANGO_PORT = "45450"
SLEEP_TIME = 1.5
IFACE_DEF: Dict[str, Any] = {}

LOG_LEVEL = logging.DEBUG
logging.basicConfig(level=LOG_LEVEL)
caplog = logging.getLogger(__name__)


def test_awg_visa_simulator_start(
    awg_visa_simulator_proc: subprocess.Popen[bytes] | None,
) -> None:
    """
    Test that simulator has been started.

    :param awg_visa_simulator_proc: process handle
    """
    caplog.info("AWG simulator process is %s", repr(awg_visa_simulator_proc))
    assert awg_visa_simulator_proc is not None
    awg_visa_simulator_pid = awg_visa_simulator_proc.pid
    ps_info = psutil.Process(awg_visa_simulator_pid)
    proc_status = ps_info.status()
    caplog.info("Simulator process %s (%s)", ps_info.name(), proc_status)
    assert proc_status in ("disk-sleep", "running")


def test_awg_visa_device_start(
    awg_visa_device_proc: subprocess.Popen[bytes] | None,
) -> None:
    """
    Test that Tango device can be started.

    :param awg_visa_device_proc: process handle
    """
    caplog.info("Tango device process is %s", repr(awg_visa_device_proc))
    assert awg_visa_device_proc is not None
    awg_visa_device_pid = awg_visa_device_proc.pid
    ps_info = psutil.Process(awg_visa_device_pid)
    proc_status = ps_info.status()
    caplog.info("Tango device process %s (%s)", ps_info.name(), proc_status)
    assert proc_status in ("disk-sleep", "running")


def test_awg_visa_simulator_online(
    awg_visa_simulator_proc: subprocess.Popen[bytes] | None,
) -> None:
    """
    Test that simulator port is open.

    :param awg_visa_simulator_proc: process handle
    """
    hostname = SIMULATOR_HOST
    _host, port = get_host_port("awgvisa")
    if not port:
        try:
            caplog.warning(
                "No SCPI port, check %s", f"{' '.join(os.listdir('/app/charts'))}"
            )
        except FileNotFoundError:
            caplog.warning("Directory /app/charts does not exist")
        port = SIMULATOR_PORT
    assert port > 1024
    caplog.info("Connect to simulator %s:%d", hostname, port)
    time.sleep(SLEEP_TIME)
    sim_ready = False
    skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        skt.connect((hostname, port))
        sim_ready = True
    except ConnectionRefusedError:
        caplog.error("Could not connect to simulator %s:%d", hostname, port)
    assert sim_ready
    skt.close()
    # TODO this not really needed, the above should suffice
    awg_visa_simulator_pid = awg_visa_simulator_proc.pid  # type: ignore[union-attr]
    caplog.debug("Check ports opened by process %d", awg_visa_simulator_pid)
    ps_info = psutil.Process(awg_visa_simulator_pid)
    port_status = {}
    for connection in ps_info.connections():
        caplog.debug("Port %d status %s", connection.laddr[1], connection.status)
        port_status[connection.laddr[1]] = connection.status
    assert port in port_status
    caplog.info("Port status %s", port_status[port])
    assert port_status[port] in ("LISTEN", "ESTABLISHED", "CLOSE_WAIT")


def test_awg_visa_device_started(
    awg_visa_device: tango.DeviceProxy,
) -> None:
    """
    Test that Tango device has been started.

    :param awg_visa_device: Tango device
    """
    assert awg_visa_device is not None
    dev_ready = False
    dev_retry = 0
    while dev_retry < 18:
        try:
            admin = awg_visa_device.adminMode
            dev_ready = True
            break
        except AttributeError:
            caplog.warning("Device attribute error")
        except tango.DevFailed:
            caplog.warning("Device failed")
        time.sleep(SLEEP_TIME)
        dev_retry += 1
    assert dev_ready
    awg_visa_device.set_timeout_millis(5000)
    admin = awg_visa_device.adminMode
    assert admin is not None


def test_awg_visa_device_online(
    awg_visa_device_proc: subprocess.Popen[bytes] | None,
) -> None:
    """
    Test that device port is open.

    :param awg_visa_device_proc: process identifier
    """
    assert awg_visa_device_proc is not None
    hostname = TANGO_HOST
    port = int(TANGO_PORT)
    skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    dev_ready = False
    dev_retry = 0
    caplog.info("Connect to device %s:%d", hostname, port)
    while dev_retry < 10:
        try:
            skt.connect((hostname, port))
            dev_ready = True
            break
        except ConnectionRefusedError:
            caplog.error("Could not connect to device %s:%d", hostname, port)
        time.sleep(SLEEP_TIME)
        dev_retry += 1
    assert dev_ready
    caplog.info("Connected to device %s:%d", hostname, port)
    skt.close()
    # TODO this is not really needed, the above should suffice
    # ps_info = psutil.Process(awg_visa_device_proc.pid)
    # port_status = {}
    # for connection in ps_info.connections():
    #     caplog.debug("Port %d status %s", connection.laddr[1], connection.status)
    #     port_status[connection.laddr[1]] = connection.status
    # assert port in port_status
    # caplog.info("Port status %s", port_status[port])
    # assert port_status[port] in ("LISTEN", "ESTABLISHED", "CLOSE_WAIT")


def test_awg_visa_config() -> None:
    """Test the configuration."""
    # pylint: disable-next=global-statement
    global IFACE_DEF
    dev_model = os.getenv("SIMULATOR_MODEL", "AWG5208I").upper()

    assert (
        dev_model in ATTRIBUTES_IDENTITY
        and dev_model in ATTRIBUTES_BUILTIN
        and dev_model in ATTRIBUTES_TESTED
        and dev_model in ATTRIBUTES_SKIP
    )

    IFACE_DEF = InterfaceDefinitionFactory()(dev_model)  # type: ignore[assignment]
    caplog.info("Interface definition %s", IFACE_DEF)
    assert IFACE_DEF

    assert IFACE_DEF["model"] == dev_model

    assert "attributes" in IFACE_DEF

    scpi_fields, _scpi_values, _scpi_mins, _scpi_maxs = get_scpi_config(dev_model)

    assert len(scpi_fields) > 1

    for attr_name in IFACE_DEF["attributes"]:
        caplog.debug("check attribute %s", attr_name)
        assert (
            attr_name in ATTRIBUTES_BUILTIN[dev_model]
            or attr_name in ATTRIBUTES_TESTED[dev_model]
            or attr_name in ATTRIBUTES_SKIP[dev_model]
        )
        assert attr_name in scpi_fields
        attr_def = IFACE_DEF["attributes"][attr_name]
        assert "read" in attr_def or "read_write" in attr_def or "write" in attr_def
        if "read" in attr_def:
            i_field = attr_def["read"]["field"]
            f_type = attr_def["read"]["field_type"]
            caplog.info("attribute %s read %s : %s", attr_name, f_type, i_field)
        elif "read_write" in attr_def:
            i_field = attr_def["read_write"]["field"]
            f_type = attr_def["read"]["field_type"]
            caplog.info("attribute %s read/write %s : %s", attr_name, f_type, i_field)
        elif "write" in attr_def:
            i_field = attr_def["write"]["field"]
            try:
                f_type = attr_def["read"]["field_type"]
            except KeyError:
                f_type = None
            if f_type:
                caplog.info("attribute %s write %s : %s", attr_name, f_type, i_field)
            else:
                caplog.warning("attribute %s write no type : %s", attr_name, i_field)
        else:
            pass
        assert i_field == scpi_fields[attr_name]


def test_awg_visa_device_on(
    awg_visa_device: tango.DeviceProxy,
) -> None:
    """
    Test that device is on.

    :param awg_visa_device: Tango device
    """
    assert awg_visa_device is not None
    dev_name = "mid-itf/awg/1"
    dev_model = os.getenv("SIMULATOR_MODEL", "AWG5208I").upper()

    assert dev_model in ATTRIBUTES_IDENTITY

    chk_name = awg_visa_device.dev_name()

    caplog.info("Device name is '%s', should be '%s'", chk_name, dev_name)
    assert chk_name == dev_name

    admin = awg_visa_device.adminMode
    assert admin is not None

    if admin == AdminMode.OFFLINE:
        caplog.info("admin mode is OFFLINE")
    else:
        awg_visa_device.adminMode = AdminMode.OFFLINE
        caplog.info("admin mode set to OFFLINE")

    try:
        awg_visa_device.adminMode = AdminMode.ONLINE
    except AttributeError as dev_err:
        caplog.error("could not set admin mode to ONLINE %s", dev_err)
        awg_visa_device.adminMode = None
    caplog.info("AWG device admin mode set to ONLINE")
    assert awg_visa_device.adminMode == AdminMode.ONLINE

    caplog.info(
        "AWG device status %s state %s",
        awg_visa_device.Status(),
        awg_visa_device.State(),
    )

    # pylint: disable-next=c-extension-no-member,protected-access
    assert awg_visa_device.State() != tango._tango.DevState.UNKNOWN

    # pylint: disable-next=c-extension-no-member,protected-access
    assert awg_visa_device.State() == tango._tango.DevState.ON

    caplog.info("AWG device status %s", awg_visa_device.Status())

    caplog.info("AWG device admin mode %s", awg_visa_device.adminMode)
    assert not awg_visa_device.adminMode


def test_awg_visa_identity(
    awg_visa_device: tango.DeviceProxy,
) -> None:
    """
    Confirm identity of device.

    :param awg_visa_device: Tango device
    """
    assert awg_visa_device is not None
    time.sleep(SLEEP_TIME)
    dev_model = os.getenv("SIMULATOR_MODEL", "AWG5208I").upper()
    assert dev_model is not None

    chk_id = ATTRIBUTES_IDENTITY[dev_model]
    caplog.info("AWG device identity should be %s", " or ".join(chk_id))
    assert chk_id is not None

    id_info = awg_visa_device.read_attribute("identity").value
    caplog.info("Identity is %s", id_info)
    assert id_info is not None
    assert id_info in chk_id


def test_awg_visa_attributes_read(
    awg_visa_device: tango.DeviceProxy,
) -> None:
    """
    Test the attributes.

    :param awg_visa_device: Tango device
    """
    assert awg_visa_device is not None
    attributes = sorted(awg_visa_device.get_attribute_list())
    caplog.debug("got %s attributes : %s", len(attributes), " ".join(attributes))
    assert attributes

    dev_model = os.getenv("SIMULATOR_MODEL", "AWG5208I").upper()

    initial_values_map = AwgSimulatorVisa.DEFAULTS.copy()
    caplog.debug("AWG device got %d initial values", len(initial_values_map))
    assert initial_values_map

    for attribute in attributes:
        caplog.debug("Test attribute %s", attribute)
        if attribute in ATTRIBUTES_SKIP[dev_model]:
            caplog.warning("skip test for %s", attribute)
        elif attribute in ATTRIBUTES_BUILTIN[dev_model]:
            attr_value = awg_visa_device.read_attribute(attribute).value
            caplog.info("Attribute %s value %s", attribute, attr_value)
            assert attr_value is not None
        elif attribute not in ATTRIBUTES_TESTED[dev_model]:
            caplog.warning("untested attribute %s", attribute)
        elif attribute not in initial_values_map:
            caplog.warning("no initial value for %s", attribute)
            attr_value = awg_visa_device.read_attribute(attribute).value
            caplog.info("Attribute %s value %s", attribute, attr_value)
            assert attr_value is not None
        else:
            initial_value = initial_values_map[attribute]
            assert initial_value is not None
            caplog.info("Attribute %s initial value %s", attribute, initial_value)
            attr_value = awg_visa_device.read_attribute(attribute).value
            caplog.info("Attribute %s actual value %s", attribute, attr_value)
            assert attr_value == initial_value


def test_awg_visa_attributes_write_valid(
    awg_visa_device: tango.DeviceProxy,
) -> None:
    """
    Test the attributes.

    :param awg_visa_device: Tango device
    """
    assert awg_visa_device is not None
    # Set instrument mode
    caplog.info("Set instrument_mode to FGEN")
    awg_visa_device.instrument_mode = "FGEN"
    # time.sleep(SLEEP_TIME)
    assert awg_visa_device.instrument_mode == "FGEN"
    # Set clock clock source
    caplog.info("Set clock_source to EXTERNAL")
    awg_visa_device.clock_source = "EXTERNAL"
    # time.sleep(SLEEP_TIME)
    assert awg_visa_device.clock_source == "EXTERNAL"
    # Set clock sample rate
    caplog.info("Set clock_sample_rate to 2 GHz")
    awg_visa_device.clock_sample_rate = 2e9
    # time.sleep(SLEEP_TIME)
    assert awg_visa_device.clock_sample_rate == 2e9
    # Set frequency
    caplog.info("Set channel1_freq to 1.1 MHz")
    awg_visa_device.channel1_freq = 1.1e6
    time.sleep(SLEEP_TIME)
    assert awg_visa_device.channel1_freq == 1.1e6
    # Set power
    caplog.info("Set channel1_amplitude_power to -10.0 dBm")
    awg_visa_device.channel1_amplitude_power = -10.0
    time.sleep(SLEEP_TIME)
    assert awg_visa_device.channel1_amplitude_power == -10.0
    # Set frequency
    caplog.info("Set channel1_phase to 90 degrees")
    awg_visa_device.channel1_phase = 90.0
    time.sleep(SLEEP_TIME)
    assert awg_visa_device.channel1_phase == 90.0
    # Set waveform type
    caplog.info("Set channel1_type to 'SQUARE'")
    awg_visa_device.channel1_type = "SQUARE"
    time.sleep(SLEEP_TIME)
    assert awg_visa_device.channel1_type[0:3] == "SQU"
    # Set state
    caplog.info("Set channel1_output_state to OFF")
    awg_visa_device.channel1_output_state = False
    time.sleep(SLEEP_TIME)
    assert awg_visa_device.channel1_output_state is False


@pytest.mark.xfail
def test_awg_visa_attributes_write_invalid_freq(
    awg_visa_device: tango.DeviceProxy,
) -> None:
    """
    Test the attributes.

    :param awg_visa_device: Tango device
    """
    if "min_value" in IFACE_DEF["attributes"]["channel1_freq"]["write"]:
        min_val = float(IFACE_DEF["attributes"]["channel1_freq"]["write"]["min_value"])
        if min_val < 0.0:
            new_val = min_val * 2
        else:
            new_val = min_val / 2
        cur_val = awg_visa_device.channel1_freq
        caplog.info("Change channel1_freq from %f to %f", cur_val, new_val)
        awg_visa_device.channel1_freq = new_val
        time.sleep(SLEEP_TIME)
        assert awg_visa_device.channel1_freq == new_val


def test_awg_visa_attributes_write_invalid_phase(
    awg_visa_device: tango.DeviceProxy,
) -> None:
    """
    Test the attributes.

    :param awg_visa_device: Tango device
    """
    if "min_value" in IFACE_DEF["attributes"]["channel1_phase"]["write"]:
        min_val = float(IFACE_DEF["attributes"]["channel1_phase"]["write"]["min_value"])
        if min_val < 0.0:
            new_val = min_val * 2
        else:
            new_val = min_val / 2
        cur_val = awg_visa_device.channel1_phase
        caplog.info("Change channel1_phase from %f to %f", cur_val, new_val)
        awg_visa_device.channel1_phase = new_val
        time.sleep(SLEEP_TIME)
        assert awg_visa_device.channel1_phase == cur_val
    if "max_value" in IFACE_DEF["attributes"]["channel2_phase"]["write"]:
        max_val = float(IFACE_DEF["attributes"]["channel2_phase"]["write"]["max_value"])
        if max_val < 0.0:
            new_val = max_val / 2
        else:
            new_val = max_val * 2
        cur_val = awg_visa_device.channel2_phase
        caplog.info("Change channel2_phase from %f to %f", cur_val, new_val)
        awg_visa_device.channel2_phase = new_val
        time.sleep(SLEEP_TIME)
        assert awg_visa_device.channel2_phase == cur_val


def test_awg_visa_offline(
    awg_visa_device: tango.DeviceProxy,
) -> None:
    """
    Test that device can be taken offline.

    :param awg_visa_device: Tango device
    """
    assert awg_visa_device is not None
    awg_visa_device.adminMode = AdminMode.OFFLINE
    caplog.info("admin mode set to OFFLINE")
    assert awg_visa_device.adminMode == AdminMode.OFFLINE


def test_awg_visa_device_admin(
    awg_visa_device: tango.DeviceProxy,
) -> None:
    """
    Check Tango admin device.

    :param  awg_visa_device: device proxy
    """
    assert awg_visa_device is not None
    adm_dev = tango.DeviceProxy(awg_visa_device.adm_name())
    caplog.info("Check admin device %s", adm_dev)
    assert adm_dev
    # TODO This results in fatal Python error, i.e. segmentation fault
    # adm_dev.Kill()


def test_awg_visa_device_stop(
    awg_visa_device_proc: subprocess.Popen[bytes] | None,
) -> None:
    """
    Stop device process.

    :param awg_visa_device_proc: process handle
    """
    assert awg_visa_device_proc is not None
    awg_visa_device_pid = awg_visa_device_proc.pid
    assert awg_visa_device_pid
    caplog.info("Stop device process %d", awg_visa_device_pid)
    awg_visa_device_proc.kill()
    time.sleep(SLEEP_TIME)
    ps_info = psutil.Process(awg_visa_device_pid)
    try:
        proc_status = ps_info.status()
    except psutil.NoSuchProcess:
        proc_status = "stopped"
    caplog.info("Tango device process %s (%s)", ps_info.name(), proc_status)
    assert proc_status in ("sleeping", "zombie", "stopped", "dead")


def test_awg_visa_simulator_stop(
    awg_visa_simulator_proc: subprocess.Popen[bytes] | None,
) -> None:
    """
    Stop simulator process.

    :param awg_visa_simulator_proc: process handle
    """
    assert awg_visa_simulator_proc is not None
    awg_visa_simulator_pid = awg_visa_simulator_proc.pid
    assert awg_visa_simulator_pid
    caplog.info("Stop simulator process %d", awg_visa_simulator_pid)
    awg_visa_simulator_proc.kill()
    time.sleep(SLEEP_TIME)
    ps_info = psutil.Process(awg_visa_simulator_pid)
    try:
        proc_status = ps_info.status()
    except psutil.NoSuchProcess:
        proc_status = "stopped"
    caplog.info("Tango device process %s (%s)", ps_info.name(), proc_status)
    assert proc_status in ("sleeping", "zombie", "stopped", "dead")
