#!/usr/bin/python3
from datetime import datetime
import decimal
import getopt
import logging
import os
import socket
import string
import sys
import time
import yaml
import matplotlib.pyplot
import numpy as np
from typing import Any, Callable, Dict, Final, Optional, Tuple

log_level_str = os.getenv("LOG_LEVEL", "WARNING").upper()
if log_level_str == "DEBUG":
    LOG_LEVEL = logging.DEBUG
elif log_level_str == "INFO":
    LOG_LEVEL = logging.INFO
elif log_level_str == "WARNING":
    LOG_LEVEL = logging.WARNING
elif log_level_str == "ERROR":
    LOG_LEVEL = logging.ERROR
else:
    LOG_LEVEL = logging.WARNING
logging.basicConfig(level=LOG_LEVEL)
logger = logging.getLogger(__name__)

top_keys = [
    "model",
    "poll_rate",
    "timeout",
    "supports_chains",
    "argument_separator ",
]

exclude_keys = [
    "get_trace",
    "reset",
]


def netcat(hostname: str, port: int, cmd: str) -> str:
    """
    Set up Telnet session with instrument.

    :param hostname: instrument IP address or hostname
    :param port: instrument port number
    :param cmd: SCPI command to be sent to instrument

    :returns: message received
    """
    logger.debug("Send %s : %s", type(cmd), cmd)
    content = bytes(cmd, encoding="utf-8")
    skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        skt.connect((hostname, port))
    except ConnectionRefusedError:
        logger.error("Could not connect to host %s:%d", hostname, port)
        return None
    skt.sendall(content)
    # skt.send(content)
    skt.shutdown(socket.SHUT_WR)
    # time.sleep(1)
    r_data = b""
    while 1:
        data = skt.recv(1024, socket.MSG_WAITALL)
        if len(data) == 0:
            break
        # logger.debug("Received:%s", repr(data))
        r_data += data
    logger.debug("Connection closed")
    skt.close()
    r_val = r_data.decode("utf-8").strip()
    logger.debug("Received:%s", r_val)
    # if not r_data:
    #     r_data = b"---"
    return r_data


def send_read(hostname: str, port: int, field: str, sentinel_string: str) -> str:
    """
    Send SCPI read command.

    :param hostname: instrument IP address or hostname
    :param port: instrument port number
    :param field: SCPI command to be sent to instrument
    :param sentinel_string: appended to command

    :returns: reply from instrument
    """
    logger.debug("Read '%s'", field)
    if "?" not in field:
        cmd = field + "?" + sentinel_string
    else:
        cmd = field + sentinel_string
    data = netcat(hostname, port, cmd)
    if data is None:
        return None, None
    r_data = data.decode("utf-8").strip()
    return cmd.strip(), r_data


def send_write(
    hostname: str,
    port: int,
    field: str,
    argument_separator: str,
    value: str,
    sentinel_string: str
) -> None:
    """
    Send SCPI write command.

    :param hostname: instrument IP address or hostname
    :param port: instrument port number
    :param field: SCPI command to be sent to instrument
    :param argument_separator: between command and value
    :param value: set attribute to this
    :param sentinel_string: appended to command
    """
    if value is not None:
        cmd = field + argument_separator + value
    else:
        cmd = field
    logger.debug("Write '%s'", cmd)
    cmd += sentinel_string
    data = netcat(hostname, port, cmd)
    # r_data = data.decode("utf-8").strip()
    # return r_data


def read_yaml_file(yaml_file: str, hostname: str, port: int, write_mode: bool) -> None:
    """
    Read and display attributes in file.

    :param yaml_file: YAML file describing SCPI commands for instrument
    :param hostname: instrument IP address or hostname
    :param port: instrument port number
    """
    try:
        y_file = open(yaml_file, 'r')
    except FileNotFoundError:
        logger.error("Could not read file %s", yaml_file)
        return
    docs = yaml.safe_load_all(y_file)
    n_docs = 0
    for doc in docs:
        # logger.debug(doc)
        for key in top_keys:
            try:
                print("%s %s" % (key, doc[key]))
            except KeyError:
                pass
        argument_separator = doc["argument_separator"]
        sentinel_string = doc["sentinel_string"]
        for attribute in doc["attributes"]:
            if attribute in exclude_keys:
                print("skip  %s" % attribute)
            elif "read" in doc["attributes"][attribute]:
                logger.debug(
                    "Read attribute %s : %s",
                    attribute,
                    doc["attributes"][attribute]["read"]
                )
                field = doc["attributes"][attribute]["read"]["field"]
                req, reply = send_read(hostname, port, field, sentinel_string)
                print("read  %s | %s | %s" % (attribute, req, reply))
            elif write_mode and "read_write" in doc["attributes"][attribute]:
                logger.debug(
                    "Read/write attribute %s : %s",
                    attribute,
                    doc["attributes"][attribute]["read_write"]
                )
                field = doc["attributes"][attribute]["read_write"]["field"]
                value = str(doc["attributes"][attribute]["read_write"]["value"])
                # send_read(hostname, port, field, sentinel_string)
                print(
                    "write %s | %s%s%s"
                    % (attribute, field, argument_separator, value)
                )
                send_write(
                    hostname,
                    port,
                    field,
                    argument_separator,
                    value,
                    sentinel_string
                )
                req, reply = send_read(hostname, port, field, sentinel_string)
                print("read  %s | %s | %s" % (attribute, req, reply))
            elif "read_write" in doc["attributes"][attribute]:
                logger.debug(
                    "Read attribute %s : %s",
                    attribute,
                    doc["attributes"][attribute]["read_write"]
                )
                field = doc["attributes"][attribute]["read_write"]["field"]
                req, reply = send_read(hostname, port, field, sentinel_string)
                print("read  %s | %s | %s" % (attribute, req, reply))
            elif write_mode and "write" in doc["attributes"][attribute]:
                logger.debug(
                    "Write attribute %s : %s",
                    attribute,
                    doc["attributes"][attribute]["write"]
                )
                field = doc["attributes"][attribute]["write"]["field"]
                try:
                    value = str(doc["attributes"][attribute]["write"]["value"])
                except KeyError:
                    value = None
                print(
                    "write %s | %s%s%s"
                    % (attribute, field, argument_separator, value)
                )
                send_write(
                    hostname,
                    port,
                    field,
                    argument_separator,
                    value,
                    sentinel_string
                )
            else:
                logger.debug(
                    "illegal attribute %s : %s",
                    attribute,
                    doc["attributes"][attribute]
                )
        n_docs += 1
    y_file.close()
    logger.debug("Read %d docs", n_docs)


def get_attribute(
    hostname: str, port: int, yaml_file: str, sentinel_string: str, attribute: str
) -> str:
    """
    Get attribute value.

    :param hostname: instrument IP address or hostname
    :param port: instrument port number
    :param yaml_file: YAML file describing SCPI commands for instrument
    :param sentinel_string: appended to SCPI command
    :param attribute: name used as Tango attribute

    :returns: reply from instrument
    """
    try:
        y_file = open(yaml_file, 'r')
    except FileNotFoundError:
        logger.error("Could not read file %s", yaml_file)
        return None
    docs = yaml.safe_load_all(y_file)
    for doc in docs:
        sentinel_string = doc["sentinel_string"]
        attr = doc["attributes"][attribute]
        logger.debug("Process attribute %s", attr)
        if "read_write" in attr:
            field = str(attr["read_write"]["field"])
        elif "read" in attr:
            field = str(attr["read"]["field"])
        else:
            logger.error("Attribute %s is not read or read_write : %s", attribute, attr)
            return None
    y_file.close()
    _cmd, r_val = send_read(hostname, port, field, sentinel_string)
    print("%s | %s | %s" % (attribute, field, r_val))
    return r_val


def set_attribute(
    hostname: str,
    port: int,
    yaml_file: str,
    sentinel_string: str,
    attribute: str,
    value: str
) -> str:
    """
    Set attribute value.

    :param hostname: instrument IP address or hostname
    :param port: instrument port number
    :param yaml_file: YAML file describing SCPI commands for instrument
    :param sentinel_string: appended to SCPI command
    :param attribute: name used as Tango attribute
    :param value: set attribute to this

    :returns: reply from instrument
    """
    try:
        y_file = open(yaml_file, 'r')
    except FileNotFoundError:
        logger.error("Could not read file %s", yaml_file)
        return None
    docs = yaml.safe_load_all(y_file)
    for doc in docs:
        sentinel_string = doc["sentinel_string"]
        argument_separator = doc["argument_separator"]
        attr = doc["attributes"][attribute]
        logger.debug("Process attribute %s", attr)
        try:
            field = attr["read_write"]["field"]
            field_type = attr["read_write"]["field_type"]
        except KeyError:
            try:
                field = attr["write"]["field"]
                field_type = attr["write"]["field_type"]
            except KeyError:
                logger.error("Attribute %s is not read or read_write", attribute)
                return None
    y_file.close()
    _cmd, r_val = send_read(hostname, port, field, sentinel_string)
    logger.info("Attribute %s current value is %s", attribute, r_val)
    send_write(hostname, port, field, argument_separator, value, sentinel_string)
    _cmd, r_val = send_read(hostname, port, field, sentinel_string)
    logger.info("Attribute %s value set to %s", attribute, r_val)
    if field_type == "float":
        if float(value) == float(r_val):
            print("Attribute %s float value set to %s" % (attribute, value))
        else:
            print(
                "Attribute %s float value not set to %s (%s)" % (
                    attribute,
                    value,
                    r_val
                )
            )
    else:
        if value == rval:
            print("Attribute %s %s value set to %s" % (attribute, field_type, value))
        else:
            print(
                "Attribute %s %s value not set to %s (%s)" % (
                    attribute,
                    value,
                    field_type,
                    r_val
                )
            )
    return r_val


def get_attributes(yaml_file: str) -> None:
    """
    Display all attributes names.

    :param yaml_file: YAML file describing SCPI commands for instrument
    """
    try:
        y_file = open(yaml_file, 'r')
    except FileNotFoundError:
        logger.error("Could not read file %s", yaml_file)
        return
    docs = yaml.safe_load_all(y_file)
    for doc in docs:
        for attribute in doc["attributes"]:
            print("%s" % attribute)
    y_file.close()


def eng_number(val: float, unit: Optional[str] = "") -> str:
    """
    Convert number to engineering format.

    :param val: input value
    :param val: unit added to end of string

    :returns: formatted string
    """
    f_str = decimal.Decimal(f"{val:.4E}").normalize().to_eng_string()
    f_str = f_str.replace("E-12", "p")
    f_str = f_str.replace("E-9", "n")
    f_str = f_str.replace("E-6", "u")
    f_str = f_str.replace("E-3", "m")
    f_str = f_str.replace("E+3", "k")
    f_str = f_str.replace("E+6", "M")
    f_str = f_str.replace("E+9", "G")
    f_str = f_str.replace("E+12", "T")
    f_str += unit
    return f_str


def get_trace_scope(
    hostname: str,
    port: int,
    data_file: str | None,
    sentinel_string: str
) -> Tuple[list[float], list[float]]:
    """
    Read trace data from instrument.

    :param hostname: instrument IP address or hostname
    :param port: instrument port number
    :param yaml_file: YAML file describing SCPI commands for instrument
    :param sentinel_string: appended to SCPI command

    :returns: tuple with lists of values
    """
    x_points = []
    y_points = []
    _cmd, read_buf = send_read(hostname, port, "HOR:SCA", sentinel_string)
    if read_buf is None:
        return x_points, y_points
    time_div = float(read_buf)
    _cmd, read_buf = send_read(hostname, port, "CURV", sentinel_string)
    if read_buf is None:
        return x_points, y_points
    logger.debug(
        "Received %d %s : %s" % (len(read_buf), type(read_buf), read_buf)
    )
    trace_data = read_buf.split(",")
    n_data = 0
    n_data_len = len(trace_data)
    n_time = 0.0
    t_step = (time_div*10)/n_data_len
    logger.debug("Time step %f", t_step)
    while n_data < n_data_len:
        n_ampl = float(trace_data[n_data])
        x_points.append(n_time)
        y_points.append(n_ampl)
        n_time += t_step
        n_data += 1
    return x_points, y_points


def get_trace_spectana(
    hostname: str,
    port: int,
    data_file: str | None,
    sentinel_string: str,
    yaml_file: str | None = None
) -> Tuple[list[float], list[float]]:
    """
    Read trace data from file or instrument.

    :param hostname: instrument IP address or hostname
    :param port: instrument port number
    :param yaml_file: YAML file describing SCPI commands for instrument
    :param sentinel_string: appended to SCPI command

    :returns: tuple with lists of values
    """
    x_points = []
    y_points = []
    if yaml_file or data_file:
        if data_file is None:
            logger.error("Data file not specified")
            return x_points, y_points
        if yaml_file is None:
            logger.error("YAML file not specified")
            return x_points, y_points
        logger.info("Read data file %s", data_file)
        try:
            d_file = open(data_file, "r")
        except FileNotFoundError:
            logger.error("Could not read file %s", data_file)
            return x_points, y_points
        read_buf = d_file.read()
        trace_data = read_buf[7:].split(",")
        d_file.close()
        logger.info("Read YAML file %s", yaml_file)
        try:
            y_file = open(yaml_file, 'r')
        except FileNotFoundError:
            logger.error("Could not read file %s", yaml_file)
            return x_points, y_points
        docs = yaml.safe_load_all(y_file)
        for doc in docs:
            sentinel_string = doc["sentinel_string"]
            f_start = float(doc["attributes"]["frequency_start"]["read_write"]["value"])
            f_stop = float(doc["attributes"]["frequency_stop"]["read_write"]["value"])
        y_file.close()
    else:
        _cmd, read_buf = send_read(hostname, port, "TRAC:DATA:ALL", sentinel_string)
        if read_buf is None:
            return x_points, y_points
        try:
            trace_data = read_buf[7:].decode().split(",")
        except AttributeError:
            trace_data = read_buf[7:].split(",")
        logger.debug("Received %d bytes : %s" % (len(trace_data), trace_data))
        _cmd, read_buf = send_read(hostname, port, "FREQ:STAR", sentinel_string)
        if read_buf is None:
            return x_points, y_points
        f_start = float(read_buf)
        _cmd, read_buf = send_read(hostname, port, "FREQ:STOP", sentinel_string)
        if read_buf is None:
            return x_points, y_points
        f_stop = float(read_buf)

    logger.info("Start frequency %f" % f_start)
    logger.info("Stop frequency %f" % f_stop)
    logger.debug("Read %d data items" % len(trace_data))
    n_data_len = len(trace_data) - 1
    f_step = (f_stop-f_start)/n_data_len
    logger.debug("Frequency step %f Hz", f_step)
    n_data_len += 1
    n_freq = f_start
    ascii_chars = set(string.printable)
    ampl_str = ''.join(filter(lambda x: x in ascii_chars, trace_data[0]))
    ampl = float(ampl_str)
    logger.debug("%9s = %.2fdBm", eng_number(n_freq, "Hz"), ampl)
    n_data = 1
    n_freq += f_step
    while n_data < n_data_len:
        ampl = float(trace_data[n_data])
        logger.debug("%9s = %.2fdBm", eng_number(n_freq, "Hz"), ampl)
        x_points.append(float(n_freq))
        y_points.append(ampl)
        n_freq += f_step
        n_data += 1
    return x_points, y_points


def write_trace(
    xlabel: str,
    xpoints: list[float],
    ylabel: str,
    ypoints: list[float],
    image_name: str | None
) -> None:
    """
    Write trace data to SVG file.

    :param xpoints: X axis values
    :param ypoints: Y axis values
    :param image_name: SVG file name
    """
    fig, _ax = matplotlib.pyplot.subplots()

    matplotlib.pyplot.plot(xpoints, ypoints)

    matplotlib.pyplot.ylabel(f"{xlabel} ")
    matplotlib.pyplot.xlabel(f"{ylabel} ")

    if image_name is not None:
        _file_name, file_ext = os.path.splitext(image_name)
        img_fmt = file_ext[1:]
    else:
        img_fmt = "svg"
        now = datetime.now()
        image_name = "trace_data_%s.%s" % (now.strftime("%Y%m%dT%H%M%S"), img_fmt)
    logger.warning("Write %s file %s", img_fmt.upper(), image_name)
    fig.savefig(image_name, format=img_fmt, dpi=1200)


def usage(p_name: str) -> None:
    """
    Display help message.

    :param p_name: executable name
    """
    print("Run all SCPI commands:")
    print("\t%s -y <yaml_file>" % p_name)
    print("Get attribute:")
    print("\t%s --attr=<ATTR> --yaml=<FILE>" % p_name)
    print("Set attribute:")
    print("\t%s --attr <ATTR> --yaml <FILE> --val <VALUE>.0" % p_name)
    print("\t%s --attr frequency_start --yaml src/ska_ser_test_equipment/spectrum_analyser_anritsu/anritsu_ms2090a.yaml -V --val 675000000.0" % p_name)
    print("Get trace:")
    print("\t%s --scope [-s <svg_file>]" % p_name)
    print("\t%s --spectana [-y <yaml_file>] [-s <svg_file>]" % p_name)
    # print("\t%s -y <yaml_file> -s <svg_file>" % p_name)


def main(y_arg: list[str]) -> None:
    """
    This is the main peanut.

    :param y_arg: command line arguments
    """
    hostname = "127.0.0.1"
    port = 9001
    sentinel_string = "\n"
    yaml_file = None
    svg_file = None
    data_file = None
    plot_trace_scope = False
    plot_trace_spectana = False
    list_attr = False
    send_cmd = None
    attribute = None
    value = None
    write_mode = False

    opts, args = getopt.getopt(
        y_arg[1:],
        "hltvwVs:y:a:d:u:",
        [
            "help",
            "list",
            "scope",
            "spectana",
            "write",
            "host=",
            "port=",
            "yaml=",
            "data=",
            "svg=",
            "cmd=",
            "attribute=",
            "value="
        ]
    )
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage(os.path.basename(y_arg[0]))
            sys.exit(1)
        elif opt in ("-y", "--yaml"):
            yaml_file = arg
        elif opt in ("-s", "--svg"):
            svg_file = arg
        elif opt in ("-d", "--data"):
            data_file = arg
        elif opt == "--host":
            hostname = arg
        elif opt == "--port":
            port = int(arg)
        elif opt in ("-l", "--list"):
            list_attr = True
        elif opt == "--scope":
            plot_trace_scope = True
        elif opt == "--spectana":
            plot_trace_spectana = True
        elif opt in ("-w", "--write"):
            write_mode = True
        elif opt in ("-a", "--attribute"):
            attribute = arg
        elif opt in ("-u", "--value"):
            value = arg
        elif opt == "--cmd":
            send_cmd = arg
        elif opt == "-v":
            logger.setLevel(logging.INFO)
        elif opt == "-V":
            logger.setLevel(logging.DEBUG)
        else:
            pass

    if plot_trace_spectana:
        x_points, y_points = get_trace_spectana(
            hostname, port, data_file, sentinel_string, yaml_file
        )
        if x_points and y_points:
            write_trace(
                "Amplitude (dBm)",
                x_points,
                "Frequency (Hz)",
                y_points,
                svg_file
            )
        else:
            logger.error("No spectrum analyser trace data to plot")
    elif plot_trace_scope:
        x_points, y_points = get_trace_scope(
            hostname, port, data_file, sentinel_string
        )
        if x_points and y_points:
            write_trace(
                "Amplitude",
                x_points,
                "Time",
                y_points,
                svg_file)
        else:
            logger.error("No oscilloscope trace data to plot")
    elif attribute is not None and yaml_file is not None and value is not None:
        set_attribute(hostname, port, yaml_file, sentinel_string, attribute, value)
    elif attribute is not None and yaml_file is not None:
        get_attribute(hostname, port, yaml_file, sentinel_string, attribute)
    elif list_attr and yaml_file is not None:
        get_attributes(yaml_file)
    elif yaml_file is not None:
        read_yaml_file(yaml_file, hostname, port, write_mode)
    elif send_cmd is not None:
        send_read(hostname, port, send_cmd, sentinel_string)
    else:
        logger.warning("Nothing to do!")


if __name__ == "__main__":
    main(sys.argv)
