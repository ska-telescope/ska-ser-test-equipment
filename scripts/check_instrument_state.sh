#!/usr/bin/bash

# source: https://github.com/mrbaseman/parse_yaml.git
function parse_yaml {
	local prefix=$2
	local separator=${3:-_}

	local indexfix
	# Detect awk flavor
	if awk --version 2>&1 | grep -q "GNU Awk" ; then
		# GNU Awk detected
		indexfix=-1
	elif awk -Wv 2>&1 | grep -q "mawk" ; then
		# mawk detected
		indexfix=0
	fi

	local s='[[:space:]]*' sm='[ \t]*' w='[a-zA-Z0-9_]*' fs=${fs:-$(echo @|tr @ '\034')} i=${i:-  }
	cat $1 | \
	awk -F$fs "{multi=0;
	if(match(\$0,/$sm\|$sm$/)){multi=1; sub(/$sm\|$sm$/,\"\");}
	if(match(\$0,/$sm>$sm$/)){multi=2; sub(/$sm>$sm$/,\"\");}
	while(multi>0){
		str=\$0; gsub(/^$sm/,\"\", str);
		indent=index(\$0,str);
		indentstr=substr(\$0, 0, indent+$indexfix) \"$i\";
		obuf=\$0;
		getline;
		while(index(\$0,indentstr)){
			obuf=obuf substr(\$0, length(indentstr)+1);
			if (multi==1){obuf=obuf \"\\\\n\";}
			if (multi==2){
				if(match(\$0,/^$sm$/))
				obuf=obuf \"\\\\n\";
				else obuf=obuf \" \";
			}
			getline;
		}
		sub(/$sm$/,\"\",obuf);
		print obuf;
		multi=0;
		if(match(\$0,/$sm\|$sm$/)){multi=1; sub(/$sm\|$sm$/,\"\");}
		if(match(\$0,/$sm>$sm$/)){multi=2; sub(/$sm>$sm$/,\"\");}
	}
	print}" | \
	sed  -e "s|^\($s\)?|\1-|" \
		-ne "s|^$s#.*||;s|$s#[^\"']*$||;s|^\([^\"'#]*\)#.*|\1|;t1;t;:1;s|^$s\$||;t2;p;:2;d" | \
	sed -ne "s|,$s\]$s\$|]|" \
		-e ":1;s|^\($s\)\($w\)$s:$s\(&$w\)\?$s\[$s\(.*\)$s,$s\(.*\)$s\]|\1\2: \3[\4]\n\1$i- \5|;t1" \
		-e "s|^\($s\)\($w\)$s:$s\(&$w\)\?$s\[$s\(.*\)$s\]|\1\2: \3\n\1$i- \4|;" \
		-e ":2;s|^\($s\)-$s\[$s\(.*\)$s,$s\(.*\)$s\]|\1- [\2]\n\1$i- \3|;t2" \
		-e "s|^\($s\)-$s\[$s\(.*\)$s\]|\1-\n\1$i- \2|;p" | \
	sed -ne "s|,$s}$s\$|}|" \
		-e ":1;s|^\($s\)-$s{$s\(.*\)$s,$s\($w\)$s:$s\(.*\)$s}|\1- {\2}\n\1$i\3: \4|;t1" \
		-e "s|^\($s\)-$s{$s\(.*\)$s}|\1-\n\1$i\2|;" \
		-e ":2;s|^\($s\)\($w\)$s:$s\(&$w\)\?$s{$s\(.*\)$s,$s\($w\)$s:$s\(.*\)$s}|\1\2: \3 {\4}\n\1$i\5: \6|;t2" \
		-e "s|^\($s\)\($w\)$s:$s\(&$w\)\?$s{$s\(.*\)$s}|\1\2: \3\n\1$i\4|;p" | \
	sed  -e "s|^\($s\)\($w\)$s:$s\(&$w\)\(.*\)|\1\2:\4\n\3|" \
		-e "s|^\($s\)-$s\(&$w\)\(.*\)|\1- \3\n\2|" | \
	sed -ne "s|^\($s\):|\1|" \
		-e "s|^\($s\)\(---\)\($s\)||" \
		-e "s|^\($s\)\(\.\.\.\)\($s\)||" \
		-e "s|^\($s\)-$s[\"']\(.*\)[\"']$s\$|\1$fs$fs\2|p;t" \
		-e "s|^\($s\)\($w\)$s:$s[\"']\(.*\)[\"']$s\$|\1$fs\2$fs\3|p;t" \
		-e "s|^\($s\)-$s\(.*\)$s\$|\1$fs$fs\2|" \
		-e "s|^\($s\)\($w\)$s:$s[\"']\?\(.*\)$s\$|\1$fs\2$fs\3|" \
		-e "s|^\($s\)[\"']\?\([^&][^$fs]\+\)[\"']$s\$|\1$fs$fs$fs\2|" \
		-e "s|^\($s\)[\"']\?\([^&][^$fs]\+\)$s\$|\1$fs$fs$fs\2|" \
		-e "s|$s\$||p" | \
	awk -F$fs "{
		gsub(/\t/,\"        \",\$1);
		if(NF>3){if(value!=\"\"){value = value \" \";}value = value  \$4;}
		else {
			if(match(\$1,/^&/)){anchor[substr(\$1,2)]=full_vn;getline};
			indent = length(\$1)/length(\"$i\");
			vname[indent] = \$2;
			value= \$3;
			for (i in vname) {if (i > indent) {delete vname[i]; idx[i]=0}}
			if(length(\$2)== 0){  vname[indent]= ++idx[indent] };
			vn=\"\"; for (i=0; i<indent; i++) { vn=(vn)(vname[i])(\"$separator\")}
			vn=\"$prefix\" vn;
			full_vn=vn vname[indent];
			if(vn==\"$prefix\")vn=\"$prefix$separator\";
			if(vn==\"_\")vn=\"__\";
		}
		assignment[full_vn]=value;
		if(!match(assignment[vn], full_vn))assignment[vn]=assignment[vn] \" \" full_vn;
		if(match(value,/^\*/)){
			ref=anchor[substr(value,2)];
			if(length(ref)==0){
				printf(\"%s=\\\"%s\\\"\n\", full_vn, value);
			} else {
				for(val in assignment){
					if((length(ref)>0)&&index(val, ref)==1){
						tmpval=assignment[val];
						sub(ref,full_vn,val);
						if(match(val,\"$separator\$\")){
							gsub(ref,full_vn,tmpval);
						} else if (length(tmpval) > 0) {
							printf(\"%s=\\\"%s\\\"\n\", val, tmpval);
						}
						assignment[val]=tmpval;
					}
				}
			}
		} else if (length(value) > 0) {
			printf(\"%s=\\\"%s\\\"\n\", full_vn, value);
		}
	}END{
		for(val in assignment){
			if(match(val,\"$separator\$\"))
			printf(\"%s=\\\"%s\\\"\n\", val, assignment[val]);
		}
	}"
}

function send_read()
{
	[ $VERBOSE -ge 2 ] && echo "Read> $1 [$2]"
	local CMD_FIELD=$1
	local CMD_FTYPE=$2
	local CMD_NAME=$(echo $CMD_FIELD | cut -d"=" -f1)
	local ATTRIB_NAME=$(echo "$CMD_NAME" | sed "s/_${CMD_FTYPE}_field//" | sed "s/attributes_//")
	local CMD_STR=$(echo $CMD_FIELD | cut -d"=" -f2 | tr -d '"')
	local VALUE_VAR=$(echo "${CMD_NAME}" | sed 's/_field/_value/' | tr [:lower:] [:upper:])
	[ $VERBOSE -ge 1 ] && echo "Read> ${ATTRIB_NAME} and $VALUE_VAR"
	# Add question mark for read
	local SEND_CMD="${CMD_STR}?"
	[ $VERBOSE -ge 1 ] && echo  "${SEND_CMD}"
	local REPLY=$({ echo -e "${SEND_CMD}${sentinel_string}\c"; sleep 2; } | nc -q 1 $SIMULATOR_HOST $SIMULATOR_PORT | tr -d '\0')
	if [ ! -z "$REPLY" ]
	then
		[ $VERBOSE -ge 1 ] && echo $REPLY
	else
		echo "[FAILED] ${ATTRIB_NAME} got no reply for '${SEND_CMD}'"
		return 1
	fi
	# Remove CR and LF
	local REPLY=$(echo $REPLY | sed 's/[\r\n]*$//')
	# Check for binary data
	if [[ $REPLY = *[![:print:]]* ]]
	then
		REPLY=$(echo $REPLY | tr -dc [:alnum:])
		REPLY="binary ${REPLY}"
	fi
	eval $VALUE_VAR=\"$REPLY\"
	local REPLY_LEN=$(echo -e "$REPLY\c" | wc -c)
	if [ $REPLY_LEN -gt 45 ]
	then
		echo -e "[  OK  ] ${ATTRIB_NAME} write '${SEND_CMD}' \c"
		echo -e "read '$(echo ${REPLY} | cut -c1-45)...' \c"
		echo "(${REPLY_LEN} bytes)"
	else
		echo "[  OK  ] ${ATTRIB_NAME} write '${SEND_CMD}' read '${REPLY}' (${REPLY_LEN} bytes)"
	fi
	return 0
}

if [ "$1" = "-h" ]
then
	echo "Usage: $0 -v [YAML_FILE]"
	exit 0
fi

VERBOSE=0
if [ "$1" = "-v" ]
then
	VERBOSE=1
	YAML_FILE=$2
elif [ "$1" = "-V" ]
then
	VERBOSE=2
	YAML_FILE=$2
else
	YAML_FILE=$1
fi
# YAML_FILE=src/ska_ser_test_equipment/spectrum_analyser/anritsu_ms2090a.yaml
if [ -z $YAML_FILE ]
then
	if [ "$SIMULATOR_MODEL" != "" ]
	then
		[ $VERBOSE -ge 1 ] && echo "Use model $SIMULATOR_MODEL"
		YAML_FILE=$(find ./src -name "*.yaml" -exec grep -l "model: $SIMULATOR_MODEL" {} \;)
		if [ -z "$YAML_FILE" ]
		then
			echo "[FAILED] YAML file name for $SIMULATOR_MODEL not found"
			exit 1
		fi
		[ $VERBOSE -ge 1 ] && echo "Read $YAML_FILE"
	else
		echo "[FAILED] YAML file name not specified and SIMULATOR_MODEL not set"
		exit 1
	fi
fi
if [ -z $SIMULATOR_HOST ]
then
	SIMULATOR_HOST="localhost"
fi
if [ -z $SIMULATOR_PORT ]
then
	SIMULATOR_PORT=9001
fi
if [ ! -r $YAML_FILE ]
then
	echo "[FAILED] Could not read $YAML_FILE"
	exit 1
fi

[ $VERBOSE -ge 1 ] && echo "Host $SIMULATOR_HOST port $SIMULATOR_PORT"
nc -zv $SIMULATOR_HOST $SIMULATOR_PORT >/dev/null 2>&1
if [ $? -ne 0 ]
then
	echo "[FAILED] Could not connect to $SIMULATOR_HOST:$SIMULATOR_PORT"
	exit 1
fi
echo -e "[  OK  ] \c"
nc -zv $SIMULATOR_HOST $SIMULATOR_PORT

eval $(parse_yaml $YAML_FILE)
echo "[  OK  ] Read file $YAML_FILE"

[ $VERBOSE -ge 2 ] && parse_yaml $YAML_FILE
# [ $VERBOSE -ge 2 ] && env
if [ "${argument_separator}" = "" ]
then
	argument_separator=" "
	echo "[ WARN ] Argument separator not set, use '${argument_separator}'"
else
	echo "[  OK  ] Argument separator is '${argument_separator}'"
fi
if [ -z ${sentinel_string} ]
then
	echo "[FAILED] Sentinel string not set"
	exit 1
fi
echo "[  OK  ] Sentinel string is '${sentinel_string}'"
[ $VERBOSE -ge 1 ] && echo "---------------------------------------"

# grep field: $YAML_FILE | cut -d: -f2- | cut -c2- | sort -u | tr -d '"' | while read CMD
FTYPE="read"
parse_yaml $YAML_FILE | grep "_${FTYPE}_field=" | while read CMD
do
	send_read $CMD $FTYPE
	[ $VERBOSE -ge 1 ] && echo "---------------------------------------"
done

FTYPE="read_write"
parse_yaml $YAML_FILE | grep "_${FTYPE}_field=" | while read CMD
do
	send_read $CMD $FTYPE
	[ $VERBOSE -ge 1 ] && echo "---------------------------------------"
done

