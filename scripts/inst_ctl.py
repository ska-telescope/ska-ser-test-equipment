#!/usr/bin/python
import argparse
import logging
import os
import sys
from socket import gethostname

from ska_ser_test_equipment.scpi.scpi_config import (
    get_scpi_read_write,
)

from ska_ser_test_equipment.oscilloscope.tektronix_mso import TektronixMso, SCOPE_ZA
from ska_ser_test_equipment.scpi.virtual_instrument import VirtualInstrument

logging.basicConfig()
logger = logging.getLogger(__name__)
log_level_str = os.getenv("LOG_LEVEL", "WARNING").upper()
if log_level_str == "DEBUG":
    LOG_LEVEL = logging.DEBUG
elif log_level_str == "INFO":
    LOG_LEVEL = logging.INFO
elif log_level_str == "WARNING":
    LOG_LEVEL = logging.WARNING
elif log_level_str == "ERROR":
    LOG_LEVEL = logging.ERROR
else:
    LOG_LEVEL = logging.WARNING
logger.setLevel(LOG_LEVEL)


def main():
    sentinel_string: str = "\r\n"
    timeout: float = 10.0
    model = os.getenv("SIMULATOR_MODEL")
    if model is None:
        # logger.error("Model not specified in SIMULATOR_MODEL")
        return
    model = model.upper()

    # Read configuration
    scpi_read, scpi_write = get_scpi_read_write(model)
    parser = argparse.ArgumentParser(
        prog="inst_ctl",
        description="Send SCPI commands to instrument",
        epilog="(c) SKAO 2023",
        allow_abbrev=True,
    )
    parser.add_argument("-v", action="store_true", help="Set logging level to INFO")
    parser.add_argument("-V", action="store_true", help="Set logging level to DEBuG")
    parser.add_argument("-A", action="store_true", help="Send all read commands")
    parser.add_argument("-H", help="Set host name")
    parser.add_argument("-P", help="Set port number")

    for attribute in scpi_read:
        # if "*" in attribute:
        field = scpi_read[attribute][0]
        arg_name = f"--{attribute.replace('_', '-')}"
        arg_help = f"{attribute[0].upper()}{attribute[1:].replace('_', ' ')}"

        value = scpi_read[attribute][1]
        ftype = scpi_read[attribute][2]
        logger.debug("Add read argument %s (type %s) value %s", arg_name, ftype, value)
        if ftype in ("int", "float", "str"):
            parser.add_argument(arg_name, help=arg_help)
        elif ftype in ("bool", "bit"):
            parser.add_argument(arg_name, action="store_true", help=arg_help)
        elif value:
            parser.add_argument(arg_name, help=arg_help)
        else:
            parser.add_argument(arg_name, help=arg_help)
    for attribute in scpi_write:
        if attribute in scpi_read:
            logger.debug("Skip attribute %s already added", attribute)
            continue
        field = scpi_write[attribute][0]
        arg_name = f"--{attribute.replace('_', '-')}"
        arg_help = f"{attribute[0].upper()}{attribute[1:].replace('_', ' ')}"

        value = scpi_write[attribute][1]
        logger.debug("Add write argument %s value %s", arg_name, value)
        if value:
            parser.add_argument(arg_name, help=field_help)
        else:
            parser.add_argument(arg_name, action="store_true", help=arg_help)
    args = parser.parse_args()
    vargs = vars(args)

    if vargs["v"] == True:
        logger.setLevel(logging.INFO)
    if vargs["V"] == True:
        logger.setLevel(logging.DEBUG)
    logger.debug("Arguments : %s", vargs)

    logger.info("Read fields: %s", scpi_read)
    logger.info("Write fields: %s", scpi_write)

    if vargs["H"] is not None:
        host = vargs["H"]
    else:
        host = os.getenv("SIMULATOR_HOST", gethostname())
    if vargs["P"] is not None:
        port = int(vargs["P"])
    else:
        port = int(os.getenv("SIMULATOR_PORT", "4000"))
    logger.info("Connect to model %s on %s:%d", model, host, port)

    scpi_cmds = {}
    if vargs["A"] == True:
        for attribute in scpi_read:
            scpi_cmd = f"{scpi_read[attribute][0]}?"
            logger.debug("Add '%s'" % scpi_cmd)
            scpi_cmds[attribute] = scpi_cmd
    else:
        for arg in vargs:
            logger.debug("Argument %s : %s", arg, vargs[arg])
            if arg in ("v", "V", "H", "P"):
                continue
            elif vargs[arg] is None:
                # logger.debug("Argument %s", arg)
                continue
            elif not vargs[arg]:
                continue
            elif vargs[arg] == True:
                scpi_cmd = f"{scpi_write[arg][0]}?"
            elif vargs[arg] == "?":
                scpi_cmd = f"{scpi_read[arg][0]}?"
            else:
                scpi_cmd = f"{scpi_read[arg][0]} {vargs[arg]}"
            logger.info("Send %s : %s", arg, scpi_cmd)
            scpi_cmds[arg] = scpi_cmd

    # Instantiate oscilloscope handle
    try:
        inst = VirtualInstrument(
            logger,
            host,
            port,
            sentinel_string,
            timeout,
            False,
        )
    except AttributeError as v_err:
        logger.error(v_err)
        sys.exit(1)

    for arg in scpi_cmds:
        inst.print_query(arg, scpi_cmds[arg])


if __name__ == "__main__":
    main()
