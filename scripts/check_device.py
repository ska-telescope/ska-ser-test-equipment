#!/usr/bin/python3
""" Test Tango device."""
import os
import sys
import tango
import time
from ska_control_model import AdminMode

from ska_ser_test_equipment.oscilloscope import OscilloscopeSimulator
from ska_ser_test_equipment.spectrum_analyser import (
    SpectrumAnalyserSimulator,
)
from ska_ser_test_equipment.spectrum_analyser_anritsu import (
    SpectrumAnalyserSimulatorAnritsu,
)

ATTRIBUTES_BUILTIN = [
    "versionId",
    "buildState",
    "adminMode",
    "loggingLevel",
    "loggingTargets",
    "healthState",
    "controlMode",
    "simulationMode",
    "testMode",
    "longRunningCommandsInQueue",
    "longRunningCommandIDsInQueue",
    "longRunningCommandStatus",
    "longRunningCommandProgress",
    "longRunningCommandResult",
    "State",
    "Status",
]

ATTRIBUTES_TESTED = {
    "MS2090A": [
        "add_marker",
        "attenuation",
        "autoattenuation",
        "command_error",
        "continuous",
        "device_error",
        "execution_error",
        "frequency_start",
        "frequency_stop",
        "flag_when_complete",
        "marker_frequency",
        "marker_power",
        "rbw",
        "rbw_auto",
        "power_cycled",
        "preamp_enabled",
        "query_error",
        "reference_level",
        "sweep_points",
        "trace1",
        "trace_format",
        "vbw",
        "vbw_auto",
    ],
    "SPECMON26B": [
        "autoattenuation",
        "attenuation",
        "continuous",
        "frequency_start",
        "frequency_stop",
        "trace1",
        "trace1_detection",
        "trace1_function",
        "trace1_average_count",
        "marker_frequency",
        "marker_power",
        "query_error",
        "device_error",
        "execution_error",
        "command_error",
        "power_cycled",
        "rbw",
        "rbw_actual",
        "rbw_auto",
        "rbw_enabled",
        "vbw",
        "vbw_enabled",
        "reference_level",
        "preamp_enabled",
    ],
    "MSO64B": [
        "acquisition",
        "acquisition_mode",
        "binary_field_width",
        "busy",
        "ch1_attenuation",
        "ch1_bandwidth",
        "ch1_clipping",
        "ch1_coupling",
        "ch1_display_vertical_scale",
        "ch1_ext_attenuation",
        "ch1_offset",
        "ch1_parameters",
        "ch1_position",
        "ch1_probe_gain",
        "ch1_probe_units",
        "ch1_select",
        "ch1_trigger_a_level",
        "ch1_trigger_b_level",
        "ch1_vertical_position",
        "ch1_vertical_scale",
        "ch1_vertical_scale_ratio",
        "current_settings",
        "date",
        "data_encoding",
        "data_source",
        "data_source_available",
        "data_start",
        "data_stop",
        "data_width",
        "display_select_source",
        "display_select_view",
        "display_spectrum_source",
        "horizontal_delay_mode",
        "horizontal_delay_time",
        "horizontal_divisions",
        "horizontal_duration",
        "horizontal_increment",
        "horizontal_mode",
        "horizontal_position",
        "horizontal_sample_rate",
        "horizontal_scale",
        # "sample_rate",
        "sweep_points",
        "time",
        "trace_data",
        "trace_format",
        "trace_parameters",
        "trace_source",
        "vertical_offset",
        "vertical_position",
        "vertical_scale",
    ],
}
ATTRIBUTES_OTHER = {
    "MS2090A": [
        "operation_complete",
    ],
    "SPECMON26B": [
        "operation_complete",
    ],
    "MSO64B": [
        "operation_complete",
    ],
}
ATTRIBUTES_SKIP = {
    "MS2090A": [
        "identity",
    ],
    "SPECMON26B": [
        "identity",
    ],
    "MSO64B": [
        "identity",
    ],
}
ATTRIBUTES_IDENTITY = {
    "MS2090A": "Anritsu,MS2090A,F00BAA7,V2038.1.19",
    "SPECMON26B": "TEKTRONIX,SPECMON26B,PQ00112, FV:3.9.0031.0",
    "MSO64B": "TEKTRONIX,MSO64B,C047394,CF:91.1CT FV:1.44.3.433",
}
VERBOSE = False
FAIL_EXIT = True

def test_builtin(tango_dev):
    """
    Test builtin attributes.

    :param tango_dev: Tango device
    """
    print(f"[  OK  ] versionId                    : {tango_dev.versionId}")
    print(f"[  OK  ] buildState                   : {tango_dev.buildState}")
    print(f"[  OK  ] loggingLevel                 : {tango_dev.loggingLevel}")
    print(f"[  OK  ] loggingTargets               : {tango_dev.loggingTargets}")
    print(f"[  OK  ] healthState                  : {tango_dev.healthState}")
    print(f"[  OK  ] controlMode                  : {tango_dev.controlMode}")
    print(f"[  OK  ] simulationMode               : {tango_dev.simulationMode}")
    print(f"[  OK  ] testMode                     : {tango_dev.testMode}")
    print(
        f"[  OK  ] longRunningCommandsInQueue   : {tango_dev.longRunningCommandsInQueue}"
    )
    print(
        f"[  OK  ] longRunningCommandIDsInQueue : {tango_dev.longRunningCommandIDsInQueue}"
    )
    print(
        f"[  OK  ] longRunningCommandStatus     : {tango_dev.longRunningCommandStatus}"
    )
    print(
        f"[  OK  ] longRunningCommandProgress   : {tango_dev.longRunningCommandProgress}"
    )
    print(
        f"[  OK  ] longRunningCommandResult     : {tango_dev.longRunningCommandResult}"
    )


def test_attributes(tango_dev):
    """
    Test attributes.

    :param tango_dev: Tango device
    """
    print(f"[  OK  ] attenuation          : {tango_dev.attenuation}")
    print(f"[  OK  ] autoattenuation      : {tango_dev.autoattenuation}")
    print(f"[  OK  ] command_error        : {tango_dev.command_error}")
    print(f"[  OK  ] continuous           : {tango_dev.continuous}")
    print(f"[  OK  ] device_error         : {tango_dev.device_error}")
    print(f"[  OK  ] execution_error      : {tango_dev.execution_error}")
    print(f"[  OK  ] frequency_start      : {tango_dev.frequency_start}")
    print(f"[  OK  ] frequency_stop       : {tango_dev.frequency_stop}")
    print(f"[  OK  ] identity             : {tango_dev.identity}")
    # print(f"[  OK  ] marker_frequency     : {tango_dev.marker_frequency}")
    # print(f"[  OK  ] marker_power         : {tango_dev.marker_power}")
    print(f"[  OK  ] operation_complete   : {tango_dev.operation_complete}")
    print(f"[  OK  ] rbw                  : {tango_dev.rbw}")
    print(f"[  OK  ] power_cycled         : {tango_dev.power_cycled}")
    print(f"[  OK  ] preamp_enabled       : {tango_dev.preamp_enabled}")
    print(f"[  OK  ] query_error          : {tango_dev.query_error}")
    print(f"[  OK  ] reference_level      : {tango_dev.reference_level}")
    print(f"[  OK  ] sweep_points         : {tango_dev.sweep_points}")
    print(f"[  OK  ] trace1               : {tango_dev.trace1}")
    print(f"[  OK  ] trace_format        : {tango_dev.trace_format}")
    # print(f"[  OK  ] vbw                  : {tango_dev.vbw}")


def test_attribute(tango_dev, attribute_name):
    attr_info = tango_dev.read_attribute(attribute_name)
    attr_value = attr_info.value
    if attr_info.dim_x > 2:
        print(f"[  OK  ] {attribute_name} has {len(attr_value)} values")
    else:
        print(f"[  OK  ] {attribute_name} : {attr_value}")
    if VERBOSE: print(f"{attr_info}")


def test_attribute_value(tango_dev, attribute_name, initial_value=None):
    attr_info = tango_dev.read_attribute(attribute_name)
    attr_value = attr_info.value
    if initial_value is None and attr_value is not None:
        print(f"[ WARN ] {attribute_name} has no initial value")
        print(f"[  OK  ] {attribute_name} : {attr_value}")
    elif attr_value is None and initial_value is not None:
        print(f"[FAILED] {attribute_name} has no value")
    elif attr_value is None:
        print(f"[FAILED] {attribute_name} has no value")
    elif attr_value != initial_value:
        print(
            f"[FAILED] {attribute_name} value is {attr_value} but it should be {initial_value}"
        )
    else:
        print(f"[  OK  ] {attribute_name} : {attr_value}")
    if VERBOSE: print(f"{attr_info}")


def main(dev_model, dev_host, dev_port, dev_name):
    """
    Start here.

    :param dev_host: hostname or IP address
    :param dev_port: TCP port number
    :param dev_name: Tango triplet, e.g. mid-itf/spectana/2
    """
    # dev = tango.DeviceProxy("tango://127.0.0.1:45450/mid-itf/spectana/2#dbase=no")
    dev_connect = f"tango://{dev_host}:{dev_port}/{dev_name}#dbase=no"
    try:
        dev = tango.DeviceProxy(dev_connect)
    except Exception:
        print(f"[FAILED] Could not connect to {dev_connect}")
        sys.exit(1)
    print(f"[  OK  ] connect to {dev_connect}")

    chk_name = dev.dev_name()
    if chk_name != dev_name:
        print(f"[FAILED] device name shouuld be {dev_name}, not {chk_name}")
        sys.exit(1)
    print(f"[  OK  ] Device name is {chk_name}")

    try:
        admin = dev.adminMode
    except Exception:
        print("[FAILED] could not read admin mode")
        sys.exit(1)
    if admin == AdminMode.OFFLINE:
        print("[  OK  ] admin mode is OFFLINE")
    else:
        dev.adminMode = AdminMode.OFFLINE
        print("[  OK  ] admin mode set to OFFLINE")

    try:
        dev.adminMode = AdminMode.ONLINE
    except AttributeError as dev_err:
        print(f"[FAILED] could not set admin mode to ONLINE")
        # print(f"[FAILED] {dev_err}")
        if FAIL_EXIT:
            sys.exit(1)
    print(f"[  OK  ] admin mode set to ONLINE")

    if dev.State() == tango._tango.DevState.UNKNOWN:
        print(f"[  OK  ] status {dev.Status()} (state {dev.State()})")
        if FAIL_EXIT:
            time.sleep(15)

    if dev.State() != tango._tango.DevState.ON:
        print(f"[ WARN ] status {dev.Status()} (state {dev.State()})")
        if FAIL_EXIT:
            time.sleep(15)

    if dev.State() != tango._tango.DevState.ON:
        print(f"[FAILED] status {dev.Status()} (state {dev.State()})")
        if FAIL_EXIT:
            sys.exit(1)

    print(f"[  OK  ] {dev.Status()}")

    if dev_model not in ATTRIBUTES_IDENTITY:
        print(f"[FAILED] model {dev_model} is unknown")
        if FAIL_EXIT:
            sys.exit(1)
    test_attribute_value(dev, "identity", ATTRIBUTES_IDENTITY[model])

    attributes = sorted(dev.get_attribute_list())
    # print(f"[  OK  ] Atrributes : {' '.join(attributes)}")
    print(f"[  OK  ] got {len(attributes)} attributes")

    # test_builtin(dev)
    if dev_model == "MS2090A":
        initial_values_map = SpectrumAnalyserSimulatorAnritsu.DEFAULTS.copy()
    if dev_model == "MSO64B":
        initial_values_map = OscilloscopeSimulator.DEFAULTS.copy()
    else:
        initial_values_map = {}
    print(f"[  OK  ] got {len(initial_values_map)} initial values")

    for attribute in attributes:
        if attribute in ATTRIBUTES_SKIP[model]:
            pass
        elif attribute in ATTRIBUTES_BUILTIN:
            test_attribute(dev, attribute)
        elif attribute in ATTRIBUTES_OTHER[model]:
            test_attribute(dev, attribute)
        elif attribute not in ATTRIBUTES_TESTED[model]:
            print(f"[ WARN ] untested attribute {attribute}")
        elif attribute not in initial_values_map:
            # print(f"[ WARN ] no initial value for {attribute}")
            test_attribute_value(dev, attribute)
        else:
            initial_value = initial_values_map[attribute]
            test_attribute_value(dev, attribute, initial_value)

    dev.adminMode = AdminMode.OFFLINE
    print("[  OK  ] admin mode set to OFFLINE")

if __name__ == "__main__":
    try:
        if sys.argv[1] == "-v":
            VERBOSE = True
    except IndexError:
        pass
    host = "127.0.0.1"
    port = 45450
    tobj = "mid-itf/scope/1"
    model = os.getenv("SIMULATOR_MODEL", "MSO64B").upper()

    try:
        main(model, host, port, tobj)
    except KeyboardInterrupt:
        print("\n[FAILED] Interrupted")
