# ska-ser-test-equipment

This project provides Tango device access to test equipment hardware
within the [Square Kilometre Array](https://skatelescope.org/).

Documentation
-------------

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-ser-test-equipment/badge/?version=latest)](https://developer.skao.int/projects/ska-ser-test-equipment/en/latest/?badge=latest)

The documentation for this project, including how to get started with it, can be found in the `docs` folder, or browsed in the SKA development portal:

* [ska-ser-test-equipment documentation](https://developer.skatelescope.org/projects/ska-ser-test-equipment/en/latest/index.html "SKA Developer Portal: ska-ser-test-equipment documentation")

Running and testing programmable attenuator without Tango database
------------------------------------------------------------------

SCPI simulator:

```
SIMULATOR_HOST=127.0.0.1 SIMULATOR_PORT=22333 SIMULATOR_MODEL=RC1DAT800030 python python ./src/ska_ser_test_equipment/prog_attenuator/prog_attenuator_simulator.py
```

Tango device server (note that 'telnet instead of 'tcp' does not work):

```
SIMULATOR_HOST=127.0.0.1 SIMULATOR_PORT=22333 SIMULATOR_MODEL=RC1DAT800030 SIMULATOR_PROTOCOL=tcp \
python ./src/ska_ser_test_equipment/prog_attenuator/prog_attenuator_device.py progattenuator -v2 \
-host 127.0.0.1 -port 45450 -nodb -dlist mid-itf/progattenuator/2
```

Tango client:

```
$ itango3

In [4]: dp = tango.DeviceProxy("tango://127.0.0.1:45450/mid-itf/progattenuator/2#dbase=no")

In [5]: dp.get_attribute_list()
Out[5]: ['buildState', 'versionId', 'loggingLevel', 'loggingTargets', 'healthState', 'adminMode', 'controlMode', 'simulationMode', 'testMode', 'longRunningCommandsInQueue', 'longRunningCommandIDsInQueue', 'longRunningCommandStatus', 'longRunningCommandProgress', 'longRunningCommandResult', 'State', 'Status']
```

Running and testing programmable attenuator with local Tango database
---------------------------------------------------------------------


Hosted Tango database:
```
export TANGO_HOST="10.164.10.5:10000"
```

Local Tango database:

```
/usr/local/bin/DataBaseds 2 -ORBendPoint giop:tcp::10000
```

SCPI simulator:

```
SIMULATOR_HOST=127.0.0.1 ./src/ska_ser_test_equipment/prog_attenuator/prog_attenuator_simulator.py
```

Tango device server:

```
SIMULATOR_HOST=127.0.0.1 ./src/ska_ser_test_equipment/prog_attenuator/prog_attenuator_device.py progattenuator -ORBendPoint giop:tcp:0.0.0.0:22333
```

Running and testing programmable attenuator with hosted Tango database
----------------------------------------------------------------------

Set host name for Tango database:
```
export TANGO_HOST="10.164.10.5:10000"
```

Tango device server:

```
SIMULATOR_HOST=127.0.0.1 ./src/ska_ser_test_equipment/prog_attenuator/prog_attenuator_device.py progattenuator -ORBendPoint giop:tcp:0.0.0.0:45450
```

Running kubernetes
------------------

Stert docker and minikube:

```
export DOCKER_TLS_VERIFY="1"
export DOCKER_HOST="tcp://192.168.58.2:2376"
export DOCKER_CERT_PATH="$HOME/.minikube/certs"
export MINIKUBE_ACTIVE_DOCKERD="minikube"

eval $(minikube -p minikube docker-env)

sudo systemctl start docker
systemctl status -l --no-pager docker

minikube start

```

Deploy Helm chart:

```
make k8s-delete-namespace
kubectl create namespace ska-ser-test-equipment
export K8S_UMBRELLA_CHART_PATH=./charts/test-parent/charts/ska-ser-test-equipment-0.9.1-dev.tgz
export HELM_RELEASE=ska-ser-test-equipment
export HELM_RELEASE=test

TANGO_HOST=tango-databaseds:10000 make k8s-install-chart
```

If the below fails (change c1d16131e to match the latest git hash):

```
helm upgrade --install test --set global.tango_host=tango-databaseds:10000 --set global.minikube=true --set test_equipment.image.registry=registry.gitlab.com/ska-telescope/ska-ser-test-equipment --set test_equipment.image.tag=0.8.3-dev.c1d16131e ./charts/test-parent/ --namespace ska-ser-test-equipment

```

Check namespaces:

```
$ kubectl get namespaces
NAME                     STATUS   AGE
default                  Active   7h4m
kube-node-lease          Active   7h4m
kube-public              Active   7h4m
kube-system              Active   7h4m
ska-ser-test-equipment   Active   6h55m
```

List pods

```
$ kubectl get pod --namespace ska-ser-test-equipment
NAME                                        READY   STATUS              RESTARTS   AGE
awg-awg5208-0                               0/1     Init:0/2            0          27s
awg-test-config-jlwcs                       0/1     Completed           0          27s
progattenuator-rc1dat800030-0               0/1     Init:0/2            0          27s
progattenuator-rc4dat6g95-0                 0/1     Init:0/2            0          25s
progattenuator-test-config-7chzh            0/1     Completed           0          27s
signalgenerator-smb100a-0                   0/1     Init:0/2            0          25s
signalgenerator-test-config-l7qmc           0/1     Completed           0          27s
signalgenerator-tsg4104a-0                  0/1     Init:0/2            0          27s
simulator-awg5208-0                         0/1     ContainerCreating   0          27s
simulator-ms2090a-0                         0/1     ContainerCreating   0          27s
simulator-rc1dat800030-0                    0/1     ContainerCreating   0          27s
simulator-rc2sp6ta12-0                      0/1     ContainerCreating   0          25s
simulator-rc4dat6g95-0                      0/1     ContainerCreating   0          27s
simulator-smb100a-0                         0/1     ContainerCreating   0          26s
simulator-specmon26b-0                      0/1     ContainerCreating   0          25s
simulator-tsg4104a-0                        0/1     ContainerCreating   0          26s
simulator-ztrc4spdta18-0                    0/1     ContainerCreating   0          26s
simulator-ztrc8spdta18-0                    0/1     ContainerCreating   0          26s
ska-tango-base-itango-console               0/1     PodInitializing     0          27s
ska-tango-base-tangodb-0                    1/1     Running             0          27s
sp6t-rc2sp6ta12-0                           0/1     Init:0/2            0          26s
sp6t-test-config-rbc7s                      0/1     Completed           0          27s
spdt-test-config-85fkh                      0/1     Completed           0          27s
spdt-ztrc4spdta18-0                         0/1     Init:0/2            0          25s
spdt-ztrc8spdta18-0                         0/1     Init:0/2            0          25s
spectrumanalyser-specmon26b-0               0/1     Init:0/2            0          27s
spectrumanalyser-test-config-8xk8k          0/1     Completed           0          27s
spectrumanalyseranritsu-ms2090a-0           0/1     Init:0/2            0          25s
spectrumanalyseranritsu-test-config-cfsdp   0/1     Completed           0          27s
tango-databaseds-0                          1/1     Running             0          27s
tangotest-test-0                            0/1     PodInitializing     0          27s
tangotest-test-config-wg266                 0/1     Completed           0          27s
```

List services:

```
$ kubectl get svc --namespace ska-ser-test-equipment
NAME                              TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)                                           AGE
awg-awg5208                       LoadBalancer   10.105.160.98    <pending>     45450:31469/TCP,45460:30492/TCP,45470:31047/TCP   2m26s
progattenuator-rc1dat800030       LoadBalancer   10.103.129.194   <pending>     45450:30981/TCP,45460:31841/TCP,45470:32091/TCP   2m26s
progattenuator-rc4dat6g95         LoadBalancer   10.98.194.105    <pending>     45450:32700/TCP,45460:32258/TCP,45470:31973/TCP   2m26s
signalgenerator-smb100a           LoadBalancer   10.101.194.234   <pending>     45450:31992/TCP,45460:30764/TCP,45470:31927/TCP   2m26s
signalgenerator-tsg4104a          LoadBalancer   10.96.6.3        <pending>     45450:31778/TCP,45460:32192/TCP,45470:31279/TCP   2m26s
simulator-awg5208                 ClusterIP      10.106.184.218   <none>        5025/TCP                                          2m26s
simulator-ms2090a                 ClusterIP      10.104.157.135   <none>        9100/TCP                                          2m26s
simulator-rc1dat800030            ClusterIP      10.104.33.137    <none>        23/TCP                                            2m26s
simulator-rc2sp6ta12              ClusterIP      10.97.35.191     <none>        5025/TCP                                          2m26s
simulator-rc4dat6g95              ClusterIP      10.102.18.65     <none>        23/TCP                                            2m26s
simulator-smb100a                 ClusterIP      10.103.121.109   <none>        5025/TCP                                          2m26s
simulator-specmon26b              ClusterIP      10.101.190.37    <none>        5025/TCP                                          2m26s
simulator-tsg4104a                ClusterIP      10.111.54.113    <none>        5025/TCP                                          2m26s
simulator-ztrc4spdta18            ClusterIP      10.102.104.26    <none>        5025/TCP                                          2m26s
simulator-ztrc8spdta18            ClusterIP      10.103.170.144   <none>        5025/TCP                                          2m26s
ska-tango-base-tangodb            NodePort       10.108.63.80     <none>        3306:31669/TCP                                    2m26s
sp6t-rc2sp6ta12                   LoadBalancer   10.102.182.246   <pending>     45450:30357/TCP,45460:32313/TCP,45470:30736/TCP   2m26s
spdt-ztrc4spdta18                 LoadBalancer   10.105.171.220   <pending>     45450:30770/TCP,45460:31003/TCP,45470:30313/TCP   2m26s
spdt-ztrc8spdta18                 LoadBalancer   10.100.15.77     <pending>     45450:32427/TCP,45460:30832/TCP,45470:30050/TCP   2m26s
spectrumanalyser-specmon26b       LoadBalancer   10.106.35.42     <pending>     45450:31148/TCP,45460:32142/TCP,45470:30859/TCP   2m26s
spectrumanalyseranritsu-ms2090a   LoadBalancer   10.100.108.97    <pending>     45450:31771/TCP,45460:30563/TCP,45470:32301/TCP   2m26s
tango-databaseds                  LoadBalancer   10.103.15.148    <pending>     10000:31906/TCP                                   2m26s
tangotest-test                    LoadBalancer   10.109.202.150   <pending>     45450:30261/TCP,45460:30539/TCP,45470:30369/TCP   2m26s

```

Start tunnel to connect to IP addresses above:

```
$ minikube tunnel
Status:
        machine: minikube
        pid: 3055931
        route: 10.96.0.0/12 -> 192.168.49.2
        minikube: Running
        services: [awg-awg5208, progattenuator-rc1dat800030, progattenuator-rc4dat6g95, signalgenerator-smb100a, signalgenerator-tsg4104a, sp6t-rc2sp6ta12, spdt-ztrc4spdta18, spdt-ztrc8spdta18, spectrumanalyser-specmon26b, tango-databaseds, tangotest-test]
    errors:
                minikube: no errors
                router: no errors
                loadbalancer emulator: no errors
```

Ruuning k9s:

```
Context: minikube                                 <0> all                      <a>… ____  __.________
Cluster: minikube                                 <1> ska-ser-test-equipment   <ctr|    |/ _/   __   \______
User:    minikube                                 <2> default                  <d> |      < \____    /  ___/
K9s Rev: v0.27.3 ⚡️v0.27.4                                                      <e> |    |  \   /    /\___ \
K8s Rev: v1.27.3                                                               <?> |____|__ \ /____//____  >
CPU:     n/a                                                                   <ctr        \/            \/
MEM:     n/a
┌───────────────────────────────────── Pods(ska-ser-test-equipment)[32] ─────────────────────────────────────┐
│ NAME↑                                      PF READY RESTARTS STATUS            IP            NODE      AGE │
│ awg-awg5208-0                              ●  1/1          0 Running           10.244.1.178  minikube  10m │
│ awg-test-config-jlwcs                      ●  0/1          0 Completed         10.244.1.187  minikube  10m │
│ progattenuator-rc1dat800030-0              ●  1/1          0 Running           10.244.1.180  minikube  10m │
│ progattenuator-rc4dat6g95-0                ●  1/1          0 Running           10.244.1.192  minikube  10m │
│ progattenuator-test-config-7chzh           ●  0/1          0 Completed         10.244.1.179  minikube  10m │
│ signalgenerator-smb100a-0                  ●  1/1          0 Running           10.244.1.200  minikube  10m │
│ signalgenerator-test-config-l7qmc          ●  0/1          0 Completed         10.244.1.177  minikube  10m │
│ signalgenerator-tsg4104a-0                 ●  1/1          0 Running           10.244.1.184  minikube  10m │
│ simulator-awg5208-0                        ●  1/1          0 Running           10.244.1.172  minikube  10m │
│ simulator-ms2090a-0                        ●  0/1          6 CrashLoopBackOff  10.244.1.171  minikube  10m │
│ simulator-rc1dat800030-0                   ●  1/1          0 Running           10.244.1.175  minikube  10m │
│ simulator-rc2sp6ta12-0                     ●  1/1          0 Running           10.244.1.195  minikube  10m │
│ simulator-rc4dat6g95-0                     ●  1/1          0 Running           10.244.1.176  minikube  10m │
│ simulator-smb100a-0                        ●  1/1          0 Running           10.244.1.194  minikube  10m │
│ simulator-specmon26b-0                     ●  1/1          0 Running           10.244.1.191  minikube  10m │
│ simulator-tsg4104a-0                       ●  1/1          0 Running           10.244.1.193  minikube  10m │
│ simulator-ztrc4spdta18-0                   ●  1/1          0 Running           10.244.1.188  minikube  10m │
│ simulator-ztrc8spdta18-0                   ●  1/1          0 Running           10.244.1.196  minikube  10m │
│ ska-tango-base-itango-console              ●  1/1          0 Running           10.244.1.186  minikube  10m │
│ ska-tango-base-tangodb-0                   ●  1/1          0 Running           10.244.1.189  minikube  10m │
│ sp6t-rc2sp6ta12-0                          ●  1/1          0 Running           10.244.1.190  minikube  10m │
│ sp6t-test-config-rbc7s                     ●  0/1          0 Completed         10.244.1.185  minikube  10m │
│ spdt-test-config-85fkh                     ●  0/1          0 Completed         10.244.1.183  minikube  10m │
│ spdt-ztrc4spdta18-0                        ●  1/1          0 Running           10.244.1.198  minikube  10m │
│ spdt-ztrc8spdta18-0                        ●  1/1          0 Running           10.244.1.199  minikube  10m │
│ spectrumanalyser-specmon26b-0              ●  1/1          0 Running           10.244.1.170  minikube  10m │
│ spectrumanalyser-test-config-8xk8k         ●  0/1          0 Completed         10.244.1.182  minikube  10m │
│ spectrumanalyseranritsu-ms2090a-0          ●  0/1          6 CrashLoopBackOff  10.244.1.197  minikube  10m │
└────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
<pod>
```

Useful commands:

```
$ minikube service --namespace ska-ser-test-equipment --all
```

## Tango device server

### Log

```
1|2023-08-29T18:07:15.751Z|DEBUG|Polling thread|marshall|marshaller.py#59|tango-device:mid-itf/spectana/1|Marshalling payload b'CALC:MARK1:STAT?' by appending sentinel b'\n'                                                            │
│ 1|2023-08-29T18:07:15.752Z|DEBUG|Polling thread|request|tcp.py#192|tango-device:mid-itf/spectana/1|TCP client sending request bytes 43414c433a4d41524b313a535441543f0a (raw string b'CALC:MARK1:STAT?\n')                                │
│ 1|2023-08-29T18:07:15.757Z|DEBUG|Polling thread|marshall|marshaller.py#59|tango-device:mid-itf/spectana/1|Marshalling payload b'POW:RF:ATT?' by appending sentinel b'\n'                                                                 │
│ 1|2023-08-29T18:07:15.757Z|DEBUG|Polling thread|request|tcp.py#192|tango-device:mid-itf/spectana/1|TCP client sending request bytes 504f573a52463a4154543f0a (raw string b'POW:RF:ATT?\n')                                               │
│ 1|2023-08-29T18:07:15.762Z|DEBUG|Polling thread|marshall|marshaller.py#59|tango-device:mid-itf/spectana/1|Marshalling payload b'POW:RF:ATT:AUTO?' by appending sentinel b'\n'                                                            │
│ 1|2023-08-29T18:07:15.762Z|DEBUG|Polling thread|request|tcp.py#192|tango-device:mid-itf/spectana/1|TCP client sending request bytes 504f573a52463a4154543a4155544f3f0a (raw string b'POW:RF:ATT:AUTO?\n')                                │
│ 1|2023-08-29T18:07:15.769Z|DEBUG|Polling thread|marshall|marshaller.py#59|tango-device:mid-itf/spectana/1|Marshalling payload b'*ESR?' by appending sentinel b'\n'                                                                       │
│ 1|2023-08-29T18:07:15.770Z|DEBUG|Polling thread|request|tcp.py#192|tango-device:mid-itf/spectana/1|TCP client sending request bytes 2a4553523f0a (raw string b'*ESR?\n')                                                                 │
│ 1|2023-08-29T18:07:15.775Z|DEBUG|Polling thread|marshall|marshaller.py#59|tango-device:mid-itf/spectana/1|Marshalling payload b'INIT:CONT?' by appending sentinel b'\n'                                                                  │
│ 1|2023-08-29T18:07:15.775Z|DEBUG|Polling thread|request|tcp.py#192|tango-device:mid-itf/spectana/1|TCP client sending request bytes 494e49543a434f4e543f0a (raw string b'INIT:CONT?\n')                                                  │
│ 1|2023-08-29T18:07:15.778Z|DEBUG|Polling thread|marshall|marshaller.py#59|tango-device:mid-itf/spectana/1|Marshalling payload b'FREQ:STAR?' by appending sentinel b'\n'                                                                  │
│ 1|2023-08-29T18:07:15.778Z|DEBUG|Polling thread|request|tcp.py#192|tango-device:mid-itf/spectana/1|TCP client sending request bytes 465245513a535441523f0a (raw string b'FREQ:STAR?\n')                                                  │
│ 1|2023-08-29T18:07:15.784Z|DEBUG|Polling thread|marshall|marshaller.py#59|tango-device:mid-itf/spectana/1|Marshalling payload b'FREQ:STOP?' by appending sentinel b'\n'                                                                  │
│ 1|2023-08-29T18:07:15.784Z|DEBUG|Polling thread|request|tcp.py#192|tango-device:mid-itf/spectana/1|TCP client sending request bytes 465245513a53544f503f0a (raw string b'FREQ:STOP?\n')                                                  │
│ 1|2023-08-29T18:07:15.791Z|DEBUG|Polling thread|marshall|marshaller.py#59|tango-device:mid-itf/spectana/1|Marshalling payload b'SENS:POW:RF:GAIN:STAT?' by appending sentinel b'\n'                                                      │
│ 1|2023-08-29T18:07:15.792Z|DEBUG|Polling thread|request|tcp.py#192|tango-device:mid-itf/spectana/1|TCP client sending request bytes 53454e533a504f573a52463a4741494e3a535441543f0a (raw string b'SENS:POW:RF:GAIN:STAT?\n')              │
│ 1|2023-08-29T18:07:15.795Z|DEBUG|Polling thread|marshall|marshaller.py#59|tango-device:mid-itf/spectana/1|Marshalling payload b'SENS:BAND:RES?' by appending sentinel b'\n'                                                              │
│ 1|2023-08-29T18:07:15.795Z|DEBUG|Polling thread|request|tcp.py#192|tango-device:mid-itf/spectana/1|TCP client sending request bytes 53454e533a42414e443a5245533f0a (raw string b'SENS:BAND:RES?\n')                                      │
│ 1|2023-08-29T18:07:15.801Z|DEBUG|Polling thread|marshall|marshaller.py#59|tango-device:mid-itf/spectana/1|Marshalling payload b'DISP:WIND:TRAC:Y:SCAL:RLEV?' by appending sentinel b'\n'                                                 │
│ 1|2023-08-29T18:07:15.801Z|DEBUG|Polling thread|request|tcp.py#192|tango-device:mid-itf/spectana/1|TCP client sending request bytes 444953503a57494e443a545241433a593a5343414c3a524c45563f0a (raw string b'DISP:WIND:TRAC:Y:SCAL:RLEV?\n │
│ 1|2023-08-29T18:07:15.808Z|DEBUG|Polling thread|marshall|marshaller.py#59|tango-device:mid-itf/spectana/1|Marshalling payload b'DISP:POIN?' by appending sentinel b'\n'                                                                  │
│ 1|2023-08-29T18:07:15.808Z|DEBUG|Polling thread|request|tcp.py#192|tango-device:mid-itf/spectana/1|TCP client sending request bytes 444953503a504f494e3f0a (raw string b'DISP:POIN?\n')                                                  │
│ 1|2023-08-29T18:07:15.812Z|DEBUG|Polling thread|marshall|marshaller.py#59|tango-device:mid-itf/spectana/1|Marshalling payload b'TRAC:DATA:ALL?' by appending sentinel b'\n'                                                              │
│ 1|2023-08-29T18:07:15.812Z|DEBUG|Polling thread|request|tcp.py#192|tango-device:mid-itf/spectana/1|TCP client sending request bytes 545241433a444154413a414c4c3f0a (raw string b'TRAC:DATA:ALL?\n')                                      │
│ 1|2023-08-29T18:07:35.844Z|DEBUG|Polling thread|marshall|marshaller.py#59|tango-device:mid-itf/spectana/1|Marshalling payload b'TRAC:DATA:ALL?' by appending sentinel b'\n'                                                              │
│ 1|2023-08-29T18:07:35.845Z|DEBUG|Polling thread|request|tcp.py#192|tango-device:mid-itf/spectana/1|TCP client sending request bytes 545241433a444154413a414c4c3f0a (raw string b'TRAC:DATA:ALL?\n')                                      │
│ 1|2023-08-29T18:07:55.873Z|DEBUG|Polling thread|marshall|marshaller.py#59|tango-device:mid-itf/spectana/1|Marshalling payload b'TRAC:DATA:ALL?' by appending sentinel b'\n'                                                              │
│ 1|2023-08-29T18:07:55.874Z|DEBUG|Polling thread|request|tcp.py#192|tango-device:mid-itf/spectana/1|TCP client sending request bytes 545241433a444154413a414c4c3f0a (raw string b'TRAC:DATA:ALL?\n')
```

## Mid ITF test instruments:

### Arbitrary Waveform Generator

Make  : Tektronics
Model : AWG5200
FQDN  : za-itf-awg.ad.skatelescope.org
IP    : 10.165.3.3
Port  : 4000
Web   : http://za-itf-awg.ad.skatelescope.org/Default.aspx

### Oscilloscope

Make  : Tektronix
Model : MSO64B
FQDN  : za-itf-oscilloscope.ad.skatelescope.org
IP    : 10.165.3.2
Port  : 4000
Web   : http://za-itf-oscilloscope.ad.skatelescope.org/

### Spectrum analyser

Make  : Anritsu
Model : MS2090A
FQDN  : za-itf-spectrum-analyser.ad.skatelescope.org
IP    : 10.165.3.4
Port  : 9001

### Programmable attenuator

Make  : Mini Circuits
Model : RCDAT-8000-30
FQDN  : za-itf-attenuator.ad.skatelescope.org
IP    : 10.165.3.6
Port  : ----

### Signal generator

Make  : Rohde and Schwarz
Model : SMB100A
FQDN  : za-itf-signal-generator.ad.skatelescope.org
IP    : 10.165.3.1
Port  : ----
Web   : http://za-itf-signal-generator.ad.skatelescope.org/webpages/web/html/ihp.php

## Dashboard

### Start up simulators and devices

```
SIMULATOR_HOST=127.0.0.1 SIMULATOR_PORT=9001 SIMULATOR_MODEL=MS2090A \
    python ./src/ska_ser_test_equipment/spectrum_analyser_anritsu/spectrum_analyser_simulator_anritsu.py

LOG_LEVEL=debug SIMULATOR_PORT=9001 SIMULATOR_MODEL=MS2090A SIMULATOR_HOST=127.0.0.1 \
    python ./src/ska_ser_test_equipment/spectrum_analyser_anritsu/spectrum_analyser_device_anritsu.py \
    spectana -v2 -host 127.0.0.1 -port 45450 -nodb -dlist mid-itf/spectana/1

SIMULATOR_HOST=127.0.0.1 SIMULATOR_PORT=5025 SIMULATOR_MODEL=TSG4104A \
    python ./src/ska_ser_test_equipment/signal_generator/signal_generator_simulator.py

SIMULATOR_PORT=5025 SIMULATOR_MODEL=TSG4104A SIMULATOR_HOST=127.0.0.1 \
    python ./src/ska_ser_test_equipment/signal_generator/signal_generator_device.py \
    siggen  -v2 -host 127.0.0.1 -port 45451 -nodb -dlist mid-itf/siggen/1
```

### Start Jupyter Notebook (on Linux)

```
konsole -qwindowtitle "Jupyter Notebook" -qwindowicon notebook -e ${HOME}/.local/bin/jupyter-notebook &
```
