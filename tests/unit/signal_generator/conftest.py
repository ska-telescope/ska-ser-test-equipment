"""Test harness for signal generator tests."""
import logging
import os
from typing import Any, Callable, Dict, Iterator, get_args

import pytest
import tango
import tango.test_context
from _pytest.fixtures import SubRequest  # for the type checker
from ska_ser_scpi import InterfaceDefinitionType, SupportedProtocol
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

from ska_ser_test_equipment.signal_generator import (
    SignalGeneratorComponentManager,
    SignalGeneratorSimulator,
)
from tests.harness import TestEquipmentTangoTestHarness


@pytest.fixture(name="model", params=["TGR2051", "TSG4104A", "SMB100A"])
def model_fixture(request: SubRequest) -> str:
    """
    Return the signal generator model.

    :param request: A pytest object giving access to the requesting test
        context.

    :returns: the signal generator model.
    """
    return request.param


@pytest.fixture(name="initial_values")
def initial_values_fixture(model: str) -> Dict[str, Any]:
    """
    Return a dictionary of expected signal generator device attribute values.

    :param model: name of the model of the signal generator.

    :returns: expected signal generator device attribute values.
    """
    identity_map = {
        "TGR2051": "THURLBY THANDAR, TGR2051, 565559, 1.03",
        "TSG4104A": "Tektronix,TSG4104A,s/nC010133,ver2.03.26",
        "SMB100A": "Rohde&Schwarz,SMB100A,1406.6000k03/183286,4.20.028.58",
    }

    initial_values = SignalGeneratorSimulator.DEFAULTS.copy()
    initial_values["power_cycled"] = True
    initial_values["identity"] = identity_map[model]
    return initial_values


@pytest.fixture(name="signal_generator_simulator")
def signal_generator_simulator_fixture(model: str) -> SignalGeneratorSimulator:
    """
    Return a signal generator simulator.

    :param model: name of the model of the signal generator.

    :returns: a signal generator simulator.
    """
    return SignalGeneratorSimulator(model, power_cycled=True)


@pytest.fixture(name="power_limit")
def power_limit_fixture() -> float:
    """
    Return arbitrary value for a signal generator device power limit.

    :returns: an arbitrary power limit value.
    """
    return 10.0


@pytest.fixture(name="simulator_test_harness_factory")
def simulator_test_harness_factory_fixture(
    model: str,
    signal_generator_simulator: SignalGeneratorSimulator,
    power_limit: float,
) -> Callable[[], TestEquipmentTangoTestHarness]:
    """
    Return a factory for simulator test harnesses.

    That is, a callable that, when called, constructs and returns a
    test harness for testing against simulators.

    :param model: name of the model of the signal generator.
    :param signal_generator_simulator: the signal generator simulator
        that the component manager under test connects to.
    :param power_limit: user defined power limit for the signal
        generator.

    :returns: a factory for hardware test harnesses.
    """

    def create_test_harness() -> TestEquipmentTangoTestHarness:
        test_harness = TestEquipmentTangoTestHarness()
        test_harness.add_signal_generator_simulator(1, signal_generator_simulator)
        test_harness.add_signal_generator_device(
            1,
            model,
            address=None,
            power_limit=power_limit,
        )
        return test_harness

    return create_test_harness


@pytest.fixture(name="hardware_test_harness_factory")
def hardware_test_harness_factory_fixture(
    model: str,
    power_limit: float,
) -> Callable[[], TestEquipmentTangoTestHarness]:
    """
    Return a factory for hardware test harnesses.

    That is, a callable that, when called, constructs and returns a
    test harness for testing against real hardware.

    :param model: name of the model of the signal generator.
    :param power_limit: user defined power limit for the signal
        generator.

    :returns: a factory for hardware test harnesses.
    """

    def create_harness() -> TestEquipmentTangoTestHarness:
        address_env_var = f"{model}_ADDRESS"
        if address_env_var not in os.environ:
            pytest.skip(
                "To run test against hardware, environment variable "
                f"{address_env_var} must be set."
            )

        [protocol, host, port_str] = os.environ[address_env_var].split(":")
        assert protocol in get_args(SupportedProtocol)
        port = int(port_str)

        test_harness = TestEquipmentTangoTestHarness()
        test_harness.add_signal_generator_device(
            1,
            model,
            address=(protocol, host, port),
            power_limit=power_limit,
        )
        return test_harness

    return create_harness


@pytest.fixture(name="test_harness")
def test_harness_fixture(
    request: SubRequest,
    simulator_test_harness_factory: Callable[[], TestEquipmentTangoTestHarness],
    hardware_test_harness_factory: Callable[[], TestEquipmentTangoTestHarness],
) -> TestEquipmentTangoTestHarness:
    """
    Return a test harness for a signal generator.

    This fixture supports two kinds of test harness:
    a simulator-based harness and one that acts on real hardware.
    It must be indirectly parametrized to return the desired
    harness(es).

    :param request: A pytest object giving access to the requesting test
        context.
    :param simulator_test_harness_factory:
        A factory for a simulator-based test harness
    :param hardware_test_harness_factory:
        A factory for a hardware-based test harness

    :raises ValueError: if parametrized with a value other than
        "Simulator" or "Hardware".

    :returns: a context manager factory for an signal generator.
    """
    if request.param == "Simulator":
        return simulator_test_harness_factory()
    if request.param == "Hardware":
        return hardware_test_harness_factory()
    raise ValueError(
        "Signal generator fixtures can only be parametrized on "
        '"Simulator" and "Hardware".'
    )


@pytest.fixture()
def signal_generator_component_manager(
    test_harness: TestEquipmentTangoTestHarness,
    interface_definition: InterfaceDefinitionType,
    logger: logging.Logger,
    communication_status_callback: Callable,
    component_state_callback: Callable,
) -> Iterator[SignalGeneratorComponentManager]:
    """
    Return the signal generator component manager under test.

    :param test_harness: a context manager that yields a running test
        context.
    :param interface_definition: definition of the signal generator's
        SCPI interface.
    :param logger: a logger for the component manager to use.
    :param communication_status_callback: a callback to be registered
        with the component manager, to be called when the status of
        communication with the hardware changes.
    :param component_state_callback: a callback to be registered with
        the component manager, to be called when the state of the component
        changes.

    :yields: a signal generator component manager, in a test harness
        context.
    """
    with test_harness as test_harness_context:
        host, port = test_harness_context.get_context("signal_generator_simulator_1")

        yield SignalGeneratorComponentManager(
            interface_definition,
            "tcp",
            host,
            port,
            logger,
            communication_status_callback,
            component_state_callback,
            update_rate=1.0,
        )


@pytest.fixture()
def signal_generator_device(
    test_harness: TestEquipmentTangoTestHarness,
) -> Iterator[tango.DeviceProxy]:
    """
    Yield a proxy to the signal generator, running in a test context.

    :param test_harness: a context manager that yields a running test
        context.

    :yields: a proxy to a signal generator device, running in the test
        harness context.
    """
    with test_harness as tango_context:
        yield tango_context.get_device("test-itf/siggen/1")


@pytest.fixture()
def change_event_callbacks() -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of change event callbacks with asynchrony support.

    :returns: a collections.defaultdict that returns change event
        callbacks by name.
    """
    return MockTangoEventCallbackGroup(
        "identity",
        "frequency",
        "power_dbm",
        "rf_output_on",
        "adminMode",
        "command_error",
        "device_error",
        "execution_error",
        "fault",
        "power",
        "power_cycled",
        "query_error",
        "state",
        timeout=5.0,
    )
