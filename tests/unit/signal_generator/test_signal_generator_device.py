"""Unit tests of the signal generator Tango device."""
import pytest
import tango
from ska_control_model import AdminMode
from ska_ser_scpi import InterfaceDefinitionType
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

from ska_ser_test_equipment.signal_generator import SignalGeneratorDevice


@pytest.mark.parametrize(
    "test_harness", ["Simulator"], indirect=True
)  # pylint: disable-next=too-few-public-methods
class TestAgainstSignalGeneratorSimulator:
    """
    Tests of the signal generator device against a signal generator simulator.

    This class is for tests that must be tested against a simulator. For
    example, tests where we put the simulator into a particular state,
    and then check that our monitoring and control layer correctly
    reports that state; or, we execute a command on our monitoring and
    control layer, and then check that the simulator has been driven
    into the expected state.

    It is also for basic device tests that don't trigger any particular
    interaction with the underlying signal generator, so don't need to
    be run against real hardware.
    """

    def test_init(  # pylint: disable=no-self-use
        self,
        signal_generator_device: SignalGeneratorDevice,
        interface_definition: InterfaceDefinitionType,
    ) -> None:
        """
        Test device initialisation.

        :param signal_generator_device: the signal generator Tango device
            under test.
        :param interface_definition: definition of the signal generator's
            SCPI interface.
        """
        assert signal_generator_device.adminMode == AdminMode.OFFLINE
        assert signal_generator_device.state() == tango.DevState.DISABLE

        device_attribute_list = signal_generator_device.get_attribute_list()
        for attr_name, spec in interface_definition["attributes"].items():
            if "field_type" not in list(spec.values())[0]:
                continue  # not an attribute
            assert attr_name in device_attribute_list


@pytest.mark.parametrize(
    "test_harness",
    ["Simulator", "Hardware"],
    indirect=True,
)  # pylint: disable-next=too-few-public-methods
class TestAgainstSignalGeneratorServer:
    """
    Tests of the signal generator device against a signal generator.

    This class is for tests that require only that a signal generator
    server is running. This server could be a simulator, or it could be
    real hardware. Tests in this class are run twice: once against a
    simulator, once against real hardware.
    """

    def test_monitor_and_control(  # pylint: disable=no-self-use
        self,
        signal_generator_device: SignalGeneratorDevice,
        interface_definition: InterfaceDefinitionType,
        change_event_callbacks: MockTangoEventCallbackGroup,
        power_limit: float,
    ) -> None:
        """
        Test device's monitoring of the signal generator.

        :param signal_generator_device: the signal generator Tango device
            under test.
        :param interface_definition: definition of the signal generator's
            SCPI interface.
        :param change_event_callbacks: dictionary of mock change event
            callbacks with asynchrony support.
        :param power_limit: user defined power limit for the signal
            generator.
        """
        assert signal_generator_device.adminMode == AdminMode.OFFLINE
        assert signal_generator_device.state() == tango.DevState.DISABLE

        signal_generator_device.subscribe_event(
            "adminMode",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["adminMode"],
        )
        change_event_callbacks.assert_change_event("adminMode", AdminMode.OFFLINE)

        signal_generator_device.subscribe_event(
            "state",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["state"],
        )
        change_event_callbacks.assert_change_event("state", tango.DevState.DISABLE)

        device_attribute_list = signal_generator_device.get_attribute_list()
        for attribute, spec in interface_definition["attributes"].items():
            if "field_type" not in list(spec.values())[0]:
                continue

            assert attribute in device_attribute_list

            signal_generator_device.subscribe_event(
                attribute,
                tango.EventType.CHANGE_EVENT,
                change_event_callbacks[attribute],
            )

        for attribute, spec in interface_definition["attributes"].items():
            if "field_type" not in list(spec.values())[0]:
                continue

            change_event_callbacks[attribute].assert_against_call(
                attribute_quality=tango.AttrQuality.ATTR_INVALID
            )
            assert getattr(signal_generator_device, attribute) is None

        # TODO: Figure out how to make @attribute recognisable as a @property.
        # pylint: disable-next=line-too-long
        signal_generator_device.adminMode = AdminMode.ONLINE  # type: ignore[assignment] # noqa: E501

        change_event_callbacks.assert_change_event("adminMode", AdminMode.ONLINE)

        assert signal_generator_device.adminMode == AdminMode.ONLINE

        change_event_callbacks.assert_change_event("state", tango.DevState.UNKNOWN)
        change_event_callbacks.assert_change_event("state", tango.DevState.ON)

        for attribute, spec in interface_definition["attributes"].items():
            if "field_type" not in list(spec.values())[0]:
                continue  # not an attribute

            # We'll receive a change event for each attribute, but we don't
            # know what value we'll get. All we know is it will be valid.
            change_event_callbacks[attribute].assert_against_call(
                attribute_quality=tango.AttrQuality.ATTR_VALID
            )

        dbm_to_write = -10.0
        if signal_generator_device.power_dbm == pytest.approx(dbm_to_write):
            # Oops, the power_dbm is already set to -10.0
            # we had better choose a different value to test our write with
            dbm_to_write = -9.0

        # Now let's test we can control the signal generator, by modifying a
        # value.
        signal_generator_device.power_dbm = dbm_to_write
        change_event_callbacks["power_dbm"].assert_change_event(
            pytest.approx(dbm_to_write)
        )
        assert signal_generator_device.power_dbm == pytest.approx(dbm_to_write)

        # Now let's test that we are not allowed to set a power value greater
        # than the power limit.
        with pytest.raises(
            tango.DevFailed,
            match="Set value for attribute power_dbm is above the maximum",
        ):
            signal_generator_device.power_dbm = power_limit + 1.0
