"""Tests of the SignalGeneratorComponentManager."""
from typing import Any, Dict

import pytest
from ska_control_model import CommunicationStatus, PowerState, TaskStatus
from ska_tango_testing.mock import MockCallable

from ska_ser_test_equipment.signal_generator import (
    SignalGeneratorComponentManager,
    SignalGeneratorSimulator,
)


@pytest.mark.parametrize("test_harness", ["Simulator"], indirect=True)
class TestAgainstSignalGeneratorSimulator:
    """
    Tests of the signal generator component manager against a simulator.

    This class is for tests that must be tested against a simulator. For
    example, tests where we put the simulator into a particular state,
    and then check that our monitoring and control layer correctly
    reports that state; or, we execute a command on our monitoring and
    control layer, and then check that the simulator has been driven
    into the expected state.
    """

    def test_monitoring(  # pylint: disable=no-self-use, too-many-arguments
        self,
        signal_generator_simulator: SignalGeneratorSimulator,
        signal_generator_component_manager: SignalGeneratorComponentManager,
        communication_status_callback: MockCallable,
        component_state_callback: MockCallable,
        initial_values: Dict[str, Any],
    ) -> None:
        """
        Test the component manager's monitoring of the signal generator.

        :param signal_generator_simulator: the signal generator simulator
            that the component manager under test connects to.
        :param signal_generator_component_manager: the component manager
            under test.
        :param communication_status_callback: a callback that has already
            been passed to the component manager, to be called when the
            status of communication with the processor changes.
        :param component_state_callback: a callback that has already been
            passed to the component manager, to be called when the state of
            the processor changes.
        :param initial_values: the initial field values
            that the simulator is set to take.
        """
        assert (
            signal_generator_component_manager.communication_state
            == CommunicationStatus.DISABLED
        )
        assert (
            signal_generator_component_manager.component_state["power"]
            == PowerState.UNKNOWN
        )
        assert signal_generator_component_manager.component_state["fault"] is None

        signal_generator_component_manager.start_communicating()

        communication_status_callback.assert_call(CommunicationStatus.NOT_ESTABLISHED)
        communication_status_callback.assert_call(CommunicationStatus.ESTABLISHED)

        # TODO: once we are reading error registers / buffers, the fault call
        # won't occur until we have started properly polling.
        component_state_callback.assert_call(
            identity=initial_values["identity"],
            power=PowerState.ON,
            fault=False,
        )

        component_state_callback.assert_call(
            rf_output_on=initial_values["rf_output_on"],
            power_dbm=pytest.approx(initial_values["power_dbm"]),
            frequency=pytest.approx(initial_values["frequency"]),
            query_error=initial_values["query_error"],
            device_error=initial_values["device_error"],
            execution_error=initial_values["execution_error"],
            command_error=initial_values["command_error"],
            power_cycled=initial_values["power_cycled"],
        )

        # The values in the simulator are static, so we won't change unless
        # we change them ourselves.
        component_state_callback.assert_not_called()

        # Now let's change a value in the simulator, and check that the
        # component manager detects and reports it
        signal_generator_simulator.set_attribute("frequency", 0.2)
        component_state_callback.assert_call(frequency=pytest.approx(0.2))

        # Now let's set the device_error attribute to True, and check
        # that we also see the fault status go to True
        signal_generator_simulator.set_attribute("device_error", True)
        component_state_callback.assert_call(device_error=True, fault=True)

        signal_generator_component_manager.stop_communicating()

        communication_status_callback.assert_call(CommunicationStatus.DISABLED)
        assert (
            signal_generator_component_manager.communication_state
            == CommunicationStatus.DISABLED
        )
        assert (
            signal_generator_component_manager.component_state["power"]
            == PowerState.UNKNOWN
        )
        assert signal_generator_component_manager.component_state["fault"] is None

    def test_write(  # pylint: disable=no-self-use, too-many-arguments
        self,
        signal_generator_simulator: SignalGeneratorSimulator,
        signal_generator_component_manager: SignalGeneratorComponentManager,
        communication_status_callback: MockCallable,
        component_state_callback: MockCallable,
        initial_values: Dict[str, Any],
    ) -> None:
        """
        Test the ability to write new values to the signal generator.

        :param signal_generator_simulator: the signal generator simulator
            that the component manager under test connects to.
        :param signal_generator_component_manager: the component manager
            under test.
        :param communication_status_callback: a callback that has already
            been passed to the component manager, to be called when the
            status of communication with the processor changes.
        :param component_state_callback: a callback that has already been
            passed to the component manager, to be called when the state of
            the processor changes.
        :param initial_values: the initial field values
            that the simulator is set to take.
        """
        signal_generator_component_manager.start_communicating()
        communication_status_callback.assert_call(CommunicationStatus.NOT_ESTABLISHED)
        communication_status_callback.assert_call(CommunicationStatus.ESTABLISHED)

        # TODO: once we are reading error registers / buffers, the fault call
        # won't occur until we have started properly polling.
        component_state_callback.assert_call(
            identity=initial_values["identity"],
            power=PowerState.ON,
            fault=False,
        )

        component_state_callback.assert_call(
            rf_output_on=initial_values["rf_output_on"],
            power_dbm=pytest.approx(initial_values["power_dbm"]),
            frequency=pytest.approx(initial_values["frequency"]),
            query_error=initial_values["query_error"],
            device_error=initial_values["device_error"],
            execution_error=initial_values["execution_error"],
            command_error=initial_values["command_error"],
            power_cycled=initial_values["power_cycled"],
        )

        signal_generator_component_manager.write_attribute(
            power_dbm=2.0,
            frequency=0.2,
        )

        component_state_callback.assert_call(
            power_dbm=pytest.approx(2.0),
            frequency=pytest.approx(0.2),
        )

        assert signal_generator_simulator.get_attribute("power_dbm") == pytest.approx(
            2.0
        )

    def test_reset(  # pylint: disable=no-self-use, too-many-arguments
        self,
        signal_generator_simulator: SignalGeneratorSimulator,
        signal_generator_component_manager: SignalGeneratorComponentManager,
        communication_status_callback: MockCallable,
        component_state_callback: MockCallable,
        task_callback: MockCallable,
        initial_values: Dict[str, Any],
    ) -> None:
        """
        Test the ability to reset the signal generator.

        :param signal_generator_simulator: the signal generator simulator
            that the component manager under test connects to.
        :param signal_generator_component_manager: the component manager
            under test.
        :param communication_status_callback: a callback that has already
            been passed to the component manager, to be called when the
            status of communication with the processor changes.
        :param component_state_callback: a callback that has already been
            passed to the component manager, to be called when the state of
            the processor changes.
        :param task_callback: a callback to use when invoking the reset
            command.
        :param initial_values: the initial field values
            that the simulator is set to take.
        """
        signal_generator_component_manager.start_communicating()
        communication_status_callback.assert_call(CommunicationStatus.NOT_ESTABLISHED)
        communication_status_callback.assert_call(CommunicationStatus.ESTABLISHED)

        # TODO: once we are reading error registers / buffers, the fault call
        # won't occur until we have started properly polling.
        component_state_callback.assert_call(
            identity=initial_values["identity"],
            power=PowerState.ON,
            fault=False,
        )

        component_state_callback.assert_call(
            rf_output_on=initial_values["rf_output_on"],
            power_dbm=pytest.approx(initial_values["power_dbm"]),
            frequency=pytest.approx(initial_values["frequency"]),
            query_error=initial_values["query_error"],
            device_error=initial_values["device_error"],
            execution_error=initial_values["execution_error"],
            command_error=initial_values["command_error"],
            power_cycled=initial_values["power_cycled"],
        )

        signal_generator_component_manager.reset(task_callback)

        task_callback.assert_call(status=TaskStatus.QUEUED)
        task_callback.assert_call(status=TaskStatus.IN_PROGRESS)
        task_callback.assert_call(status=TaskStatus.COMPLETED)

        component_state_callback.assert_call(
            frequency=pytest.approx(10000000),
            power_dbm=pytest.approx(0.0),
        )

        assert signal_generator_simulator.get_attribute("power_dbm") == pytest.approx(
            0.0
        )

        assert signal_generator_simulator.get_attribute("frequency") == pytest.approx(
            10000000.0
        )

        assert not signal_generator_simulator.get_attribute("rf_output_on")
