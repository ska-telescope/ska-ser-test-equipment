"""Test harness for Anritsu spectrum analyser tests."""
import logging
import os
import socket
import subprocess
import time
from typing import Any

import psutil
import pytest
import tango
import tango.test_context
from ska_control_model import AdminMode

from ska_ser_test_equipment.interface_definitions import InterfaceDefinitionFactory
from ska_ser_test_equipment.scpi.scpi_config import get_host_port, get_scpi_config
from ska_ser_test_equipment.spectrum_analyser_anritsu import (
    SpectrumAnalyserSimulatorAnritsu,
)

ATTRIBUTES_BUILTIN = {
    "MS2090A": [
        # "versionId",
        # "buildState",
        "adminMode",
        # "loggingLevel",
        # "loggingTargets",
        # "healthState",
        # "controlMode",
        # "simulationMode",
        # "testMode",
        # "longRunningCommandsInQueue",
        # "longRunningCommandIDsInQueue",
        # "longRunningCommandStatus",
        # "longRunningCommandProgress",
        # "longRunningCommandResult",
        "State",
        "Status",
    ]
}
ATTRIBUTES_TESTED = {
    "MS2090A": [
        "add_marker",
        "attenuation",
        "autoattenuation",
        # "command_error",
        "continuous",
        # "device_error",
        # "execution_error",
        "frequency_start",
        "frequency_stop",
        # "flag_when_complete",
        "marker_frequency",
        "marker_power",
        # "operationm_complete",
        "rbw",
        "rbw_auto",
        # "power_cycled",
        "preamp_enabled",
        # "query_error",
        "reference_level",
        "sweep_points",
        "trace1",
        "trace_format",
        "vbw",
        "vbw_auto",
    ],
}
ATTRIBUTES_SKIP = {
    "MS2090A": [
        "clear_markers",
        "get_trace",
        "identity",
        "marker_find_peak",
        "reset",
        "trace1",
        "command_error",
        "device_error",
        "execution_error",
        "query_error",
        "power_cycled",
        "operation_complete",
        "flag_when_complete",
    ],
}
ATTRIBUTES_IDENTITY = {
    "MS2090A": "Anritsu,MS2090A,F00BAA7,V2038.1.19",
}
SIMULATOR_HOST = "127.0.0.1"
SIMULATOR_PORT = 9001
TANGO_HOST = "127.0.0.1"
TANGO_PORT = "45450"
SLEEP_TIME = 1.5
IFACE_DEF = None

LOG_LEVEL = logging.DEBUG
logging.basicConfig(level=LOG_LEVEL)
caplog = logging.getLogger(__name__)


def test_spectrum_analyser_simulator_start(
    spectrum_analyser_simulator_proc: subprocess.Popen[bytes] | None,
) -> None:
    """
    Test that simulator has been started.

    :param spectrum_analyser_simulator_proc: process handle
    """
    assert spectrum_analyser_simulator_proc
    caplog.info(
        "Process for spectrum analyser simulator started %s",
        repr(spectrum_analyser_simulator_proc),
    )
    spectrum_analyser_simulator_pid = spectrum_analyser_simulator_proc.pid
    caplog.info("Simulator PID is %d", spectrum_analyser_simulator_pid)
    process = psutil.Process(spectrum_analyser_simulator_pid)
    caplog.info(
        "Simulator process %s (%s) : %s",
        process.name(),
        process.status(),
        process.cmdline(),
    )
    assert process.status() in ("running", "disk-sleep")


def test_spectrum_analyser_device_start(
    spectrum_analyser_device_proc: subprocess.Popen[bytes] | None,
) -> None:
    """
    Test that Tango device can be started.

    :param spectrum_analyser_device_proc: process handle
    """
    assert spectrum_analyser_device_proc
    caplog.info(
        "Process for spectrum analyser device started %s",
        repr(spectrum_analyser_device_proc),
    )
    spectrum_analyser_device_pid = spectrum_analyser_device_proc.pid
    caplog.info("Tango device PID is %s", str(spectrum_analyser_device_pid))
    process = psutil.Process(spectrum_analyser_device_pid)
    caplog.info("Tango device process %s (%s)", process.name(), process.status())
    caplog.info("Tango device : %s", process.cmdline())
    assert process.status() in ("running", "disk-sleep")


def test_spectrum_analyser_simulator_online(
    spectrum_analyser_simulator_proc: subprocess.Popen[bytes] | None,
) -> None:
    """
    Test that simulator port is open.

    :param spectrum_analyser_simulator_proc: process handle
    """
    assert spectrum_analyser_simulator_proc
    caplog.info(
        "Process for spectrum analyser simulator started %s",
        repr(spectrum_analyser_simulator_proc),
    )
    hostname = SIMULATOR_HOST
    _host, port = get_host_port("spectrumanalyseranritsu")
    if not port:
        try:
            caplog.warning(
                "No SCPI port, check %s", f"{' '.join(os.listdir('/app/charts'))}"
            )
        except FileNotFoundError:
            caplog.warning("Directory /app/charts does not exist")
        port = SIMULATOR_PORT
    assert port > 1024
    caplog.debug("Connect to simulator %s:%d", hostname, port)
    time.sleep(SLEEP_TIME)
    sim_ready = False
    sim_retry = 0
    while sim_retry < 10:
        skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            skt.connect((hostname, port))
            sim_ready = True
            caplog.warning("Connected to simulator %s:%d", hostname, port)
            break
        except ConnectionRefusedError:
            caplog.warning("Could not connect to simulator %s:%d", hostname, port)
        time.sleep(SLEEP_TIME)
        if not sim_retry:
            spectrum_analyser_simulator_pid = spectrum_analyser_simulator_proc.pid
            caplog.info("Simulator PID is %d", spectrum_analyser_simulator_pid)
            process = psutil.Process(spectrum_analyser_simulator_pid)
            caplog.info(
                "Simulator process %s (%s) : %s",
                process.name(),
                process.status(),
                process.cmdline(),
            )
            assert process.status() in ("running", "disk-sleep")
        sim_retry += 1
    assert sim_ready
    skt.close()


def test_spectrum_analyser_device_started(
    spectrum_analyser_device: tango.DeviceProxy,
) -> None:
    """
    Test that Tango device has been started.

    :param spectrum_analyser_device: Tango device
    """
    try:
        admin = spectrum_analyser_device.adminMode
    except AttributeError:
        caplog.warning("Device not ready yet")
        time.sleep(30)
    spectrum_analyser_device.set_timeout_millis(5000)
    admin = spectrum_analyser_device.adminMode
    assert admin is not None


def test_spectrum_analyser_device_online(spectrum_analyser_device_proc: Any) -> None:
    """
    Test that device port is open.

    :param spectrum_analyser_device_proc: process handle
    """
    assert spectrum_analyser_device_proc
    hostname = TANGO_HOST
    port = int(TANGO_PORT)
    skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    dev_ready = False
    dev_retry = 0
    caplog.debug("Connect to device %s:%d", hostname, port)
    while dev_retry < 10:
        try:
            skt.connect((hostname, port))
            dev_ready = True
            caplog.info("Connected to device %s:%d", hostname, port)
            break
        except ConnectionRefusedError:
            caplog.warning("Could not connect to Tango device %s:%d", hostname, port)
        # if not dev_retry:
        time.sleep(3)
        dev_retry += 1
    assert dev_ready
    caplog.info("Connected to device %s:%d", hostname, port)
    skt.close()
    # spectrum_analyser_device_pid = spectrum_analyser_device_proc.pid
    # process = psutil.Process(spectrum_analyser_device_pid)
    # port_status = {}
    # for connection in process.connections():
    #     caplog.info("Port %d status %s", connection.laddr[1], connection.status)
    #     port_status[connection.laddr[1]] = connection.status
    # assert port in port_status
    # assert port_status[port] == "LISTEN" or port_status[port] == "ESTABLISHED"


def test_spectrum_analyser_anritsu_config() -> None:
    """Test the configuration."""
    # pylint: disable-next=global-statement
    global IFACE_DEF
    dev_model = os.getenv("SIMULATOR_MODEL", "MS2090A").upper()

    assert (
        dev_model in ATTRIBUTES_IDENTITY
        and dev_model in ATTRIBUTES_BUILTIN
        and dev_model in ATTRIBUTES_TESTED
        and dev_model in ATTRIBUTES_SKIP
    )

    IFACE_DEF = InterfaceDefinitionFactory()(dev_model)
    caplog.debug("interface definition %s", IFACE_DEF)
    assert IFACE_DEF

    assert IFACE_DEF["model"] == dev_model

    assert "attributes" in IFACE_DEF

    scpi_fields, _scpi_values, _scpi_mins, _scpi_maxs = get_scpi_config(dev_model)

    assert len(scpi_fields) > 1

    for attr_name in IFACE_DEF["attributes"]:
        caplog.debug("check attribute %s", attr_name)
        assert (
            attr_name in ATTRIBUTES_BUILTIN[dev_model]
            or attr_name in ATTRIBUTES_TESTED[dev_model]
            or attr_name in ATTRIBUTES_SKIP[dev_model]
        )
        assert attr_name in scpi_fields
        attr_def = IFACE_DEF["attributes"][attr_name]
        assert "read" in attr_def or "read_write" in attr_def or "write" in attr_def
        if "read" in attr_def:
            i_field = attr_def["read"]["field"]
            f_type = attr_def["read"]["field_type"]
            caplog.info("attribute %s read %s : %s", attr_name, f_type, i_field)
        elif "read_write" in attr_def:
            i_field = attr_def["read_write"]["field"]
            f_type = attr_def["read"]["field_type"]
            caplog.info("attribute %s read/write %s : %s", attr_name, f_type, i_field)
        elif "write" in attr_def:
            i_field = attr_def["write"]["field"]
            try:
                f_type = attr_def["read"]["field_type"]
            except KeyError:
                f_type = None
            if f_type:
                caplog.info("attribute %s write %s : %s", attr_name, f_type, i_field)
            else:
                caplog.warning("attribute %s write no type : %s", attr_name, i_field)
        else:
            pass
        assert i_field == scpi_fields[attr_name]


def test_spectrum_analyser_device_on(
    spectrum_analyser_device: tango.DeviceProxy,
) -> None:
    """
    Test that device is on.

    :param spectrum_analyser_device: Tango device
    """
    dev_name = "mid-itf/spectana/1"
    dev_model = os.getenv("SIMULATOR_MODEL", "MS2090A").upper()

    assert dev_model in ATTRIBUTES_IDENTITY

    chk_name = spectrum_analyser_device.dev_name()

    caplog.info("Device name is '%s', should be '%s'", chk_name, dev_name)
    assert chk_name == dev_name

    # try:
    #     admin = spectrum_analyser_device.adminMode
    # except AttributeError:
    #     caplog.warning("Device not ready yet")
    #     time.sleep(30)
    admin = spectrum_analyser_device.adminMode
    assert admin is not None

    if admin == AdminMode.OFFLINE:
        caplog.info("admin mode is OFFLINE")
    else:
        spectrum_analyser_device.adminMode = AdminMode.OFFLINE
        caplog.info("admin mode set to OFFLINE")

    try:
        spectrum_analyser_device.adminMode = AdminMode.ONLINE
    except AttributeError as dev_err:
        caplog.error("could not set admin mode to ONLINE %s", dev_err)
        spectrum_analyser_device.adminMode = None
    caplog.info("admin mode set to ONLINE")
    assert spectrum_analyser_device.adminMode == AdminMode.ONLINE

    caplog.info(
        "status %s state %s",
        spectrum_analyser_device.Status(),
        spectrum_analyser_device.State(),
    )

    # pylint: disable-next=c-extension-no-member,protected-access
    assert spectrum_analyser_device.State() != tango._tango.DevState.UNKNOWN

    # pylint: disable-next=c-extension-no-member,protected-access
    assert spectrum_analyser_device.State() == tango._tango.DevState.ON

    caplog.info("status %s", spectrum_analyser_device.Status())


def test_spectrum_analyser_anritsu_identity(
    spectrum_analyser_device: tango.DeviceProxy,
) -> None:
    """
    Confirm identity of device.

    :param spectrum_analyser_device: Tango device
    """
    # time.sleep(10)
    dev_model = os.getenv("SIMULATOR_MODEL", "MS2090A").upper()
    assert dev_model is not None

    chk_id = ATTRIBUTES_IDENTITY[dev_model]
    caplog.info("identity should be %s", chk_id)
    assert chk_id is not None

    id_info = spectrum_analyser_device.read_attribute("identity").value
    caplog.info("identity is %s", id_info)
    assert id_info is not None
    assert id_info == chk_id


def test_spectrum_analyser_anritsu_attributes_read(
    spectrum_analyser_device: tango.DeviceProxy,
) -> None:
    """
    Test the attributes.

    :param spectrum_analyser_device: Tango device
    """
    attributes = sorted(spectrum_analyser_device.get_attribute_list())
    caplog.debug("Got %s attributes : %s", len(attributes), " ".join(attributes))
    assert attributes

    dev_model = os.getenv("SIMULATOR_MODEL", "MS2090A").upper()

    initial_values_map = SpectrumAnalyserSimulatorAnritsu.DEFAULTS.copy()
    caplog.debug("Got %d initial values", len(initial_values_map))
    assert initial_values_map

    for attribute in attributes:
        caplog.debug("Test attribute %s", attribute)
        if attribute in ATTRIBUTES_SKIP[dev_model]:
            caplog.warning("skip test for %s", attribute)
        elif attribute in ATTRIBUTES_BUILTIN[dev_model]:
            attr_value = spectrum_analyser_device.read_attribute(attribute).value
            caplog.info("Attribute %s value %s", attribute, attr_value)
            assert attr_value is not None
        elif attribute not in ATTRIBUTES_TESTED[dev_model]:
            caplog.warning("untested attribute %s", attribute)
        elif attribute not in initial_values_map:
            caplog.warning("no initial value for %s", attribute)
            attr_value = spectrum_analyser_device.read_attribute(attribute).value
            caplog.info("Attribute %s value %s", attribute, attr_value)
            assert attr_value is not None
        else:
            time.sleep(SLEEP_TIME)
            initial_value = initial_values_map[attribute]
            assert initial_value is not None
            attr_value = spectrum_analyser_device.read_attribute(attribute).value
            caplog.info(
                "Attribute %s initial value '%s' actual value '%s'",
                attribute,
                initial_value,
                attr_value,
            )
            assert attr_value == initial_value


# pylint: disable-next=too-many-statements
def test_spectrum_analyser_anritsu_attributes_write(
    spectrum_analyser_device: tango.DeviceProxy,
) -> None:
    """
    Test the attributes.

    :param spectrum_analyser_device: Tango device
    """
    assert spectrum_analyser_device is not None
    # Set trace_format
    caplog.info("Set trace_format to REAL")
    spectrum_analyser_device.trace_format = "REAL"
    time.sleep(SLEEP_TIME)
    assert spectrum_analyser_device.trace_format == "REAL"
    # Set attenuation
    caplog.info("Set attenuation to 10")
    spectrum_analyser_device.attenuation = 10
    time.sleep(SLEEP_TIME)
    assert spectrum_analyser_device.attenuation == 10
    # Set frequency start
    caplog.info("Set frequency start to 1e8")
    spectrum_analyser_device.frequency_start = 100000000
    time.sleep(SLEEP_TIME)
    assert spectrum_analyser_device.frequency_start == 100000000
    # Set frequency stop
    caplog.info("Set frequency stop to 2e8")
    spectrum_analyser_device.frequency_stop = 200000000
    time.sleep(SLEEP_TIME)
    assert spectrum_analyser_device.frequency_stop == 200000000
    # Set rbw
    rbw_value = 4000000
    caplog.info("Set rbw to %d", rbw_value)
    spectrum_analyser_device.rbw = rbw_value
    time.sleep(SLEEP_TIME)
    assert spectrum_analyser_device.rbw == rbw_value
    # Set reference level
    caplog.info("Set reference_level to 20")
    spectrum_analyser_device.reference_level = 20
    time.sleep(SLEEP_TIME)
    assert spectrum_analyser_device.reference_level == 20
    # Set
    caplog.info("Set sweep_points to 1000")
    spectrum_analyser_device.sweep_points = 1000
    time.sleep(SLEEP_TIME)
    assert spectrum_analyser_device.sweep_points == 1000
    # Set vbw
    vbw_value = float(20000000)
    caplog.info("Set vbw to %f", vbw_value)
    spectrum_analyser_device.vbw = vbw_value
    time.sleep(SLEEP_TIME)
    assert spectrum_analyser_device.vbw == vbw_value
    # Set preamp enabled
    caplog.info("Set preamp_enabled to on")
    spectrum_analyser_device.preamp_enabled = 1
    time.sleep(SLEEP_TIME)
    assert spectrum_analyser_device.preamp_enabled
    # Set continuous
    caplog.info("Set continuous to on")
    spectrum_analyser_device.continuous = 1
    time.sleep(SLEEP_TIME)
    assert spectrum_analyser_device.continuous
    # Set autoattenuation
    caplog.info("Set autoattenuation to on")
    spectrum_analyser_device.autoattenuation = 1
    time.sleep(SLEEP_TIME)
    assert spectrum_analyser_device.autoattenuation
    # Set rbw_auto
    caplog.info("Set rbw_auto to on")
    spectrum_analyser_device.rbw_auto = 1
    time.sleep(SLEEP_TIME)
    assert spectrum_analyser_device.rbw_auto
    # Set vbw_auto
    caplog.info("Set vbw_auto to on")
    spectrum_analyser_device.vbw_auto = 1
    time.sleep(SLEEP_TIME)
    assert spectrum_analyser_device.vbw_auto


def test_spectrum_analyser_anritsu_attributes_write_invalid_freq(
    spectrum_analyser_device: tango.DeviceProxy,
) -> None:
    """
    Test the attributes.

    :param spectrum_analyser_device: Tango device
    """
    # Set frequency start
    freq_a = spectrum_analyser_device.frequency_start
    caplog.info("Set frequency start to -200000000000")
    spectrum_analyser_device.frequency_start = -200000000000
    time.sleep(SLEEP_TIME)
    assert spectrum_analyser_device.frequency_start == freq_a
    freq_b = spectrum_analyser_device.frequency_stop
    caplog.info("Set frequency stop to 399999999990")
    spectrum_analyser_device.frequency_stop = 399999999990
    time.sleep(SLEEP_TIME)
    assert spectrum_analyser_device.frequency_stop == freq_b


@pytest.mark.xfail
def test_trace_float(spectana_dev: tango.DeviceProxy) -> None:
    """
    Test that trace data is available.

    :param spectana_dev: Tango device
    """
    trace = spectana_dev.trace1
    if str(trace[0]) == "#":
        caplog.warning("trace data in string format")
        assert False
        return
    caplog.info("trace data %d items", len(trace))
    marker_power = max(trace)
    try:
        caplog.info("marker power is %.2f dBm", marker_power)
    except ValueError:
        caplog.error("marker power invalid")
        assert False
        return
    freq_a = spectana_dev.frequency_start
    freq_b = spectana_dev.frequency_stop
    hz_per_sample = (freq_b - freq_a) / (len(trace) - 1)
    caplog.info("Sample size is %.1f Hz", hz_per_sample)
    peak_candidates_idx = [i for i, x in enumerate(trace) if x == marker_power]
    peak_candidates_freq = [
        round(freq_a + i * hz_per_sample, 1) for i in peak_candidates_idx
    ]
    if len(peak_candidates_freq) > 1:
        caplog.warning("Marker frequencies are %s", str(peak_candidates_freq))
    else:
        caplog.info("Marker frequency is %f", peak_candidates_freq[0])


def test_trace_str(spectrum_analyser_device: tango.DeviceProxy) -> None:
    """
    Test that trace data is available.

    :param spectrum_analyser_device: Tango device
    """
    trace = spectrum_analyser_device.trace1
    if not trace:
        caplog.error("trace data not available")
        assert False
        return
    if str(trace[0]) != "#":
        caplog.error("trace data not in string format: %s", trace)
        assert False
        return
    n_chk = int(trace[1])
    n_len = int(trace[2 : 2 + n_chk])
    if len(trace) == 2 + n_chk + n_len:
        caplog.info("trace data %d bytes", n_len)
    else:
        caplog.warning("trace data %d should be %d", n_len, len(trace) - n_chk - 2)
    trace_vals = trace[2 + n_chk :].split(",")
    # if len(trace_vals) == display_points:
    #     caplog.info("trace data %d items", display_points)
    # else:
    #     caplog.warning(
    #         "trace data items %d should be %d", display_points, len(trace_vals)
    #     )
    display_points = spectrum_analyser_device.sweep_points
    caplog.info("trace data %d items", display_points)
    assert len(trace_vals) <= display_points
    freq_a = spectrum_analyser_device.frequency_start
    freq_b = spectrum_analyser_device.frequency_stop
    # hz_per_sample = (freq_b - freq_a) / (len(trace) - 1)
    # caplog.info(f"Sample size is {hz_per_sample:1} Hz")
    marker_power = float(trace_vals[0])
    n_mark = 0
    n_vals = 1
    for trace_val in trace_vals[1:]:
        trace_dbm = float(trace_val)
        if trace_dbm > marker_power:
            marker_power = trace_dbm
            n_mark = n_vals
        n_vals += 1
    caplog.info("marker_power is %.2f dBm", marker_power)
    f_span = freq_b - freq_a
    assert f_span > 0
    peak_freq = freq_a + (f_span / (display_points - 1)) * n_mark
    caplog.info("marker_frequency is %.0f Hz", peak_freq)


def test_trace_bin(spectrum_analyser_device: tango.DeviceProxy) -> None:
    """
    Test that trace data is available.

    :param spectrum_analyser_device: Tango device
    """
    trace = spectrum_analyser_device.trace1_bin
    if len(trace) == 0:
        caplog.error("trace data not available")
        assert False
        return
    caplog.info("Read %d trace values", len(trace))
    display_points = spectrum_analyser_device.sweep_points
    caplog.info("trace data %d items", display_points)
    assert len(trace) <= display_points


def test_spectrum_analyser_anritsu_offline(
    spectrum_analyser_device: tango.DeviceProxy,
) -> None:
    """
    Test that device can be taken offline.

    :param spectrum_analyser_device: Tango device
    """
    spectrum_analyser_device.adminMode = AdminMode.OFFLINE
    caplog.info("admin mode set to OFFLINE")
    assert spectrum_analyser_device.adminMode == AdminMode.OFFLINE


def test_spectrum_analyser_simulator_stop(
    spectrum_analyser_simulator_proc: subprocess.Popen[bytes] | None,
) -> None:
    """
    Stop simulator process.

    :param spectrum_analyser_simulator_proc: process handle
    """
    assert spectrum_analyser_simulator_proc
    spectrum_analyser_simulator_pid = spectrum_analyser_simulator_proc.pid
    caplog.info("Stop simulator PID %s", str(spectrum_analyser_simulator_pid))
    spectrum_analyser_simulator_proc.kill()
    time.sleep(SLEEP_TIME)
    process = psutil.Process(spectrum_analyser_simulator_pid)
    try:
        proc_status = process.status()
    except psutil.NoSuchProcess:
        proc_status = "stopped"
    caplog.info("Simulator process %s (%s)", process.name(), proc_status)
    assert proc_status in ("sleeping", "zombie", "stopped", "dead")


def test_spectrum_analyser_device_stop(
    spectrum_analyser_device: tango.DeviceProxy,
) -> None:
    """
    Stop device process.

    :param  spectrum_analyser_device: device proxy
    """
    adm_dev = tango.DeviceProxy(spectrum_analyser_device.adm_name())
    caplog.info("Stop Tango device %s", adm_dev)
    assert adm_dev
    # TODO Fatal Python error: Segmentation fault
    # adm_dev.Kill()


def test_spectrum_analyser_device_stopped(
    spectrum_analyser_device_proc: subprocess.Popen[bytes] | None,
) -> None:
    """
    Stop device process.

    :param spectrum_analyser_device_proc: process handle
    """
    assert spectrum_analyser_device_proc
    spectrum_analyser_device_pid = spectrum_analyser_device_proc.pid
    caplog.info("Stop Tango device PID %s", str(spectrum_analyser_device_pid))
    spectrum_analyser_device_proc.kill()
    time.sleep(SLEEP_TIME)
    process = psutil.Process(spectrum_analyser_device_pid)
    try:
        proc_status = process.status()
    except psutil.NoSuchProcess:
        proc_status = "stopped"
    caplog.info("Tango device process %s (%s)", process.name(), proc_status)
    assert proc_status in ("sleeping", "zombie", "stopped", "dead")
