"""Test harness for oscilloscope tests."""
import logging
import os
from typing import Any, Callable, Dict, Iterator, get_args

import pytest
import tango
import tango.test_context
from _pytest.fixtures import SubRequest  # for the type checker
from ska_ser_scpi import InterfaceDefinitionType, SupportedProtocol
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

from ska_ser_test_equipment.oscilloscope import (
    OscilloscopeComponentManager,
    OscilloscopeSimulator,
)
from tests.harness import TestEquipmentTangoTestHarness


@pytest.fixture(name="model", params=["MSO64B"])
def model_fixture(request: SubRequest) -> str:
    """
    Return the oscilloscope model.

    :param request: A pytest object giving access to the requesting test
        context.

    :returns: the oscilloscope model.
    """
    return request.param


@pytest.fixture(name="initial_values")
def initial_values_fixture(model: str) -> Dict[str, Any]:
    """
    Return a dictionary of expected oscilloscope device attribute values.

    :param model: name of the model of the oscilloscope.

    :returns: expected oscilloscope device attribute values.
    """
    identity_map = {
        "MSO54": "TEKTRONIX,MSO54,C100123,CF:91.1CT FV:1.2.0.2886",
        "MSO64B": "TEKTRONIX,MSO64B,C047394,CF:91.1CT FV:1.44.3.433",
    }

    initial_values = OscilloscopeSimulator.DEFAULTS.copy()
    initial_values["power_cycled"] = True
    initial_values["identity"] = identity_map[model]
    return initial_values


@pytest.fixture(name="oscilloscope_simulator")
def oscilloscope_simulator_fixture(model: str) -> OscilloscopeSimulator:
    """
    Return a oscilloscope simulator.

    :param model: name of the model of the oscilloscope.

    :returns: a oscilloscope simulator.
    """
    return OscilloscopeSimulator(model, power_cycled=True)


@pytest.fixture(name="simulator_test_harness_factory")
def simulator_test_harness_factory_fixture(
    model: str,
    oscilloscope_simulator: OscilloscopeSimulator,
) -> Callable[[], TestEquipmentTangoTestHarness]:
    """
    Return a factory for simulator test harnesses.

    That is, a callable that, when called, constructs and returns a
    test harness for testing against simulators.

    :param model: name of the model of the oscilloscope.
    :param oscilloscope_simulator: the oscilloscope simulator
        that the component manager under test connects to.

    :returns: a factory for hardware test harnesses.
    """

    def create_test_harness() -> TestEquipmentTangoTestHarness:
        test_harness = TestEquipmentTangoTestHarness()
        test_harness.add_oscilloscope_simulator(1, oscilloscope_simulator)
        test_harness.add_oscilloscope_device(
            1,
            model,
            address=None,
        )
        return test_harness

    return create_test_harness


@pytest.fixture(name="hardware_test_harness_factory")
def hardware_test_harness_factory_fixture(
    model: str,
) -> Callable[[], TestEquipmentTangoTestHarness]:
    """
    Return a factory for hardware test harnesses.

    That is, a callable that, when called, constructs and returns a
    test harness for testing against real hardware.

    :param model: name of the model of the oscilloscope.

    :returns: a factory for hardware test harnesses.
    """

    def create_harness() -> TestEquipmentTangoTestHarness:
        address_env_var = f"{model}_ADDRESS"
        if address_env_var not in os.environ:
            pytest.skip(
                "To run test against hardware, environment variable "
                f"{address_env_var} must be set."
            )

        [protocol, host, port_str] = os.environ[address_env_var].split(":")
        assert protocol in get_args(SupportedProtocol)
        port = int(port_str)

        test_harness = TestEquipmentTangoTestHarness()
        test_harness.add_oscilloscope_device(
            1,
            model,
            address=(protocol, host, port),
        )
        return test_harness

    return create_harness


@pytest.fixture(name="test_harness")
def test_harness_fixture(
    request: SubRequest,
    simulator_test_harness_factory: Callable[[], TestEquipmentTangoTestHarness],
    hardware_test_harness_factory: Callable[[], TestEquipmentTangoTestHarness],
) -> TestEquipmentTangoTestHarness:
    """
    Return a test harness for a oscilloscope.

    This fixture supports two kinds of test harness:
    a simulator-based harness and one that acts on real hardware.
    It must be indirectly parametrized to return the desired
    harness(es).

    :param request: A pytest object giving access to the requesting test
        context.
    :param simulator_test_harness_factory:
        A factory for a simulator-based test harness
    :param hardware_test_harness_factory:
        A factory for a hardware-based test harness

    :raises ValueError: if parametrized with a value other than
        "Simulator" or "Hardware".

    :returns: a context manager factory for an oscilloscope.
    """
    if request.param == "Simulator":
        return simulator_test_harness_factory()
    if request.param == "Hardware":
        return hardware_test_harness_factory()
    raise ValueError(
        "Oscilloscope fixtures can only be parametrized on "
        '"Simulator" and "Hardware".'
    )


@pytest.fixture()
def oscilloscope_component_manager(
    test_harness: TestEquipmentTangoTestHarness,
    interface_definition: InterfaceDefinitionType,
    logger: logging.Logger,
    communication_status_callback: Callable,
    component_state_callback: Callable,
) -> Iterator[OscilloscopeComponentManager]:
    """
    Return the oscilloscope component manager under test.

    :param test_harness: a context manager that yields a running test
        context.
    :param interface_definition: definition of the oscilloscope's
        SCPI interface.
    :param logger: a logger for the component manager to use.
    :param communication_status_callback: a callback to be registered
        with the component manager, to be called when the status of
        communication with the hardware changes.
    :param component_state_callback: a callback to be registered with
        the component manager, to be called when the state of the component
        changes.

    :yields: a oscilloscope component manager, in a test harness
        context.
    """
    with test_harness as test_harness_context:
        host, port = test_harness_context.get_context("oscilloscope_simulator_1")

        yield OscilloscopeComponentManager(
            interface_definition,
            "tcp",
            host,
            port,
            logger,
            communication_status_callback,
            component_state_callback,
            update_rate=1.0,
        )


@pytest.fixture()
def oscilloscope_device(
    test_harness: TestEquipmentTangoTestHarness,
) -> Iterator[tango.DeviceProxy]:
    """
    Yield a proxy to the oscilloscope, running in a test context.

    :param test_harness: a context manager that yields a running test
        context.

    :yields: a proxy to a oscilloscope device, running in the test
        harness context.
    """
    with test_harness as tango_context:
        yield tango_context.get_device("test-itf/scope/1")


@pytest.fixture()
def change_event_callbacks() -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of change event callbacks with asynchrony support.

    :returns: a collections.defaultdict that returns change event
        callbacks by name.
    """
    return MockTangoEventCallbackGroup(
        "acquisition",
        "acquisition_mode",
        "binary_field_width",
        "busy",
        "ch1_attenuation",
        "ch1_bandwidth",
        "ch1_clipping",
        "ch1_coupling",
        "ch1_display_vertical_scale",
        "ch1_ext_attenuation",
        "ch1_offset",
        "ch1_parameters",
        "ch1_position",
        "ch1_probe_gain",
        "ch1_probe_units",
        "ch1_select",
        "ch1_trigger_a_level",
        "ch1_trigger_b_level",
        "ch1_vertical_position",
        "ch1_vertical_scale",
        "ch1_vertical_scale_ratio",
        "ch2_attenuation",
        "ch2_bandwidth",
        "ch2_clipping",
        "ch2_coupling",
        "ch2_display_vertical_scale",
        "ch2_ext_attenuation",
        "ch2_offset",
        "ch2_parameters",
        "ch2_position",
        "ch2_probe_gain",
        "ch2_probe_units",
        "ch2_select",
        "ch2_trigger_a_level",
        "ch2_trigger_b_level",
        "ch2_vertical_position",
        "ch2_vertical_scale",
        "ch2_vertical_scale_ratio",
        "ch3_attenuation",
        "ch3_bandwidth",
        "ch3_clipping",
        "ch3_coupling",
        "ch3_display_vertical_scale",
        "ch3_ext_attenuation",
        "ch3_offset",
        "ch3_parameters",
        "ch3_position",
        "ch3_probe_gain",
        "ch3_probe_units",
        "ch3_select",
        "ch3_trigger_a_level",
        "ch3_trigger_b_level",
        "ch3_vertical_position",
        "ch3_vertical_scale",
        "ch3_vertical_scale_ratio",
        "ch4_attenuation",
        "ch4_bandwidth",
        "ch4_clipping",
        "ch4_coupling",
        "ch4_display_vertical_scale",
        "ch4_ext_attenuation",
        "ch4_offset",
        "ch4_parameters",
        "ch4_position",
        "ch4_probe_gain",
        "ch4_probe_units",
        "ch4_select",
        "ch4_trigger_a_level",
        "ch4_trigger_b_level",
        "ch4_vertical_position",
        "ch4_vertical_scale",
        "ch4_vertical_scale_ratio",
        "current_settings",
        "data_encoding",
        "data_source",
        "data_source_available",
        "data_start",
        "data_stop",
        "data_width",
        "date",
        "display_select_source",
        "display_select_view",
        "display_spectrum_source",
        "flag_when_complete",
        "horizontal_delay_mode",
        "horizontal_delay_time",
        "horizontal_divisions",
        "horizontal_duration",
        "horizontal_increment",
        "horizontal_mode",
        "horizontal_position",
        "horizontal_sample_rate",
        "horizontal_scale",
        "identity",
        "operation_complete",
        "sweep_points",
        "time",
        "trace_data",
        "trace_format",
        "trace_parameters",
        "trigger_zero",
        "trace_source",
        "vertical_offset",
        "vertical_position",
        "vertical_scale",
        "adminMode",
        "command_error",
        "device_error",
        "execution_error",
        "fault",
        "power",
        "power_cycled",
        "query_error",
        "state",
        timeout=5.0,
    )
