"""Unit tests of the oscilloscope Tango device."""
import logging

import pytest
import tango
from ska_control_model import AdminMode
from ska_ser_scpi import InterfaceDefinitionType
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

from ska_ser_test_equipment.oscilloscope import OscilloscopeDevice

LOG_LEVEL = logging.DEBUG
logging.basicConfig(level=LOG_LEVEL)
caplog = logging.getLogger(__name__)


@pytest.mark.parametrize(
    "test_harness", ["Simulator"], indirect=True
)  # pylint: disable-next=too-few-public-methods
class TestAgainstOscilloscopeSimulator:
    """
    Tests of the oscilloscope device against a oscilloscope simulator.

    This class is for tests that must be tested against a simulator. For
    example, tests where we put the simulator into a particular state,
    and then check that our monitoring and control layer correctly
    reports that state; or, we execute a command on our monitoring and
    control layer, and then check that the simulator has been driven
    into the expected state.

    It is also for basic device tests that don't trigger any particular
    interaction with the underlying oscilloscope, so don't need to
    be run against real hardware.
    """

    def test_init(  # pylint: disable=no-self-use
        self,
        oscilloscope_device: OscilloscopeDevice,
        interface_definition: InterfaceDefinitionType,
    ) -> None:
        """
        Test device initialisation.

        :param oscilloscope_device: the oscilloscope Tango device
            under test.
        :param interface_definition: definition of the oscilloscope's
            SCPI interface.
        """
        assert oscilloscope_device.adminMode == AdminMode.OFFLINE
        assert oscilloscope_device.state() == tango.DevState.DISABLE

        device_attribute_list = oscilloscope_device.get_attribute_list()
        for attr_name, spec in interface_definition["attributes"].items():
            if "field_type" not in list(spec.values())[0]:
                continue  # not an attribute
            assert attr_name in device_attribute_list


@pytest.mark.parametrize(
    "test_harness",
    ["Simulator", "Hardware"],
    indirect=True,
)  # pylint: disable-next=too-few-public-methods
class TestAgainstOscilloscopeServer:
    """
    Tests of the oscilloscope device against a oscilloscope.

    This class is for tests that require only that a oscilloscope
    server is running. This server could be a simulator, or it could be
    real hardware. Tests in this class are run twice: once against a
    simulator, once against real hardware.
    """

    def test_monitor_and_control(  # pylint: disable=no-self-use
        self,
        oscilloscope_device: OscilloscopeDevice,
        interface_definition: InterfaceDefinitionType,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test device's monitoring of the oscilloscope.

        :param oscilloscope_device: the oscilloscope Tango device
            under test.
        :param interface_definition: definition of the oscilloscope's
            SCPI interface.
        :param change_event_callbacks: dictionary of mock change event
            callbacks with asynchrony support.
        """
        assert oscilloscope_device.adminMode == AdminMode.OFFLINE
        assert oscilloscope_device.state() == tango.DevState.DISABLE

        oscilloscope_device.subscribe_event(
            "adminMode",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["adminMode"],
        )
        change_event_callbacks.assert_change_event("adminMode", AdminMode.OFFLINE)

        oscilloscope_device.subscribe_event(
            "state",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["state"],
        )
        change_event_callbacks.assert_change_event("state", tango.DevState.DISABLE)

        device_attribute_list = oscilloscope_device.get_attribute_list()
        for attribute, spec in interface_definition["attributes"].items():
            if "field_type" not in list(spec.values())[0]:
                continue

            assert attribute in device_attribute_list

            oscilloscope_device.subscribe_event(
                attribute,
                tango.EventType.CHANGE_EVENT,
                change_event_callbacks[attribute],
            )

        for attribute, spec in interface_definition["attributes"].items():
            if "field_type" not in list(spec.values())[0]:
                continue

            change_event_callbacks[attribute].assert_against_call(
                attribute_quality=tango.AttrQuality.ATTR_INVALID
            )
            assert getattr(oscilloscope_device, attribute) is None

        # TODO: Figure out how to make @attribute recognisable as a @property.
        # pylint: disable-next=line-too-long
        oscilloscope_device.adminMode = AdminMode.ONLINE  # type: ignore[assignment] # noqa: E501

        change_event_callbacks.assert_change_event("adminMode", AdminMode.ONLINE)

        assert oscilloscope_device.adminMode == AdminMode.ONLINE

        change_event_callbacks.assert_change_event("state", tango.DevState.UNKNOWN)
        change_event_callbacks.assert_change_event("state", tango.DevState.ON)

        # TODO make it work
        # for attribute, spec in interface_definition["attributes"].items():
        #     if "field_type" not in list(spec.values())[0]:
        #         caplog.debug("Skip event callback for attribute %s", attribute)
        #         continue  # not an attribute
        #     caplog.info("Change event callback for attribute %s", attribute)
        #     # We'll receive a change event for each attribute, but we don't
        #     # know what value we'll get. All we know is it will be valid.
        #     change_event_callbacks[attribute].assert_against_call(
        #         attribute_quality=tango.AttrQuality.ATTR_VALID
        #     )

        atten_to_write = 10.0
        if oscilloscope_device.ch1_attenuation == pytest.approx(atten_to_write):
            # Oops, the power_dbm is already set to -10.0
            # we had better choose a different value to test our write with
            atten_to_write = 9.0

        # Now let's test we can control the oscilloscope, by modifying a
        # value.
        oscilloscope_device.ch1_attenuation = atten_to_write
        change_event_callbacks["ch1_attenuation"].assert_change_event(
            pytest.approx(atten_to_write)
        )
        assert oscilloscope_device.ch1_attenuation == pytest.approx(atten_to_write)
