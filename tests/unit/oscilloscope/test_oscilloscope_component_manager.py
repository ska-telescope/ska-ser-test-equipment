"""Tests of the OscilloscopeComponentManager."""
import logging
from typing import Any, Dict

import pytest
from ska_control_model import CommunicationStatus, PowerState, TaskStatus
from ska_tango_testing.mock import MockCallable

from ska_ser_test_equipment.oscilloscope import (
    OscilloscopeComponentManager,
    OscilloscopeSimulator,
)

LOG_LEVEL = logging.DEBUG
logging.basicConfig(level=LOG_LEVEL)
caplog = logging.getLogger(__name__)


@pytest.mark.parametrize("test_harness", ["Simulator"], indirect=True)
class TestAgainstOscilloscopeSimulator:
    """
    Tests of the signal generator component manager against a simulator.

    This class is for tests that must be tested against a simulator. For
    example, tests where we put the simulator into a particular state,
    and then check that our monitoring and control layer correctly
    reports that state; or, we execute a command on our monitoring and
    control layer, and then check that the simulator has been driven
    into the expected state.
    """

    def test_monitoring(  # pylint: disable=no-self-use, too-many-arguments
        self,
        oscilloscope_simulator: OscilloscopeSimulator,
        oscilloscope_component_manager: OscilloscopeComponentManager,
        communication_status_callback: MockCallable,
        component_state_callback: MockCallable,
        initial_values: Dict[str, Any],
    ) -> None:
        """
        Test the component manager's monitoring of the signal generator.

        :param oscilloscope_simulator: the signal generator simulator
            that the component manager under test connects to.
        :param oscilloscope_component_manager: the component manager
            under test.
        :param communication_status_callback: a callback that has already
            been passed to the component manager, to be called when the
            status of communication with the processor changes.
        :param component_state_callback: a callback that has already been
            passed to the component manager, to be called when the state of
            the processor changes.
        :param initial_values: the initial field values
            that the simulator is set to take.
        """
        assert (
            oscilloscope_component_manager.communication_state
            == CommunicationStatus.DISABLED
        )
        assert (
            oscilloscope_component_manager.component_state["power"]
            == PowerState.UNKNOWN
        )
        assert oscilloscope_component_manager.component_state["fault"] is None

        oscilloscope_component_manager.start_communicating()

        communication_status_callback.assert_call(CommunicationStatus.NOT_ESTABLISHED)
        communication_status_callback.assert_call(CommunicationStatus.ESTABLISHED)

        # TODO: once we are reading error registers / buffers, the fault call
        # won't occur until we have started properly polling.
        component_state_callback.assert_call(
            identity=initial_values["identity"],
            power=PowerState.ON,
            fault=False,
        )

        # TODO make it work
        # component_state_callback.assert_call(
        #     ch1_attenuation=initial_values["ch1_attenuation"],
        #     query_error=initial_values["query_error"],
        #     device_error=initial_values["device_error"],
        #     execution_error=initial_values["execution_error"],
        #     command_error=initial_values["command_error"],
        #     power_cycled=initial_values["power_cycled"],
        # )

        # TODO make it work
        # The values in the simulator are static, so we won't change unless
        # we change them ourselves.
        # component_state_callback.assert_not_called()

        # Now let's change a value in the simulator, and check that the
        # component manager detects and reports it
        oscilloscope_simulator.set_attribute("ch1_attenuation", 0.2)
        # component_state_callback.assert_call(ch1_attenuation=0.2)

        # Now let's set the device_error attribute to True, and check
        # that we also see the fault status go to True
        oscilloscope_simulator.set_attribute("device_error", True)
        # TODO make it work
        # component_state_callback.assert_call(device_error=True, fault=True)

        oscilloscope_component_manager.stop_communicating()

        communication_status_callback.assert_call(CommunicationStatus.DISABLED)
        assert (
            oscilloscope_component_manager.communication_state
            == CommunicationStatus.DISABLED
        )
        assert (
            oscilloscope_component_manager.component_state["power"]
            == PowerState.UNKNOWN
        )
        assert oscilloscope_component_manager.component_state["fault"] is None

    def test_write(  # pylint: disable=no-self-use, too-many-arguments
        self,
        oscilloscope_simulator: OscilloscopeSimulator,
        oscilloscope_component_manager: OscilloscopeComponentManager,
        communication_status_callback: MockCallable,
        component_state_callback: MockCallable,
        initial_values: Dict[str, Any],
    ) -> None:
        """
        Test the ability to write new values to the signal generator.

        :param oscilloscope_simulator: the signal generator simulator
            that the component manager under test connects to.
        :param oscilloscope_component_manager: the component manager
            under test.
        :param communication_status_callback: a callback that has already
            been passed to the component manager, to be called when the
            status of communication with the processor changes.
        :param component_state_callback: a callback that has already been
            passed to the component manager, to be called when the state of
            the processor changes.
        :param initial_values: the initial field values
            that the simulator is set to take.
        """
        oscilloscope_component_manager.start_communicating()
        communication_status_callback.assert_call(CommunicationStatus.NOT_ESTABLISHED)
        communication_status_callback.assert_call(CommunicationStatus.ESTABLISHED)

        # TODO: once we are reading error registers / buffers, the fault call
        # won't occur until we have started properly polling.
        component_state_callback.assert_call(
            identity=initial_values["identity"],
            power=PowerState.ON,
            fault=False,
        )

        # TODO make it work
        # component_state_callback.assert_call(
        #     ch1_attenuation=initial_values["ch1_attenuation"],
        #     query_error=initial_values["query_error"],
        #     device_error=initial_values["device_error"],
        #     execution_error=initial_values["execution_error"],
        #     command_error=initial_values["command_error"],
        #     power_cycled=initial_values["power_cycled"],
        # )

        oscilloscope_component_manager.write_attribute(ch1_attenuation=0.2)

        caplog.info(
            "CH1 attenuation set to %f",
            oscilloscope_simulator.get_attribute("ch1_attenuation"),
        )

        # TODO make it work
        # component_state_callback.assert_call(ch1_attenuation=0.2)

        # TODO make it work
        # assert oscilloscope_simulator.get_attribute("ch1_attenuation") == 0.2

    def test_reset(  # pylint: disable=no-self-use, too-many-arguments
        self,
        oscilloscope_simulator: OscilloscopeSimulator,
        oscilloscope_component_manager: OscilloscopeComponentManager,
        communication_status_callback: MockCallable,
        component_state_callback: MockCallable,
        task_callback: MockCallable,
        initial_values: Dict[str, Any],
    ) -> None:
        """
        Test the ability to reset the signal generator.

        :param oscilloscope_simulator: the signal generator simulator
            that the component manager under test connects to.
        :param oscilloscope_component_manager: the component manager
            under test.
        :param communication_status_callback: a callback that has already
            been passed to the component manager, to be called when the
            status of communication with the processor changes.
        :param component_state_callback: a callback that has already been
            passed to the component manager, to be called when the state of
            the processor changes.
        :param task_callback: a callback to use when invoking the reset
            command.
        :param initial_values: the initial field values
            that the simulator is set to take.
        """
        oscilloscope_component_manager.start_communicating()
        communication_status_callback.assert_call(CommunicationStatus.NOT_ESTABLISHED)
        communication_status_callback.assert_call(CommunicationStatus.ESTABLISHED)

        # TODO: once we are reading error registers / buffers, the fault call
        # won't occur until we have started properly polling.
        component_state_callback.assert_call(
            identity=initial_values["identity"],
            power=PowerState.ON,
            fault=False,
        )

        # TODO make it work
        # component_state_callback.assert_call(
        #     ch1_attenuation=initial_values["ch1_attenuation"],
        #     query_error=initial_values["query_error"],
        #     device_error=initial_values["device_error"],
        #     execution_error=initial_values["execution_error"],
        #     command_error=initial_values["command_error"],
        #     power_cycled=initial_values["power_cycled"],
        # )

        oscilloscope_component_manager.reset(task_callback)

        task_callback.assert_call(status=TaskStatus.QUEUED)
        task_callback.assert_call(status=TaskStatus.IN_PROGRESS)
        task_callback.assert_call(status=TaskStatus.COMPLETED)

        # TODO make it work
        # component_state_callback.assert_call(ch1_attenuation=0.0)

        caplog.info(
            "CH1 attenuation after reset is %f",
            oscilloscope_simulator.get_attribute("ch1_attenuation"),
        )

        # TODO make it work
        # assert oscilloscope_simulator.get_attribute("ch1_attenuation") == 0.0
