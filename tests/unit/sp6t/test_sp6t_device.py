"""Unit tests of the switch matrix Tango device."""

import pytest
from ska_control_model import AdminMode
from ska_ser_scpi import InterfaceDefinitionType
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from tango import AttrQuality, DevState, EventType

from ska_ser_test_equipment.sp6t import SP6TDevice


@pytest.mark.parametrize(
    "test_harness", ["Simulator"], indirect=True
)  # pylint: disable-next=too-few-public-methods
class TestAgainstSP6TSimulator:
    """
    Tests of the switch matrix device against a simulator.

    This class is for tests that must be tested against a simulator. For
    example, tests where we put the simulator into a particular state,
    and then check that our monitoring and control layer correctly
    reports that state; or, we execute a command on our monitoring and
    control layer, and then check that the simulator has been driven
    into the expected state.

    It is also for basic device tests that don't trigger any particular
    interaction with the underlying switch matrix, so don't need to
    be run against real hardware.
    """

    def test_init(  # pylint: disable=no-self-use
        self,
        sp6t_device: SP6TDevice,
        interface_definition: InterfaceDefinitionType,
    ) -> None:
        """
        Test device initialisation.

        :param sp6t_device: the switch matrix Tango device
            under test.
        :param interface_definition: definition of the switch matrix's
            SCPI interface.
        """
        assert sp6t_device.adminMode == AdminMode.OFFLINE
        assert sp6t_device.state() == DevState.DISABLE

        device_attribute_list = sp6t_device.get_attribute_list()
        for attr_name, spec in interface_definition["attributes"].items():
            if "field_type" not in list(spec.values()):
                continue  # not an attribute
            assert attr_name in device_attribute_list


@pytest.mark.parametrize(
    "test_harness",
    ["Simulator", "Hardware"],
    indirect=True,
)  # pylint: disable-next=too-few-public-methods
class TestAgainstSP6TServer:
    """
    Tests of the switch matrix device against a switch matrix.

    This class is for tests that require only that a switch matrix
    server is running. This server could be a simulator, or it could be
    real hardware. Tests in this class are run twice: once against a
    simulator, once against real hardware.
    """

    def test_monitor_and_control(  # pylint: disable=no-self-use
        self,
        sp6t_device: SP6TDevice,
        change_event_callbacks: MockTangoEventCallbackGroup,
        interface_definition: InterfaceDefinitionType,
    ) -> None:
        """
        Test device's monitoring of the switch matrix.

        :param sp6t_device: the switch matrix Tango device
            under test.
        :param change_event_callbacks: dictionary of mock change event
            callbacks with asynchrony support.
        :param interface_definition: definition of the switch matrix's
            SCPI interface.
        """
        assert sp6t_device.adminMode == AdminMode.OFFLINE
        assert sp6t_device.state() == DevState.DISABLE

        sp6t_device.subscribe_event(
            "adminMode",
            EventType.CHANGE_EVENT,
            change_event_callbacks["adminMode"],
        )
        change_event_callbacks.assert_change_event("adminMode", AdminMode.OFFLINE)

        sp6t_device.subscribe_event(
            "state",
            EventType.CHANGE_EVENT,
            change_event_callbacks["state"],
        )
        change_event_callbacks.assert_change_event("state", DevState.DISABLE)

        device_attribute_list = sp6t_device.get_attribute_list()
        for attribute_name in interface_definition["attributes"].keys():
            assert attribute_name in device_attribute_list

            sp6t_device.subscribe_event(
                attribute_name,
                EventType.CHANGE_EVENT,
                change_event_callbacks[attribute_name],
            )

        for attribute in interface_definition["attributes"].keys():
            change_event_callbacks[attribute].assert_against_call(
                attribute_quality=AttrQuality.ATTR_INVALID
            )
            assert getattr(sp6t_device, attribute) is None

        # TODO: Figure out how to make @attribute recognisable as a @property.
        sp6t_device.adminMode = AdminMode.ONLINE  # type: ignore[assignment]

        change_event_callbacks.assert_change_event("adminMode", AdminMode.ONLINE)

        assert sp6t_device.adminMode == AdminMode.ONLINE

        change_event_callbacks.assert_change_event("state", DevState.UNKNOWN)
        change_event_callbacks.assert_change_event("state", DevState.ON)

        for attribute in interface_definition["attributes"].keys():
            # We'll receive a change event for each attribute, but we don't
            # know what value we'll get. All we know is it will be valid.
            change_event_callbacks[attribute].assert_against_call(
                attribute_quality=AttrQuality.ATTR_VALID
            )

        set_switch_a = 2
        if sp6t_device.switch_a == set_switch_a:
            set_switch_a = 1

        # Now let's test we can control the switch matrix, by modifying a
        # value.
        sp6t_device.switch_a = set_switch_a
        change_event_callbacks["switch_a"].assert_change_event(set_switch_a)
        assert sp6t_device.switch_a == pytest.approx(set_switch_a)  # noqa: E501
