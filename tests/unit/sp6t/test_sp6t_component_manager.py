"""Tests of the SP6TComponentManager."""
from typing import Any, Dict, List

import pytest
from ska_control_model import CommunicationStatus, PowerState, TaskStatus
from ska_tango_testing.mock import MockCallable

from ska_ser_test_equipment.sp6t import SP6TComponentManager, SP6TSimulator


@pytest.mark.parametrize("test_harness", ["Simulator"], indirect=True)
class TestAgainstSP6TSimulator:
    """
    Tests of the switch matrix component manager against a simulator.

    This class is for tests that must be tested against a simulator. For
    example, tests where we put the simulator into a particular state,
    and then check that our monitoring and control layer correctly
    reports that state; or, we execute a command on our monitoring and
    control layer, and then check that the simulator has been driven
    into the expected state.
    """

    def test_monitoring(  # pylint: disable=no-self-use, too-many-arguments
        self,
        sp6t_simulator: SP6TSimulator,
        sp6t_component_manager: SP6TComponentManager,
        communication_status_callback: MockCallable,
        component_state_callback: MockCallable,
        initial_values: Dict[str, Any],
        switch_list: List[str],
    ) -> None:
        """
        Test the component manager's monitoring of the switch matrix.

        :param sp6t_simulator: the switch matrix simulator
            that the component manager under test connects to.
        :param sp6t_component_manager: the component manager
            under test.
        :param communication_status_callback: a callback that has already
            been passed to the component manager, to be called when the
            status of communication with the processor changes.
        :param component_state_callback: a callback that has already been
            passed to the component manager, to be called when the state of
            the processor changes.
        :param initial_values: the initial field
            values that the simulator is set to take.
        :param switch_list: list of switches defined for the switch
            matrix under test.
        """
        assert (
            sp6t_component_manager.communication_state == CommunicationStatus.DISABLED
        )
        assert sp6t_component_manager.component_state["power"] == PowerState.UNKNOWN
        assert sp6t_component_manager.component_state["fault"] is None

        sp6t_component_manager.start_communicating()

        communication_status_callback.assert_call(CommunicationStatus.NOT_ESTABLISHED)
        communication_status_callback.assert_call(CommunicationStatus.ESTABLISHED)

        # TODO: once we are reading error registers / buffers, the fault call
        # won't occur until we have started properly polling.
        component_state_callback.assert_call(
            power=PowerState.ON,
            fault=False,
            model_name=initial_values["model_name"],
        )

        initial_switches: Dict[str, int] = dict.fromkeys(switch_list, 0)

        component_state_callback.assert_call(
            **initial_switches,
        )

        # The values in the simulator are static, so we won't change unless
        # we change them ourselves.
        component_state_callback.assert_not_called()

        # Now let's change a value in the simulator, and check that the
        # component manager detects and reports it

        sp6t_simulator.set_attribute("switch_a", 3)
        component_state_callback.assert_call(switch_a=3)

        sp6t_component_manager.stop_communicating()

        communication_status_callback.assert_call(CommunicationStatus.DISABLED)
        assert (
            sp6t_component_manager.communication_state == CommunicationStatus.DISABLED
        )
        assert sp6t_component_manager.component_state["power"] == PowerState.UNKNOWN
        assert sp6t_component_manager.component_state["fault"] is None

    def test_write(  # pylint: disable=no-self-use, too-many-arguments
        self,
        sp6t_simulator: SP6TSimulator,
        sp6t_component_manager: SP6TComponentManager,  # noqa: E501
        communication_status_callback: MockCallable,
        component_state_callback: MockCallable,
        initial_values: Dict[str, Any],
        switch_list: List[str],
    ) -> None:
        """
        Test the ability to write new values to the switch matrix.

        :param sp6t_simulator: the switch matrix simulator
            that the component manager under test connects to.
        :param sp6t_component_manager: the component manager
            under test.
        :param communication_status_callback: a callback that has already
            been passed to the component manager, to be called when the
            status of communication with the processor changes.
        :param component_state_callback: a callback that has already been
            passed to the component manager, to be called when the state of
            the processor changes.
        :param initial_values: the initial field
            values that the simulator is set to take.
        :param switch_list: list of switches defined for the switch
            matrix under test.
        """
        sp6t_component_manager.start_communicating()
        communication_status_callback.assert_call(CommunicationStatus.NOT_ESTABLISHED)
        communication_status_callback.assert_call(CommunicationStatus.ESTABLISHED)

        # TODO: once we are reading error registers / buffers, the fault call
        # won't occur until we have started properly polling.
        component_state_callback.assert_call(
            model_name=initial_values["model_name"],
            power=PowerState.ON,
            fault=False,
        )

        initial_switches: Dict[str, bool | int] = dict.fromkeys(switch_list, 0)

        component_state_callback.assert_call(
            **initial_switches,
        )

        set_switches: Dict[str, bool | int] = dict.fromkeys(switch_list, 2)

        sp6t_component_manager.write_attribute(
            **set_switches,
        )

        component_state_callback.assert_call(
            **set_switches,
        )

        for switch in set_switches:
            assert sp6t_simulator.get_attribute(switch) == 2

    def test_reset(  # pylint: disable=no-self-use, too-many-arguments
        self,
        sp6t_simulator: SP6TSimulator,
        sp6t_component_manager: SP6TComponentManager,  # noqa: E501
        communication_status_callback: MockCallable,
        component_state_callback: MockCallable,
        task_callback: MockCallable,
        initial_values: Dict[str, Any],
        switch_list: List[str],
    ) -> None:
        """
        Test the ability to reset the switch matrix.

        :param sp6t_simulator: the switch matrix simulator
            that the component manager under test connects to.
        :param sp6t_component_manager: the component manager
            under test.
        :param communication_status_callback: a callback that has already
            been passed to the component manager, to be called when the
            status of communication with the processor changes.
        :param component_state_callback: a callback that has already been
            passed to the component manager, to be called when the state of
            the processor changes.
        :param task_callback: a callback to use when invoking the reset
            command.
        :param initial_values: the initial field
            values that the simulator is set to take.
        :param switch_list: list of switches defined for the switch
            matrix under test.
        """
        sp6t_component_manager.start_communicating()
        communication_status_callback.assert_call(CommunicationStatus.NOT_ESTABLISHED)
        communication_status_callback.assert_call(CommunicationStatus.ESTABLISHED)

        # TODO: once we are reading error registers / buffers, the fault call
        # won't occur until we have started properly polling.
        component_state_callback.assert_call(
            model_name=initial_values["model_name"],
            power=PowerState.ON,
            fault=False,
        )

        initial_switches: Dict[str, bool | int] = dict.fromkeys(switch_list, 0)

        component_state_callback.assert_call(
            **initial_switches,
        )

        # Set the switches to arbritary value in order to test ability to reset
        # back to default
        set_switches: Dict[str, bool | int] = dict.fromkeys(switch_list, 5)

        sp6t_component_manager.write_attribute(
            **set_switches,
        )

        component_state_callback.assert_call(
            **set_switches,
        )

        sp6t_component_manager.reset(task_callback)

        task_callback.assert_call(status=TaskStatus.QUEUED)
        task_callback.assert_call(status=TaskStatus.IN_PROGRESS)
        task_callback.assert_call(status=TaskStatus.COMPLETED)

        reset_switches: Dict[str, int] = dict.fromkeys(switch_list, 0)

        component_state_callback.assert_call(**reset_switches)

        for switch in reset_switches:
            assert sp6t_simulator.get_attribute(switch) == 0
