"""Unit tests of the spectrum analyser Tango device."""
import pytest
from ska_control_model import AdminMode
from ska_ser_scpi import InterfaceDefinitionType
from ska_tango_testing.mock.placeholders import Anything
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from tango import AttrQuality, DevState, EventType
from tango import __version_info__ as tango_version

from ska_ser_test_equipment.spectrum_analyser import SpectrumAnalyserDevice


@pytest.mark.parametrize(
    "test_harness", ["Simulator"], indirect=True
)  # pylint: disable-next=too-few-public-methods
class TestAgainstSpectrumAnalyserSimulator:
    """
    Tests of the spectrum analyser device against a simulator.

    This class is for tests that must be tested against a simulator. For
    example, tests where we put the simulator into a particular state,
    and then check that our monitoring and control layer correctly
    reports that state; or, we execute a command on our monitoring and
    control layer, and then check that the simulator has been driven
    into the expected state.

    It is also for basic device tests that don't trigger any particular
    interaction with the underlying spectrum analyser, so don't need to
    be run against real hardware.
    """

    def test_init(  # pylint: disable=no-self-use
        self,
        spectrum_analyser_device: SpectrumAnalyserDevice,
        interface_definition: InterfaceDefinitionType,
    ) -> None:
        """
        Test device initialisation.

        :param spectrum_analyser_device: the spectrum analyser Tango device
            under test.
        :param interface_definition: definition of the spectrum analyser's
            SCPI interface.
        """
        assert spectrum_analyser_device.adminMode == AdminMode.OFFLINE
        assert spectrum_analyser_device.state() == DevState.DISABLE

        device_attribute_list = spectrum_analyser_device.get_attribute_list()
        for attr_name, spec in interface_definition["attributes"].items():
            if "field_type" not in spec:
                continue  # not an attribute
            assert attr_name in device_attribute_list


@pytest.mark.parametrize(
    "test_harness",
    ["Simulator", "Hardware"],
    indirect=True,
)  # pylint: disable-next=too-few-public-methods
class TestAgainstSpectrumAnalyserServer:
    """
    Tests of the spectrum analyser device against a spectrum analyser.

    This class is for tests that require only that a spectrum analyser
    server is running. This server could be a simulator, or it could be
    real hardware. Tests in this class are run twice: once against a
    simulator, once against real hardware.
    """

    def test_monitor_and_control(
        self,
        spectrum_analyser_device: SpectrumAnalyserDevice,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        # pylint: disable=no-self-use,too-many-locals,too-many-statements
        """
        Test device's monitoring of the spectrum analyser.

        :param spectrum_analyser_device: the spectrum analyser Tango device
            under test.
        :param change_event_callbacks: dictionary of mock change event
            callbacks with asynchrony support.
        """
        assert spectrum_analyser_device.adminMode == AdminMode.OFFLINE
        assert spectrum_analyser_device.state() == DevState.DISABLE

        spectrum_analyser_device.subscribe_event(
            "longRunningCommandStatus",
            EventType.CHANGE_EVENT,
            change_event_callbacks["command_status"],
        )
        command_events = change_event_callbacks["command_status"]
        command_events.assert_change_event(
            () if tango_version >= (9, 4) else None,  # TODO: remove when we're all 9.4+
        )

        spectrum_analyser_device.subscribe_event(
            "adminMode",
            EventType.CHANGE_EVENT,
            change_event_callbacks["adminMode"],
        )
        change_event_callbacks.assert_change_event("adminMode", AdminMode.OFFLINE)

        spectrum_analyser_device.subscribe_event(
            "state",
            EventType.CHANGE_EVENT,
            change_event_callbacks["state"],
        )
        change_event_callbacks.assert_change_event("state", DevState.DISABLE)

        device_attribute_list = spectrum_analyser_device.get_attribute_list()
        for attribute_name in [
            "attenuation",
            "command_error",
            "continuous",
            "device_error",
            "execution_error",
            "frequency_start",
            "frequency_stop",
            "identity",
            "marker_frequency",
            "marker_power",
            "power_cycled",
            "query_error",
            "trace1",
        ]:
            assert attribute_name in device_attribute_list

            spectrum_analyser_device.subscribe_event(
                attribute_name,
                EventType.CHANGE_EVENT,
                change_event_callbacks[attribute_name],
            )

        for attribute in [
            "attenuation",
            "command_error",
            "continuous",
            "device_error",
            "execution_error",
            "frequency_start",
            "frequency_stop",
            "identity",
            "marker_frequency",
            "marker_power",
            "power_cycled",
            "query_error",
            "trace1",
        ]:
            change_event_callbacks[attribute].assert_against_call(
                attribute_quality=AttrQuality.ATTR_INVALID
            )
            assert getattr(spectrum_analyser_device, attribute) is None

        # TODO: Figure out how to make @attribute recognisable as a @property.
        # pylint: disable-next=line-too-long
        spectrum_analyser_device.adminMode = AdminMode.ONLINE  # type: ignore[assignment] # noqa: E501

        change_event_callbacks.assert_change_event("adminMode", AdminMode.ONLINE)

        assert spectrum_analyser_device.adminMode == AdminMode.ONLINE

        change_event_callbacks.assert_change_event("state", DevState.UNKNOWN)
        change_event_callbacks.assert_change_event("state", DevState.ON)

        for attribute in [
            "attenuation",
            "command_error",
            "continuous",
            "device_error",
            "execution_error",
            "frequency_start",
            "frequency_stop",
            "identity",
            "marker_frequency",
            "marker_power",
            "power_cycled",
            "query_error",
            "trace1",
        ]:
            # We'll receive a change event for each attribute, but we don't
            # know what value we'll get. All we know is it will be valid.
            change_event_callbacks[attribute].assert_against_call(
                attribute_quality=AttrQuality.ATTR_VALID
            )

        frequency_start = 1.0e9  # 1 GHz
        if spectrum_analyser_device.frequency_start == pytest.approx(frequency_start):
            # Oops, the frequency start is already set to 1 GHz
            # we had better choose a different value to test our write with
            frequency_start = 1.1e9

        # Now let's test we can control the spectrum analyser, by modifying a
        # value.
        spectrum_analyser_device.frequency_start = frequency_start
        change_event_callbacks["frequency_start"].assert_change_event(
            pytest.approx(frequency_start)
        )
        assert spectrum_analyser_device.frequency_start == pytest.approx(
            frequency_start
        )  # noqa: E501

        [_, [reset_cmd_id]] = spectrum_analyser_device.Reset()
        command_events.assert_change_event((reset_cmd_id, "QUEUED"))
        command_events.assert_change_event((reset_cmd_id, "IN_PROGRESS"))
        command_events.assert_change_event((reset_cmd_id, "COMPLETED"))

        [_, [command_id]] = spectrum_analyser_device.Acquire()
        command_events.assert_change_event(
            (reset_cmd_id, "COMPLETED", command_id, "QUEUED")
        )
        command_events.assert_change_event(
            (reset_cmd_id, "COMPLETED", command_id, "IN_PROGRESS")
        )
        command_events.assert_change_event(
            (reset_cmd_id, "COMPLETED", command_id, "COMPLETED")
        )

        before = pytest.approx(spectrum_analyser_device.marker_frequency)
        try:
            change_event_callbacks["marker_frequency"].assert_change_event(
                before, lookahead=10, consume_nonmatches=True
            )
        except AssertionError:
            pass

        spectrum_analyser_device.FindPeak()
        evt = change_event_callbacks["marker_frequency"].assert_change_event(Anything)
        assert before != evt["attribute_value"]

        trace = spectrum_analyser_device.trace1

        marker_power = pytest.approx(spectrum_analyser_device.marker_power)
        assert max(trace) == marker_power

        # find each of the maximum-power frequencies (probably only one)
        freq_a = spectrum_analyser_device.frequency_start
        freq_b = spectrum_analyser_device.frequency_stop
        hz_per_sample = (freq_b - freq_a) / (len(trace) - 1)

        peak_candidates_idx = [i for i, x in enumerate(trace) if x == marker_power]
        peak_candidates_freq = [freq_a + i * hz_per_sample for i in peak_candidates_idx]

        assert (
            pytest.approx(spectrum_analyser_device.marker_frequency)
            in peak_candidates_freq
        )
