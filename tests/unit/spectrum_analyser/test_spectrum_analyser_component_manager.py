"""Tests of the SpectrumAnalyserComponentManager."""
from typing import Any, Dict

import pytest
from ska_control_model import CommunicationStatus, PowerState, TaskStatus
from ska_tango_testing.mock import MockCallable

from ska_ser_test_equipment.spectrum_analyser import (
    SpectrumAnalyserComponentManager,
    SpectrumAnalyserSimulator,
)


@pytest.mark.parametrize("test_harness", ["Simulator"], indirect=True)
class TestAgainstSpectrumAnalyserSimulator:
    """
    Tests of the spectrum analyser component manager against a simulator.

    This class is for tests that must be tested against a simulator. For
    example, tests where we put the simulator into a particular state,
    and then check that our monitoring and control layer correctly
    reports that state; or, we execute a command on our monitoring and
    control layer, and then check that the simulator has been driven
    into the expected state.
    """

    def test_monitoring(  # pylint: disable=no-self-use, too-many-arguments
        self,
        spectrum_analyser_simulator: SpectrumAnalyserSimulator,
        spectrum_analyser_component_manager: SpectrumAnalyserComponentManager,
        communication_status_callback: MockCallable,
        component_state_callback: MockCallable,
        initial_values: Dict[str, Any],
    ) -> None:
        """
        Test the component manager's monitoring of the spectrum analyser.

        :param spectrum_analyser_simulator: the spectrum analyser simulator
            that the component manager under test connects to.
        :param spectrum_analyser_component_manager: the component manager
            under test.
        :param communication_status_callback: a callback that has already
            been passed to the component manager, to be called when the
            status of communication with the processor changes.
        :param component_state_callback: a callback that has already been
            passed to the component manager, to be called when the state of
            the processor changes.
        :param initial_values: the initial field
            values that the simulator is set to take.
        """
        assert (
            spectrum_analyser_component_manager.communication_state
            == CommunicationStatus.DISABLED
        )
        assert (
            spectrum_analyser_component_manager.component_state["power"]
            == PowerState.UNKNOWN
        )
        assert spectrum_analyser_component_manager.component_state["fault"] is None

        spectrum_analyser_component_manager.start_communicating()

        communication_status_callback.assert_call(CommunicationStatus.NOT_ESTABLISHED)
        communication_status_callback.assert_call(CommunicationStatus.ESTABLISHED)

        # TODO: once we are reading error registers / buffers, the fault call
        # won't occur until we have started properly polling.
        component_state_callback.assert_call(
            power=PowerState.ON,
            fault=False,
            identity=initial_values["identity"],
        )

        component_state_callback.assert_call(
            attenuation=pytest.approx(initial_values["attenuation"]),
            autoattenuation=initial_values["autoattenuation"],
            command_error=initial_values["command_error"],
            continuous=initial_values["continuous"],
            device_error=initial_values["device_error"],
            execution_error=initial_values["execution_error"],
            frequency_start=pytest.approx(initial_values["frequency_start"]),
            frequency_stop=pytest.approx(initial_values["frequency_stop"]),
            marker_frequency=pytest.approx(initial_values["marker_frequency"]),
            marker_power=pytest.approx(initial_values["marker_power"]),
            power_cycled=initial_values["power_cycled"],
            preamp_enabled=initial_values["preamp_enabled"],
            query_error=initial_values["query_error"],
            reference_level=initial_values["reference_level"],
            trace1_average_count=initial_values["trace1_average_count"],
            trace1=initial_values["trace1"],
            trace1_detection=initial_values["trace1_detection"],
            trace1_function=initial_values["trace1_function"],
            rbw=initial_values["rbw"],
            rbw_actual=pytest.approx(initial_values["rbw_actual"]),
            rbw_auto=initial_values["rbw_auto"],
            rbw_enabled=initial_values["rbw_enabled"],
            vbw=pytest.approx(initial_values["vbw"]),
            vbw_enabled=initial_values["vbw_enabled"],
        )

        # The values in the simulator are static, so we won't change unless
        # we change them ourselves.
        component_state_callback.assert_not_called()

        # Now let's change a value in the simulator, and check that the
        # component manager detects and reports it
        spectrum_analyser_simulator.set_attribute("attenuation", 0.1)
        component_state_callback.assert_call(attenuation=pytest.approx(0.1))

        spectrum_analyser_component_manager.stop_communicating()

        communication_status_callback.assert_call(CommunicationStatus.DISABLED)
        assert (
            spectrum_analyser_component_manager.communication_state
            == CommunicationStatus.DISABLED
        )
        assert (
            spectrum_analyser_component_manager.component_state["power"]
            == PowerState.UNKNOWN
        )
        assert spectrum_analyser_component_manager.component_state["fault"] is None

    def test_write(  # pylint: disable=no-self-use, too-many-arguments
        self,
        spectrum_analyser_simulator: SpectrumAnalyserSimulator,
        spectrum_analyser_component_manager: SpectrumAnalyserComponentManager,
        communication_status_callback: MockCallable,
        component_state_callback: MockCallable,
        initial_values: Dict[str, Any],
    ) -> None:
        """
        Test the ability to write new values to the spectrum analyser.

        :param spectrum_analyser_simulator: the spectrum analyser simulator
            that the component manager under test connects to.
        :param spectrum_analyser_component_manager: the component manager
            under test.
        :param communication_status_callback: a callback that has already
            been passed to the component manager, to be called when the
            status of communication with the processor changes.
        :param component_state_callback: a callback that has already been
            passed to the component manager, to be called when the state of
            the processor changes.
        :param initial_values: the initial field
            values that the simulator is set to take.
        """
        spectrum_analyser_component_manager.start_communicating()
        communication_status_callback.assert_call(CommunicationStatus.NOT_ESTABLISHED)
        communication_status_callback.assert_call(CommunicationStatus.ESTABLISHED)

        # TODO: once we are reading error registers / buffers, the fault call
        # won't occur until we have started properly polling.
        component_state_callback.assert_call(
            identity=initial_values["identity"],
            power=PowerState.ON,
            fault=False,
        )

        component_state_callback.assert_call(
            attenuation=pytest.approx(initial_values["attenuation"]),
            autoattenuation=initial_values["autoattenuation"],
            command_error=initial_values["command_error"],
            continuous=initial_values["continuous"],
            device_error=initial_values["device_error"],
            execution_error=initial_values["execution_error"],
            frequency_start=pytest.approx(initial_values["frequency_start"]),
            frequency_stop=pytest.approx(initial_values["frequency_stop"]),
            marker_frequency=pytest.approx(initial_values["marker_frequency"]),
            marker_power=pytest.approx(initial_values["marker_power"]),
            power_cycled=initial_values["power_cycled"],
            preamp_enabled=initial_values["preamp_enabled"],
            query_error=initial_values["query_error"],
            reference_level=initial_values["reference_level"],
            trace1=initial_values["trace1"],
            trace1_average_count=initial_values["trace1_average_count"],
            trace1_detection=initial_values["trace1_detection"],
            trace1_function=initial_values["trace1_function"],
            rbw=pytest.approx(initial_values["rbw"]),
            rbw_actual=pytest.approx(initial_values["rbw_actual"]),
            rbw_auto=pytest.approx(initial_values["rbw_auto"]),
            rbw_enabled=pytest.approx(initial_values["rbw_enabled"]),
            vbw=pytest.approx(initial_values["vbw"]),
            vbw_enabled=pytest.approx(initial_values["vbw_enabled"]),
        )

        spectrum_analyser_component_manager.write_attribute(
            attenuation=0.1,
            continuous=not initial_values["continuous"],
        )

        component_state_callback.assert_call(
            attenuation=pytest.approx(0.1),
            continuous=not initial_values["continuous"],
        )

        assert spectrum_analyser_simulator.get_attribute(
            "attenuation"
        ) == pytest.approx(0.1)

    def test_reset(  # pylint: disable=no-self-use, too-many-arguments
        self,
        spectrum_analyser_simulator: SpectrumAnalyserSimulator,
        spectrum_analyser_component_manager: SpectrumAnalyserComponentManager,
        communication_status_callback: MockCallable,
        component_state_callback: MockCallable,
        task_callback: MockCallable,
        initial_values: Dict[str, Any],
    ) -> None:
        """
        Test the ability to reset the spectrum analyser.

        :param spectrum_analyser_simulator: the spectrum analyser simulator
            that the component manager under test connects to.
        :param spectrum_analyser_component_manager: the component manager
            under test.
        :param communication_status_callback: a callback that has already
            been passed to the component manager, to be called when the
            status of communication with the processor changes.
        :param component_state_callback: a callback that has already been
            passed to the component manager, to be called when the state of
            the processor changes.
        :param task_callback: a callback to use when invoking the reset
            command.
        :param initial_values: the initial field
            values that the simulator is set to take.
        """
        spectrum_analyser_component_manager.start_communicating()
        communication_status_callback.assert_call(CommunicationStatus.NOT_ESTABLISHED)
        communication_status_callback.assert_call(CommunicationStatus.ESTABLISHED)

        # TODO: once we are reading error registers / buffers, the fault call
        # won't occur until we have started properly polling.
        component_state_callback.assert_call(
            identity=initial_values["identity"],
            power=PowerState.ON,
            fault=False,
        )

        component_state_callback.assert_call(
            attenuation=pytest.approx(initial_values["attenuation"]),
            autoattenuation=initial_values["autoattenuation"],
            continuous=initial_values["continuous"],
            command_error=initial_values["command_error"],
            device_error=initial_values["device_error"],
            execution_error=initial_values["execution_error"],
            frequency_start=pytest.approx(initial_values["frequency_start"]),
            frequency_stop=pytest.approx(initial_values["frequency_stop"]),
            marker_frequency=pytest.approx(initial_values["marker_frequency"]),
            marker_power=pytest.approx(initial_values["marker_power"]),
            power_cycled=initial_values["power_cycled"],
            preamp_enabled=initial_values["preamp_enabled"],
            query_error=initial_values["query_error"],
            rbw=pytest.approx(initial_values["rbw"]),
            rbw_actual=pytest.approx(initial_values["rbw_actual"]),
            rbw_auto=initial_values["rbw_auto"],
            rbw_enabled=initial_values["rbw_enabled"],
            reference_level=initial_values["reference_level"],
            trace1=initial_values["trace1"],
            trace1_average_count=initial_values["trace1_average_count"],
            trace1_detection=initial_values["trace1_detection"],
            trace1_function=initial_values["trace1_function"],
            vbw=pytest.approx(initial_values["vbw"]),
            vbw_enabled=initial_values["vbw_enabled"],
        )

        spectrum_analyser_component_manager.reset(task_callback)

        task_callback.assert_call(status=TaskStatus.QUEUED)
        task_callback.assert_call(status=TaskStatus.IN_PROGRESS)
        task_callback.assert_call(status=TaskStatus.COMPLETED)

        component_state_callback.assert_call(
            frequency_start=pytest.approx(1.4175e9),
            frequency_stop=pytest.approx(1.5825e9),
            reference_level=10.0,
        )

        assert spectrum_analyser_simulator.get_attribute(
            "frequency_start"
        ) == pytest.approx(1.4175e9)

        assert spectrum_analyser_simulator.get_attribute(
            "frequency_stop"
        ) == pytest.approx(1.5825e9)

        assert not spectrum_analyser_simulator.get_attribute("continuous")

    def test_acquire(  # pylint: disable=no-self-use
        self,
        spectrum_analyser_component_manager: SpectrumAnalyserComponentManager,
        communication_status_callback: MockCallable,
        task_callback: MockCallable,
    ) -> None:
        """
        Test the Acquire() command.

        :param spectrum_analyser_component_manager: the component manager
            under test.
        :param communication_status_callback: a callback that has already
            been passed to the component manager, to be called when the
            status of communication with the processor changes.
        :param task_callback: a callback to use when invoking the reset
            command.
        """
        spectrum_analyser_component_manager.start_communicating()
        communication_status_callback.assert_call(CommunicationStatus.NOT_ESTABLISHED)
        communication_status_callback.assert_call(CommunicationStatus.ESTABLISHED)

        spectrum_analyser_component_manager.acquire(task_callback)
        task_callback.assert_call(status=TaskStatus.QUEUED)
        task_callback.assert_call(status=TaskStatus.IN_PROGRESS)
        task_callback.assert_call(status=TaskStatus.COMPLETED)
