"""Test harness for spectrum analyser tests."""
import logging
import os
from typing import Any, Callable, Dict, Iterator, get_args

import pytest
import tango
import tango.test_context
from _pytest.fixtures import SubRequest  # for the type checker
from ska_ser_scpi import InterfaceDefinitionType, SupportedProtocol
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

from ska_ser_test_equipment.spectrum_analyser import (
    SpectrumAnalyserComponentManager,
    SpectrumAnalyserSimulator,
)
from tests.harness import TestEquipmentTangoTestHarness


@pytest.fixture(name="model", params=["SPECMON26B", "RSA5103B"])
def model_fixture(request: SubRequest) -> str:
    """
    Return the spectrum analyser model.

    :param request: A pytest object giving access to the requesting test
        context.

    :returns: the spectrum analyser model.
    """
    return request.param


@pytest.fixture(name="initial_values")
def initial_values_fixture(model: str) -> Dict[str, Any]:
    """
    Return a dictionary of expected spectrum analyser device attribute values.

    :param model: name of the model of the spectrum analyser.

    :returns: expected spectrum analyser device attribute values.
    """
    identity_map = {
        "SPECMON26B": "TEKTRONIX,SPECMON26B,PQ00112, FV:3.9.0031.0",
        "RSA5103B": "TEKTRONIX,RSA5103B,B051464, FV:4.5.0093.0",
    }

    initial_values = SpectrumAnalyserSimulator.DEFAULTS.copy()
    initial_values["identity"] = identity_map[model]
    initial_values["power_cycled"] = True
    return initial_values


@pytest.fixture(name="spectrum_analyser_simulator")
def spectrum_analyser_simulator_fixture(
    model: str,
) -> SpectrumAnalyserSimulator:
    """
    Return a spectrum analyser simulator.

    :param model: name of the model of the spectrum analyser.

    :returns: a spectrum analyser simulator.
    """
    return SpectrumAnalyserSimulator(model, power_cycled=True)


@pytest.fixture(name="simulator_test_harness_factory")
def simulator_test_harness_factory_fixture(
    model: str,
    spectrum_analyser_simulator: SpectrumAnalyserSimulator,
) -> Callable[[], TestEquipmentTangoTestHarness]:
    """
    Return a factory for simulator test harnesses.

    That is, a callable that, when called, constructs and returns a
    test harness for testing against simulators.

    :param model: name of the model of the spectrum analyser.
    :param spectrum_analyser_simulator: the spectrum analyser simulator
        that the component manager under test connects to.

    :returns: a factory for hardware test harnesses.
    """

    def create_test_harness() -> TestEquipmentTangoTestHarness:
        test_harness = TestEquipmentTangoTestHarness()
        test_harness.add_spectrum_analyser_simulator(1, spectrum_analyser_simulator)
        test_harness.add_spectrum_analyser_device(
            1,
            model,
            address=None,
        )
        return test_harness

    return create_test_harness


@pytest.fixture(name="hardware_test_harness_factory")
def hardware_test_harness_factory_fixture(
    model: str,
) -> Callable[[], TestEquipmentTangoTestHarness]:
    """
    Return a factory for hardware test harnesses.

    That is, a callable that, when called, constructs and returns a
    test harness for testing against real hardware.

    :param model: name of the model of the spectrum analyser.

    :returns: a factory for hardware test harnesses.
    """

    def create_harness() -> TestEquipmentTangoTestHarness:
        address_env_var = f"{model}_ADDRESS"
        if address_env_var not in os.environ:
            pytest.skip(
                "To run test against hardware, environment variable "
                f"{address_env_var} must be set."
            )

        [protocol, host, port_str] = os.environ[address_env_var].split(":")
        assert protocol in get_args(SupportedProtocol)
        port = int(port_str)

        test_harness = TestEquipmentTangoTestHarness()
        test_harness.add_spectrum_analyser_device(
            1,
            model,
            address=(protocol, host, port),
        )
        return test_harness

    return create_harness


@pytest.fixture(name="test_harness")
def test_harness_fixture(
    request: SubRequest,
    simulator_test_harness_factory: Callable[[], TestEquipmentTangoTestHarness],
    hardware_test_harness_factory: Callable[[], TestEquipmentTangoTestHarness],
) -> TestEquipmentTangoTestHarness:
    """
    Return a test harness for a spectrum analyser.

    This fixture supports two kinds of test harness:
    a simulator-based harness and one that acts on real hardware.
    It must be indirectly parametrized to return the desired
    harness(es).

    :param request: A pytest object giving access to the requesting test
        context.
    :param simulator_test_harness_factory:
        A factory for a simulator-based test harness.
    :param hardware_test_harness_factory:
        A factory for a hardware-based test harness.

    :raises ValueError: if parametrized with a value other than
        "Simulator" or "Hardware".

    :returns: a context manager factory for a spectrum analyser.
    """
    if request.param == "Simulator":
        return simulator_test_harness_factory()
    if request.param == "Hardware":
        return hardware_test_harness_factory()
    raise ValueError(
        "spectrum analyser fixtures can only be parametrized on "
        '"Simulator" and "Hardware".'
    )


@pytest.fixture()
def spectrum_analyser_component_manager(
    test_harness: TestEquipmentTangoTestHarness,
    interface_definition: InterfaceDefinitionType,
    logger: logging.Logger,
    communication_status_callback: Callable,
    component_state_callback: Callable,
) -> Iterator[SpectrumAnalyserComponentManager]:
    """
    Return the spectrum analyser component manager under test.

    :param test_harness: a context manager that yields a running test
        context.
    :param interface_definition: definition of the spectrum analyser's
        SCPI interface.
    :param logger: a logger for the component manager to use.
    :param communication_status_callback: a callback to be registered
        with the component manager, to be called when the status of
        communication with the hardware changes.
    :param component_state_callback: a callback to be registered with
        the component manager, to be called when the state of the component
        changes.

    :yields: a spectrum analyser component manager, in a test harness context.
    """
    with test_harness as test_harness_context:
        host, port = test_harness_context.get_context("spectrum_analyser_simulator_1")

        yield SpectrumAnalyserComponentManager(
            interface_definition,
            "tcp",
            host,
            port,
            logger,
            communication_status_callback,
            component_state_callback,
            update_rate=1.0,  # speed it up for testing purposes
        )


@pytest.fixture()
def spectrum_analyser_device(
    test_harness: TestEquipmentTangoTestHarness,
) -> Iterator[tango.DeviceProxy]:
    """
    Yield a proxy to the spectrum analyser, running in a test context.

    :param test_harness: a context manager that yields a running test
        context.

    :yields: a proxy to a spectrum analyser device, running in the test
        harness context.
    """
    with test_harness as test_harness_context:
        yield test_harness_context.get_device("test-itf/spectana/1")


@pytest.fixture()
def change_event_callbacks() -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of change event callbacks with asynchrony support.

    :returns: a collections.defaultdict that returns change event
        callbacks by name.
    """
    return MockTangoEventCallbackGroup(
        "adminMode",
        "attenuation",
        "command_error",
        "command_status",
        "continuous",
        "device_error",
        "execution_error",
        "frequency_start",
        "frequency_stop",
        "identity",
        "marker_frequency",
        "marker_power",
        "power_cycled",
        "query_error",
        "state",
        "trace1",
        timeout=5.0,
    )
