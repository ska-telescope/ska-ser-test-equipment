"""Unit tests of the programmable attenuator Tango device."""

import pytest
from ska_control_model import AdminMode
from ska_ser_scpi import InterfaceDefinitionType
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from tango import AttrQuality, DevState, EventType

from ska_ser_test_equipment.prog_attenuator import ProgAttenuatorDevice


@pytest.mark.parametrize(
    "test_harness", ["Simulator"], indirect=True
)  # pylint: disable-next=too-few-public-methods
class TestAgainstProgAttenuatorSimulator:
    """
    Tests of the programmable attenuator device against a simulator.

    This class is for tests that must be tested against a simulator. For
    example, tests where we put the simulator into a particular state,
    and then check that our monitoring and control layer correctly
    reports that state; or, we execute a command on our monitoring and
    control layer, and then check that the simulator has been driven
    into the expected state.

    It is also for basic device tests that don't trigger any particular
    interaction with the underlying programmable attenuator, so don't need to
    be run against real hardware.
    """

    def test_init(  # pylint: disable=no-self-use
        self,
        prog_attenuator_device: ProgAttenuatorDevice,
        interface_definition: InterfaceDefinitionType,
    ) -> None:
        """
        Test device initialisation.

        :param prog_attenuator_device: the programmable attenuator Tango device
            under test.
        :param interface_definition: definition of the programmable attenuator's
            SCPI interface.
        """
        assert prog_attenuator_device.adminMode == AdminMode.OFFLINE
        assert prog_attenuator_device.state() == DevState.DISABLE

        device_attribute_list = prog_attenuator_device.get_attribute_list()
        for attr_name, spec in interface_definition["attributes"].items():
            if "field_type" not in list(spec.values()):
                continue  # not an attribute
            assert attr_name in device_attribute_list


@pytest.mark.parametrize(
    "test_harness",
    ["Simulator", "Hardware"],
    indirect=True,
)  # pylint: disable-next=too-few-public-methods
class TestAgainstProgAttenuatorServer:
    """
    Tests of the programmable attenuator device against a programmable attenuator.

    This class is for tests that require only that a programmable attenuator
    server is running. This server could be a simulator, or it could be
    real hardware. Tests in this class are run twice: once against a
    simulator, once against real hardware.
    """

    def test_monitor_and_control(  # pylint: disable=no-self-use
        self,
        prog_attenuator_device: ProgAttenuatorDevice,
        change_event_callbacks: MockTangoEventCallbackGroup,
        interface_definition: InterfaceDefinitionType,
    ) -> None:
        """
        Test device's monitoring of the programmable attenuator.

        :param prog_attenuator_device: the programmable attenuator Tango device
            under test.
        :param change_event_callbacks: dictionary of mock change event
            callbacks with asynchrony support.
        :param interface_definition: definition of the programmable attenuator's
            SCPI interface.
        """
        assert prog_attenuator_device.adminMode == AdminMode.OFFLINE
        assert prog_attenuator_device.state() == DevState.DISABLE

        prog_attenuator_device.subscribe_event(
            "adminMode",
            EventType.CHANGE_EVENT,
            change_event_callbacks["adminMode"],
        )
        change_event_callbacks.assert_change_event("adminMode", AdminMode.OFFLINE)

        prog_attenuator_device.subscribe_event(
            "state",
            EventType.CHANGE_EVENT,
            change_event_callbacks["state"],
        )
        change_event_callbacks.assert_change_event("state", DevState.DISABLE)

        device_attribute_list = prog_attenuator_device.get_attribute_list()
        for attribute in interface_definition["attributes"].keys():
            assert attribute in device_attribute_list

            prog_attenuator_device.subscribe_event(
                attribute,
                EventType.CHANGE_EVENT,
                change_event_callbacks[attribute],
            )

        for attribute in interface_definition["attributes"].keys():
            change_event_callbacks[attribute].assert_against_call(
                attribute_quality=AttrQuality.ATTR_INVALID
            )
            assert getattr(prog_attenuator_device, attribute) is None

        # TODO: Figure out how to make @attribute recognisable as a @property.
        prog_attenuator_device.adminMode = AdminMode.ONLINE  # type: ignore[assignment]

        change_event_callbacks.assert_change_event("adminMode", AdminMode.ONLINE)

        assert prog_attenuator_device.adminMode == AdminMode.ONLINE

        change_event_callbacks.assert_change_event("state", DevState.UNKNOWN)
        change_event_callbacks.assert_change_event("state", DevState.ON)

        for attribute in interface_definition["attributes"].keys():
            # We'll receive a change event for each attribute, but we don't
            # know what value we'll get. All we know is it will be valid.
            change_event_callbacks[attribute].assert_against_call(
                attribute_quality=AttrQuality.ATTR_VALID
            )

        channel_1 = 12.34
        if prog_attenuator_device.channel_1 == 12.34:
            channel_1 = 43.21
        # Now let's test we can control the programmable attenuator, by modifying a
        # value.
        prog_attenuator_device.channel_1 = channel_1
        change_event_callbacks["channel_1"].assert_change_event(channel_1)
        assert prog_attenuator_device.channel_1 == pytest.approx(
            channel_1
        )  # noqa: E501
