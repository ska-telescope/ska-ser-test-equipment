"""Tests of the ProgAttenuatorComponentManager."""
from typing import Any, Dict, List

import pytest
from ska_control_model import CommunicationStatus, PowerState, TaskStatus
from ska_tango_testing.mock import MockCallable

from ska_ser_test_equipment.prog_attenuator import (
    ProgAttenuatorComponentManager,
    ProgAttenuatorSimulator,
)


@pytest.mark.parametrize("test_harness", ["Simulator"], indirect=True)
class TestAgainstProgAttenuatorSimulator:
    """
    Tests of the programmable attenuator's component manager against a simulator.

    This class is for tests that must be tested against a simulator. For
    example, tests where we put the simulator into a particular state,
    and then check that our monitoring and control layer correctly
    reports that state; or, we execute a command on our monitoring and
    control layer, and then check that the simulator has been driven
    into the expected state.
    """

    def test_monitoring(  # pylint: disable=no-self-use, too-many-arguments
        self,
        prog_attenuator_simulator: ProgAttenuatorSimulator,
        prog_attenuator_component_manager: ProgAttenuatorComponentManager,
        communication_status_callback: MockCallable,
        component_state_callback: MockCallable,
        initial_values: Dict[str, Any],
        channel_list: List[str],
    ) -> None:
        """
        Test the component manager's monitoring of the programmable attenuator.

        :param prog_attenuator_simulator: the programmable attenuator simulator
            that the component manager under test connects to.
        :param prog_attenuator_component_manager: the component manager
            under test.
        :param communication_status_callback: a callback that has already
            been passed to the component manager, to be called when the
            status of communication with the processor changes.
        :param component_state_callback: a callback that has already been
            passed to the component manager, to be called when the state of
            the processor changes.
        :param initial_values: the initial field
            values that the simulator is set to take.
        :param channel_list: list of channels defined for the programmable
            attenuator under test.
        """
        assert (
            prog_attenuator_component_manager.communication_state
            == CommunicationStatus.DISABLED
        )
        assert (
            prog_attenuator_component_manager.component_state["power"]
            == PowerState.UNKNOWN
        )
        assert prog_attenuator_component_manager.component_state["fault"] is None

        prog_attenuator_component_manager.start_communicating()

        communication_status_callback.assert_call(CommunicationStatus.NOT_ESTABLISHED)
        communication_status_callback.assert_call(CommunicationStatus.ESTABLISHED)

        # TODO: once we are reading error registers / buffers, the fault call
        # won't occur until we have started properly polling.
        component_state_callback.assert_call(
            power=PowerState.ON,
            fault=False,
            model_name=initial_values["model_name"],
        )

        initial_channels: Dict[str, float] = dict.fromkeys(channel_list, 0.0)

        component_state_callback.assert_call(
            **initial_channels,
        )

        # The values in the simulator are static, so we won't change unless
        # we change them ourselves.
        component_state_callback.assert_not_called()

        # Now let's change a value in the simulator, and check that the
        # component manager detects and reports it

        prog_attenuator_simulator.set_attribute("channel_1", 15.75)
        component_state_callback.assert_call(channel_1=15.75)

        prog_attenuator_component_manager.stop_communicating()

        communication_status_callback.assert_call(CommunicationStatus.DISABLED)
        assert (
            prog_attenuator_component_manager.communication_state
            == CommunicationStatus.DISABLED
        )
        assert (
            prog_attenuator_component_manager.component_state["power"]
            == PowerState.UNKNOWN
        )
        assert prog_attenuator_component_manager.component_state["fault"] is None

    def test_write(  # pylint: disable=no-self-use, too-many-arguments
        self,
        prog_attenuator_simulator: ProgAttenuatorSimulator,
        prog_attenuator_component_manager: ProgAttenuatorComponentManager,  # noqa: E501
        communication_status_callback: MockCallable,
        component_state_callback: MockCallable,
        initial_values: Dict[str, Any],
        channel_list: List[str],
    ) -> None:
        """
        Test the ability to write new values to the programmable attenuator.

        :param prog_attenuator_simulator: the programmable attenuator simulator
            that the component manager under test connects to.
        :param prog_attenuator_component_manager: the component manager
            under test.
        :param communication_status_callback: a callback that has already
            been passed to the component manager, to be called when the
            status of communication with the processor changes.
        :param component_state_callback: a callback that has already been
            passed to the component manager, to be called when the state of
            the processor changes.
        :param initial_values: the initial field
            values that the simulator is set to take.
        :param channel_list: list of channels defined for the programmable
            attenuator under test.
        """
        prog_attenuator_component_manager.start_communicating()
        communication_status_callback.assert_call(CommunicationStatus.NOT_ESTABLISHED)
        communication_status_callback.assert_call(CommunicationStatus.ESTABLISHED)

        # TODO: once we are reading error registers / buffers, the fault call
        # won't occur until we have started properly polling.
        component_state_callback.assert_call(
            model_name=initial_values["model_name"],
            power=PowerState.ON,
            fault=False,
        )

        initial_channels: Dict[str, float] = dict.fromkeys(channel_list, 0.0)

        component_state_callback.assert_call(
            **initial_channels,
        )

        set_channels: Dict[str, float] = dict.fromkeys(channel_list, 12.25)

        prog_attenuator_component_manager.write_attribute(
            **set_channels,
        )

        component_state_callback.assert_call(
            **set_channels,
        )

        for channel in set_channels:
            assert prog_attenuator_simulator.get_attribute(channel) == 12.25

    def test_reset(  # pylint: disable=no-self-use, too-many-arguments
        self,
        prog_attenuator_simulator: ProgAttenuatorSimulator,
        prog_attenuator_component_manager: ProgAttenuatorComponentManager,  # noqa: E501
        communication_status_callback: MockCallable,
        component_state_callback: MockCallable,
        task_callback: MockCallable,
        initial_values: Dict[str, Any],
        channel_list: List[str],
    ) -> None:
        """
        Test the ability to reset the programmable attenuator.

        :param prog_attenuator_simulator: the programmable attenuator simulator
            that the component manager under test connects to.
        :param prog_attenuator_component_manager: the component manager
            under test.
        :param communication_status_callback: a callback that has already
            been passed to the component manager, to be called when the
            status of communication with the processor changes.
        :param component_state_callback: a callback that has already been
            passed to the component manager, to be called when the state of
            the processor changes.
        :param task_callback: a callback to use when invoking the reset
            command.
        :param initial_values: the initial field
            values that the simulator is set to take.
        :param channel_list: list of channels defined for the programmable
            attenuator under test.
        """
        prog_attenuator_component_manager.start_communicating()
        communication_status_callback.assert_call(CommunicationStatus.NOT_ESTABLISHED)
        communication_status_callback.assert_call(CommunicationStatus.ESTABLISHED)

        # TODO: once we are reading error registers / buffers, the fault call
        # won't occur until we have started properly polling.
        component_state_callback.assert_call(
            model_name=initial_values["model_name"],
            power=PowerState.ON,
            fault=False,
        )

        initial_channels: Dict[str, float] = dict.fromkeys(channel_list, 0.0)

        component_state_callback.assert_call(
            **initial_channels,
        )

        # Set the channels to arbritary value in order to test ability to reset
        # back to default
        set_channels: Dict[str, float] = dict.fromkeys(channel_list, 10.50)

        prog_attenuator_component_manager.write_attribute(
            **set_channels,
        )

        component_state_callback.assert_call(
            **set_channels,
        )

        prog_attenuator_component_manager.reset(task_callback)

        task_callback.assert_call(status=TaskStatus.QUEUED)
        task_callback.assert_call(status=TaskStatus.IN_PROGRESS)
        task_callback.assert_call(status=TaskStatus.COMPLETED)

        reset_channels: Dict[str, float] = dict.fromkeys(channel_list, 0.0)

        component_state_callback.assert_call(**reset_channels)

        for channel in reset_channels:
            assert prog_attenuator_simulator.get_attribute(channel) == 0
