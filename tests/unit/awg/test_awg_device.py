"""Unit tests of the arbitrary waveform generator (AWG) Tango device."""
import time
from typing import Tuple

import pytest
import tango
from ska_control_model import AdminMode
from ska_ser_scpi import InterfaceDefinitionType
from ska_tango_testing.mock.placeholders import Anything
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

from ska_ser_test_equipment.awg import AwgDevice

AWG_PLAYING = 2  # AWG Playing waveform ( green play icon)
AWG_STOPPED_PLAYING = 0  # AWG Stopped ( not playing - grayed out)


@pytest.mark.parametrize(
    "test_harness", ["Simulator"], indirect=True
)  # pylint: disable-next=too-few-public-methods
class TestAgainstAwgSimulator:  # pylint: disable=too-few-public-methods
    """
    Tests of the AWG device against an AWG simulator.

    This class is for tests that must be tested against a simulator. For
    example, tests where we put the simulator into a particular state,
    and then check that our monitoring and control layer correctly
    reports that state; or, we execute a command on our monitoring and
    control layer, and then check that the simulator has been driven
    into the expected state.

    It is also for basic device tests that don't trigger any particular
    interaction with the underlying AWG, so don't need to be run against
    real hardware.
    """

    def test_init(  # pylint: disable=no-self-use
        self,
        awg_device: AwgDevice,
        interface_definition: InterfaceDefinitionType,
    ) -> None:
        """
        Test device initialisation.

        :param awg_device: the AWG Tango device under test.
        :param interface_definition: definition of the AWG's SCPI
            interface.
        """
        assert awg_device.adminMode == AdminMode.OFFLINE
        assert awg_device.state() == tango.DevState.DISABLE

        device_attribute_list = awg_device.get_attribute_list()
        for attr_name, spec in interface_definition["attributes"].items():
            if "field_type" not in spec:
                continue  # not an attribute
            assert attr_name in device_attribute_list


@pytest.mark.parametrize(
    "test_harness",
    ["Simulator", "Hardware"],
    indirect=True,
)
class TestAgainstAwgServer:
    """
    Tests of the AWG device against an AWG.

    This class is for tests that require only that an AWG server is
    running. This server could be a simulator, or it could be real
    hardware. Tests in this class are run twice: once against a
    simulator, once against real hardware.
    """

    def test_monitor_and_control(
        self,
        awg_device: AwgDevice,
        interface_definition: InterfaceDefinitionType,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test device's monitoring of the AWG.

        :param awg_device: the AWG Tango device under test.
        :param interface_definition: definition of the AWG's SCPI
            interface.
        :param change_event_callbacks: dictionary of mock change event
            callbacks with asynchrony support.
        """
        assert awg_device.adminMode == AdminMode.OFFLINE
        assert awg_device.state() == tango.DevState.DISABLE

        self._subscribe_events(awg_device, change_event_callbacks, interface_definition)

        awg_device.adminMode = AdminMode.ONLINE  # type: ignore[assignment]
        change_event_callbacks.assert_change_event("adminMode", AdminMode.ONLINE)
        assert awg_device.adminMode == AdminMode.ONLINE

        change_event_callbacks.assert_change_event("state", tango.DevState.UNKNOWN)
        change_event_callbacks.assert_change_event("state", tango.DevState.ON)

        for attribute, spec in interface_definition["attributes"].items():
            if "field_type" not in list(spec.values())[0]:
                continue  # not an attribute
            if "read" not in spec:
                continue  # not readable

            # We'll receive a change event for each attribute, but we don't
            # know what value we'll get. All we know is it will be valid.
            change_event_callbacks[attribute].assert_against_call(
                attribute_quality=tango.AttrQuality.ATTR_VALID
            )

        instrument_mode_to_write = "FGEN"
        if awg_device.instrument_mode == instrument_mode_to_write:
            # instrument_mode already FGEN so choose AWG instead
            instrument_mode_to_write = "AWG"

        # Now let's test we can control the AWG, by modifying a value.
        awg_device.instrument_mode = instrument_mode_to_write
        change_event_callbacks["instrument_mode"].assert_change_event(
            instrument_mode_to_write
        )
        assert awg_device.instrument_mode == instrument_mode_to_write

        # Test setting Chan 1 Frequency below 0 raises an error.
        with pytest.raises(
            tango.DevFailed,
            match="Set value for attribute channel1_freq is below the minimum",
        ):
            awg_device.channel1_freq = -10

    def _subscribe_events(  # pylint: disable=no-self-use
        self,
        awg_device: AwgDevice,
        change_event_callbacks: MockTangoEventCallbackGroup,
        interface_definition: InterfaceDefinitionType,
    ) -> None:
        awg_device.subscribe_event(
            "adminMode",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["adminMode"],
        )
        change_event_callbacks.assert_change_event("adminMode", AdminMode.OFFLINE)

        awg_device.subscribe_event(
            "state",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["state"],
        )

        change_event_callbacks.assert_change_event("state", tango.DevState.DISABLE)

        device_attribute_list = awg_device.get_attribute_list()

        for attribute, spec in interface_definition["attributes"].items():
            if "read" not in spec:
                continue
            if "field_type" not in list(spec.values())[0]:
                continue

            assert attribute in device_attribute_list

            awg_device.subscribe_event(
                attribute,
                tango.EventType.CHANGE_EVENT,
                change_event_callbacks[attribute],
            )

        for attribute, spec in interface_definition["attributes"].items():
            if "read" not in spec:
                continue
            if "field_type" not in list(spec.values())[0]:
                continue

            change_event_callbacks[attribute].assert_against_call(
                attribute_quality=tango.AttrQuality.ATTR_INVALID
            )
            assert getattr(awg_device, attribute) is None

        awg_device.subscribe_event(
            "longRunningCommandStatus",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["command_status"],
        )

        change_event_callbacks.assert_change_event("command_status", Anything)

    def _check_attribute_validity(  # pylint: disable=no-self-use
        self,
        interface_definition: InterfaceDefinitionType,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        for attribute, spec in interface_definition["attributes"].items():
            if "field_type" not in list(spec.values())[0]:
                continue  # not an attribute
            if "read" not in spec:
                continue  # not readable
            # We'll receive a change event for each attribute, but we don't
            # know what value we'll get. All we know is it will be valid.
            change_event_callbacks[attribute].assert_against_call(
                attribute_quality=tango.AttrQuality.ATTR_VALID
            )

    def _setup_fgen_waveforms(
        self, awg_device: AwgDevice, change_event_callbacks: MockTangoEventCallbackGroup
    ) -> None:
        """
        Run specific ITF operations for Analog chain correlation test.

        Produce 2 111 MHz sine waves on channels 1 and 4 at 0 dB amplitude power
        :param awg_device: the AWG Tango device under test.
        :param change_event_callbacks: dictionary of mock change event callbacks.
        """
        lookahead = 100
        wave = "SINE"  # Type of Waveform same type for both channels
        freq = 111e6  # Hz  (Frequency is locked for both)
        power = 0  # dBm 632.5 mVpp

        if awg_device.channel1_type != wave:
            awg_device.channel1_type = wave
            change_event_callbacks["channel1_type"].assert_change_event(
                wave, lookahead=lookahead
            )

        assert awg_device.channel1_type == wave
        if awg_device.channel1_freq != pytest.approx(freq):
            awg_device.channel1_freq = freq
            change_event_callbacks["channel1_freq"].assert_change_event(
                pytest.approx(freq), lookahead=lookahead
            )
        assert awg_device.channel1_freq == pytest.approx(freq)
        if awg_device.channel1_amplitude_power != pytest.approx(power):
            awg_device.channel1_amplitude_power = power
            change_event_callbacks["channel1_amplitude_power"].assert_change_event(
                pytest.approx(power), lookahead=lookahead
            )
        assert awg_device.channel1_amplitude_power == pytest.approx(power)

        if awg_device.channel4_type != wave:
            awg_device.channel4_type = wave
            change_event_callbacks["channel4_type"].assert_change_event(
                wave, lookahead=lookahead
            )
        assert awg_device.channel4_type == wave
        if awg_device.channel4_freq != pytest.approx(freq):
            awg_device.channel4_freq = freq
            change_event_callbacks["channel4_freq"].assert_change_event(
                pytest.approx(freq), lookahead=lookahead
            )
        assert awg_device.channel4_freq == pytest.approx(freq)

        if awg_device.channel4_amplitude_power != pytest.approx(power):
            awg_device.channel4_amplitude_power = power
            change_event_callbacks["channel4_amplitude_power"].assert_change_event(
                pytest.approx(power), lookahead=lookahead
            )
        assert awg_device.channel4_amplitude_power == pytest.approx(power)

        # All outputs should already off after reset
        self._set_output_state(awg_device, "1", True, change_event_callbacks)
        self._set_output_state(awg_device, "4", True, change_event_callbacks)

    def _set_output_state(  # pylint: disable=no-self-use
        self,
        awg_device: AwgDevice,
        chan: str,
        desired_value: bool,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        attribute = f"channel{chan}_output_state"
        current_value = getattr(awg_device, attribute)
        if current_value is not desired_value:
            setattr(awg_device, attribute, desired_value)
            change_event_callbacks[attribute].assert_change_event(
                desired_value, lookahead=100
            )
            value = getattr(awg_device, attribute)
            assert (
                value is desired_value
            ), f"{attribute} expected: {desired_value} actual: {value}"

    def _reset_task_completed(  # pylint: disable=no-self-use
        self, command_id: str, status_pair: Tuple[str, str]
    ) -> bool:
        return command_id == status_pair[0] and "COMPLETED" == status_pair[1]

    def test_itf_correlation_awg_inputs_using_fgen(
        self,
        awg_device: AwgDevice,
        interface_definition: InterfaceDefinitionType,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test the AWG Function Generator operations.

        :param awg_device: the AWG Tango device under test.
        :param interface_definition: definition of the AWG's SCPI
            interface.
        :param change_event_callbacks: dictionary of mock change event callbacks.
        """
        assert awg_device.adminMode == AdminMode.OFFLINE
        assert awg_device.state() == tango.DevState.DISABLE

        self._subscribe_events(awg_device, change_event_callbacks, interface_definition)

        awg_device.adminMode = AdminMode.ONLINE  # type: ignore[assignment]
        change_event_callbacks.assert_change_event("adminMode", AdminMode.ONLINE)
        assert awg_device.adminMode == AdminMode.ONLINE

        change_event_callbacks.assert_change_event("state", tango.DevState.UNKNOWN)
        change_event_callbacks.assert_change_event("state", tango.DevState.ON)

        self._check_attribute_validity(interface_definition, change_event_callbacks)

        [_, [command_id]] = awg_device.Reset()

        change_event_callbacks.assert_change_event(
            "command_status", (command_id, "QUEUED")
        )
        change_event_callbacks.assert_change_event(
            "command_status", (command_id, "IN_PROGRESS")
        )
        change_event_callbacks.assert_change_event(
            "command_status", (command_id, "COMPLETED")
        )

        self._check_attributes_have_reset_values(awg_device)

        awg_device.instrument_mode = "FGEN"
        change_event_callbacks["instrument_mode"].assert_change_event(
            "FGEN", lookahead=10
        )
        assert awg_device.instrument_mode == "FGEN"
        self._setup_fgen_waveforms(awg_device, change_event_callbacks)
        awg_device.Play()
        change_event_callbacks["playing"].assert_change_event(AWG_PLAYING)

        time.sleep(5)
        # now turn off
        awg_device.Stop()
        change_event_callbacks["playing"].assert_change_event(AWG_STOPPED_PLAYING)

    def _check_attributes_have_reset_values(  # pylint: disable=no-self-use
        self, awg_device: AwgDevice
    ) -> None:
        assert (
            awg_device.instrument_mode == "AWG"
        ), "after reset instrument mode should be AWG instead is FGEN"
        assert (
            awg_device.clock_source == "INT"
        ), "after reset click source should be INT"
        assert awg_device.clock_sample_rate == pytest.approx(
            2.5e9
        ), "after reset clock sample arte should be apprx 2.5e9"
        assert (
            awg_device.playing == 0
        ), f"after device reset playing should be 0 actual = {awg_device.playing}"
        for i in range(1, 9):
            channel_type = getattr(awg_device, f"channel{i}_type")
            channel_freq = getattr(awg_device, f"channel{i}_freq")
            channel_phase = getattr(awg_device, f"channel{i}_phase")
            channel_amp_power = getattr(awg_device, f"channel{i}_amplitude_power")
            channel_output_state = getattr(awg_device, f"channel{i}_output_state")

            assert (
                channel_type == "SINE"
            ), f"channel{i}_type expected: SINE actual: {channel_type}"
            assert channel_freq == pytest.approx(
                1.2e6
            ), f"channel{i}_freq expected: 1.2e6 Hz actual: {channel_freq}"
            assert channel_phase == pytest.approx(
                0.0
            ), f"channel{i}_phase expected: 0 actual: {channel_phase}"
            assert channel_amp_power == pytest.approx(
                1.4806253546
            ), f"channel{i}_amp. power expected: 1.4806 actual: {channel_amp_power}"
            assert (
                channel_output_state is False
            ), f"channel{i}_output_state expected: False actual: {channel_output_state}"

    def test_send_scpi(
        self,
        awg_device: AwgDevice,
        interface_definition: InterfaceDefinitionType,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test sending raw scpi commands to the AWG.

        :param awg_device: the AWG Tango device under test.
        :param interface_definition: definition of the AWG's SCPI
            interface.
        :param change_event_callbacks: dictionary of mock change event
            callbacks with asynchrony support.
        """
        assert awg_device.adminMode == AdminMode.OFFLINE
        assert awg_device.state() == tango.DevState.DISABLE

        self._subscribe_events(awg_device, change_event_callbacks, interface_definition)

        awg_device.adminMode = AdminMode.ONLINE  # type: ignore[assignment]
        change_event_callbacks.assert_change_event("adminMode", AdminMode.ONLINE)
        assert awg_device.adminMode == AdminMode.ONLINE

        change_event_callbacks.assert_change_event("state", tango.DevState.UNKNOWN)
        change_event_callbacks.assert_change_event("state", tango.DevState.ON)

        self._check_attribute_validity(interface_definition, change_event_callbacks)

        [_, [command_id]] = awg_device.Reset()

        change_event_callbacks.assert_change_event(
            "command_status", (command_id, "QUEUED")
        )
        change_event_callbacks.assert_change_event(
            "command_status", (command_id, "IN_PROGRESS")
        )
        change_event_callbacks.assert_change_event(
            "command_status", (command_id, "COMPLETED")
        )

        self._check_attributes_have_reset_values(awg_device)

        awg_device.instrument_mode = "FGEN"
        change_event_callbacks["instrument_mode"].assert_change_event(
            "FGEN", lookahead=10, consume_nonmatches=True
        )
        assert awg_device.instrument_mode == "FGEN"

        awg_device.channel1_freq = 23.5
        change_event_callbacks["channel1_freq"].assert_change_event(
            pytest.approx(23.5), lookahead=10
        )
        assert awg_device.channel1_freq == pytest.approx(23.5)

        awg_device.Write("FGEN:CHANNEL1:FREQUENCY 34.7")
        chan1_freq = float(awg_device.Query("FGEN:CHANNEL1:FREQUENCY?"))
        assert chan1_freq == pytest.approx(
            34.7
        ), f"chan1_freq: expected: approx 34.7 actual: {chan1_freq}"

        awg_device.Write("*RST")
        change_event_callbacks["instrument_mode"].assert_change_event(
            "AWG", lookahead=10
        )
        self._check_attributes_have_reset_values(awg_device)

    def test_get_scpi(
        self,
        awg_device: AwgDevice,
        interface_definition: InterfaceDefinitionType,
        change_event_callbacks: MockTangoEventCallbackGroup,
    ) -> None:
        """
        Test querying the AWG using raw scpi.

        :param awg_device: the AWG Tango device under test.
        :param interface_definition: definition of the AWG's SCPI
            interface.
        :param change_event_callbacks: dictionary of mock change event
            callbacks with asynchrony support.
        """
        assert awg_device.adminMode == AdminMode.OFFLINE
        assert awg_device.state() == tango.DevState.DISABLE

        self._subscribe_events(awg_device, change_event_callbacks, interface_definition)

        awg_device.adminMode = AdminMode.ONLINE  # type: ignore[assignment]
        change_event_callbacks.assert_change_event("adminMode", AdminMode.ONLINE)
        assert awg_device.adminMode == AdminMode.ONLINE

        change_event_callbacks.assert_change_event("state", tango.DevState.UNKNOWN)
        change_event_callbacks.assert_change_event("state", tango.DevState.ON)

        self._check_attribute_validity(interface_definition, change_event_callbacks)

        [_, [command_id]] = awg_device.Reset()

        change_event_callbacks.assert_change_event(
            "command_status", (command_id, "QUEUED")
        )
        change_event_callbacks.assert_change_event(
            "command_status", (command_id, "IN_PROGRESS")
        )
        change_event_callbacks.assert_change_event(
            "command_status", (command_id, "COMPLETED")
        )

        self._check_attributes_have_reset_values(awg_device)

        awg_device.instrument_mode = "FGEN"
        change_event_callbacks["instrument_mode"].assert_change_event(
            "FGEN", lookahead=10
        )
        assert (
            awg_device.instrument_mode == "FGEN"
        ), f"instrument_mode expected: FGEN actual: {awg_device.instrument_mode}"

        awg_device.channel1_freq = 23.5
        change_event_callbacks["channel1_freq"].assert_change_event(
            pytest.approx(23.5), lookahead=10
        )
        assert awg_device.channel1_freq == pytest.approx(23.5)

        identity = str(awg_device.Query("*IDN?"))
        assert identity == "TEKTRONIX,AWG5208,B040881,FV:8.1.0266.0"

        instrument_mode_via_scpi = awg_device.Query("INSTRUMENT:MODE?")
        assert (
            instrument_mode_via_scpi == "FGEN"
        ), f"Expected FGEN: Actual: {instrument_mode_via_scpi}"
