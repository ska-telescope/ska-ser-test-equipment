"""Test harness for arbitrary waveform generator (AWG) unit tests."""
import logging
import os
from typing import Any, Callable, Dict, Iterator, get_args

import pytest
import tango
import tango.test_context
from _pytest.fixtures import SubRequest  # for the type checker
from ska_ser_scpi import InterfaceDefinitionType, SupportedProtocol
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

from ska_ser_test_equipment.awg import AwgComponentManager, AwgSimulator
from tests.harness import TestEquipmentTangoTestHarness


@pytest.fixture(name="model", params=["AWG5208"])
def model_fixture(request: SubRequest) -> str:
    """
    Return the AWG model.

    :param request: A pytest object giving access to the requesting test
        context.

    :returns: the AWG model.
    """
    return request.param


@pytest.fixture(name="initial_values")
def initial_values_fixture(model: str) -> Dict[str, Any]:
    """
    Return a dictionary of expected AWG device attribute values.

    :param model: name of the model of the AWG.

    :returns: expected AWG device attribute values.
    """
    identity_map = {
        "AWG5208": "TEKTRONIX,AWG5208,B040881,FV:8.1.0266.0",
    }

    initial_values = AwgSimulator.DEFAULTS.copy()
    initial_values["power_cycled"] = True
    initial_values["identity"] = identity_map[model]
    return initial_values


@pytest.fixture(name="awg_simulator")
def awg_simulator_fixture(model: str) -> AwgSimulator:
    """
    Return an AWG simulator.

    :param model: name of the model of the AWG.

    :returns: an AWG simulator.
    """
    return AwgSimulator(model, power_cycled=True)


@pytest.fixture(name="power_limit")
def power_limit_fixture() -> float:
    """
    Return arbitrary value for an AWG device power limit.

    :returns: an arbitrary power limit value.
    """
    return 10.0


@pytest.fixture(name="simulator_test_harness_factory")
def simulator_test_harness_factory_fixture(
    model: str,
    awg_simulator: AwgSimulator,
    power_limit: float,
) -> Callable[[], TestEquipmentTangoTestHarness]:
    """
    Return a factory for simulator test harnesses.

    That is, a callable that, when called, constructs and returns a
    test harness for testing against simulators.

    :param model: name of the model of the AWG.
    :param awg_simulator: the AWG simulator
        that the component manager under test connects to.
    :param power_limit: user defined power limit for the AWG.

    :returns: a factory for hardware test harnesses.
    """

    def create_test_harness() -> TestEquipmentTangoTestHarness:
        test_harness = TestEquipmentTangoTestHarness()
        test_harness.add_awg_simulator(1, awg_simulator)
        test_harness.add_awg_device(
            1,
            model,
            address=None,
            power_limit=power_limit,
        )
        return test_harness

    return create_test_harness


@pytest.fixture(name="hardware_test_harness_factory")
def hardware_test_harness_factory_fixture(
    model: str,
    power_limit: float,
) -> Callable[[], TestEquipmentTangoTestHarness]:
    """
    Return a factory for hardware test harnesses.

    That is, a callable that, when called, constructs and returns a
    test harness for testing against real hardware.

    :param model: name of the model of the AWG.
    :param power_limit: user defined power limit for the AWG.

    :returns: a factory for hardware test harnesses.
    """

    def create_harness() -> TestEquipmentTangoTestHarness:
        address_env_var = f"{model}_ADDRESS"
        if address_env_var not in os.environ:
            pytest.skip(
                "To run test against hardware, environment variable "
                f"{address_env_var} must be set."
            )

        [protocol, host, port_str] = os.environ[address_env_var].split(":")
        assert protocol in get_args(SupportedProtocol)
        port = int(port_str)

        test_harness = TestEquipmentTangoTestHarness()
        test_harness.add_awg_device(
            1,
            model,
            address=(protocol, host, port),
            power_limit=power_limit,
        )
        return test_harness

    return create_harness


@pytest.fixture(name="test_harness")
def test_harness_fixture(
    request: SubRequest,
    simulator_test_harness_factory: Callable[[], TestEquipmentTangoTestHarness],
    hardware_test_harness_factory: Callable[[], TestEquipmentTangoTestHarness],
) -> TestEquipmentTangoTestHarness:
    """
    Return a test harness for an AWG.

    This fixture supports two kinds of test harness:
    a simulator-based harness and one that acts on real hardware.
    It must be indirectly parametrized to return the desired
    harness(es).

    :param request: A pytest object giving access to the requesting test
        context.
    :param simulator_test_harness_factory:
        A factory for a simulator-based test harness.
    :param hardware_test_harness_factory:
        A factory for a hardware-based test harness.

    :raises ValueError: if parametrized with a value other than
        "Simulator" or "Hardware".

    :returns: a context manager factory for an AWG.
    """
    if request.param == "Simulator":
        return simulator_test_harness_factory()
    if request.param == "Hardware":
        return hardware_test_harness_factory()
    raise ValueError(
        "AWG fixtures can only be parametrized on " '"Simulator" and "Hardware".'
    )


@pytest.fixture()
def awg_component_manager(
    test_harness: TestEquipmentTangoTestHarness,
    interface_definition: InterfaceDefinitionType,
    logger: logging.Logger,
    communication_status_callback: Callable,
    component_state_callback: Callable,
) -> Iterator[AwgComponentManager]:
    """
    Return the AWG component manager under test.

    :param test_harness: a context manager that yields a running test
        context.
    :param interface_definition: definition of the AWG's SCPI interface.
    :param logger: a logger for the component manager to use.
    :param communication_status_callback: a callback to be registered
        with the component manager, to be called when the status of
        communication with the hardware changes.
    :param component_state_callback: a callback to be registered with
        the component manager, to be called when the state of the component
        changes.

    :yields: an AWG component manager, in a test harness context.
    """
    with test_harness as test_harness_context:
        host, port = test_harness_context.get_context("awg_simulator_1")

        yield AwgComponentManager(
            interface_definition,
            "tcp",
            host,
            port,
            logger,
            communication_status_callback,
            component_state_callback,
            update_rate=1.0,
        )


@pytest.fixture()
def awg_device(
    test_harness: TestEquipmentTangoTestHarness,
) -> Iterator[tango.DeviceProxy]:
    """
    Yield a proxy to the AWG, running in a test context.

    :param test_harness: a context manager that yields a running test
        context.

    :yields: a proxy to an AWG device, running in the test
        harness context.
    """
    with test_harness as test_harness_context:
        yield test_harness_context.get_device("test-itf/awg/1")


@pytest.fixture()
def change_event_callbacks() -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of change event callbacks with asynchrony support.

    :returns: a collections.defaultdict that returns change event
        callbacks by name.
    """
    return MockTangoEventCallbackGroup(
        "identity",
        "instrument_mode",
        "clock_source",
        "clock_sample_rate",
        *[f"channel{chan}_type" for chan in range(1, 9)],
        *[f"channel{chan}_phase" for chan in range(1, 9)],
        *[f"channel{chan}_freq" for chan in range(1, 9)],
        *[f"channel{chan}_amplitude_power" for chan in range(1, 9)],
        *[f"channel{chan}_output_state" for chan in range(1, 9)],
        "fault",
        "query_error",
        "state",
        "adminMode",
        "power",
        "device_error",
        "execution_error",
        "state",
        "command_error",
        "adminMode",
        "power_cycled",
        "command_status",
        "playing",
        timeout=45.0,
    )
