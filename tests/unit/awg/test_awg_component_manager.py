"""Tests of the AwgComponentManager."""
from typing import Any, Dict

import pytest
from ska_control_model import CommunicationStatus, PowerState, TaskStatus
from ska_tango_testing.mock import MockCallable

from ska_ser_test_equipment.awg import AwgComponentManager, AwgSimulator


@pytest.mark.parametrize("test_harness", ["Simulator"], indirect=True)
class TestAgainstAwgSimulator:
    """
    Tests of the AWG component manager against a simulator.

    This class is for tests that must be tested against a simulator. For
    example, tests where we put the simulator into a particular state,
    and then check that our monitoring and control layer correctly
    reports that state; or, we execute a command on our monitoring and
    control layer, and then check that the simulator has been driven
    into the expected state.
    """

    def test_monitoring(  # pylint: disable=no-self-use, too-many-arguments
        self,
        awg_simulator: AwgSimulator,
        awg_component_manager: AwgComponentManager,
        communication_status_callback: MockCallable,
        component_state_callback: MockCallable,
        initial_values: Dict[str, Any],
    ) -> None:
        """
        Test the component manager's monitoring of the AWG.

        :param awg_simulator: the AWG simulator that the component
             manager under test connects to.
        :param awg_component_manager: the component manager under test
        :param communication_status_callback: a callback that has already
            been passed to the component manager, to be called when the
            status of communication with the processor changes.
        :param component_state_callback: a callback that has already been
            passed to the component manager, to be called when the state of
            the processor changes.
        :param initial_values: the initial field values that the
            simulator is set to take.
        """
        assert awg_component_manager.communication_state == CommunicationStatus.DISABLED
        assert awg_component_manager.component_state["power"] == PowerState.UNKNOWN
        assert awg_component_manager.component_state["fault"] is None

        awg_component_manager.start_communicating()

        communication_status_callback.assert_call(CommunicationStatus.NOT_ESTABLISHED)
        communication_status_callback.assert_call(CommunicationStatus.ESTABLISHED)

        # TODO: once we are reading error registers / buffers, the fault call
        # won't occur until we have started properly polling.
        component_state_callback.assert_call(
            identity=initial_values["identity"],
            power=PowerState.ON,
            fault=False,
        )

        component_state_callback.assert_call(
            query_error=initial_values["query_error"],
            device_error=initial_values["device_error"],
            execution_error=initial_values["execution_error"],
            command_error=initial_values["command_error"],
            power_cycled=initial_values["power_cycled"],
            instrument_mode=initial_values["instrument_mode"],
            clock_source=initial_values["clock_source"],
            clock_sample_rate=initial_values["clock_sample_rate"],
            playing=initial_values["playing"],
            **{
                f"channel{chan}_type": initial_values[f"channel{chan}_type"]
                for chan in range(1, 9)
            },
            **{
                f"channel{chan}_phase": initial_values[f"channel{chan}_phase"]
                for chan in range(1, 9)
            },
            **{
                f"channel{chan}_freq": initial_values[f"channel{chan}_freq"]
                for chan in range(1, 9)
            },
            **{
                f"channel{chan}_amplitude_power": initial_values[
                    f"channel{chan}_amplitude_power"
                ]
                for chan in range(1, 9)
            },
            **{
                f"channel{chan}_output_state": initial_values[
                    f"channel{chan}_output_state"
                ]
                for chan in range(1, 9)
            },
        )

        # The values in the simulator are static, so we won't change unless
        # we change them ourselves.
        component_state_callback.assert_not_called()

        # Now let's change a value in the simulator, and check that the
        # component manager detects and reports it
        awg_simulator.set_attribute("channel1_freq", 2000)
        component_state_callback.assert_call(channel1_freq=pytest.approx(2000))

        # Now let's set the device_error attribute to True, and check
        # that we also see the fault status go to True
        awg_simulator.set_attribute("device_error", True)
        component_state_callback.assert_call(device_error=True, fault=True)

        awg_component_manager.stop_communicating()

        communication_status_callback.assert_call(CommunicationStatus.DISABLED)
        assert awg_component_manager.communication_state == CommunicationStatus.DISABLED
        assert awg_component_manager.component_state["power"] == PowerState.UNKNOWN
        assert awg_component_manager.component_state["fault"] is None

    def test_write(  # pylint: disable=no-self-use, too-many-arguments
        self,
        awg_simulator: AwgSimulator,
        awg_component_manager: AwgComponentManager,
        communication_status_callback: MockCallable,
        component_state_callback: MockCallable,
        initial_values: Dict[str, Any],
    ) -> None:
        """
        Test the ability to write new values to the AWG.

        :param awg_simulator: the AWG simulator that the component
            manager under test connects to.
        :param awg_component_manager: the component manager
            under test.
        :param communication_status_callback: a callback that has already
            been passed to the component manager, to be called when the
            status of communication with the processor changes.
        :param component_state_callback: a callback that has already been
            passed to the component manager, to be called when the state of
            the processor changes.
        :param initial_values: the initial field values that the
            simulator is set to take.
        """
        awg_component_manager.start_communicating()
        communication_status_callback.assert_call(CommunicationStatus.NOT_ESTABLISHED)
        communication_status_callback.assert_call(CommunicationStatus.ESTABLISHED)

        # TODO: once we are reading error registers / buffers, the fault call
        # won't occur until we have started properly polling.
        component_state_callback.assert_call(
            identity=initial_values["identity"],
            power=PowerState.ON,
            fault=False,
        )

        component_state_callback.assert_call(
            query_error=initial_values["query_error"],
            device_error=initial_values["device_error"],
            execution_error=initial_values["execution_error"],
            command_error=initial_values["command_error"],
            power_cycled=initial_values["power_cycled"],
            instrument_mode=initial_values["instrument_mode"],
            clock_source=initial_values["clock_source"],
            clock_sample_rate=initial_values["clock_sample_rate"],
            playing=initial_values["playing"],
            **{
                f"channel{chan}_type": initial_values[f"channel{chan}_type"]
                for chan in range(1, 9)
            },
            **{
                f"channel{chan}_phase": initial_values[f"channel{chan}_phase"]
                for chan in range(1, 9)
            },
            **{
                f"channel{chan}_freq": initial_values[f"channel{chan}_freq"]
                for chan in range(1, 9)
            },
            **{
                f"channel{chan}_amplitude_power": initial_values[
                    f"channel{chan}_amplitude_power"
                ]
                for chan in range(1, 9)
            },
            **{
                f"channel{chan}_output_state": initial_values[
                    f"channel{chan}_output_state"
                ]
                for chan in range(1, 9)
            },
        )

        awg_component_manager.write_attribute(
            instrument_mode="FGEN",
            channel1_type="SQUARE",
            channel1_freq=3000,
            channel1_phase=45,
            channel4_type="SQUARE",
            channel4_freq=2000,
            channel4_phase=-60,
        )

        component_state_callback.assert_call(
            instrument_mode="FGEN",
            channel1_type="SQUARE",
            channel1_freq=3000,
            channel1_phase=45,
            channel4_type="SQUARE",
            channel4_freq=2000,
            channel4_phase=-60,
        )

        assert awg_simulator.get_attribute("instrument_mode") == "FGEN"
        assert awg_simulator.get_attribute("channel1_type") == "SQUARE"
        assert awg_simulator.get_attribute("channel1_freq") == pytest.approx(3000)
        assert awg_simulator.get_attribute("channel1_phase") == pytest.approx(45)
        assert awg_simulator.get_attribute("channel4_type") == "SQUARE"
        assert awg_simulator.get_attribute("channel4_freq") == pytest.approx(2000)
        assert awg_simulator.get_attribute("channel4_phase") == pytest.approx(-60)

    def test_reset(  # pylint: disable=no-self-use, too-many-arguments
        self,
        awg_simulator: AwgSimulator,
        awg_component_manager: AwgComponentManager,
        communication_status_callback: MockCallable,
        component_state_callback: MockCallable,
        task_callback: MockCallable,
        initial_values: Dict[str, Any],
    ) -> None:
        """
        Test the ability to reset the AWG.

        :param awg_simulator: the AWG simulator that the component
            manager under test connects to.
        :param awg_component_manager: the component manager
            under test.
        :param communication_status_callback: a callback that has already
            been passed to the component manager, to be called when the
            status of communication with the processor changes.
        :param component_state_callback: a callback that has already been
            passed to the component manager, to be called when the state of
            the processor changes.
        :param task_callback: a callback to use when invoking the reset
            command.
        :param initial_values: the initial field values that the
            simulator is set to take.
        """
        awg_component_manager.start_communicating()
        communication_status_callback.assert_call(CommunicationStatus.NOT_ESTABLISHED)
        communication_status_callback.assert_call(CommunicationStatus.ESTABLISHED)

        # TODO: once we are reading error registers / buffers, the fault call
        # won't occur until we have started properly polling.
        component_state_callback.assert_call(
            identity=initial_values["identity"],
            power=PowerState.ON,
            fault=False,
        )

        component_state_callback.assert_call(
            query_error=initial_values["query_error"],
            device_error=initial_values["device_error"],
            execution_error=initial_values["execution_error"],
            command_error=initial_values["command_error"],
            power_cycled=initial_values["power_cycled"],
            instrument_mode=initial_values["instrument_mode"],
            clock_source=initial_values["clock_source"],
            clock_sample_rate=initial_values["clock_sample_rate"],
            playing=initial_values["playing"],
            **{
                f"channel{chan}_type": initial_values[f"channel{chan}_type"]
                for chan in range(1, 9)
            },
            **{
                f"channel{chan}_phase": initial_values[f"channel{chan}_phase"]
                for chan in range(1, 9)
            },
            **{
                f"channel{chan}_freq": initial_values[f"channel{chan}_freq"]
                for chan in range(1, 9)
            },
            **{
                f"channel{chan}_amplitude_power": initial_values[
                    f"channel{chan}_amplitude_power"
                ]
                for chan in range(1, 9)
            },
            **{
                f"channel{chan}_output_state": initial_values[
                    f"channel{chan}_output_state"
                ]
                for chan in range(1, 9)
            },
        )

        # Write values that are different to the initial state
        # so that reset will cause a measurable state change.

        awg_component_manager.write_attribute(instrument_mode="foobar")
        component_state_callback.assert_call(instrument_mode="foobar")
        assert awg_simulator.get_attribute("instrument_mode") == "foobar"

        awg_component_manager.write_attribute(clock_source="EFIXED")
        component_state_callback.assert_call(clock_source="EFIXED")
        assert awg_simulator.get_attribute("clock_source") == "EFIXED"

        awg_component_manager.write_attribute(clock_sample_rate=23)
        component_state_callback.assert_call(clock_sample_rate=pytest.approx(23))
        assert awg_simulator.get_attribute("clock_sample_rate") == pytest.approx(23)

        for i in range(1, 9):
            awg_component_manager.write_attribute(**{f"channel{i}_type": "TRIANGLE"})
            component_state_callback.assert_call(**{f"channel{i}_type": "TRIANGLE"})
            assert awg_simulator.get_attribute(f"channel{i}_type") == "TRIANGLE"

            awg_component_manager.write_attribute(**{f"channel{i}_freq": 562.3})
            component_state_callback.assert_call(
                **{f"channel{i}_freq": pytest.approx(562.3)}
            )
            assert awg_simulator.get_attribute(f"channel{i}_freq") == pytest.approx(
                562.3
            )

            awg_component_manager.write_attribute(**{f"channel{i}_amplitude_power": 23})
            component_state_callback.assert_call(
                **{f"channel{i}_amplitude_power": pytest.approx(23)}
            )
            assert awg_simulator.get_attribute(
                f"channel{i}_amplitude_power"
            ) == pytest.approx(23)

            awg_component_manager.write_attribute(**{f"channel{i}_phase": 15})
            component_state_callback.assert_call(
                **{f"channel{i}_phase": pytest.approx(15)}
            )
            assert awg_simulator.get_attribute(f"channel{i}_phase") == pytest.approx(15)

            awg_component_manager.write_attribute(**{f"channel{i}_output_state": True})
            component_state_callback.assert_call(**{f"channel{i}_output_state": True})
            assert awg_simulator.get_attribute(f"channel{i}_output_state") is True

        awg_component_manager.reset(task_callback)

        task_callback.assert_call(status=TaskStatus.QUEUED)
        task_callback.assert_call(status=TaskStatus.IN_PROGRESS)
        task_callback.assert_call(status=TaskStatus.COMPLETED, lookahead=2)

        component_state_callback.assert_call(
            instrument_mode="AWG",
            clock_source="INT",
            clock_sample_rate=pytest.approx(2.5e9),
            **{f"channel{i}_type": "SINE" for i in range(1, 9)},
            **{f"channel{i}_freq": pytest.approx(1.2e6) for i in range(1, 9)},
            **{
                f"channel{i}_amplitude_power": pytest.approx(1.4806253546)
                for i in range(1, 9)
            },
            **{f"channel{i}_phase": pytest.approx(0) for i in range(1, 9)},
            **{f"channel{i}_output_state": False for i in range(1, 9)},
        )

        assert awg_simulator.get_attribute("instrument_mode") == "AWG"
        assert awg_simulator.get_attribute("clock_source") == "INT"
        for i in range(1, 9):
            assert awg_simulator.get_attribute(f"channel{i}_output_state") is False
