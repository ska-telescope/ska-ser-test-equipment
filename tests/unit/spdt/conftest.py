"""Test harness for switch matrix tests."""
import logging
import os
from typing import Any, Callable, Dict, Iterator, List, get_args

import pytest
import tango
import tango.test_context
from _pytest.fixtures import SubRequest  # for the type checker
from ska_ser_scpi import InterfaceDefinitionType, SupportedProtocol
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

from ska_ser_test_equipment.spdt import SPDTComponentManager, SPDTSimulator
from tests.harness import TestEquipmentTangoTestHarness


@pytest.fixture(name="model", params=["ZTRC4SPDTA18", "ZTRC8SPDTA18"])
def model_fixture(request: SubRequest) -> str:
    """
    Return the switch matrix model.

    :param request: A pytest object giving access to the requesting test
        context.

    :returns: the switch matrix model.
    """
    return request.param


@pytest.fixture(name="switch_list")
def switch_list_fixture(
    interface_definition: InterfaceDefinitionType,
) -> List[str]:
    """
    Return a list of switches defined on the switch matrix device.

    :param interface_definition: definition of the switch matrix's
        SCPI interface.

    :returns: list of switches defined on the switch matrix device.
    """
    switch_list: List[str] = [
        key for key in interface_definition["attributes"].keys() if "switch" in key
    ]
    return switch_list


@pytest.fixture(name="initial_values")
def initial_values_fixture(model: str) -> Dict[str, Any]:
    """
    Return a dictionary of expected switch matrix device attribute values.

    :param model: name of the model of the switch matrix.

    :returns: expected switch matrix device attribute values.
    """
    model_name_map = {
        "ZTRC4SPDTA18": "MN=ZTRC-4SPDT-A18",
        "ZTRC8SPDTA18": "MN=ZTRC-8SPDT-A18",
    }

    # pylint: disable-next=protected-access
    initial_values = SPDTSimulator(model)._attribute_values.copy()
    initial_values["model_name"] = model_name_map[model]
    return initial_values


@pytest.fixture(name="spdt_simulator")
def spdt_simulator_fixture(
    model: str,
) -> SPDTSimulator:
    """
    Return a switch matrix simulator.

    :param model: name of the model of the switch matrix.

    :returns: a switch matrix simulator.
    """
    return SPDTSimulator(model)


@pytest.fixture(name="simulator_test_harness_factory")
def simulator_test_harness_factory_fixture(
    model: str,
    spdt_simulator: SPDTSimulator,
) -> Callable[[], TestEquipmentTangoTestHarness]:
    """
    Return a factory for simulator test harnesses.

    That is, a callable that, when called, constructs and returns a
    test harness for testing against simulators.

    :param model: name of the model of the switch matrix.
    :param spdt_simulator: the switch matrix simulator
        that the component manager under test connects to.

    :returns: a factory for hardware test harnesses.
    """

    def create_test_harness() -> TestEquipmentTangoTestHarness:
        test_harness = TestEquipmentTangoTestHarness()
        test_harness.add_spdt_simulator(1, spdt_simulator)
        test_harness.add_spdt_device(
            1,
            model,
            address=None,
        )
        return test_harness

    return create_test_harness


@pytest.fixture(name="hardware_test_harness_factory")
def hardware_test_harness_factory_fixture(
    model: str,
) -> Callable[[], TestEquipmentTangoTestHarness]:
    """
    Return a factory for hardware test harnesses.

    That is, a callable that, when called, constructs and returns a
    test harness for testing against real hardware.

    :param model: name of the model of the switch matrix.

    :returns: a factory for hardware test harnesses.
    """

    def create_harness() -> TestEquipmentTangoTestHarness:
        address_env_var = f"{model}_ADDRESS"
        if address_env_var not in os.environ:
            pytest.skip(
                "To run test against hardware, environment variable "
                f"{address_env_var} must be set."
            )

        [protocol, host, port_str] = os.environ[address_env_var].split(":")
        assert protocol in get_args(SupportedProtocol)
        port = int(port_str)

        test_harness = TestEquipmentTangoTestHarness()
        test_harness.add_spdt_device(
            1,
            model,
            address=(protocol, host, port),
        )
        return test_harness

    return create_harness


@pytest.fixture(name="test_harness")
def test_harness_fixture(
    request: SubRequest,
    simulator_test_harness_factory: Callable[[], TestEquipmentTangoTestHarness],
    hardware_test_harness_factory: Callable[[], TestEquipmentTangoTestHarness],
) -> TestEquipmentTangoTestHarness:
    """
    Return a test harness for a switch matrix.

    This fixture supports two kinds of test harness:
    a simulator-based harness and one that acts on real hardware.
    It must be indirectly parametrized to return the desired
    harness(es).

    :param request: A pytest object giving access to the requesting test
        context.
    :param simulator_test_harness_factory:
        A factory for a simulator-based test harness.
    :param hardware_test_harness_factory:
        A factory for a hardware-based test harness.

    :raises ValueError: if parametrized with a value other than
        "Simulator" or "Hardware".

    :returns: a context manager factory for a switch matrix.
    """
    if request.param == "Simulator":
        return simulator_test_harness_factory()
    if request.param == "Hardware":
        return hardware_test_harness_factory()
    raise ValueError(
        "switch matrix fixtures can only be parametrized on "
        '"Simulator" and "Hardware".'
    )


@pytest.fixture()
def spdt_component_manager(
    test_harness: TestEquipmentTangoTestHarness,
    interface_definition: InterfaceDefinitionType,
    logger: logging.Logger,
    communication_status_callback: Callable,
    component_state_callback: Callable,
) -> Iterator[SPDTComponentManager]:
    """
    Return the switch matrix component manager under test.

    :param test_harness: a context manager that yields a running test
        context.
    :param interface_definition: definition of the switch matrix's
        SCPI interface.
    :param logger: a logger for the component manager to use.
    :param communication_status_callback: a callback to be registered
        with the component manager, to be called when the status of
        communication with the hardware changes.
    :param component_state_callback: a callback to be registered with
        the component manager, to be called when the state of the component
        changes.

    :yields: a switch matrix component manager, in a test harness context.
    """
    with test_harness as test_harness_context:
        host, port = test_harness_context.get_context("spdt_simulator_1")

        yield SPDTComponentManager(
            interface_definition,
            "tcp",
            host,
            port,
            logger,
            communication_status_callback,
            component_state_callback,
            update_rate=3.0,  # speed it up for testing purposes
        )


@pytest.fixture()
def spdt_device(
    test_harness: TestEquipmentTangoTestHarness,
) -> Iterator[tango.DeviceProxy]:
    """
    Yield a proxy to the switch matrix, running in a test context.

    :param test_harness: a context manager that yields a running test
        context.

    :yields: a proxy to a switch matrix device, running in the test
        harness context.
    """
    with test_harness as test_harness_context:
        yield test_harness_context.get_device("test-itf/spdt/1")


@pytest.fixture()
def change_event_callbacks(switch_list: List[str]) -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of change event callbacks with asynchrony support.

    :param switch_list: a list of switches on the switch matrix device.

    :returns: a collections.defaultdict that returns change event
        callbacks by name.
    """
    return MockTangoEventCallbackGroup(
        "adminMode",
        "state",
        "model_name",
        *switch_list,
        timeout=5.0,
    )
