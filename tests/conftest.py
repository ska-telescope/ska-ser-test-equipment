"""Tests of the ska-ser-test-equipment package."""
import pytest
from ska_ser_scpi import InterfaceDefinitionType

from ska_ser_test_equipment.interface_definitions import InterfaceDefinitionFactory


@pytest.fixture()
def interface_definition(model: str) -> InterfaceDefinitionType:
    """
    Return the SCPI interface definition for an instrument model.

    :param model: the name of the instrument model.

    :returns: the SCPI interface definition.
    """
    return InterfaceDefinitionFactory()(model)
