"""This module contains functional test harness configuration."""
import os
from typing import Any, Dict, Generator, get_args

import pytest
import tango
from ska_ser_scpi import SupportedProtocol
from ska_tango_testing.harness import TangoTestHarnessContext
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup

from ska_ser_test_equipment.signal_generator import SignalGeneratorSimulator
from ska_ser_test_equipment.spectrum_analyser import SpectrumAnalyserSimulator
from tests.harness import TestEquipmentTangoTestHarness


# TODO: https://github.com/pytest-dev/pytest-forked/issues/67
# We're stuck on pytest 6.2 until this gets fixed, and this version of
# pytest is not fully typehinted
def pytest_addoption(parser) -> None:  # type: ignore[no-untyped-def]
    """
    Add a command line option to pytest.

    This is a pytest hook, here implemented to add the `--true-context`
    option, used to indicate that a true Tango subsystem is available,
    so there is no need for the test harness to spin up a Tango test
    context.

    :param parser: the command line options parser.
    """
    parser.addoption(
        "--true-context",
        action="store_true",
        default=False,
        help=(
            "Tell pytest that you have a true Tango context and don't "
            "need to spin up a Tango test context"
        ),
    )


@pytest.fixture(name="signal_generator_model", scope="session")
def signal_generator_model_fixture() -> str:
    """
    Return the signal generator model.

    :returns: the signal generator model.
    """
    return "TSG4104A"


@pytest.fixture(name="signal_generator_initial_values", scope="session")
def signal_generator_initial_values_fixture(
    signal_generator_model: str,
) -> Dict[str, Any]:
    """
    Return a dictionary of expected signal generator device attribute values.

    :param signal_generator_model: name of the model of the signal
        generator.

    :returns: expected signal generator device attribute values.
    """
    identities = {
        "TSG4104A": "Tektronix,TSG4104A,s/nC010133,ver2.03.26",
        "SMB100A": "Rohde&Schwarz,SMB100A,1406.6000k03/183286,4.20.028.58",
    }

    initial_values = SignalGeneratorSimulator.DEFAULTS.copy()
    initial_values["identity"] = identities[signal_generator_model]
    initial_values["power_cycled"] = True
    return initial_values


@pytest.fixture(name="spectrum_analyser_model", scope="session")
def spectrum_analyser_model_fixture() -> str:
    """
    Return the spectrum analyser model.

    :returns: the spectrum analyser model.
    """
    return "SPECMON26B"


@pytest.fixture(name="true_context", scope="session")
def true_context_fixture(request: pytest.FixtureRequest) -> bool:
    """
    Return whether to test against an existing Tango deployment.

    If True, then Tango is already deployed, and the tests will be run
    against that deployment.

    If False, then Tango is not deployed, so the test harness will stand
    up a test context and run the tests against that.

    :param request: A pytest object giving access to the requesting test
        context.

    :returns: whether to test against an existing Tango deployment.
    """
    if request.config.getoption("--true-context"):
        return True
    if os.getenv("TRUE_TANGO_CONTEXT", None):
        return True
    return False


@pytest.fixture(name="instrument_addresses", scope="session")
def instrument_addresses_fixture(
    signal_generator_model: str,
    spectrum_analyser_model: str,
) -> dict[str, tuple[str, str, int] | None]:
    """
    Return the addresses of the instruments.

    :param signal_generator_model:
        Name of the model of the signal generator.
    :param spectrum_analyser_model:
        Name of the model of the signal generator.

    :returns: a dictionary where keys are instrument names
        (currently instrument models),
        and values are either protocol-host-port tuples, or None.
        In the latter case, the test harness is responsible
        to launch a simulator in place of the instrument.
    """
    addresses: dict[str, tuple[str, str, int] | None] = {}

    for model in [signal_generator_model, spectrum_analyser_model]:
        address_env_var = f"{signal_generator_model}_ADDRESS"
        if address_env_var in os.environ:
            [protocol, host, port_str] = os.environ[address_env_var].split(":")
            assert protocol in get_args(SupportedProtocol)

            addresses[model] = (protocol, host, int(port_str))
        else:
            addresses[model] = None
    return addresses


@pytest.fixture(scope="session")
def deployment_has_simulators(
    instrument_addresses: dict[str, tuple[str, str, int] | None],
) -> bool:
    """
    Return whether this test deployment has any simulators in it.

    Some BDD test steps can only be run against real hardware; this
    fixture tells those steps whether or not to run.

    :param instrument_addresses: Addresses of the instruments.

    :returns: whether this test deployment has any simulators in it.
    """
    return None in instrument_addresses.values()


@pytest.fixture(name="test_harness_context", scope="session")
def test_harness_context_fixture(
    true_context: bool,
    instrument_addresses: dict[str, tuple[str, str, int] | None],
    signal_generator_model: str,
    spectrum_analyser_model: str,
) -> Generator[TangoTestHarnessContext, None, None]:
    """
    Yield a Tango context containing the devices under test.

    :param true_context: Whether to test against an existing Tango
        deployment.
    :param instrument_addresses: Addresses of the instruments.
    :param signal_generator_model:
        Name of the model of the signal generator.
    :param spectrum_analyser_model:
        Name of the model of the signal generator.

    :yields: a Tango context containing the devices under test.
    """
    test_harness = TestEquipmentTangoTestHarness()
    if not true_context:
        address = instrument_addresses[signal_generator_model]
        if address is None:
            test_harness.add_signal_generator_simulator(
                1,
                SignalGeneratorSimulator(signal_generator_model, power_cycled=True),
            )
        test_harness.add_signal_generator_device(
            1,
            signal_generator_model,
            address=address,
        )

        address = instrument_addresses[spectrum_analyser_model]
        if address is None:
            test_harness.add_spectrum_analyser_simulator(
                1,
                SpectrumAnalyserSimulator(spectrum_analyser_model),
            )
        test_harness.add_spectrum_analyser_device(
            1,
            spectrum_analyser_model,
            address=address,
        )

    with test_harness as test_harness_context:
        yield test_harness_context


@pytest.fixture(scope="session")
def signal_generator_device(
    test_harness_context: TangoTestHarnessContext,
) -> tango.DeviceProxy:
    """
    Return a proxy to the spectrum analyser device.

    :param test_harness_context:
        the test context in which the device is running.

    :returns: a proxy to the signal generator device.
    """
    return test_harness_context.get_device("test-itf/siggen/1")


@pytest.fixture(scope="session")
def spectrum_analyser_device(
    test_harness_context: TangoTestHarnessContext,
) -> tango.DeviceProxy:
    """
    Return a proxy to the spectrum analyser device.

    :param test_harness_context:
        the test context in which the device is running.

    :returns: a proxy to the spectrum analyser device.
    """
    return test_harness_context.get_device("test-itf/spectana/1")


@pytest.fixture(scope="session")
def change_event_callbacks() -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of change event callbacks with asynchrony support.

    :returns: a collections.defaultdict that returns change event
        callbacks by name.
    """
    return MockTangoEventCallbackGroup(
        "siggen_adminMode",
        "siggen_state",
        "siggen_identity",
        "siggen_frequency",
        "siggen_power_dbm",
        "siggen_rf_output_on",
        "siggen_query_error",
        "siggen_device_error",
        "siggen_execution_error",
        "siggen_command_error",
        "siggen_power_cycled",
        "spectana_adminMode",
        "spectana_state",
        "spectana_marker_frequency",
        "spectana_marker_power",
        "spectana_frequency_start",
        "spectana_frequency_stop",
        timeout=10.0,
    )
