# -*- coding: utf-8 -*-
"""This module provides a flexible test harness for testing Tango devices."""
from __future__ import annotations

import threading
from contextlib import contextmanager
from types import TracebackType
from typing import TYPE_CHECKING, Any, Callable, Iterator

from ska_control_model import LoggingLevel
from ska_tango_testing.harness import TangoTestHarness, TangoTestHarnessContext

if TYPE_CHECKING:
    from ska_ser_test_equipment.awg import AwgSimulator
    from ska_ser_test_equipment.oscilloscope import OscilloscopeSimulator
    from ska_ser_test_equipment.prog_attenuator import ProgAttenuatorSimulator
    from ska_ser_test_equipment.signal_generator import SignalGeneratorSimulator
    from ska_ser_test_equipment.sp6t import SP6TSimulator
    from ska_ser_test_equipment.spdt import SPDTSimulator
    from ska_ser_test_equipment.spectrum_analyser import SpectrumAnalyserSimulator
    from ska_ser_test_equipment.spectrum_analyser_anritsu import (
        SpectrumAnalyserSimulatorAnritsu,
    )


@contextmanager
def server_context_manager_factory(
    backend: Callable[[Iterator[bytes]], bytes | None],
) -> Iterator[tuple[str | bytes | bytearray, int]]:
    """
    Return a context manager factory for a server.

    That is, a callable that, when called,
    returns a context manager that spins up a server for the provided backend,
    yields it for use in testing,
    and then shuts its down afterwards.

    :param backend: the backend for the TCP server

    :yields: a server context manager factory.
    """
    # pylint: disable-next=import-outside-toplevel
    from ska_ser_devices.client_server import TcpServer

    server = TcpServer("localhost", 0, backend)
    with server:
        server_thread = threading.Thread(
            name="Signal generator simulator thread",
            target=server.serve_forever,
        )
        server_thread.daemon = True  # don't hang on exit
        server_thread.start()
        yield server.server_address
        server.shutdown()


class TestEquipmentTangoTestHarness:
    """A test harness for testing monitoring and control of test equipment."""

    def __init__(self: TestEquipmentTangoTestHarness) -> None:
        """Initialise a new test harness instance."""
        self._tango_test_harness = TangoTestHarness()
        self.model: str | None = None

    def add_awg_simulator(
        self: TestEquipmentTangoTestHarness,
        awg_id: int,
        simulator: AwgSimulator,
    ) -> None:
        """
        And an AWG simulator to the test harness.

        :param awg_id:
            an ID number for the AWG simulator.
            This is used to ensure that the right Tango device
            connects to the right simulator.
        :param simulator: the simulator backend to be served
            by a TCP server in the test harness.
        """
        self._tango_test_harness.add_context_manager(
            f"awg_simulator_{awg_id}",
            server_context_manager_factory(simulator),
        )

    def add_awg_device(  # pylint: disable=too-many-arguments
        self: TestEquipmentTangoTestHarness,
        awg_id: int,
        model: str,
        address: tuple[str, str, int] | None,
        power_limit: float = 10.0,
        update_rate: float = 1.0,
        logging_level: int = int(LoggingLevel.DEBUG),
    ) -> None:
        """
        Add an AWG Tango device to the test harness.

        :param awg_id: An ID number for the AWG.
            This will be used to constructed the Tango device name,
            and also to point the AWG Tango device
            at the correct AWG simulator, if any.
        :param model: name of the model of the AWG.
        :param address: address of the AWG
            to be monitored and controlled by this Tango device.
            It is a tuple of protocol ("tcp" or "telnet"),
            hostname or IP address, and port.
        :param power_limit: user defined power limit for the AWG.
        :param update_rate: how often updates to attribute values should
            be provided.
        :param logging_level: the Tango device's default logging level.
        """
        host: Callable[[dict[str, Any]], str] | str  # for the type checker
        port: Callable[[dict[str, Any]], int] | int  # for the type checker

        self.model = model

        if address is None:
            server_id = f"awg_simulator_{awg_id}"
            protocol = "tcp"

            def host(context: dict[str, Any]) -> str:
                return context[server_id][0]

            def port(context: dict[str, Any]) -> int:
                return context[server_id][1]

        else:
            (protocol, host, port) = address

        self._tango_test_harness.add_device(
            f"test-itf/awg/{awg_id}",
            "ska_ser_test_equipment.awg.AwgDevice",
            Model=model,
            Protocol=protocol,
            Host=host,
            Port=port,
            PowerLimit=power_limit,
            UpdateRate=update_rate,
            LoggingLevelDefault=logging_level,
        )

    def add_prog_attenuator_simulator(
        self: TestEquipmentTangoTestHarness,
        prog_attenuator_id: int,
        simulator: ProgAttenuatorSimulator,
    ) -> None:
        """
        And a programmable attenuator simulator to the test harness.

        :param prog_attenuator_id:
            an ID number for the programmable attenuator simulator.
            This is used to ensure that the right Tango device
            connects to the right simulator.
        :param simulator: the simulator backend to be served
            by a TCP server in the test harness.
        """
        self._tango_test_harness.add_context_manager(
            f"prog_attenuator_simulator_{prog_attenuator_id}",
            server_context_manager_factory(simulator),
        )

    def add_prog_attenuator_device(  # pylint: disable=too-many-arguments
        self: TestEquipmentTangoTestHarness,
        prog_attenuator_id: int,
        model: str,
        address: tuple[str, str, int] | None,
        update_rate: float = 1.0,
        logging_level: int = int(LoggingLevel.DEBUG),
    ) -> None:
        """
        Add a progammable attenuator Tango device to the test harness.

        :param prog_attenuator_id:
            An ID number for the programmable attenuator.
            This will be used to constructed the Tango device name,
            and also to point the programmable attenuator Tango device
            at the correct programmable attenuator simulator, if any.
        :param model: name of the model of the programmable attenuator.
        :param address: address of the programmable attenuator
            to be monitored and controlled by this Tango device.
            It is a tuple of protocol ("tcp" or "telnet"),
            hostname or IP address, and port.
        :param update_rate: how often updates to attribute values should
            be provided.
        :param logging_level: the Tango device's default logging level.
        """
        host: Callable[[dict[str, Any]], str] | str  # for the type checker
        port: Callable[[dict[str, Any]], int] | int  # for the type checker

        self.model = model

        if address is None:
            server_id = f"prog_attenuator_simulator_{prog_attenuator_id}"
            protocol = "tcp"

            def host(context: dict[str, Any]) -> str:
                return context[server_id][0]

            def port(context: dict[str, Any]) -> int:
                return context[server_id][1]

        else:
            (protocol, host, port) = address

        self._tango_test_harness.add_device(
            f"test-itf/prog_attenuator/{prog_attenuator_id}",
            "ska_ser_test_equipment.prog_attenuator.ProgAttenuatorDevice",
            Model=model,
            Protocol=protocol,
            Host=host,
            Port=port,
            UpdateRate=update_rate,
            LoggingLevelDefault=logging_level,
        )

    def add_signal_generator_simulator(
        self: TestEquipmentTangoTestHarness,
        signal_generator_id: int,
        simulator: SignalGeneratorSimulator,
    ) -> None:
        """
        And a signal generator simulator to the test harness.

        :param signal_generator_id:
            an ID number for the signal generator simulator.
            This is used to ensure that the right Tango device
            connects to the right simulator.
        :param simulator: the simulator backend to be served
            by a TCP server in the test harness.
        """
        self._tango_test_harness.add_context_manager(
            f"signal_generator_simulator_{signal_generator_id}",
            server_context_manager_factory(simulator),
        )

    def add_signal_generator_device(  # pylint: disable=too-many-arguments
        self: TestEquipmentTangoTestHarness,
        signal_generator_id: int,
        model: str,
        address: tuple[str, str, int] | None,
        power_limit: float = 10.0,
        update_rate: float = 1.0,
        logging_level: int = int(LoggingLevel.DEBUG),
    ) -> None:
        """
        Add a signal generator Tango device to the test harness.

        :param signal_generator_id: an ID number for the signal
            generator. This will be used to constructed the Tango device
            name, and also to point the signal generator at the correct
            signal generator simulator, if any.
        :param model: name of the model of the signal generator.
        :param address: address of the signal generator to be monitored
            and controlled by this Tango device. It is a tuple of
            protocol ("tcp" or "telnet"), hostname or IP address, and
            port.
        :param power_limit: user defined power limit for the signal
            generator.
        :param update_rate: how often updates to attribute values should
            be provided.
        :param logging_level: the Tango device's default logging level.
        """
        host: Callable[[dict[str, Any]], str] | str  # for the type checker
        port: Callable[[dict[str, Any]], int] | int  # for the type checker

        self.model = model

        if address is None:
            server_id = f"signal_generator_simulator_{signal_generator_id}"
            protocol = "tcp"

            def host(context: dict[str, Any]) -> str:
                return context[server_id][0]

            def port(context: dict[str, Any]) -> int:
                return context[server_id][1]

        else:
            (protocol, host, port) = address

        self._tango_test_harness.add_device(
            f"test-itf/siggen/{signal_generator_id}",
            "ska_ser_test_equipment.signal_generator.SignalGeneratorDevice",
            Model=model,
            Protocol=protocol,
            Host=host,
            Port=port,
            PowerLimit=power_limit,
            UpdateRate=update_rate,
            LoggingLevelDefault=logging_level,
        )

    def add_spectrum_analyser_simulator(
        self: TestEquipmentTangoTestHarness,
        spectrum_analyser_id: int,
        simulator: SpectrumAnalyserSimulator,
    ) -> None:
        """
        And a spectrum analyser simulator to the test harness.

        :param spectrum_analyser_id:
            an ID number for the spectrum analyser simulator.
            This is used to ensure that the right Tango device
            connects to the right simulator.
        :param simulator: the simulator backend to be served
            by a TCP server in the test harness.
        """
        self._tango_test_harness.add_context_manager(
            f"spectrum_analyser_simulator_{spectrum_analyser_id}",
            server_context_manager_factory(simulator),
        )

    def add_spectrum_analyser_simulator_anritsu(
        self: TestEquipmentTangoTestHarness,
        spectrum_analyser_id: int,
        simulator: SpectrumAnalyserSimulatorAnritsu,
    ) -> None:
        """
        And a spectrum analyser simulator to the test harness.

        :param spectrum_analyser_id:
            an ID number for the spectrum analyser simulator.
            This is used to ensure that the right Tango device
            connects to the right simulator.
        :param simulator: the simulator backend to be served
            by a TCP server in the test harness.
        """
        self._tango_test_harness.add_context_manager(
            f"spectrum_analyser_simulator_{spectrum_analyser_id}",
            server_context_manager_factory(simulator),
        )

    def add_spectrum_analyser_device(  # pylint: disable=too-many-arguments
        self: TestEquipmentTangoTestHarness,
        spectrum_analyser_id: int,
        model: str,
        address: tuple[str, str, int] | None,
        update_rate: float = 1.0,
        logging_level: int = int(LoggingLevel.DEBUG),
    ) -> None:
        """
        Add a spectrum analyser Tango device to the test harness.

        :param spectrum_analyser_id:
            An ID number for the spectrum analyser.
            This will be used to constructed the Tango device name,
            and also to point the spectrum analyser Tango device
            at the correct spectrum analyser simulator, if any.
        :param model: name of the model of the spectrum analyser.
        :param address: address of the spectrum analyser
            to be monitored and controlled by this Tango device.
            It is a tuple of protocol ("tcp" or "telnet"),
            hostname or IP address, and port.
        :param update_rate: how often updates to attribute values should
            be provided.
        :param logging_level: the Tango device's default logging level.
        """
        host: Callable[[dict[str, Any]], str] | str  # for the type checker
        port: Callable[[dict[str, Any]], int] | int  # for the type checker

        self.model = model

        if address is None:
            server_id = f"spectrum_analyser_simulator_{spectrum_analyser_id}"
            protocol = "tcp"

            def host(context: dict[str, Any]) -> str:
                return context[server_id][0]

            def port(context: dict[str, Any]) -> int:
                return context[server_id][1]

        else:
            (protocol, host, port) = address

        if self.model == "MS2090A":
            self._tango_test_harness.add_device(
                f"test-itf/spectana/{spectrum_analyser_id}",
                # pylint: disable-next=line-too-long
                "ska_ser_test_equipment.spectrum_analyser_anritsu.SpectrumAnalyserDeviceAnritsu",  # noqa: E501
                Model=model,
                Protocol=protocol,
                Host=host,
                Port=port,
                UpdateRate=update_rate,
                LoggingLevelDefault=logging_level,
            )
        else:
            self._tango_test_harness.add_device(
                f"test-itf/spectana/{spectrum_analyser_id}",
                "ska_ser_test_equipment.spectrum_analyser.SpectrumAnalyserDevice",
                Model=model,
                Protocol=protocol,
                Host=host,
                Port=port,
                UpdateRate=update_rate,
                LoggingLevelDefault=logging_level,
            )

    def add_sp6t_simulator(
        self: TestEquipmentTangoTestHarness,
        sp6t_id: int,
        simulator: SP6TSimulator,
    ) -> None:
        """
        And a switch matrix simulator to the test harness.

        :param sp6t_id:
            an ID number for the switch matrix simulator.
            This is used to ensure that the right Tango device
            connects to the right simulator.
        :param simulator: the simulator backend to be served
            by a TCP server in the test harness.
        """
        self._tango_test_harness.add_context_manager(
            f"sp6t_simulator_{sp6t_id}",
            server_context_manager_factory(simulator),
        )

    def add_sp6t_device(  # pylint: disable=too-many-arguments
        self: TestEquipmentTangoTestHarness,
        sp6t_id: int,
        model: str,
        address: tuple[str, str, int] | None,
        update_rate: float = 1.0,
        logging_level: int = int(LoggingLevel.DEBUG),
    ) -> None:
        """
        Add a switch matrix Tango device to the test harness.

        :param sp6t_id:
            An ID number for the switch matrix.
            This will be used to constructed the Tango device name,
            and also to point the switch matrix Tango device
            at the correct switch matrix simulator, if any.
        :param model: name of the model of the switch matrix.
        :param address: address of the switch matrix
            to be monitored and controlled by this Tango device.
            It is a tuple of protocol ("tcp" or "telnet"),
            hostname or IP address, and port.
        :param update_rate: how often updates to attribute values should
            be provided.
        :param logging_level: the Tango device's default logging level.
        """
        host: Callable[[dict[str, Any]], str] | str  # for the type checker
        port: Callable[[dict[str, Any]], int] | int  # for the type checker

        self.model = model

        if address is None:
            server_id = f"sp6t_simulator_{sp6t_id}"
            protocol = "tcp"

            def host(context: dict[str, Any]) -> str:
                return context[server_id][0]

            def port(context: dict[str, Any]) -> int:
                return context[server_id][1]

        else:
            (protocol, host, port) = address

        self._tango_test_harness.add_device(
            f"test-itf/sp6t/{sp6t_id}",
            "ska_ser_test_equipment.sp6t.SP6TDevice",
            Model=model,
            Protocol=protocol,
            Host=host,
            Port=port,
            UpdateRate=update_rate,
            LoggingLevelDefault=logging_level,
        )

    def add_spdt_simulator(
        self: TestEquipmentTangoTestHarness,
        spdt_id: int,
        simulator: SPDTSimulator,
    ) -> None:
        """
        And a switch matrix simulator to the test harness.

        :param spdt_id:
            an ID number for the switch matrix simulator.
            This is used to ensure that the right Tango device
            connects to the right simulator.
        :param simulator: the simulator backend to be served
            by a TCP server in the test harness.
        """
        self._tango_test_harness.add_context_manager(
            f"spdt_simulator_{spdt_id}",
            server_context_manager_factory(simulator),
        )

    def add_spdt_device(  # pylint: disable=too-many-arguments
        self: TestEquipmentTangoTestHarness,
        spdt_id: int,
        model: str,
        address: tuple[str, str, int] | None,
        update_rate: float = 1.0,
        logging_level: int = int(LoggingLevel.DEBUG),
    ) -> None:
        """
        Add a switch matrix Tango device to the test harness.

        :param spdt_id:
            An ID number for the switch matrix.
            This will be used to constructed the Tango device name,
            and also to point the switch matrix Tango device
            at the correct switch matrix simulator, if any.
        :param model: name of the model of the switch matrix.
        :param address: address of the switch matrix
            to be monitored and controlled by this Tango device.
            It is a tuple of protocol ("tcp" or "telnet"),
            hostname or IP address, and port.
        :param update_rate: how often updates to attribute values should
            be provided.
        :param logging_level: the Tango device's default logging level.
        """
        host: Callable[[dict[str, Any]], str] | str  # for the type checker
        port: Callable[[dict[str, Any]], int] | int  # for the type checker

        self.model = model

        if address is None:
            server_id = f"spdt_simulator_{spdt_id}"
            protocol = "tcp"

            def host(context: dict[str, Any]) -> str:
                return context[server_id][0]

            def port(context: dict[str, Any]) -> int:
                return context[server_id][1]

        else:
            (protocol, host, port) = address

        self._tango_test_harness.add_device(
            f"test-itf/spdt/{spdt_id}",
            "ska_ser_test_equipment.spdt.SPDTDevice",
            Model=model,
            Protocol=protocol,
            Host=host,
            Port=port,
            UpdateRate=update_rate,
            LoggingLevelDefault=logging_level,
        )

    def add_oscilloscope_simulator(
        self: TestEquipmentTangoTestHarness,
        oscilloscope_id: int,
        simulator: OscilloscopeSimulator,
    ) -> None:
        """
        And an oscilloscope simulator to the test harness.

        :param oscilloscope_id:
            an ID number for the oscilloscope simulator.
            This is used to ensure that the right Tango device
            connects to the right simulator.
        :param simulator: the simulator backend to be served
            by a TCP server in the test harness.
        """
        self._tango_test_harness.add_context_manager(
            f"oscilloscope_simulator_{oscilloscope_id}",
            server_context_manager_factory(simulator),
        )

    def add_oscilloscope_device(  # pylint: disable=too-many-arguments
        self: TestEquipmentTangoTestHarness,
        oscilloscope_id: int,
        model: str,
        address: tuple[str, str, int] | None,
        update_rate: float = 1.0,
        logging_level: int = int(LoggingLevel.DEBUG),
    ) -> None:
        """
        Add an oscilloscope Tango device to the test harness.

        :param oscilloscope_id: an ID number for the oscilloscope.
            This will be used to constructed the Tango device
            name, and also to point the oscilloscope at the correct
            oscilloscope simulator, if any.
        :param model: name of the model of the oscilloscope.
        :param address: address of the oscilloscope to be monitored
            and controlled by this Tango device. It is a tuple of
            protocol ("tcp" or "telnet"), hostname or IP address, and
            port.
        :param update_rate: how often updates to attribute values should
            be provided.
        :param logging_level: the Tango device's default logging level.
        """
        host: Callable[[dict[str, Any]], str] | str  # for the type checker
        port: Callable[[dict[str, Any]], int] | int  # for the type checker

        self.model = model

        if address is None:
            server_id = f"oscilloscope_simulator_{oscilloscope_id}"
            protocol = "tcp"

            def host(context: dict[str, Any]) -> str:
                return context[server_id][0]

            def port(context: dict[str, Any]) -> int:
                return context[server_id][1]

        else:
            (protocol, host, port) = address

        self._tango_test_harness.add_device(
            f"test-itf/scope/{oscilloscope_id}",
            "ska_ser_test_equipment.oscilloscope.OscilloscopeDevice",
            Model=model,
            Protocol=protocol,
            Host=host,
            Port=port,
            UpdateRate=update_rate,
            LoggingLevelDefault=logging_level,
        )

    def __enter__(
        self: TestEquipmentTangoTestHarness,
    ) -> TangoTestHarnessContext:
        """
        Enter the context.

        :returns: the entered context.
        """
        return self._tango_test_harness.__enter__()

    def __exit__(
        self: TestEquipmentTangoTestHarness,
        exc_type: type[BaseException] | None,
        exception: BaseException | None,
        trace: TracebackType | None,
    ) -> bool | None:
        """
        Exit the context.

        :param exc_type: the type of exception thrown in the with block,
            if any.
        :param exception: the exception thrown in the with block, if
            any.
        :param trace: the exception traceback, if any,

        :returns: whether the exception (if any) has been fully handled
            by this method and should be swallowed i.e. not re-raised.
        """
        return self._tango_test_harness.__exit__(exc_type, exception, trace)
